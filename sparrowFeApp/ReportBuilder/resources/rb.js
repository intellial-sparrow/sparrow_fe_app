﻿ReportBuilder = {
    entityNumber: '',
    reportType: '',
    reportTitle: '',
    uniqueid: 1001,
    orderStatus: '',
    reportId: 0,
    userId: 0,
    userName: 0,
    countingNo: 0,
    CommentDetailList: 0,
    PreDefinedSolutions: null,
   
    events: function () {
        $("#btnPreView").on('click', function () {
            ReportBuilder.showPreView();
        });

        $("#btnPreViewException").on('click', function () {
            ReportBuilder.showPreView();
        });


        $("#ddlExceptionType").on('change', function () {

            //var type = $("#ddlExceptionType option:selected").val();

        });

        $('input[type=radio][name=switch_2]').change(function () {

            ReportBuilder.bindPreDefinedSolutions(this.value);

            // alert($('input[type=radio][name=switch_2]:checked').val());
        });

        $("#btnCancel").on('click', function () {
            $("#reportSendPopup").dialog("close");
            ReportBuilder.overlay();
        });

        $("#btnSaveException").on('click', function () {

            if ($('input[type=radio][name=switch_2]:checked').val() == "RBPPAEXCEPTION") {

                var allowedExtensions = {
                    '.zip': 1
                };
                if ($("#updSIFile").get(0).files.length > 0) {

                    var match = /\..+$/;
                    var ext = $("#updSIFile").get(0).files[0].name.match(match);
                    if (allowedExtensions[ext]) {
                        ReportBuilder.saveException();
                    }
                    else {
                        $("#lblerrorMessage").show();
                        $("#lblerrorMessage").html("Select only .Zip file.");
                        $('.pageloader').hide();
                        setTimeout(function () {
                            $("#lblerrorMessage").hide();
                            $("#lblerrorMessage").html("");
                        }, 5000);
                        return false;
                    }
                    //
                }
                else {
                    ReportBuilder.post("rbapi.aspx?action=9&entityNumber=" + ReportBuilder.entityNumber, "", function (data) {
                        if (data.toLowerCase() == 'false'.toString()) {
                            $("#lblerrorMessage").show();
                            $("#lblerrorMessage").html("Please select SI file.");
                            $('.pageloader').hide();
                            setTimeout(function () {
                                $("#lblerrorMessage").hide();
                                $("#lblerrorMessage").html("");
                            }, 5000);
                            
                            return false;
                        }
                        else {
                            ReportBuilder.saveException();
                        }
                    }, "", true);
                }
            }
            else {
                ReportBuilder.saveException();
            }

        });

        $("#ddlPredefineSolution").on('change', function () {

            var codeid = $("#ddlPredefineSolution option:selected").val();
            $.each(ReportBuilder.PreDefinedSolutions, function (key, value) {
                if (codeid == value.codeid) {
                    $("#txtPredefinedescription").html(value.detail);
                }
            });
        });

        $("#btnSaveReport").on('click', function () {
            var viewtype = ReportBuilder.getQueryStringValue("viewtype");
            if (viewtype != "view"){ 
                $("#lblMessage").show();
            ReportBuilder.saveReport();
        }
        });

        $("#btnSendReport").on('click', function () {
            var viewtype = ReportBuilder.getQueryStringValue("viewtype");
            if (viewtype != "view") {
                if (ReportBuilder.entityNumber.indexOf("B") == 0) {
                    $("#lblMessage").show();
                    $("#lblMessage").html("Generating report not allowed for Basket/Inquiry.");
                    $("#lblMessage").addClass("errorMessage");
                    setTimeout(function () {
                        $("#lblMessage").hide();
                        $("#lblMessage").html("Report saved.");
                        $("#lblMessage").removeClass("errorMessage");
                    }, 5000);
                }
                else {
                    ReportBuilder.saveReport();
                    ReportBuilder.sendReportPopup();
                }
            }
        });

        $('body').on('change', '.toggleChkbx', function () {
            var uniqueId = $(this).attr("uniqueId");
            if ($(this).prop('checked')) {
                $("#divTopic" + uniqueId).attr("isActive", true);
                $("#" + uniqueId).attr("isActive", true);
            }
            else {
                $("#divTopic" + uniqueId).attr("isActive", false);
                $("#" + uniqueId).attr("isActive", false);
            }
        });



        $('body').on('mouseover', '.topic-inner img', function () {
            var img = $(this);
            if (!$(this).parent().is(".div-preViewimage")) {
                img.wrap("<div class='div-preViewimage'></div>").parent().prepend("<a href='#' id='btnDeleteimg' data-toggle='tooltip' data-placement='bottom' title='Delete' class='btn btn-primary a-btn-slide-text btnReport btn-editor  display-Right' onclick='ReportBuilder.removePreViewimage()'><span class='glyphicon glyphicon-trash'></span><span><strong></strong></span> </a>");
            }
        })

        $(document).on('change', '.imgPlaceHolder', function (e) {
            var file = this;
            var fileUpload = $(file).get(0);
            var fileType = fileUpload.files[0].type;
            var size = (fileUpload.files[0].size || fileUpload.files[0].fileSize) / 1024;
            if (fileType.indexOf("image/png") == -1 && fileType.indexOf("image/jpg") == -1 && fileType.indexOf("image/gif") == -1 && fileType.indexOf("image/jpeg") == -1) {
                alert("Please select png, jpg or gif image only ");
                return;
            }
            if (size > 250)
            {
                alert("Image size should not be exceeded to 250 KB.");
                return;
            }
            if (fileUpload.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = document.createElement("img");
                    img.style.maxWidth = "95%";
                    img.classList.add("previewImage");
                    img.src = e.target.result
                    img.onload = function () {
                        var height = this.height;
                        var width = this.width;
                        var img1 = document.createElement("img");
                        img1.style.maxWidth = "95%";
                        img1.classList.add("previewImage");
                        img1.src = e.target.result
                        $(file).closest("div").replaceWith(img1);
                        return true;
                    };
                  
                }
                reader.readAsDataURL(fileUpload.files[0]);
            }
        });

        $('body').on('mouseleave', '.div-preViewimage', function () {
           
            $('#btnDeleteimg').remove();

            // $(this).children('img').unwrap();
            $(this).children('img').unwrap();
            {
                $(this).unwrap();
            }

        })
        //note-editor 

    },

    chngExceptionType: function () {
        ReportBuilder.reportType = $("#ddlReportType option:selected").val();
        $("#reportTypePopup").dialog("close");

    },

    removePreViewimage: function () {
        $('#btnDeleteimg').parent().remove();
    },
    saveException: function () {

        $("#btnSaveException").hide();
        $("#btnCancel").hide();

        var postData = new FormData();
        var fileUpload = $("#updSIFile").get(0);
        var files = fileUpload.files;
        for (var i = 0; i < files.length; i++) {
            postData.append(files[i].name, files[i]);
        }
        postData.append("entityNumber", ReportBuilder.entityNumber);
        postData.append("action", 1);
        postData.append("userid", ReportBuilder.userId);
        postData.append("userName", ReportBuilder.userName);
        postData.append("reportType", ReportBuilder.reportType);
        postData.append("exceptiontype", $('input[type=radio][name=switch_2]:checked').val());
        postData.append("PredefineSolutionID", $("#ddlPredefineSolution option:selected").val());
        postData.append("PredefinedescriptionValue", $("#txtPredefinedescription").text());
        ReportBuilder.post("RBHandler.ashx", postData, function (data) {
            if (data.Status == 1) {
                $("#reportSendPopup").dialog("close");
                ReportBuilder.overlay();
                window.location.href = "/shop/orders/eccOrderModified.aspx?successmsgtitle=APPROVED&action=generateexception";
            }
            else {
                $("#lblerrorMessage").show();
                $("#lblerrorMessage").html(data.Message);
                setTimeout(function () {
                    $("#lblerrorMessage").hide();
                    $("#lblerrorMessage").html("");
                }, 5000);

            }
            $("#btnSaveException").show();
            $("#btnCancel").show();

        }, false);
    },
    loadReport: function () {
    
        ReportBuilder.post("rbapi.aspx?action=1&userid=" + ReportBuilder.userId + "&reportType=" + ReportBuilder.reportType + "&entityNumber=" + ReportBuilder.entityNumber + "&username=" + ReportBuilder.userName, "", function (data) {

            ReportBuilder.bindTopics(data.TopicList); 
            ReportBuilder.userName = data.ReportDetail.userName;
            ReportBuilder.reportTitle = data.ReportDetail.reportTitle;
            ReportBuilder.orderStatus = data.ReportDetail.orderStatus;
            ReportBuilder.CommentDetailList = data.CommentDetailList;
            $('#spnReporttitle').html("Report Builder" + '-' + ReportBuilder.entityNumber);
            $('#lblStatus').html(ReportBuilder.orderStatus);
            ReportBuilder.editReport(ReportBuilder.reportid);
           
            //}
        });
    },
    showComments: function (uid, topicid) {
        //$("form")[0].reset();

        var subject = $("#divTopic" + uid + " .topictitle h1").text() + "-" + ReportBuilder.entityNumber;
        var commentid = ReportBuilder.entityNumber + '-' + topicid + '-' + uid;
        commentExt.showTopicComments(ReportBuilder.entityNumber, "RBComment", commentid);

    },
    getCommentCount: function (uid, topicid) {
        var commentid = ReportBuilder.entityNumber + '-' + topicid + '-' + uid;

        ReportBuilder.post("rbapi.aspx?action=7&entityNumber=" + entityNumber + "&" + commentid, "", function (result) {
            ReportBuilder.PreDefinedSolutions = result.PreDefinedSolutions;
            ReportBuilder.exceptionTypePopup(result.listRBExcType);
        });
    },
    showPreView: function () {
        ReportBuilder.getReportHtml(true, function (html, hasActiveTopic) {

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            document.getElementById("reportPreViewHtmlFream").height = height - 200;
            document.getElementById("reportPreViewHtmlFream").width = width - 50;
            $("#reportPreViewPopup").dialog({
                title: 'Preview -' + ReportBuilder.entityNumber,
                modal: true,
                height: height-50,
                width: width-50,
                resizable: false,
                closeonescape: false,
                open: function (event, ui) {
                    $(this).parent().css({
                        position: 'absolute',
                        left: ($(window).width() - $(this).parent().outerWidth()) / 2,
                        top: 10,
                        padding: 0
                    });
                },

                Cancel: function() {
                    $("#reportPreViewPopup").dialog('close');
                    $('.ui-dialog .ui-dialog-titlebar-close').hide();

                }

            });

            $('.ui-dialog .ui-dialog-titlebar-close').show();
            $("#reportPreViewPopup").dialog("open");
            var myFrame = $("#reportPreViewHtmlFream").contents().find('body');
            myFrame.html(html);

        })
    },


    saveReport: function () {

        ReportBuilder.getReportHtml(false, function (html, hasActiveTopic) {
            
            //$("#btnsave-" + uid);
            var alltopicid = $("#hdnRBTopicId").val();
            var backupid = $("#hdnBackupid").val(); 
            var data = { "reportData": html, "hasActiveTopic": hasActiveTopic, "entityNumber": ReportBuilder.entityNumber, "userid": ReportBuilder.userId, "userName": ReportBuilder.userName, "reportType": ReportBuilder.reportType, "reportTitle": ReportBuilder.reportTitle, "rdtopicid": alltopicid, "backupid": backupid };
            ReportBuilder.post("rbapi.aspx?action=4&backupid="+backupid, data, function () {

                setTimeout(function () {
                    $("#lblMessage").hide();
                }, 5000);
            }, "application/json; charset=utf-8");
        })
    },

    getReportHtml: function (isPreview, callback) {
        var html = '';
        var strTemplateName = $('input[type=radio][name=switch_2]:checked').val();
        var entitytype = ReportBuilder.reportType;
        if (entitytype == '' || entitytype.toLowerCase() == "RBPIXPECT".toLowerCase()) {
            strTemplateName = "COMMON";
        }
        var entitynumber = ReportBuilder.entityNumber;
        if (entitynumber.substring(0, 1) == "B") {
            strTemplateName = "OFFER";
        }
        $.get(strTemplateName + "-preview.html?v=7.8", function (data) {
            var contener = ''
            if (isPreview) {
                contener = '-inner';
                $(".countingNoCls").show();
            }

            //------------ remove all numbering and assign new numbering
            var counttopic = 0;
            $(".topic" + contener).each(function () {
                if (isPreview) {
                    var uid = $(this).attr("id");
                    var maindiv = document.getElementById(uid)
                    var isactive = maindiv.getAttribute("isActive");
                    if (isactive == "true") {
                        counttopic = counttopic + 1;
                        var childdiv = maindiv.childNodes[1].childNodes[1];
                        childdiv.innerText = counttopic + ".";
                    }
                }
            });
            var hasActiveTopic = false;
            var topicval = '';
            $(".topic" + contener).each(function () {
                var uid = $(this).attr("id");
                var topicid = $(this).attr("topicid");
                var isActive = false;
                if ($(this).attr("isActive") != undefined) {
                    isActive = $(this).attr("isActive");
                }
                $(this).find('input:text').replaceWith(function () {
                    if (this.value == "") {
                        return "<span class='spnPlaceHolder' ><span>";
                    }
                    else {
                        return "<span class='spnPlaceHolder' > " + this.value + "<span>";
                    }
                });
                if (isActive == "true")
                {
                    hasActiveTopic = true;
                }
                if (isPreview) {
                    $('.fileinput-new').hide();
                    if (isActive == "true") {
                        html = html + $(this).outerHTML();
                    } 
                    var tblContentHTML = ''; 
                    $(".topictitle .topicText").each(function () { 
                        var numberdisplay = $(this)[0].parentNode.parentNode.attributes["isactive"].value;
                        if (numberdisplay == "true") {
                            tblContentHTML += '<li><a href = \"#' + $(this)[0].attributes["unid"].value + '\">' + $(this)[0].innerText + '</a></li>';
                        }
                    })
                 
                    var htmlData = data;
                    var $html = $('<div />', { html: htmlData });
                    $html.find('#ulContent').html(tblContentHTML);
                    data = $html.html()
                }
                else if (!isPreview) {
                    ReportBuilder.removeTextEditor('#divTopic' + uid);
                    html = html + $(this).outerHTML();
                }
                $(this).find('.spnPlaceHolder').replaceWith(function () {
                    return "<input class='txtPlaceHolder' value='" + this.innerText + "'>";
                });
                $('.fileinput-new').show();
                topicval +=topicid + ",";
            });
            $("#hdnRBTopicId").val(topicval);

            $(".countingNoCls").hide();
            var topicObj = {
                topics: {
                    uniqueid: 1,
                    topicDetail: html
                },
                header: {
                    title: ReportBuilder.reportTitle + " " + ReportBuilder.entityNumber
                }
            }
            if (callback) {
                callback(Mustache.render(data, topicObj), hasActiveTopic);
            }
            else {
                html = Mustache.render(data, topicObj);
            }
        });
        return html;
    },

    removeTopic: function (uid) {
        $(".divtopic[uid='" + uid + "']").remove();
    },

    editTopic: function (uid) {
        $("#btnsave-" + uid).css("display", "block");
        $("#btnEdit-" + uid).css("display", "none");
        $('#div-drop-area-' + uid).css("display", "none");
        $('#divTopic' + uid).find('input:text').each(function () {
            $('#divTopic' + uid).find('input:text').replaceWith(function () {
                if (this.value == "") {
                    return "<span class='spnPlaceHolder' >[#$$#]<span>";
                }
                else {
                    return "<span class='spnPlaceHolder' >" + this.value + "<span>";
                }
            });

        });
        ReportBuilder.addTextEditor('#divTopic' + uid);
        $("body").off("mouseover", ".topic-inner img", function () { });
    },

    addTextEditor: function (id) {
       
        $(id).summernote({
            toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['style', 'ul', 'ol', 'paragraph']]
        , ['Insert', ['picture', 'link', 'table', 'hr']]
        , ['Misc', ['codeview', 'redo', 'undo']]

            ],

            disableDragAndDrop: true,
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            //callbacks: {
            //    //onPaste: function (e) {
            //    //    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
            //    //    e.preventDefault();
            //    //   // document.execCommand('insertText', false, bufferText);
            //    //},
                
      
            //},
            maximumImageFileSize: 256000

        });
    },

    removeTextEditor: function (id) {
        var markup = $(id).summernote('code');
        $(id).summernote('destroy');
    },

    editReport: function (reportId) {
        var result = "";
        ReportBuilder.post("rbapi.aspx?action=5&entityNumber=" + ReportBuilder.entityNumber + "&reportId=" + reportId + "&reportType=" + ReportBuilder.reportType + "", "", function (data) {
            result = data;
            var html = '';
            $('.pageloader').show();
            $(result).find('.topic').each(function () {
                var html = $(this).html();
                var id = this.id.replace("divTopic", "");
                var topicId = $(this).attr("topicid");
                var isActive = $(this).attr("isActive");
                ReportBuilder.uniqueid = id;
                var CommentClass = '';

                if (ReportBuilder.CommentDetailList != null) {
                    var i;
                    for (i = 0; i < ReportBuilder.CommentDetailList.length ; i++) {
                        var comment = ReportBuilder.CommentDetailList[i];

                        var commentid = ReportBuilder.entityNumber + '-' + topicId + '-' + id;
                        if (commentid == comment.commentorderId) {
                            CommentClass = 'btn-primary-Red';
                        }
                    }
                }


                var hdrcountingNo = $(this).attr("countingNo") == undefined ? '0' : $(this).attr("countingNo");
                ReportBuilder.countingNo = hdrcountingNo;
                var topicObj = {
                    topics: {
                        uniqueid: ReportBuilder.uniqueid,
                        topicid: topicId,
                        topicDetail: html,
                        CommentClass: CommentClass
                    }
                }
                var template = document.getElementById('editreportTemplate').innerHTML;
                $('#divTopiccontainer').append(Mustache.render(template, topicObj));
                var fileinput = document.querySelector('#hdrFileUplaodCntrl' + ReportBuilder.uniqueid);
                var filedroparea = document.querySelector('.file-drop-area');
                var jssetnumber = $("[fileUploadid=" + ReportBuilder.uniqueid + "]");
                filedroparea.addEventListener('dragenter', dragHover);
                filedroparea.addEventListener('dragleave', dragLeave);
                function isactive() {
                    filedroparea.classList.add('is-active');
                    filedroparea.classList.remove('drop-hover');
                }
                function dragHover() {
                    filedroparea.classList.add('drop-hover');
                }
                function dragLeave() {
                    filedroparea.classList.remove('drop-hover');
                }
                fileinput.addEventListener('change', function () {

                    $('#imgProcessing').css('display', 'none');
                    var count = fileinput.files.length;
                    if (count === 1) {
                        //jssetnumber.text(fileinput.value.split('\\').pop());
                        ReportBuilder.uploadFile(fileinput.id);
                    }
                });
                $("input[id^='toggle-two']").bootstrapToggle({
                    on: 'Open',
                    off: 'Close'
                });
                $(".toggle").addClass('toggleButtonClass');
                $(".toggle-off").attr('title', 'CLOSE topic(s) will not be displayed in PDF/Preview');
                $(".toggle-on").attr('title', 'OPEN topic(s) will be displayed in PDF/Preview');
                if (isActive == "true") {
                    $("#toggle-two-" + id).prop('checked', true).change();
                    $("#" + ReportBuilder.uniqueid).attr("isActive", true);
                }
                else {
                    $("#toggle-two-" + id).prop('checked', false).change();
                    $("#" + ReportBuilder.uniqueid).attr("isActive", false);
                }
            });
            $('.pageloader').hide();
            $('#Innercontainer').show();
            var viewtype = ReportBuilder.getQueryStringValue("viewtype");
            if (viewtype == "view") {
                $('#btnSaveReport').attr("title", "");
                $('#btnSendReport').attr("title", "");
                $('#btnSaveReport').attr('disabled', true);
                $('#btnSendReport').attr('disabled', true);
                $('#topic-list').hide();
                $(".styelDisInlineblock").find("input, button, submit, textarea, select, a").attr("disabled", true);

                $(".styelDisInlineblock").find("input, button, submit, textarea, select, a").prop("onclick", null).off("click");
            }
           
            return html;
        }, "", true);
    },

    saveTopic: function (uid) {
        
        $('#div-drop-area-' + uid).css("display", "block");
        $("#btnsave-" + uid).css("display", "none");
        $("#btnEdit-" + uid).css("display", "block");
        ReportBuilder.removeTextEditor('#divTopic' + uid);
        
        $('#divTopic' + uid).find('.spnPlaceHolder').each(function () {
            $('#divTopic' + uid).find('.spnPlaceHolder').replaceWith(function () {
                if (this.innerText == '[#$$#]') {
                    return "<input class='txtPlaceHolder'>";
                }
                else {
                    return "<input class='txtPlaceHolder' value='" + this.innerText + "'>";
                }
            });

        });
    },

    bindTopics: function (data) {
        var options = {
            data: data,
            getValue: "name",
            template: {
                type: "custom",
                method: function (value, item) {
                    return value;
                }
            },

            //template: {
            //    type: "description",
            //    fields: {
            //        description: "details"
            //    }
            //},
           
            list: {
                match: {
                    enabled: true
                },
                onChooseEvent: function () {
                    
                    var selectedData = $("#topic-list").getSelectedItemData();
                    ReportBuilder.post("rbapi.aspx?action=2&id=" + selectedData.id, "", ReportBuilder.GetTopicDetail);
                    $("#hdnRBToic").val(selectedData.id);
                    $("#topic-list").innerHTML = "";
                    $("#topic-list").val("");
                }
            },
            theme: "plate-dark",
            adjustWidth: false
        };
        $("#topic-list").easyAutocomplete(options);
    },

    uploadFile: function (id) {
        //var postData = new FormData();
        var fileUpload = $("#" + id).get(0);
        //var files = fileUpload.files;
        //for (var i = 0; i < files.length; i++) {
        // 
        //    postData.append(files[i].name, files[i]);
        //}
        //postData.append("Action", "3");
        var fileType = fileUpload.files[0].type;
        var size = (fileUpload.files[0].size || fileUpload.files[0].fileSize) / 1024; 
        if (fileType.indexOf("image/png") == -1 && fileType.indexOf("image/jpg") == -1 && fileType.indexOf("image/gif") == -1 && fileType.indexOf("image/jpeg") == -1) {
            alert("Please select png, jpg or gif image only ");
            return;
        }
        if (size > 250) {
            alert("Image size should not be exceeded to 250 KB.");
            return;
        }
        if (fileUpload.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = new Image();
      
                img.src = e.target.result
                img.onload = function () {
                    var img1 = document.createElement("img");
                    img1.style.maxWidth = "95%";
                    img1.classList.add("previewImage");
                    img1.src = e.target.result
                    $("#divTopic" + id.replace("hdrFileUplaodCntrl", "")).append(img1);
                    return true;
                };

                // $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(fileUpload.files[0]);
        }

        //ReportBuilder.post("RBHandler.ashx", postData, function (result) {
        //    var img = document.createElement("img");
        //    img.classList.add("previewImage");
        //    img.src = 'http://be.eurocircuits.com/images/Newlogo_843.png';
        //    $("#unicTopic" + id.replace("hdrFileUplaodCntrl", "")).append(img);

        //},false);
    },

    addPlaceHolder: function (htmlString) {
        htmlString = htmlString.replac(/[#$$#]/g, "<input type='text'>");
        return htmlString;
    },

    GetTopicDetail: function (data) {
        
        ReportBuilder.uniqueid = parseInt(ReportBuilder.uniqueid) + 1;
        ReportBuilder.countingNo = parseInt(ReportBuilder.countingNo) + 1;
        var d = new Date();
       
        var topicObj = {
            topics: {
                uniqueid: ReportBuilder.uniqueid,
                countingNo: ReportBuilder.countingNo,
                topicid: data.id,
                topic: data.topic,
                userName: ReportBuilder.userName,
                entityNumber: ReportBuilder.entityNumber,
                orderStatus: ReportBuilder.orderStatus,
                dateTime: ReportBuilder.formatDate(d, 'ddd MMM d \a\\t h:mm TT', true),
                topicDetail: data.topicDetail,
            }
           
        }

        var template = document.getElementById('reportTemplate').innerHTML;
        $('#divTopiccontainer').append(Mustache.render(template, topicObj));
        var fileinput = document.querySelector('#hdrFileUplaodCntrl' + ReportBuilder.uniqueid);
        var filedroparea = document.querySelector('.file-drop-area');
        var jssetnumber = $("[fileUploadid=" + ReportBuilder.uniqueid + "]");
        filedroparea.addEventListener('dragenter', dragHover);
        filedroparea.addEventListener('dragleave', dragLeave);
        function isactive() {
            filedroparea.classList.add('is-active');
            filedroparea.classList.remove('drop-hover');
        }
        function dragHover() {
            filedroparea.classList.add('drop-hover');
        }
        function dragLeave() {
            filedroparea.classList.remove('drop-hover');
        }

       
        fileinput.addEventListener('change', function () {
            $('#imgProcessing').css('display', 'none');
            var count = fileinput.files.length;
            if (count === 1) {
                //jssetnumber.text(fileinput.value.split('\\').pop());
                ReportBuilder.uploadFile(fileinput.id);
            }
        });
        $("input[id^='toggle-two']").bootstrapToggle({
            on: 'Open',
            off: 'Close'
        });
        $(".toggle").addClass('toggleButtonClass');
        $("input[id^='toggle-two']").bootstrapToggle('Active')
        $("#toggle-two-" + ReportBuilder.uniqueid).bootstrapToggle('on')
        $(".toggle-off").attr('title', 'CLOSE topic(s) will not be displayed in PDF/Preview');
        $(".toggle-on").attr('title', 'OPEN topic(s) will be displayed in PDF/Preview');
    },

    
    post: function (url, postData, callback, contentType, isHTML) {

        processData = true;
        data = postData;
        if (contentType || contentType === undefined) {
            contentType = "application/json; charset=utf-8";
            data = JSON.stringify(postData)
        }
        else {
            processData = false;
            contentType = false
        }

        $.ajax({
            url: url,
            type: "POST",
            data: data,
            contentType: contentType,
            processData: processData,
            beforeSend: function () {
                $('.pageloader').show();
            },
            success: function (data) {
          
                if (isHTML) {
                    if (callback) {
                        callback(data);

                    }
                }
                else if (callback) {
                    callback($.parseJSON(data));
                    $('.pageloader').hide();
                }
                else {
                    $('.pageloader').hide();
                }
            },
            complete: function () {
                // $('.pageloader').hide();
            },
            error: function (errorData) {
                console.log(errorData);
                if (callback) {
                    callback({ Status: 0 });
                }
            }
        });
    },

    formatDate: function (date, format, utc) {
        var MMMM = ["\x00", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var MMM = ["\x01", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var dddd = ["\x02", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var ddd = ["\x03", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        function ii(i, len) { var s = i + ""; len = len || 2; while (s.length < len) s = "0" + s; return s; }

        var y = utc ? date.getUTCFullYear() : date.getFullYear();
        format = format.replace(/(^|[^\\])yyyy+/g, "$1" + y);
        format = format.replace(/(^|[^\\])yy/g, "$1" + y.toString().substr(2, 2));
        format = format.replace(/(^|[^\\])y/g, "$1" + y);

        var M = (utc ? date.getUTCMonth() : date.getMonth()) + 1;
        format = format.replace(/(^|[^\\])MMMM+/g, "$1" + MMMM[0]);
        format = format.replace(/(^|[^\\])MMM/g, "$1" + MMM[0]);
        format = format.replace(/(^|[^\\])MM/g, "$1" + ii(M));
        format = format.replace(/(^|[^\\])M/g, "$1" + M);

        var d = utc ? date.getUTCDate() : date.getDate();
        format = format.replace(/(^|[^\\])dddd+/g, "$1" + dddd[0]);
        format = format.replace(/(^|[^\\])ddd/g, "$1" + ddd[0]);
        format = format.replace(/(^|[^\\])dd/g, "$1" + ii(d));
        format = format.replace(/(^|[^\\])d/g, "$1" + d);

        var H = utc ? date.getUTCHours() : date.getHours();
        format = format.replace(/(^|[^\\])HH+/g, "$1" + ii(H));
        format = format.replace(/(^|[^\\])H/g, "$1" + H);

        var h = H > 12 ? H - 12 : H == 0 ? 12 : H;
        format = format.replace(/(^|[^\\])hh+/g, "$1" + ii(h));
        format = format.replace(/(^|[^\\])h/g, "$1" + h);

        var m = utc ? date.getUTCMinutes() : date.getMinutes();
        format = format.replace(/(^|[^\\])mm+/g, "$1" + ii(m));
        format = format.replace(/(^|[^\\])m/g, "$1" + m);

        var s = utc ? date.getUTCSeconds() : date.getSeconds();
        format = format.replace(/(^|[^\\])ss+/g, "$1" + ii(s));
        format = format.replace(/(^|[^\\])s/g, "$1" + s);

        var f = utc ? date.getUTCMilliseconds() : date.getMilliseconds();
        format = format.replace(/(^|[^\\])fff+/g, "$1" + ii(f, 3));
        f = Math.round(f / 10);
        format = format.replace(/(^|[^\\])ff/g, "$1" + ii(f));
        f = Math.round(f / 10);
        format = format.replace(/(^|[^\\])f/g, "$1" + f);

        var T = H < 12 ? "AM" : "PM";
        format = format.replace(/(^|[^\\])TT+/g, "$1" + T);
        format = format.replace(/(^|[^\\])T/g, "$1" + T.charAt(0));

        var t = T.toLowerCase();
        format = format.replace(/(^|[^\\])tt+/g, "$1" + t);
        format = format.replace(/(^|[^\\])t/g, "$1" + t.charAt(0));

        var tz = -date.getTimezoneOffset();
        var K = utc || !tz ? "Z" : tz > 0 ? "+" : "-";
        if (!utc) {
            tz = Math.abs(tz);
            var tzHrs = Math.floor(tz / 60);
            var tzMin = tz % 60;
            K += ii(tzHrs) + ":" + ii(tzMin);
        }
        format = format.replace(/(^|[^\\])K/g, "$1" + K);

        var day = (utc ? date.getUTCDay() : date.getDay()) + 1;
        format = format.replace(new RegExp(dddd[0], "g"), dddd[day]);
        format = format.replace(new RegExp(ddd[0], "g"), ddd[day]);

        format = format.replace(new RegExp(MMMM[0], "g"), MMMM[M]);
        format = format.replace(new RegExp(MMM[0], "g"), MMM[M]);

        format = format.replace(/\\(.)/g, "$1");

        return format;
    },

    getQueryStringValue: function (key) {
        return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    },
    bindPreDefinedSolutions: function (type) {
        $('#ddlPredefineSolution').empty();
        $("#trSIData").hide();
        // $("#updSIFile").attr('disabled', 'disabled');
        $.each(ReportBuilder.PreDefinedSolutions, function (key, value) {

            if (key == 0) {
                $("#txtPredefinedescription").html(value.detail);
            }
            if (type != "RBPPAEXCEPTION") {
                if (value.code != "Pre-production approval request") {
                    $('<option value="' + value.codeid + '">' + value.code + '</option>').appendTo($('#ddlPredefineSolution'));
                }
            }
            else {
                if (value.code == "Pre-production approval request") {
                    $('<option value="' + value.codeid + '">' + value.code + '</option>').appendTo($('#ddlPredefineSolution'));
                    $("#txtPredefinedescription").html(value.detail);
                    $("#trSIData").show();
                }
            }

        });

        var codeid = $("#ddlPredefineSolution option:selected").val();
        $.each(ReportBuilder.PreDefinedSolutions, function (key, value) {
            if (codeid == value.codeid) {
                $("#txtPredefinedescription").html(value.detail);
            }
        });
    },
    exceptionTypePopup: function (listRBExcType) {
        //$('#ddlExceptionType').empty();
        //$.each(listRBExcType, function (key, value) {
        //    $('<option value="' + key + '">' + value + '</option>').appendTo($('#ddlExceptionType'));
        //});

        ReportBuilder.bindPreDefinedSolutions("RBEXCEPTION");

        //$("#reportTypePopup").dialog({
        //    title: 'Select report type',
        //    modal: true,
        //    height: 150,
        //    width: 400,
        //    resizable: false,
        //    closeonescape: false,
        //    open: function (event, ui) {
        //        $(this).parent().css({
        //            position: 'absolute',
        //            left: ($(window).width() - $(this).parent().outerWidth()) / 2,
        //            top: 75,
        //            padding: 0
        //        });
        //    }
        //});
        //$("#reportTypePopup").dialog("open");

    },
    overlay: function () {
        el = document.getElementById("overlay");
        el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
    }
    ,
    sendReportPopup: function () {
        $("#reportSendPopup").dialog({
            title: 'Register exception -' + ReportBuilder.entityNumber,
            modal: true,
            height: 600,
            width: 600,
            resizable: false,
            closeonescape: false,
            open: function (event, ui) {
                $(this).parent().css({
                    position: 'absolute',
                    left: ($(window).width() - $(this).parent().outerWidth()) / 2,
                    top: 100,
                    padding: 0

                });
            }
        });
        ReportBuilder.overlay();
        $("#reportSendPopup").dialog("open");


    },

    init: function () {

        ReportBuilder.reportid = ReportBuilder.getQueryStringValue("reportid");
        ReportBuilder.entityNumber = ReportBuilder.getQueryStringValue("entityNumber");
        ReportBuilder.userId = ReportBuilder.getQueryStringValue("userid");
        ReportBuilder.userName = ReportBuilder.getQueryStringValue("username");
        $('#lblEntityNumber').html(ReportBuilder.entityNumber);

        //  new Date().format('ddd MMM d \a\t h:mm TT');
        ReportBuilder.events();

    }
}
$(document).ready(function () {
    
    var entityNumber = ReportBuilder.getQueryStringValue("entityNumber");
    var reporttype = ReportBuilder.getQueryStringValue("reporttype");
    if (reporttype.toLowerCase() == "RBPIXPECT".toLowerCase()) {
        ReportBuilder.reportType = reporttype;
    }
    else {
        ReportBuilder.post("rbapi.aspx?action=7&entityNumber=" + entityNumber, "", function (result) {
            ReportBuilder.PreDefinedSolutions = result.PreDefinedSolutions;
            ReportBuilder.exceptionTypePopup(result.listRBExcType);
        });
    } 
    ReportBuilder.init();
    ReportBuilder.loadReport();
    $('#btnPreView').css("display", "block");
    $('#btnSaveReport').css("display", "block");
    $('#btnSendReport').css("display", "block");
    if (reporttype.toLowerCase() == "RBPIXPECT".toLowerCase()) {
        $('#btnSendReport').css("display", "none");
    }
    $('[data-toggle="tooltip"]').tooltip();
   
    jQuery.fn.outerHTML = function () {
        return jQuery('<div />').append(this.eq(0).clone()).html();
    };

});