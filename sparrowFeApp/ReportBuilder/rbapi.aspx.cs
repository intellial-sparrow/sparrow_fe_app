﻿using EC09WebApp.shop.rb;
using System;
using System.IO;
using System.Web;

namespace sparrowFeApp.shop.rb
{
    public partial class rbapi : sparrowFeApp.core.BasePage //System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(sparrowFeApp.core.AppSessions.EccUserId == "" || sparrowFeApp.core.AppSessions.EccUserId == "0")
            {
                sparrowFeApp.core.AppSessions.EccUserId = Request["userid"] ?? "0";
            }
            ReportBuilderUtilities obj = new ReportBuilderUtilities();
            int action = Convert.ToInt32(Request["Action"] ?? "0");
            string id = Request["id"] ?? "0";
            string result = "";
            string entityNumber = Request["entityNumber"] ?? "0";
            string reportType = Request["reportType"] ?? "0";
            string reportId = Request["reportid"] ?? "0";
            long userid = 0;
            long.TryParse(Request["userid"] ?? "0", out userid);
            string username = Request["username"] ?? "0";
            string backupidnew = Request["backupid"] ?? "0";
            long rb_id = 0;
            long.TryParse(Request["rb_id"] ?? "0", out rb_id);
            string backup_id = "";
            if (Request["backup_id"] != null)
            {
                backup_id = Request["backup_id"] ?? "0";
            }
            switch (action)
            {
                case (int)Action.GetTopicList:
                    {
                        result = ReportBuilderUtilities.GetReportDetail(reportType, entityNumber, userid,username); //todo
                        break;
                    }
                case (int)Action.GetTopicDetail:
                    {
                        result = ReportBuilderUtilities.GetTopicdetail(id);
                        break;
                    }

                case (int)Action.UploadFile:
                    {
                        HttpFileCollection files = Request.Files;
                        result = obj.UploadFile(files[0]);
                        break;
                    }
                case (int)Action.SaveReport:
                    {
                        var bodyStream = new StreamReader(HttpContext.Current.Request.InputStream);
                        bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
                        string backupId = "0";
                        if (Request.UrlReferrer.ToString().Contains("sessionId"))
                        {
                            backupId = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["sessionId"].ToString() ?? "0";
                        }
                        var bodyText = bodyStream.ReadToEnd();
                          result = obj.SaveReport(bodyText, backupidnew); //todo
                        break;
                    }
                case (int)Action.getHTMLFromServer:
                    {
                        result = obj.getHTMLFromServer(entityNumber, reportId, reportType);
                        break;
                    }
                case (int)Action.getPreView:
                    {

                        var objs = obj.GetReportPreViewHtml(entityNumber, true, reportType);
                        result = objs.content;
                        break;
                    }
                case (int)Action.GetExceptionTypes:
                    {
                        result = ReportBuilderUtilities.getExceptionTypes(entityNumber);
                        break;
                    }
                case (int)Action.getPreviewForHistory:
                    {
                        result = obj.getPreviewForHistory(rb_id, backup_id);
                        break;
                    }
                case (int)Action.CheckSIFileUpload:
                    {
                        result = obj.CheckSIFileUpload(entityNumber);
                        break;
                    }
            }
            Response.Write(result);
        }

        public enum Action
        {
            GetTopicList = 1,
            GetTopicDetail = 2,
            UploadFile = 3,
            SaveReport = 4,
            getHTMLFromServer = 5,
            getPreView = 6,
            GetExceptionTypes = 7,
            getPreviewForHistory = 8,
            CheckSIFileUpload = 9
        }
    }
}