﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace EC09WebApp.shop.rb
{
    /// <summary>
    /// Summary description for RBHandler
    /// </summary>
    public class RBHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int api = Convert.ToInt32(context.Request["Action"] ?? "0");
            CallAPI(api, context);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private void CallAPI(int api, HttpContext context)
        {
            ReportBuilderUtilities obj = new ReportBuilderUtilities();
            string id = context.Request["action"] ?? "0";
            string result = "";
            switch (api)
            {
                case (int)Action.SaveException:
                    {
                        HttpFileCollection files = context.Request.Files;
                        byte[] fileData = null;
                        decimal size = 0;
                        if (context.Request.Files.Count > 0)
                        {
                            size = context.Request.Files[0].ContentLength / 1024;
                            using (var binaryReader = new BinaryReader(context.Request.Files[0].InputStream))
                            {
                                fileData = binaryReader.ReadBytes(context.Request.Files[0].ContentLength);
                            }
                        }
                        string solutionDescription = context.Request["PredefinedescriptionValue"];
                        string orderNumber = context.Request["entityNumber"] ?? "0";
                        string PredefineSolutionCode = context.Request["PredefineSolutionID"] ?? ""; 
                        string exceptiontype = context.Request["exceptiontype"] ?? "0";
                        string userid = context.Request["userid"] ?? "0";
                        string userName = context.Request["username"] ?? "0";
                        result = obj.generateException(orderNumber, PredefineSolutionCode, solutionDescription, exceptiontype, fileData, size, userid);
                        //context.Response.Redirect(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CofiguratorUrl"]) + "/shop/orders/eccOrderModified.aspx?successmsgtitle=APPROVED");
                        break;
                    }
            }
            context.Response.Write(result);
        }
    }


    public enum Action
    {
        SaveException = 1
    }
}