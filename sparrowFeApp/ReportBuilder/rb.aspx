﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--<%@ Register Src="~/shop/WUCComments.ascx" TagPrefix="uc1" TagName="WUCComments" %>--%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">


<head runat="server">

    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <script src="resources/jquery-1.10.2.min.js"></script>
    <script src="resources/jquery-ui.min.js"></script>
    <script src="resources/jquery.easy-autocomplete.js"></script>
    <link href="resources/easy-autocomplete.css" rel="stylesheet" />
    <link href="resources/summernote/jquery-ui.css" rel="stylesheet" />
    <link href="resources/easy-autocomplete.themes.css?v=1" rel="stylesheet" />
    <link href="resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="bootstrap-toggle-master/css/bootstrap-toggle.min.css" rel="stylesheet" />
    <link href="../JS/jquery-ui.css" rel="stylesheet" />
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript">
        (function defineMustache(global, factory) { if (typeof exports === "object" && exports && typeof exports.nodeName !== "string") { factory(exports) } else if (typeof define === "function" && define.amd) { define(["exports"], factory) } else { global.Mustache = {}; factory(global.Mustache) } })(this, function mustacheFactory(mustache) { var objectToString = Object.prototype.toString; var isArray = Array.isArray || function isArrayPolyfill(object) { return objectToString.call(object) === "[object Array]" }; function isFunction(object) { return typeof object === "function" } function typeStr(obj) { return isArray(obj) ? "array" : typeof obj } function escapeRegExp(string) { return string.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&") } function hasProperty(obj, propName) { return obj != null && typeof obj === "object" && propName in obj } var regExpTest = RegExp.prototype.test; function testRegExp(re, string) { return regExpTest.call(re, string) } var nonSpaceRe = /\S/; function isWhitespace(string) { return !testRegExp(nonSpaceRe, string) } var entityMap = { "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;", "/": "&#x2F;", "`": "&#x60;", "=": "&#x3D;" }; function escapeHtml(string) { return String(string).replace(/[&<>"'`=\/]/g, function fromEntityMap(s) { return entityMap[s] }) } var whiteRe = /\s*/; var spaceRe = /\s+/; var equalsRe = /\s*=/; var curlyRe = /\s*\}/; var tagRe = /#|\^|\/|>|\{|&|=|!/; function parseTemplate(template, tags) { if (!template) return []; var sections = []; var tokens = []; var spaces = []; var hasTag = false; var nonSpace = false; function stripSpace() { if (hasTag && !nonSpace) { while (spaces.length) delete tokens[spaces.pop()] } else { spaces = [] } hasTag = false; nonSpace = false } var openingTagRe, closingTagRe, closingCurlyRe; function compileTags(tagsToCompile) { if (typeof tagsToCompile === "string") tagsToCompile = tagsToCompile.split(spaceRe, 2); if (!isArray(tagsToCompile) || tagsToCompile.length !== 2) throw new Error("Invalid tags: " + tagsToCompile); openingTagRe = new RegExp(escapeRegExp(tagsToCompile[0]) + "\\s*"); closingTagRe = new RegExp("\\s*" + escapeRegExp(tagsToCompile[1])); closingCurlyRe = new RegExp("\\s*" + escapeRegExp("}" + tagsToCompile[1])) } compileTags(tags || mustache.tags); var scanner = new Scanner(template); var start, type, value, chr, token, openSection; while (!scanner.eos()) { start = scanner.pos; value = scanner.scanUntil(openingTagRe); if (value) { for (var i = 0, valueLength = value.length; i < valueLength; ++i) { chr = value.charAt(i); if (isWhitespace(chr)) { spaces.push(tokens.length) } else { nonSpace = true } tokens.push(["text", chr, start, start + 1]); start += 1; if (chr === "\n") stripSpace() } } if (!scanner.scan(openingTagRe)) break; hasTag = true; type = scanner.scan(tagRe) || "name"; scanner.scan(whiteRe); if (type === "=") { value = scanner.scanUntil(equalsRe); scanner.scan(equalsRe); scanner.scanUntil(closingTagRe) } else if (type === "{") { value = scanner.scanUntil(closingCurlyRe); scanner.scan(curlyRe); scanner.scanUntil(closingTagRe); type = "&" } else { value = scanner.scanUntil(closingTagRe) } if (!scanner.scan(closingTagRe)) throw new Error("Unclosed tag at " + scanner.pos); token = [type, value, start, scanner.pos]; tokens.push(token); if (type === "#" || type === "^") { sections.push(token) } else if (type === "/") { openSection = sections.pop(); if (!openSection) throw new Error('Unopened section "' + value + '" at ' + start); if (openSection[1] !== value) throw new Error('Unclosed section "' + openSection[1] + '" at ' + start) } else if (type === "name" || type === "{" || type === "&") { nonSpace = true } else if (type === "=") { compileTags(value) } } openSection = sections.pop(); if (openSection) throw new Error('Unclosed section "' + openSection[1] + '" at ' + scanner.pos); return nestTokens(squashTokens(tokens)) } function squashTokens(tokens) { var squashedTokens = []; var token, lastToken; for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) { token = tokens[i]; if (token) { if (token[0] === "text" && lastToken && lastToken[0] === "text") { lastToken[1] += token[1]; lastToken[3] = token[3] } else { squashedTokens.push(token); lastToken = token } } } return squashedTokens } function nestTokens(tokens) { var nestedTokens = []; var collector = nestedTokens; var sections = []; var token, section; for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) { token = tokens[i]; switch (token[0]) { case "#": case "^": collector.push(token); sections.push(token); collector = token[4] = []; break; case "/": section = sections.pop(); section[5] = token[2]; collector = sections.length > 0 ? sections[sections.length - 1][4] : nestedTokens; break; default: collector.push(token) } } return nestedTokens } function Scanner(string) { this.string = string; this.tail = string; this.pos = 0 } Scanner.prototype.eos = function eos() { return this.tail === "" }; Scanner.prototype.scan = function scan(re) { var match = this.tail.match(re); if (!match || match.index !== 0) return ""; var string = match[0]; this.tail = this.tail.substring(string.length); this.pos += string.length; return string }; Scanner.prototype.scanUntil = function scanUntil(re) { var index = this.tail.search(re), match; switch (index) { case -1: match = this.tail; this.tail = ""; break; case 0: match = ""; break; default: match = this.tail.substring(0, index); this.tail = this.tail.substring(index) } this.pos += match.length; return match }; function Context(view, parentContext) { this.view = view; this.cache = { ".": this.view }; this.parent = parentContext } Context.prototype.push = function push(view) { return new Context(view, this) }; Context.prototype.lookup = function lookup(name) { var cache = this.cache; var value; if (cache.hasOwnProperty(name)) { value = cache[name] } else { var context = this, names, index, lookupHit = false; while (context) { if (name.indexOf(".") > 0) { value = context.view; names = name.split("."); index = 0; while (value != null && index < names.length) { if (index === names.length - 1) lookupHit = hasProperty(value, names[index]); value = value[names[index++]] } } else { value = context.view[name]; lookupHit = hasProperty(context.view, name) } if (lookupHit) break; context = context.parent } cache[name] = value } if (isFunction(value)) value = value.call(this.view); return value }; function Writer() { this.cache = {} } Writer.prototype.clearCache = function clearCache() { this.cache = {} }; Writer.prototype.parse = function parse(template, tags) { var cache = this.cache; var tokens = cache[template]; if (tokens == null) tokens = cache[template] = parseTemplate(template, tags); return tokens }; Writer.prototype.render = function render(template, view, partials) { var tokens = this.parse(template); var context = view instanceof Context ? view : new Context(view); return this.renderTokens(tokens, context, partials, template) }; Writer.prototype.renderTokens = function renderTokens(tokens, context, partials, originalTemplate) { var buffer = ""; var token, symbol, value; for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) { value = undefined; token = tokens[i]; symbol = token[0]; if (symbol === "#") value = this.renderSection(token, context, partials, originalTemplate); else if (symbol === "^") value = this.renderInverted(token, context, partials, originalTemplate); else if (symbol === ">") value = this.renderPartial(token, context, partials, originalTemplate); else if (symbol === "&") value = this.unescapedValue(token, context); else if (symbol === "name") value = this.escapedValue(token, context); else if (symbol === "text") value = this.rawValue(token); if (value !== undefined) buffer += value } return buffer }; Writer.prototype.renderSection = function renderSection(token, context, partials, originalTemplate) { var self = this; var buffer = ""; var value = context.lookup(token[1]); function subRender(template) { return self.render(template, context, partials) } if (!value) return; if (isArray(value)) { for (var j = 0, valueLength = value.length; j < valueLength; ++j) { buffer += this.renderTokens(token[4], context.push(value[j]), partials, originalTemplate) } } else if (typeof value === "object" || typeof value === "string" || typeof value === "number") { buffer += this.renderTokens(token[4], context.push(value), partials, originalTemplate) } else if (isFunction(value)) { if (typeof originalTemplate !== "string") throw new Error("Cannot use higher-order sections without the original template"); value = value.call(context.view, originalTemplate.slice(token[3], token[5]), subRender); if (value != null) buffer += value } else { buffer += this.renderTokens(token[4], context, partials, originalTemplate) } return buffer }; Writer.prototype.renderInverted = function renderInverted(token, context, partials, originalTemplate) { var value = context.lookup(token[1]); if (!value || isArray(value) && value.length === 0) return this.renderTokens(token[4], context, partials, originalTemplate) }; Writer.prototype.renderPartial = function renderPartial(token, context, partials) { if (!partials) return; var value = isFunction(partials) ? partials(token[1]) : partials[token[1]]; if (value != null) return this.renderTokens(this.parse(value), context, partials, value) }; Writer.prototype.unescapedValue = function unescapedValue(token, context) { var value = context.lookup(token[1]); if (value != null) return value }; Writer.prototype.escapedValue = function escapedValue(token, context) { var value = context.lookup(token[1]); if (value != null) return mustache.escape(value) }; Writer.prototype.rawValue = function rawValue(token) { return token[1] }; mustache.name = "mustache.js"; mustache.version = "2.2.1"; mustache.tags = ["{{", "}}"]; var defaultWriter = new Writer; mustache.clearCache = function clearCache() { return defaultWriter.clearCache() }; mustache.parse = function parse(template, tags) { return defaultWriter.parse(template, tags) }; mustache.render = function render(template, view, partials) { if (typeof template !== "string") { throw new TypeError('Invalid template! Template should be a "string" ' + 'but "' + typeStr(template) + '" was given as the first ' + "argument for mustache#render(template, view, partials)") } return defaultWriter.render(template, view, partials) }; mustache.to_html = function to_html(template, view, partials, send) { var result = mustache.render(template, view, partials); if (isFunction(send)) { send(result) } else { return result } }; mustache.escape = escapeHtml; mustache.Scanner = Scanner; mustache.Context = Context; mustache.Writer = Writer });
    </script>

    <link href="resources/summernote/summernote.css" rel="stylesheet" />
    <script src="resources/summernote/summernote.js?v=1.3"></script>

    <script src="resources/rb.js?v=2.20"></script>
    <script runat="server" type="C#">

        protected void Page_Load(object sender, EventArgs e)
        { 
            hdnBackupid.Value = Guid.NewGuid().ToString();
            // Session["userId"] = Request["userid"] ?? "0";
        }
    </script>
    <style type="text/css">
        .switch-field {
            font-family: "Open sans", Tahoma, Verdana, sans-serif;
            overflow: hidden;
        }

        .switch-title {
            margin-bottom: 6px;
        }

        .switch-field input {
            position: absolute !important;
            clip: rect(0, 0, 0, 0);
            height: 1px;
            width: 1px;
            border: 0;
            overflow: hidden;
        }

        .ui-widget-header {
            background: #2a9f00;
            color: white;
            font-weight: bold;
            border-radius: 0;
        }

        .ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
            border-bottom-left-radius: 0;
        }

        .ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
            border-top-left-radius: 0;
        }

        .ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
            border-bottom-right-radius: 0;
        }

        .ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
            border-bottom-left-radius: 0;
        }

        .ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr {
            border-top-right-radius: 0;
        }

        .ui-dialog {
            z-index: 1060 !important;
        }

        .switch-field label {
            float: left;
        }

        #reportSendPopup label {
            padding-left: 0;
        }

        .lable-textBold {
            font-weight: 700;
        }

        .switch-field label {
            display: inline-block;
            width: 183px;
            background-color: #fffff;
            color: #555;
            font-size: 14px;
            font-weight: normal;
            text-align: center;
            text-shadow: none;
            padding: 5px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-transition: all 0.1s ease-in-out;
            -moz-transition: all 0.1s ease-in-out;
            -ms-transition: all 0.1s ease-in-out;
            -o-transition: all 0.1s ease-in-out;
            transition: all 0.1s ease-in-out;
        }

            .switch-field label:hover {
                cursor: pointer;
            }

        .switch-field input:checked + label {
            background-color: #2a9f00;
            -webkit-box-shadow: none;
            box-shadow: none;
            color: #ffffff;
        }

        .switch-field label:first-of-type {
            border-radius: 1px 0 0 1px;
        }

        .switch-field label:last-of-type {
            border-radius: 0 1px 1px 0;
        }

        .fileUplaodDiv {
            width: 70%;
            border: 1px dashed;
            padding: 1%;
            margin-top: 10px;
        }

        .btnReport,
        .btn-primary {
            background-color: #fff !important;
            color: #a8b1b7 !important;
            font-size: 14px;
        }

         .btn-primary-Red {
           border-color: #e87f25 !important;
           color: #e87f25 !important;
        }
        .btn-primary-Red:hover {
            border-color: #2a9f00 !important;
        }

        .display-Right {
            float: right;
            margin: 5px;
        }

        .containers {
            margin-top: 5px;
            margin-bottom: 300px;
        }

        #divcontainer {
            margin: 10px;
            margin-top: 0;
            width: 96%;
        }

        input[type="file"] {
            display: block;
            padding: 10px 10px 10px 0px;
            font-size: 14px;
            font-family: inherit;
            border: none;
        }

        .file-input {
            position: absolute;
            left: 0;
            height: 59px;
            width: 100%;
            cursor: pointer;
            opacity: 0;
            margin-top: -29px;
        }

        .topicDetail {
            margin-bottom: 25px;
        }

        .file-drop-area {
            border: 2px dashed rgba(10, 10, 10, 0.2);
            padding: 6px;
            height: 40px;
            text-align: center;
            background-color: #FFF8DC;
        }

        .is-active {
            border: 2px dashed #fff;
            background: rgb(42, 159, 0);
            color: #fff;
        }

        .file-drop-area:hover {
            box-shadow: 0px 8px 44px rgba(0,0,0,0.5);
            border: 2px dashed rgb(42, 159, 0);
        }

            .file-drop-area:hover .glyphicon-picture, .file-drop-area:hover .glyphicon-paperclip {
                color: #2a9f00;
            }

        .drop-hover {
            box-shadow: 0px 8px 44px rgba(0,0,0,0.5);
        }

        label {
            font-weight: normal;
        }

        .file-msg {
            font-weight: 300;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            display: inline-block;
            /*max-width: calc(100% - 130px);*/
            vertical-align: middle;
            width: 85%;
        }



        .file-input:focus {
            outline: none;
        }

        .divtopic {
            border: 1px solid #2a9f00;
            padding-bottom: 10px;
            margin-bottom: 10px;
            border-radius: 3px;
            padding-left: 10px;
            padding-right: 10px;
        }

        table {
            font-family: Segoe UI;
            font-size: 14px;
        }

        #topic-list {
            width: 100%;
        }


        #parent {
            border: solid 2px #000000;
        }

        #header {
            width: 97%;
            color: rgb(42, 159, 0);
            font-size: 25px;
        }

            #header table {
                /*background-color: Green;*/
                color: #ffffff;
            }

        #maincontent {
            width: 100%;
        }

        .headerthanks {
            /*background-color: Green;*/
            color: #000000;
            font-size: 14px;
            font-weight: bold;
        }

        .sechead {
            background: none repeat scroll 0 0 #e5e5e5;
            color: #000000;
            height: 25px;
            padding: 2px;
            border-radius: 5px;
        }

        #tblException td {
            padding-top: 20px;
        }

        .secSubhead {
            color: #000000;
            border-bottom: 1px solid solid #000000;
            padding-bottom: 5px;
            padding-top: 5px;
        }

        #pricetab {
            background-color: Green;
            color: White;
        }

        #footertab {
            width: 100%;
        }

        .mainheader {
            height: 55px;
        }

        .footer {
            background-color: #222222;
            color: white;
            padding: 7px;
        }

        .nopading td {
            padding-left: 0;
        }

        .leftBorder {
            border-left-width: 1px;
            border-left-style: solid;
            border-left-color: Black;
            padding-left: 4px;
        }

        .rightBorder {
            border-right-width: 1px;
            border-right-style: solid;
            border-right-color: Black;
            word-wrap: break-word;
            padding-right: 4px;
        }

        .topBorder {
            border-top-width: 1px;
            border-top-style: solid;
            border-top-color: Black;
        }

        .bottomBorder {
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: Black;
        }

        .topictitle {
            color: #2a9f00;
            padding-bottom: 15px;
        }

        .topic {
            margin-top: -9px;
        }

        .txtPlaceHolder {
            width: 6%;
            min-width: 30px;
            border-width: 0px;
            border: 1px solid #ccc;
            border-radius: 3px;
            height: 25px;
            background-color: red;
        }

            .txtPlaceHolder:hover, .txtPlaceHolder:focus {
                border: 1px solid #6bad6b;
                background-color: white;
            }

        .previewImage {
            margin: 5px;
        }

        .navbar-default {
            border-color: #f8f8f8 !important;
        }

        h1 {
            margin: 0px;
        }

        .btn-primary {
            border-color: #a8b1b7;
        }

        a:hover, a:focus, a:visited {
            background-color: #fff;
            color: #2a9f00;
        }

        .btnReport:hover, btn-primary:hover {
            border-color: #2a9f00;
            color: #fff !important;
            background-color: #2a9f00 !important;
        }

        .btn {
            padding: 7px 10px;
            border-radius: 31px;
            background-color: #fff;
            color: #a8b1b7;
        }
           #btnSendReport{

               color: #2a9f00 !important;
         }
        
           #btnSendReport {
            border-color: #2a9f00 !important;
        }

           #btnSendReport:hover {
            border-color: #2a9f00;
            color: #fff !important;
            background-color: #2a9f00 !important;
        }

        .btn-editor {
            padding: 3px 6px;
            border-radius: 15px;
            background-color: #fff;
            color: #a8b1b7;
        }

        .eac-item span {
            display: block;
            height: 50px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }


        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

            .toggle.ios .toggle-handle {
                border-radius: 20px;
            }

        .orderstatus {
            background: #2a9f00;
            -moz-border-radius: 3px;
            border-radius: 3px;
            color: #fff;
            display: inline-block;
            font-size: 12px;
            padding: 0 8px;
            padding-bottom: 2px;
            text-decoration: none;
            margin-right: 3px;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

            .toggle.ios .toggle-handle {
                border-radius: 20px;
            }

        .toggleButtonClass {
            min-height: 28px !important;
            height: 28px !important;
            padding: 3px 10px !important;
            float: right !important;
            margin-top: 5px !important;
            width: 80px !important;
        }

        .toggle .btn {
            padding-top: 3px;
            padding-right: 40px;
        }

        .toggle-on.btn {
            background-color: #2a9f00 !important;
            color: white !important;
        }

        .toggleButtonClass .btn-primary:hover {
            background-color: #2a9f00 !important;
        }

        .div-preViewimage {
            /*border: 1px solid #ccc;*/
            color: #555;
            display: inline-block;
            /*padding: 10px;
            box-sizing: border-box;*/
            /*box-shadow: 5px 5px 2px rgba(10, 10, 10, 0.2);*/
        }

            .div-preViewimage img {
                border: 1px solid #ccc;
            }

        #btnDeleteimg {
            right: 25px;
            float: right;
            position: relative;
            top: -14px;
        }

        .pageloader {
            display: none;
            position: absolute;
            left: 45%;
            z-index: 20;
        }

        .divPrognew {
            background: #e8791c none repeat scroll 0 0;
            border-radius: 1px;
            color: #fff;
            font-size: 12px;
            padding: 5px 7px;
            position: fixed;
            z-index: 100000;
        }

        .btn-primary:hover {
            border-color: #2a9f00;
        }

        .toggle-off.btn {
            padding-left: 30px;
        }

        .ReportTitle {
            font-size: 27px;
            color: #2a9f00;
        }

        .reportMessage {
            font-size: 20px;
            color: #2a9f00;
            margin-top: 2.5%;
            display: none;
            font-weight: normal;
        }

        .errorMessage {
            font-size: 12px;
            color: red;
            display: none;
            font-weight: normal;
        }

        .ui-dialog .ui-dialog-titlebar-close {
            display: none;
        }
      

        .lblReportType {
            font-size: 20px;
            font-weight: normal;
            color: #2a9f00;
        }

        .popupOkBtn {
            float: right;
            color: white;
            background-color: #E8791C;
            border: 1px solid #E8791C;
            border-radius: 4px;
        }

        .fileinput .btn {
            vertical-align: middle;
        }

        .btn-file {
            position: relative;
            overflow: hidden;
            vertical-align: middle;
            background-color: #FFF8DC;
            border: 2px dashed rgba(10, 10, 10, 0.2);
            border-radius: unset;
        }

            .btn-file:hover {
                background-color: #FFF8DC !important;
                border: 2px dashed #2a9f00 !important;
                color: black !important;
                border-radius: unset;
                color: #2a9f00;
            }

            .btn-file > input {
                position: absolute;
                top: 0;
                right: 0;
                width: 100%;
                height: 100%;
                margin: 0;
                font-size: 23px;
                cursor: pointer;
                filter: alpha(opacity=0);
                opacity: 0;
                direction: ltr;
            }

        .btn-default:hover {
            background-color: #2a9f00;
            color: #fff;
        }

        .glyphicon-picture, .glyphicon-paperclip {
            color: gray;
        }

        .btn-file:hover .glyphicon-paperclip {
            color: #2a9f00;
        }

        .btn-file:hover .glyphicon-picture {
            color: #2a9f00;
        }

        .form-control {
            border-color: #ccc;
            -webkit-box-shadow: none;
            box-shadow: none;
        }

            .form-control:focus {
                border-color: #ccc;
                -webkit-box-shadow: none;
                box-shadow: none;
            }

        .commentsMaxi {
            right: -403px !important;
        }

        .commentsCancel {
            right: -413px !important;
        }

        .contener {
            width: 598px !important;
        }

        .top-head {
            width: 100% !important;
        }

        .createpop {
            width: 99% !important;
        }

        .buttonStatus {
            width: 75px !important;
        }

        .buttons {
            background: none repeat scroll 0 0 #2b2b2b;
            padding: 7px 10px;
            border: 0 none;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -khtml-border-radius: 4px;
            color: #FFFFFF;
            clear: left;
            cursor: pointer;
            font-size: 12px !important;
            font-weight: normal;
        }

            .buttons:hover {
                background: none repeat scroll 0 0 #2a9d00;
                color: #FFFFFF;
                padding: 7px 10px;
                text-decoration: none;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                -khtml-border-radius: 4px;
            }

        .reportSavedTd {
            width: 60%;
            align: right;
        }

        .table1style {
            width: 100%;
            padding-right: 16px;
        }

        .orderStatusDiv {
            margin-top: -27px;
            padding-bottom: 15px;
        }

        .spnUserName {
            font-size: 14px;
            color: #838383;
        }

        .spnDateTime {
            font-size: 14px;
            color: #838383;
        }

        .imgProcessing {
            float: right;
            display: none;
            margin-top: 3px;
        }

        .styelDisInlineblock {
            display: inline-block;
        }

        .styelWidth {
            width: 100%;
        }

        .lstExcCount {
            color: #2a9f00;
            width: 1.5%;
            float: left;
            margin-top: 30px;
        }

        .listCount {
            width: 2%;
            float: left;
            margin-top: 0px;
        }

        .spnCreatedBy {
            font-size: 12px;
        }

        .spnOn {
            font-size: 12px;
        }

        .topicTtlNum {
            float: left;
            margin-right: 10px;
        }

        #overlay {
            visibility: hidden;
            position: absolute;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            text-align: center;
            z-index: 1050;
        }

            #overlay div {
                width: 300px;
                margin: 100px auto;
                background-color: #fff;
                border: 1px solid #000;
                text-align: center;
            }

       
    </style>
</head>
<body>
    <div id="overlay">
    </div>
    <div class="pageloader divPrognew">
        <div class="styelDisInlineblock">
           <%-- <img src="/shop/images/Small_Loading.gif" />--%>
                        <img src="/ReportBuilder/resources/images/Small_Loading.gif" />

           
        </div>
        <div style="vertical-align: top;" class="styelDisInlineblock">
            <span id="lblLoading">Loading ...</span>
        </div>
    </div>
    <div id="divcontainer">
        <nav class="navbar navbar-default navbar-fixed-top">

            <table border="0" cellpadding="0" cellspacing="0" id="header" class="mainheader">
                <tr>
                    <td width="20%" align="left"></td>
                    <td align="center" style="vertical-align: middle central">
                        <span id="spnReporttitle" class="ReportTitle">Report Builder</span>
                    </td>
                    <td width="30%" align="left">

                        <table class="styelWidth">
                            <tr>
                                <td class="reportSavedTd">
                                    <label class="reportMessage" id="lblMessage">Report saved</label>
                                </td>
                                <td style="width: 10%;">
                                    <a href="#" id="btnPreView" style="display:none;" data-toggle="tooltip" data-placement="bottom" title="Preview" class="btn btn-primary a-btn-slide-text btnReport display-Right">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                        <span><strong></strong></span>
                                    </a>
                                </td>
                                <td style="width: 10%;">
                                    <a href="#" id="btnSaveReport" style="display:none;" data-toggle="tooltip" data-placement="bottom" title="Save report!" class="btn btn-primary a-btn-slide-text btnReport display-Right">
                                        <span class="glyphicon  glyphicon-floppy-disk" aria-hidden="true"></span>
                                        <span><strong></strong></span>
                                    </a>
                                </td>

                                <td style="width: 10%;">
                                    <a href="#" id="btnSendReport" style="display:none;" data-toggle="tooltip" data-placement="bottom" title="Generate report!" class="btn btn-primary a-btn-slide-text btnReport display-Right">
                                        <span class="glyphicon  glyphicon-envelope"  aria-hidden="true"></span>
                                        <span><strong></strong></span>
                                    </a>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </nav>

        <table id="Table1" cellspacing="0" class="table1style">
            <tr>
                <td colspan="2" align="center">
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="containers" id="Innercontainer" style="display:none;">
                        <div id="divTopiccontainer"></div>
                        <input id="topic-list" placeholder="Search your topic here" />
                    </div>
                </td>
            </tr>
        </table>

        <div id="reportTypePopup" style="display: none;">
            <%-- <label class="lblReportType">Select report type</label>
            <br />--%>
            <select id="ddlReportType" class="input-sm form-control"></select>
            <br />
            <button id="btnOK" class="popupOkBtn" onclick="ReportBuilder.chngExceptionType()">Ok</button>
        </div>

        <div id="reportPreViewPopup" style="display: none;">
            <div id="reportPreViewHtml" style="width: 100%;">
               <iframe height="200" style="border:0" width="500" id="reportPreViewHtmlFream"></iframe>
            </div>
        </div>

        
        <div id="reportSendPopup" style="display: none;">
            <label class="errorMessage" id="lblerrorMessage"></label>
            <table id="tblException" style="width: 100%;">
                <tr>
                    <td>
                        <label id="lbltxtEntityNumber">Order/inquiry no.</label>
                    </td>
                    <td>
                        <label class="lable-textBold" id="lblEntityNumber">OrderNumber</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label id="lbltxtStatus">Status</label>
                    </td>
                    <td>
                        <label class="lable-textBold" id="lblStatus">OrderNumber</label>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label id="lbltxtExceptionType">Exception type</label>
                    </td>
                    <td>
                        <div class="switch-field">
                            <input type="radio" id="switch_left" name="switch_2" value="RBEXCEPTION" checked="checked" />
                            <label for="switch_left">Exception</label>
                            <input type="radio" id="switch_right" name="switch_2" value="RBPPAEXCEPTION" />
                            <label for="switch_right">PPA Exception</label>
                        </div>
                    </td>
                </tr>
                <%--    <tr>
                    <td>
                        <label id="lbltxtExceptionType">Exception type</label>
                    </td>
                    <td>
                        <select id="ddlExceptionType" class="input-sm form-control"></select>
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        <label id="lbltxtPredefine">Predefine solution</label></td>
                    <td>
                        <select id="ddlPredefineSolution" class="input-sm form-control" style="width: 97%;"></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label id="lbltxtPredefinedescription">Predefine solution description</label></td>
                    <td>
                        <textarea style="width: 97%; height: 200px" disabled="disabled" id="txtPredefinedescription"> </textarea>
                    </td>
                </tr>

                <tr id="trSIData" style="display:none;">

                    <td>
                        <label id="lbltxtAttachFile">Attach SI File</label></td>
                    <td>

                      <input type="file" style="padding: 0;" id="updSIFile"  title="Upload SI file." accept='application/zip' />

                    </td>

                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button id="btnSaveException" class="buttons btn-default " type="button">Generate exception</button>
                       <%-- &nbsp   &nbsp 
                        <button id="btnPreViewException" class="buttons btn-default " type="button">Preview</button>--%>
                        &nbsp   &nbsp
                        <button id="btnCancel" class="buttons btn-default" type="button">Cancel</button>

                    </td>
                </tr>
                <%--  <tr>
                    <td colspan="2">
                        <label class="errorMessage" id="lblerrorMessage"></label>
                    </td>
                </tr>--%>
            </table>

        </div>


    </div>
    <script id="reportTemplate" type="text/html">
        <div>
            {{#topics}}
        <div class="divtopic" id="div-topic-{{uniqueid}}" topicid="{{topicid}}" uid="{{uniqueid}}">
            <div class="styelDisInlineblock styelWidth">



                <a  id="btnComment-{{uniqueid}}" data-toggle="tooltip" data-placement="bottom" title="Comment" onclick="ReportBuilder.showComments({{uniqueid}},{{topicid}});" class="btn btn-primary a-btn-slide-text btnReport btn-editor  display-Right" onclick="ReportBuilder.removeTopic({{uniqueid}})">
                    <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
                    <span><strong></strong></span>
                </a>

                <a  id="btnDelete-{{uniqueid}}" data-toggle="tooltip" data-placement="bottom" title="Delete" class="btn btn-primary a-btn-slide-text btnReport btn-editor  display-Right" onclick="ReportBuilder.removeTopic({{uniqueid}})">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    <span><strong></strong></span>
                </a>
                <a  id="btnEdit-{{uniqueid}}" data-toggle="tooltip" data-placement="bottom" title="Edit" class="btn btn-primary a-btn-slide-text btnReport btn-editor  display-Right" onclick="ReportBuilder.editTopic({{uniqueid}})">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong></strong></span>
                </a>

                <a id="btnsave-{{uniqueid}}" data-toggle="tooltip" data-placement="bottom" title="Save"  class="btn btn-primary a-btn-slide-text btnReport btn-editor  display-Right" onclick="ReportBuilder.saveTopic({{uniqueid}})" style="display: none;">
                    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
                    <span><strong></strong></span>
                </a>
                <input type="checkbox" id="toggle-two-{{uniqueid}}" uniqueid="{{uniqueid}}" class="toggleChkbx" />
            </div>
            <div class="styelDisInlineblock styelWidth">
                <table class="styelWidth">
                    <tr>
                        <td>


                            <div class="topic" id="{{uniqueid}}" topicid="{{topicid}}" uid="{{uniqueid}}" countingno="{{countingNo}}">
                                <div class="orderStatusDiv">
                                    <span class="orderstatus">{{orderStatus}} </span>
                                    <span class="spnCreatedBy">Created by</span>
                                    <span class="spnUserName">{{userName}}</span>
                                    <span class="spnOn">on : </span><span class="spnDateTime">{{dateTime}} </span>
                                </div>

                                <div id="divTopic{{uniqueid}}" class="topic-inner" topicid="{{topicid}}" uid="{{uniqueid}}">
                                    <div class="topictitle">
                                        <h1 style="float: left; margin-right: 10px; width: 1.5%; display: none" class="countingNoCls">{{countingNo}}.</h1>
                                        <h1 style="display: inline-block; width: 95%;" class="topicText" unid="divTopic{{uniqueid}}">{{{topic}}}</h1>
                                    </div>
                                    <div id="topic-{{uniqueid}}" class="topicDetail">
                                        {{{topicDetail}}}
                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="file-drop-area" id="div-drop-area-{{uniqueid}}" title="Attach image">
                                <span class="file-msg js-set-number" fileuploadid="{{uniqueid}}"><span class='glyphicon glyphicon-picture'></span>&nbsp;&nbsp;<span class='glyphicon glyphicon-paperclip'></span></span><span class="fileinput-filename"></span><span class="fileinput-new"></span>
                                 <%-- <img id="imgProcessing" alt="Processing.." title="Processing.." class="imgProcessing" src="Images/processing_w.gif" />--%>
                                <div id="imgProcessing" class="imgProcessing"></div>
                                <input type="file" id="hdrFileUplaodCntrl{{uniqueid}}" class="file-input" accept="image/*" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
            {{/topics}}
        </div>

    </script>

    <script id="editreportTemplate" type="text/html">
        <div>
            {{#topics}}
        <div class="divtopic" id="div-topic-{{uniqueid}}" topicid="{{topicid}}" uid="{{uniqueid}}">
            <div class="styelDisInlineblock styelWidth">

                <a  id="btnComment-{{uniqueid}}" data-toggle="tooltip" data-placement="bottom" title="Comment" onclick="ReportBuilder.showComments({{uniqueid}},{{topicid}});" class="btn btn-primary a-btn-slide-text btnReport btn-editor  display-Right {{CommentClass}}" onclick="ReportBuilder.removeTopic({{uniqueid}})">
                    <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
                    <span><strong></strong></span>
                </a>
                <a href="#" id="btnDelete-{{uniqueid}}" data-toggle="tooltip" data-placement="bottom" title="Delete" class="btn btn-primary a-btn-slide-text btnReport btn-editor  display-Right" onclick="ReportBuilder.removeTopic({{uniqueid}})">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    <span><strong></strong></span>
                </a>
                <a  id="btnEdit-{{uniqueid}}" data-toggle="tooltip" data-placement="bottom" title="Edit" class="btn btn-primary a-btn-slide-text btnReport btn-editor  display-Right  " onclick="return ReportBuilder.editTopic({{uniqueid}})">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong></strong></span>
                </a>


                <a id="btnsave-{{uniqueid}}" data-toggle="tooltip" data-placement="bottom" title="Save" class="btn btn-primary a-btn-slide-text btnReport btn-editor  display-Right" onclick=" return ReportBuilder.saveTopic({{uniqueid}})" style="display: none;">
                    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
                    <span><strong></strong></span>
                </a>
                <input type="checkbox" id="toggle-two-{{uniqueid}}" uniqueid="{{uniqueid}}" class="toggleChkbx" />
            </div>
            <div class="styelDisInlineblock styelWidth">
                <table class="styelWidth">
                    <tr>
                        <td>
                            <div class="topic" id="{{uniqueid}}" topicid="{{topicid}}" uid="{{uniqueid}}">
                                {{{topicDetail}}}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="file-drop-area" id="div-drop-area-{{uniqueid}}">
                                <span class="file-msg js-set-number" fileuploadid="{{uniqueid}}"><span class='glyphicon glyphicon-picture'></span>&nbsp;&nbsp;<span class='glyphicon glyphicon-paperclip'></span></span><span class="fileinput-filename"><span class="fileinput-new"></span></span>
                               <%-- <img id="imgProcessing" alt="Processing.." title="Processing.." class="imgProcessing" src="Images/processing_w.gif" />--%>
                                <div id="imgProcessing" class="imgProcessing"></div>
                                <input type="file" id="hdrFileUplaodCntrl{{uniqueid}}" class="file-input" accept="image/*" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
            {{/topics}}
        </div>

    </script>
    <form name="test" runat="server" id="frnml">
<%--        <uc1:WUCComments runat="server" ID="WUCComments" />--%>

        <!----- by Nidhi --->
        <asp:HiddenField ID="hdnRBTopicId" runat="server" />
         <asp:HiddenField ID="hdnBackupid" runat="server" /> 
    </form>
     <script>
        $(window).load(function () {
            $('.pageloader').show();
        });
    </script>
</body>
</html>
