﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using RestSharp;
using System.Xml;
using System.Runtime.InteropServices;
using sparrowFeApp.core;

namespace EC09WebApp.shop.rb
{
    // public sealed class HttpResponse
    public class ReportBuilderUtilities
    {
        const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
        public static string GetReportDetail(string reportType, string entityNumber, long userid,string username)
        {
            List<TopicList> list = new List<TopicList>();
            string sql = string.Format("exec getUpdatedTopiCacheVersion {0}", 0);

            reportType = "RBEXCEPTION";
            int version = 1;

            dynamic topicList = HttpContext.Current.Cache.Get("CachedTopicList_" + reportType + "_" + version);
            if (topicList == null)
            {
                string xmlData = GetXMLData("rbtopics", "516f30f0-b587-4381-abf6-8b91f06efa59-1");
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlTextReader(new StringReader(xmlData)));
                if (xmlData != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        TopicList obj = new TopicList();
                        obj.name = (string)row["rb_topic"];
                        obj.id = Convert.ToString(row["id"]);
                        list.Add(obj);
                    }
                    HttpContext.Current.Cache.Insert("CachedTopicList_" + reportType + "_" + +version + "", list, null, DateTime.Now.AddDays(3), Cache.NoSlidingExpiration);
                }
            }
            else
            {
                list = topicList;
            }
            sql = string.Format("select name,code from sales_orderstatus where id = (SELECT order_status_id FROM sales_orderdetail where order_num = '{0}')", entityNumber);
            Database.DA da = new Database.DA();
            DataTable dt = da.GetDataTable(sql);
            string orderStatus = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                orderStatus = dt.Rows[0]["name"].ToString();
            }

            ReportDetail objdetail = new ReportDetail();
            objdetail.reportData = "";
            objdetail.userName = username;
            objdetail.reportType = reportType;
            objdetail.reportTitle = "Report Builder-";
            objdetail.entityNumber = entityNumber;
            objdetail.orderStatus = orderStatus; 


            List<CommentDetail> CommentDetailList = new List<CommentDetail>();
            return JsonConvert.SerializeObject(new { Status = 1, TopicList = list, ReportDetail = objdetail, CommentDetailList = CommentDetailList });
        }

        public static string GetTopicdetail(string id)
        {

            List<TopicList> list = new List<TopicList>();
            string sql = string.Format("exec getUpdatedTopiCacheVersion {0}", 0);

            int version = 1;
            dynamic topicList = HttpContext.Current.Cache.Get("CachedTopicList_" + version);
            if (topicList == null)
            {
                string xmlData = GetXMLData("rbtopics", "516f30f0-b587-4381-abf6-8b91f06efa59-1");
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlTextReader(new StringReader(xmlData)));
                if (xmlData != null)
                {

                    string topic = "";
                    string details = "";
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        if (id == Convert.ToString(row["id"]))
                        {
                            topic = (string)row["rb_topic"];
                            details = (string)row["rb_topic_detail"];
                            break;
                        }
                    }
                    details = details.Replace("[#$$#]", "<input class='txtPlaceHolder' type='text'>");
                    string fileHolde = "<div class='fileinput fileinput-new' data-provides='fileinput' title='Attach image'><span class='btn btn-file'><span class='glyphicon glyphicon-picture'></span>&nbsp;&nbsp;<span class='glyphicon glyphicon-paperclip'></span><input class='imgPlaceHolder' type = 'file' accept='image/*'/></span><span class=fileinput-filename></span><span class=fileinput-new></span></div>";
                    details = details.Replace("[#$IMAGE$#]", fileHolde);
                    return JsonConvert.SerializeObject(new { Status = 1, topicDetail = details, topic = topic });
                }
            }
            return "";
        }
        public static string GetFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);
            fileName = Path.GetFileNameWithoutExtension(fileName);
            string regexSearch = new string(Path.GetInvalidFileNameChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            fileName = r.Replace(fileName, "");
            if (fileName.Length > 30)
            {
                fileName = fileName.Substring(0, 30);
            }
            return fileName + extension;
        }

        public string generateException(string orderNumber, string PredefineSolutionCode,string solutionDescription, string excptionType, byte[] fBytes, decimal size,string userid)
        {
            Database.DA objDA = new Database.DA();
            DataSet ds = objDA.GetDataSet(string.Format("select id,order_status_id,operator_id,is_assembly from sales_orderdetail where order_num= '{0}'", orderNumber));//ToDO IscustsupplyParts,IsFreeParts
            DataSet dsOrderStatus = objDA.GetDataSet(string.Format("select id, code from sales_orderstatus where code in ('ASSEMBLYINCROSSCHECK', 'CROSSCHECK') and is_active = true")); 
            string result = "";
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                long orderId = 0; 
                long ordStatusId = 0;
                long operatorId = 0;
                long.TryParse(ds.Tables[0].Rows[0]["id"].ToString(), out orderId);
                long.TryParse(ds.Tables[0].Rows[0]["order_status_id"].ToString(), out ordStatusId);
                long.TryParse(ds.Tables[0].Rows[0]["operator_id"].ToString(), out operatorId);
                bool is_assembly_data = false;
                //bool IscustsupplyParts = false;
                //bool IsFreeParts = false; 
                bool.TryParse(ds.Tables[0].Rows[0]["is_assembly"].ToString(), out is_assembly_data);
                //bool.TryParse(ds.Tables[0].Rows[0]["IscustsupplyParts"].ToString(), out IscustsupplyParts);
                //bool.TryParse(ds.Tables[0].Rows[0]["IsFreeParts"].ToString(), out IsFreeParts);

                string FilePath = "";
                DataSet dsOrderPath = objDA.GetDataSet(string.Format("select file_path from sales_file where entity_number = '{0}' and file_type = 'ORD'", orderNumber));
                if (dsOrderPath != null && dsOrderPath.Tables.Count > 0 && dsOrderPath.Tables[0].Rows.Count > 0)
                {
                    FilePath = dsOrderPath.Tables[0].Rows[0]["file_path"].ToString();
                    if(FilePath == "")
                    { 
                        result = "Order has no XML file."; 
                        return JsonConvert.SerializeObject(new { Status = 0, Message = result });
                    }
                }

                long lngAssemblyCCStatusId = 0, lngCROSSCHECKStatsId = 0;
                if (dsOrderStatus != null && dsOrderStatus.Tables.Count > 0 && dsOrderStatus.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsOrderStatus.Tables[0].Rows.Count; i++)
                    {
                        if (dsOrderStatus.Tables[0].Rows[i]["code"].ToString().ToUpper() == "ASSEMBLYINCROSSCHECK")
                        {
                            long.TryParse(dsOrderStatus.Tables[0].Rows[i]["id"].ToString(), out lngAssemblyCCStatusId);
                        }
                        else if (dsOrderStatus.Tables[0].Rows[i]["code"].ToString().ToUpper() == "CROSSCHECK")
                        {
                            long.TryParse(dsOrderStatus.Tables[0].Rows[i]["id"].ToString(), out lngCROSSCHECKStatsId);
                        }
                    }
                }
                 
                if (excptionType.ToUpper() == "RBPPAEXCEPTION")//----------------Check assembly Order PPA Exception is Allowed or not when order status not in assembly,BOM Analysis and BOM Cross Check
                {
                    bool isAllow = false;
                    if (is_assembly_data)
                    {
                        if (ordStatusId == lngAssemblyCCStatusId)
                        {
                            isAllow = true;
                        }
                    }
                    else
                    {
                        if (ordStatusId == lngCROSSCHECKStatsId)
                        {
                            isAllow = true;
                        }
                    }
                    if (!isAllow)
                    {
                        result = "PPA exception can not be generated in this section.";
                        if (ordStatusId == lngCROSSCHECKStatsId)
                        {
                            result = "PPA exception can not be generated other then BOM crosscheck section.";
                        }
                        return JsonConvert.SerializeObject(new { Status = 0, Message = result });
                    }

                    //--------------------- For Assembly Order check component price
                    if (is_assembly_data && FilePath != "")
                    { 
                        string OrderXml = Common.GetOrderXMLFromFileServer(FilePath, orderNumber);
                        if (OrderXml != "")
                        {
                            decimal decComponentTotalNetPrice = 0;
                            decimal decAssemblyTotalNetPrice = 0;
                            XmlDocument xdoc = new XmlDocument();
                            xdoc.LoadXml(OrderXml);
                            System.Xml.XmlNode nodeBoardProperties3 = xdoc.SelectSingleNode("OrderDetail/OrderProperties3");
                            if (nodeBoardProperties3.SelectSingleNode("ComponentTotalNetPrice") != null)
                            {
                                decimal.TryParse(nodeBoardProperties3.SelectSingleNode("ComponentTotalNetPrice").InnerText, out decComponentTotalNetPrice);
                            }
                            if (nodeBoardProperties3.SelectSingleNode("AssemblyTotalNetPrice") != null)
                            {
                                decimal.TryParse(nodeBoardProperties3.SelectSingleNode("AssemblyTotalNetPrice").InnerText, out decAssemblyTotalNetPrice);
                            }
                            if (decComponentTotalNetPrice == 0 || decAssemblyTotalNetPrice == 0)
                            {
                                result = "Component(s) price is not completed, PPA exception can not be generated.";
                                return JsonConvert.SerializeObject(new { Status = 0, Message = result });
                            }
                        }
                    }
                }
                string strSIFilePath = "";
                if (FilePath != "")
                {
                    strSIFilePath = StoreSiFile(fBytes,FilePath,orderNumber);
                }
                
                string crmuserid = userid;
                //bool isExceptionInserted = Common.InsertException(userid,orderNumber,PredefineSolutionCode, solutionDescription, excptionType);

                //if (AppSessions.EccUserId != null)
                //{
                //    crmuserid = AppSessions.EccUserId;
                //}

                string siFilename = "";
                if(strSIFilePath != "")
                {
                    siFilename = "SI_" + orderNumber + ".zip";
                    strSIFilePath = "\\" + strSIFilePath.Replace(siFilename, "");  
                }

                long lngUserid = 0;
                long.TryParse(userid, out lngUserid); 
                long crm_rel_id = Common.GetCrmRelIdFromUserid(lngUserid); 
                string jsonFeApp = "";
                var feApp = new { fun_name = "file_sync", entity_number = orderNumber, file_name= siFilename, file_type= "report_builder_exce", pre_defined_sol = PredefineSolutionCode, exe_type = excptionType, crm_rel_id = crm_rel_id.ToString(), file_path = strSIFilePath, source = "FE", target = "EC" };
                jsonFeApp = Newtonsoft.Json.JsonConvert.SerializeObject(feApp);
                Common feAppData = new Common();
                feAppData.FeAppAPI(jsonFeApp);
                
                return JsonConvert.SerializeObject(new { Status = 1 });
            }
            return JsonConvert.SerializeObject(new { Status = 0 });
        }

        public static bool IsImpersonation()
        {
            System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = null;
            impersonateOnFileServer = ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);
            if (impersonateOnFileServer != null)//todo
            {
                return true;
            }
            return false;//todo 
        }
        public static WindowsImpersonationContext ImpersonateFileServer(string Username, string Password, string v)
        {
            string Domain = Constant.FileServerDomain;
            IntPtr tokenHandle = new IntPtr(0);
            int dwLogonProvider = Convert.ToInt16(Constant.DwLogonProvider);
            int dwLogonType = Convert.ToInt16(Constant.DwLogonType);
            bool returnValue = LogonUser(Username, Domain, Password, dwLogonType, dwLogonProvider, ref tokenHandle);
            WindowsIdentity ImpersonatedIdentity = new WindowsIdentity(tokenHandle);
            WindowsImpersonationContext MyImpersonation = ImpersonatedIdentity.Impersonate();
            return MyImpersonation;
        }
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        private string GenException(long OrderId, string excptionType, string SolutionDescriptionCode, long userId, string EntityNumber, string PreDefineSolutionId, string SolutionDescription, long CustCompanyId, long ordUserId, long OrdeStatusId, long operatorId)
        {
            //try
            //{
            //    core.DA objDA = new core.DA();
            //    //    EC09WebApp.API.gd objAPI = new EC09WebApp.API.gd();
            //    //    EC09WebApp.API.gf gf = new EC09WebApp.API.gf();
            //    //    EC09WebApp.EC09WebService.Exceptions objException = new EC09WebService.Exceptions();
            //    bool _IsOrder = true;
            //    Guid RandomNo = new Guid();
            //    genException Exception = new genException();

            //    if (_IsOrder)
            //        Exception.OrderId = OrderId;
            //    else
            //        Exception.InqOfferId = OrderId;
            //    Exception.Remarks = "";
            //    Exception.Comments = "";
            //    Exception.IsReqCAction = false;
            //    Exception.ProblemDescription = "See attached document.";
            //    Exception.SolutionDescription = SolutionDescription;
            //    Exception.PreDefineProblemId = Convert.ToInt64(662);
            //    Exception.PreDefineSolutionId = Convert.ToInt64(PreDefineSolutionId);
            //    Exception.IsDocument = true;
            //    long ExceptionId = 0;
            //    string recordType = "Order";
            //    if (!_IsOrder)
            //    {
            //        recordType = "Inquiry";
            //    }
            //    string sqlExcAvail = string.Format("exec ecc_CheckExceptionAvail {0}", OrderId);
            //    DataSet ds = objDA.GetDataSet(sqlExcAvail);
            //    if (ds != null && ds.Tables[0].Rows.Count > 0)
            //    {
            //        return "Exception already generated for this order.";
            //    }
            //    ExceptionId = objException.InsertUpdateRaiseException(Exception, false, false, true, true, userId, (Guid)RandomNo, recordType, CustCompanyId, ordUserId, OrdeStatusId, operatorId, "Internal");
            //    if (ExceptionId != 0)
            //    {
            //        //  excptionType = "RBEXCEPTION";
            //        //if (SolutionDescriptionCode == "Pre-production approval request")
            //        //{
            //        //    excptionType = "RBPPAEXCEPTION";
            //        //}
            //        string filePath = "Exception_Files" + "\\" + Convert.ToString(DateTime.UtcNow.Year) + "\\" + Convert.ToString(DateTime.UtcNow.Month) + "\\" + Convert.ToString(DateTime.UtcNow.Day) + "\\" + ExceptionId; ;
            //        PutFileForReportBuilder(ExceptionId.ToString(), filePath, EntityNumber, 3, userId, excptionType);
            //        if (excptionType == "RBPPAEXCEPTION")
            //        {
            //            objDA.GetDataSet("update genExceptions set IsDocument=1 where ExceptionId=" + ExceptionId);
            //            objDA.GetDataSet("update GenFiles set ModifiedBy=null,ModifiedDate=null,IsModified=null where Number='" + ExceptionId + "' and FileTypeCode='EXCP'");
            //        }
            //    }

            return "OK";
            //}
            //catch
            //{
            //    return "Error";
            //}
        }

        public string StoreSiFile(byte[] fBytes,string filepath,string entitynumber)
        {
            try
            {
                if (fBytes != null)
                {
                    Guid guid = Guid.NewGuid();
                    string sesssionid = guid.ToString();
                    if (IsImpersonation())
                    {
                        string RootPath = Constant.FileServerPath;
                        string SiPath = RootPath + "" + filepath;
                        string Filename = "SI_"+ entitynumber + ".zip";
                        if (!Directory.Exists(SiPath))
                        {
                            Directory.CreateDirectory(SiPath);
                        }
                        File.WriteAllBytes(SiPath + "\\" + Filename, fBytes);
                        return filepath + "\\" + Filename;
                    }
                }
                return "";
            }
            catch (Exception )
            {
                return "";
            }
        }

        public void PutFileForReportBuilder(string exceptionId, string filePath, string entityNumber, int FSID, long createdBy, string exceptionType)
        {
            //   genExceptionLog objException = new genExceptionLog();  //todo
            //try
            //{
            //    //    EC09WebApp.API.gd objgd = new EC09WebApp.API.gd();  //todo
            //    string strFileServerDomain = System.Configuration.ConfigurationManager.AppSettings["FileServerDomain"].ToString();
            //    DataTable dt1 = objgd.GetData(string.Format("select RootPath,Username,[Password]  from genfileservers where FileServerID = {0} ", FSID), Keydata).Tables[0];
            //    string DirUrl = dt1.Rows[0]["RootPath"].ToString() + "\\" + filePath;
            //    WindowsImpersonationContext MyImpersonation = EC09.Common.WindowsImpersonation.ImpersonateFileServer(dt1.Rows[0]["Username"].ToString(), dt1.Rows[0]["Password"].ToString());
            //    if (Directory.Exists(DirUrl) == false)
            //    {
            //        Directory.CreateDirectory(DirUrl);
            //    }
            //    string folderPath = filePath + "\\";
            //    filePath = DirUrl + "\\" + "RB_" + exceptionId + ".pdf";
            //    EC09WebApp.shop.rb.ReportBuilderUtilities rbUtilities = new EC09WebApp.shop.rb.ReportBuilderUtilities();
            //    var objTemplate = rbUtilities.GetReportPreViewHtml(entityNumber, false, exceptionType);
            //    if (objTemplate == null)
            //    {
            //        return;
            //    }
            //    string html = objTemplate.content;
            //    string header = objTemplate.header;
            //    string footer = objTemplate.footer;
            //    HtmlToPdf.EC09HtmlToPdf objCHP = new HtmlToPdf.EC09HtmlToPdf();
            //    objCHP.Credentials = new System.Net.NetworkCredential(dt1.Rows[0]["Username"].ToString(), dt1.Rows[0]["Password"].ToString());
            //    objCHP.Url = System.Configuration.ConfigurationManager.AppSettings["PdfService"].ToString();

            //    objCHP.PdfConverterWithHF(html, header, footer, "RB_" + exceptionId + ".pdf", filePath, true, dt1.Rows[0]["Username"].ToString(), dt1.Rows[0]["Password"].ToString(), strFileServerDomain, false);

            //    string typecode = "EXCP";
            //    string cron = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
            //    long length = new System.IO.FileInfo(filePath).Length;
            //    string sql = string.Format("Insert into GenFiles (FileName,Number,FilePath,FileSize,FileTypeCode,FileServerId,CreatedBy,CreatedDate)" +
            //        "values('{0}','{1}','{2}',{3},'{4}',{5},{6},'{7}')", "RB_" + exceptionId + ".pdf", exceptionId, folderPath, length, typecode, FSID, createdBy, cron);
            //    objgd.PutData(sql, Keydata);
            //}
            //catch (Exception ex)
            //{
            //    throw (ex); //do
            //    //   objException.LogException(ex, 0, false); //todo
            //}
        }

        public string UploadFile(HttpPostedFile file)
        {
            string FolderPath = "";
            string filePath = "";
            string fileName = GetFileName(file.FileName);
            
            if (IsImpersonation())
            {
                FolderPath = "\\\\Devserver\\fs\\";

                if (!Directory.Exists(FolderPath + "\\RBData"))
                {
                    Directory.CreateDirectory(FolderPath + "\\RBData");
                }
                filePath = FolderPath + "\\" + System.Guid.NewGuid() + fileName;
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                file.SaveAs(filePath);
            }
            
            return JsonConvert.SerializeObject(new { Status = 1, path = filePath });
        }

        public string getHTMLFromServer(string entityNumber, string reportId, string reportType)
        {
            try
            {

                int reportid = 0;
                Int32.TryParse(reportId, out reportid);
                string html = "";
                Database.DA objDA = new Database.DA();
                DataSet dsRBDtlEntityNumber = objDA.GetDataSet(string.Format("select * From sales_rbdetails  where  entity_number = '{0}' and is_deleted <> true ", entityNumber));

                if (dsRBDtlEntityNumber.Tables[0].Rows.Count > 0)
                {
                    string filePath = dsRBDtlEntityNumber.Tables[0].Rows[0]["rb_data_path"].ToString();
                    if (IsImpersonation())
                    {
                        var fileStream = new FileStream(Constant.FileServerPath + filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        using (var streamReader = new StreamReader(fileStream))
                        {
                            html = streamReader.ReadToEnd();
                        }
                    }
                }
                return html;
            }
            catch (Exception )
            { return null; }
        }
        public void DeleteRBFile(string entityNumber)
        {
            //    EC09WebApp.API.gd objAPI = new EC09WebApp.API.gd(); //todo
            //  objAPI.PutData(string.Format("exec DeleteRBFiles '{0}'", entityNumber), Keydata); //todo
        }


        public string SaveReport(string reportContent, string backupId)
        {
            ReportDetail obj = (ReportDetail)JsonConvert.DeserializeObject(reportContent, (typeof(ReportDetail)));
            try
            {
                string FolderPath = "";
                string filePath = "";
                string id = "";

                Database.DA objDA = new Database.DA();
                if (IsImpersonation())
                {
                    FolderPath = "\\RBData" + "\\" + DateTime.Now.Year + "\\" + DateTime.Now.Month + "\\" + DateTime.Now.Day;
                    DataSet dsRBDtlEntityNumber = objDA.GetDataSet("select * From sales_rbdetails  where  entity_number = '" + obj.entityNumber + "' and is_deleted <> true ");
                    //   DataSet dsRBDtlEntityNumber = objDA.GetDataSet(string.Format("select * From sales_rbdetails  where  entity_number = '{0}' and entity_type='{1}'", obj.entityNumber, obj.reportType));

                    if (dsRBDtlEntityNumber != null && dsRBDtlEntityNumber.Tables.Count > 0 && dsRBDtlEntityNumber.Tables[0].Rows.Count > 0)
                    {
                        filePath = dsRBDtlEntityNumber.Tables[0].Rows[0]["rb_data_path"].ToString();
                        id = dsRBDtlEntityNumber.Tables[0].Rows[0]["id"].ToString();
                    }
                    else
                    {
                        if (!Directory.Exists(Constant.FileServerPath + FolderPath + "\\" + obj.entityNumber))
                        {
                            Directory.CreateDirectory(Constant.FileServerPath + FolderPath + "\\" + obj.entityNumber);
                        }
                        filePath = FolderPath + "\\" + obj.entityNumber + "\\" + obj.entityNumber + "_" + obj.reportType + ".html";
                    }

                    string bckupFilePath = Constant.FileServerPath + filePath.Replace(obj.entityNumber + "_" + obj.reportType + ".html", string.Empty) + "\\" + "backup_" + obj.entityNumber + "_" + obj.reportType + "_" + backupId + ".html";
                    File.WriteAllText(Constant.FileServerPath + filePath, obj.reportData, Encoding.UTF8);
                    File.WriteAllText(bckupFilePath, obj.reportData, Encoding.UTF8);


                    if (id == "")
                    {

                        string sql1 = null;
                        sql1 = "insert into sales_rbdetails (entity_number,entity_type,rb_data_path,is_deleted,created_by_id) values('" + obj.entityNumber + "','" + obj.reportType + "','" + filePath + "','false','1' )";
                        objDA.GetDataSet(sql1);
                        sql1 = "INSERT INTO Sales_exception (entity_number,entity_type,data_path,created_by) VALUES ('" + obj.entityNumber + "','" + obj.reportType + "','" + filePath + "','1')";
                        //sql1 = string.Format("insert into sales_rbdetails (entity_number,entity_type,rb_data_path,is_deleted,created_by_id) values( '{0}' '{1}' '{2}' '{3}' '{4}' ) ", obj.entityNumber, obj.reportType, filePath, false, 1);
                        //sql1 = string.Format("insert into sales_rbdetails (entity_number,entity_type,rb_data_path,created_by,created_on,is_deleted) values('@entity_number', '@entity_type',' @rb_data_path', '@created_by', 'GETDATE()', 0)");
                        // By Nidhi
                    }
                    else
                    {
                        string topicval = obj.rdtopicid.TrimEnd(',');
                        string sql2 = string.Format("update sales_rbdetails set entity_number='{0}',entity_type='{1}',rb_data_path='{2}' where entity_number = '{3}'", obj.entityNumber, obj.reportType, filePath, obj.entityNumber);
                        objDA.GetDataSet(sql2);
                        //objgd.PutData(sql2, Keydata);
                        //}}
                    }
                    bool isRBData = false;
                    if (obj.hasActiveTopic)
                    {
                        isRBData = true;
                    }
                    string sql3 = null;
                    //  sql3 = "update into sales_orderdetail (entity_number,entity_type,rb_data_path,is_deleted,created_by_id) values('" + obj.entityNumber + "','" + obj.reportType + "','" + filePath + "','true','1' )";
                    if (!obj.entityNumber.StartsWith("B"))
                    {
                        sql3 = "UPDATE sales_orderdetail  SET  report_builder = '"+ isRBData + "'  where order_num='" + obj.entityNumber + "'"; 
                    }
                    else
                    {
                        sql3 = "UPDATE sales_basketinquirydetail  SET  report_builder = '"+ isRBData + "'  where order_num='" + obj.entityNumber + "'"; 
                    }
                    objDA.GetDataSet(sql3);
                }
                //test
                string fileName = Path.GetFileName(filePath);

                string filePathnw = filePath.Replace(obj.entityNumber + "_" + obj.reportType + ".html", string.Empty);
                 
                long crm_rel_id = 0; 
                crm_rel_id = Common.GetCrmRelIdFromUserid(obj.userid); 
                if(filePathnw.Trim() != "" && filePathnw.Contains(obj.entityNumber))
                {
                    var feApp = new { fun_name = "file_sync", entity_number = obj.entityNumber, file_name = "", file_type = "report_builder", file_path = filePathnw, backup_id = backupId, crm_rel_id = crm_rel_id.ToString(), source = "FE", target = "EC" };
                    string jsonFeApp = JsonConvert.SerializeObject(feApp);
                    Common feAppData = new Common();
                    feAppData.FeAppAPI(jsonFeApp);
                }  
                return JsonConvert.SerializeObject(new { Status = 1, path = filePath });
            }
            catch (Exception )
            {
                return null;
            }
        }

        public RepordTemplate GetReportPreViewHtml(string entityNumber, bool ispreview, string entityType)
        {
            //   EC09WebApp.API.gd objAPI = new EC09WebApp.API.gd(); //todo
            RepordTemplate obj = new RepordTemplate();
            try
            {
                string sql = string.Format("exec getHTMLFilePath '{0}',{1},'{2}'", entityNumber, 0, entityType);
                //       DataSet ds = objAPI.GetData(sql, Keydata);//todo
                //if (ds.Tables[0].Rows.Count > 0)//todo
                //{
                //    string entity_type = entityType;
                //    string tableContents = "";
                //    string html = getHTMLFromServer(entityNumber, "0", entity_type);
                //    HtmlDocument document2 = new HtmlDocument();
                //    document2.LoadHtml(html);
                //    HtmlNode[] nodes = document2.DocumentNode.SelectNodes("//div").ToArray();
                //    string htmlPreview = "";

                //    foreach (var tag in nodes)//todo
                //    {
                //        var classValue = tag.Attributes["class"] == null ? "" : tag.Attributes["class"].Value;
                //        if (classValue.Contains("fileinput-new"))
                //        {
                //            tag.Remove();
                //        }

                //    }
                //    HtmlNode[] h1Nodes = document2.DocumentNode.SelectNodes("//h1").ToArray();//todo
                //    foreach (var tag in h1Nodes)
                //    {
                //        var classValue = tag.Attributes["class"] == null ? "" : tag.Attributes["class"].Value;
                //        if (classValue.Contains("countingNoCls"))
                //        {
                //            tag.Remove();
                //        }
                //    }
                //    int count = 0;
                //    foreach (HtmlNode item in nodes)//todo
                //    {
                //        var classValue = item.Attributes["class"] == null ? "" : item.Attributes["class"].Value;
                //        string id = item.Attributes["id"] == null ? "" : item.Attributes["id"].Value;

                //        if (classValue == "topic-inner")
                //        {
                //            if (item.Attributes["isActive"].Value == "true")
                //            {
                //                foreach (HtmlNode citem in item.ChildNodes.ToArray())
                //                {
                //                    var childclassValue = citem.Attributes["class"] == null ? "" : citem.Attributes["class"].Value;
                //                    if (childclassValue == "topictitle")
                //                    {
                //                        tableContents = tableContents + "<li><a href = \"#" + item.Id + "\">" + citem.InnerText + " </a></li>";
                //                        count++;
                //                    }
                //                }
                //                htmlPreview = htmlPreview + " <div class=\"divtopic\"><h1 style=\" float: left;margin-right: 10px;color:#2a9f00;\">" + count + ".</h1><div id=\"divTopicContener\"> " + item.OuterHtml + "</div></div>";
                //            }
                //        }

                //    }



                //    if (entity_type == "")//todo
                //    {
                //       entity_type = "COMMON";// entity_type = "RBEXCEPTION"; 
                //    }
                //    if(entityNumber.StartsWith("B"))
                //    {
                //        entity_type = "OFFER";
                //    }
                //    HtmlDocument document = new HtmlDocument();
                //    string temlatepsth = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath) + "\\shop\\rb\\" + entity_type + "-preview.html";
                //    document.Load(temlatepsth);

                //    //HtmlNode[] nodesDivs = document.DocumentNode.SelectNodes("//div").ToArray();
                //    //foreach (var tag in nodesDivs)
                //    //{

                //    //    var classValue = tag.Attributes["class"] == null ? "" : tag.Attributes["class"].Value;
                //    //    if (classValue == "additionalInfo")
                //    //    {
                //    //        HtmlNode[] h1nodes = tag.SelectNodes("//h1").ToArray();
                //    //        foreach (var h1node in h1nodes)
                //    //        {
                //    //            var className = h1node.Attributes["class"] == null ? "" : h1node.Attributes["class"].Value;
                //    //            if (className == "listCount")
                //    //            {
                //    //                count++;
                //    //                h1node.InnerHtml = count.ToString() + ".";
                //    //            }
                //    //        }
                //    //    }
                //    //}

                //    HtmlNode itempre = document.GetElementbyId("InnerContener");
                //    itempre.InnerHtml = htmlPreview;
                //    HtmlNode reportTitle = document2.GetElementbyId("reportTitle");
                //    HtmlNode reportTitlenew = document.GetElementbyId("reportTitle");
                //    reportTitlenew.InnerHtml = reportTitle.InnerHtml;

                //    //HtmlNode header = document.GetElementbyId("tblheader");
                //    //string tblheader = header.OuterHtml.ToString();
                //    //HtmlNode trheader = document.GetElementbyId("trheader");




                //    //HtmlNode Footer = document.GetElementbyId("tblFooter");
                //    //string tblFooter = Footer.OuterHtml.ToString();
                //    //HtmlNode trFooter = document.GetElementbyId("trFooter");

                //    HtmlNode Contents = document.GetElementbyId("ulContent");
                //    //HtmlNode nodeTOCTitle = document.DocumentNode.SelectSingleNode("//h1[@class='tblOfContent']");
                //    //nodeTOCTitle.Attributes["style"].Value = "display:block";
                //    if (Contents != null)
                //    {
                //        string ulContent = Contents.InnerHtml;

                //        tableContents = tableContents + ulContent;
                //        Contents.InnerHtml = tableContents;
                //    }
                //    if (!ispreview)
                //    {

                //        //  EC09WebApp.Classes.Common.RepordTemplate objRptTmplt = new Classes.Common.RepordTemplate();//todo
                //        // objRptTmplt = EC09WebApp.Classes.Common.getHeaderFooterFromHTML(document.DocumentNode.OuterHtml);//todo
                //        //  obj.header = objRptTmplt.header;//todo
                //        //  obj.footer = objRptTmplt.footer;//todo
                //        //  obj.content = objRptTmplt.content;//todo
                //    }
                //    else
                //    {
                //        obj.content = document.DocumentNode.OuterHtml;
                //    }


                //    return obj;
                //}
                return null;
            }
            catch (Exception ex)
            {
                //   genExceptionLog objException = new genExceptionLog();//todo
                //  objException.LogExceptiondata("Report builder exception" + entityNumber, 0, ex.Message);//todo
                return null;
            }
        }

        //public static WindowsImpersonationContext ImpersonateOnFileServer(string Username, string Domain, string Password) //todo
        //{
        //    return EC09.Common.WindowsImpersonation.ImpersonateFileServer(Username, Password);//todo
        //}

        public static string GetXMLData(string type, string key)
        {
            var client = new RestClient(Constant.ECurl);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
            request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"type\"\r\n\r\n" + type + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"key\"\r\n\r\n" + key + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public static string getExceptionTypes(string entityNumber)
        {
            try
            {
                string xmldata = GetXMLData("ExceptionData", "516f30f0-b587-4381-abf6-8b91f06efa59-1");
                DataSet DS = new DataSet();
                DS.ReadXml(new XmlTextReader(new StringReader(xmldata)));
                if (DS != null)
                {
                    DataTable dtRBexcType = DS.Tables[0];
                    DataTable dtPreDefinedSolution = DS.Tables[1];
                    List<Selectlist> PreDefinedSolutions = new List<Selectlist>();
                    //Database.DA objDA = new Database.DA();
                    //DataSet dtPreDefinedSolution = objDA.GetDataSet(string.Format("select spp.Code as CodeId, sps.code as ShortDescription, sps.name as LongDescription from sales_predefine_problem as spp inner join sales_predefine_solution as sps on sps.code = spp.code where spp.is_active = true")); 
                    List<Selectlist> listRBExcType = new List<Selectlist>();

                    foreach (DataRow row in dtPreDefinedSolution.Rows)
                    {
                        Selectlist objPreDefinedSolution = new Selectlist()
                        {
                            codeid = row["Code"].ToString(),
                            code = row["ShortDescription"].ToString(),
                            detail= row["LongDescription"].ToString()
                        };
                        PreDefinedSolutions.Add(objPreDefinedSolution);
                    }

                    foreach (DataRow row in dtRBexcType.Rows)//todo
                    {
                        Selectlist objPreDefinedSolution = new Selectlist()
                        {
                            codeid = row["Code"].ToString(),
                            code = row["UsageDescription"].ToString()
                        };
                        listRBExcType.Add(objPreDefinedSolution);
                    }
                    return JsonConvert.SerializeObject(new { listRBExcType = listRBExcType, PreDefinedSolutions = PreDefinedSolutions });
                }
                return "";
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        //DataSet ds = objAPI.GetData(sql, Keydata);//todo

        //    DataTable dtRBexcType = ds.Tables[0];//todo
        //    DataTable dtPreDefinedSolution = ds.Tables[1];//todo
        //    Dictionary<string, string> listRBExcType = new Dictionary<string, string>();
        //    List<PreDefinedSolution> PreDefinedSolutions = new List<PreDefinedSolution>();




        public string getPreviewForHistory(long rb_id, string backup_id)
        {
            try
            {
                string sql = string.Format("exec getFilePathFromRBId {0}", rb_id);
                //   EC09WebApp.API.gd objAPI = new EC09WebApp.API.gd(); //todo
                //    DataSet ds = objAPI.GetData(sql, Keydata); //todo
                string dataPath = "";
                //   dataPath = ds.Tables[0].Rows[0]["rb_data_path"].ToString();//todo
                //   string entityNumber = ds.Tables[0].Rows[0]["entity_number"].ToString();//todo
                //   string entityType = ds.Tables[0].Rows[0]["entity_type"].ToString();//todo
                //    string backupFileName = "backup_" + entityNumber + "_" + entityType + "_" + backup_id + ".html";//todo
                //    dataPath = dataPath.Replace(entityNumber + "_" + entityType + ".html", backupFileName);//todo


                string html = "";
                if (IsImpersonation())
                {
                    html = File.ReadAllText(dataPath);
                }
                return html;
            }
            catch (Exception ex)
            {
                //      genExceptionLog objException = new genExceptionLog(); //todo
                //   objException.LogExceptiondata("Report builder exception", 0, ex.Message);//todo
                return null;
            }
        }
        public string CheckSIFileUpload(string entityNumber)
        {
            try
            {
                string sql = string.Format("exec Check_SIFileExist '{0}'", entityNumber);
                //   EC09WebApp.API.gd objAPI = new EC09WebApp.API.gd();//todo
                //    DataSet ds = objAPI.GetData(sql, Keydata); //todo
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) //todo
                //{
                //    if (Convert.ToBoolean(ds.Tables[0].Rows[0]["isSIFile"].ToString()) == true) //todo
                //    {
                //        return "true"; //todo
                //    }
                //}
                return "false";
            }
            catch (Exception)
            {
                //   genExceptionLog objException = new genExceptionLog(); //todo
                //  objException.LogExceptiondata("Report builder exception-CheckSIFileUpload", 0, ex.Message);//todo
                return "false";
            }
        }

        public class TopicList
        {
            public string name { get; set; }
            public string id { get; set; }
            public string details { get; set; }
        }

        public class Selectlist
        {
            public string codeid { get; set; }
            public string code { get; set; }
            public string detail { get; set; }
        }

        public class ReportDetail
        {
            public string entityNumber;
            public long userid;
            public string userName;
            public string reportType;
            public string reportTitle;
            public string orderStatus;
            public string reportData;
            public bool hasActiveTopic;
            public string rdtopicid;
        }

        public class RepordTemplate
        {
            public string content { get; set; }
            public string header { get; set; }
            public string footer { get; set; }

        }
        public class CommentDetail
        {
            public string commentorderId;
            public int count;

        }
    }
}