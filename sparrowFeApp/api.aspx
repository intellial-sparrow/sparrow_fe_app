﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="RestSharp" %>
<%@ Import Namespace="Newtonsoft.Json.Linq" %>


<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["type"] != null && Request["type"] == "token")
        {
            getToken();
        }
        else if (Request["type"] != null && Request["type"] == "downloaddata" && Request.RequestType == "POST" && Request["key"]=="PV6irM5xqh8FAassqvXDQpWRnvt7c2yK")
        {
            Download();
        }
    }

    public void getToken()
    {
        long ticks = DateTime.UtcNow.Ticks;
        string tokenNum = ""; //username = ispl123 password = ispl123
        string msg = "";
        byte isTokenGenrated = 0;

        if (sparrowFeApp.core.Constant.Username.Equals(Request["username"]) && sparrowFeApp.core.Constant.Password.Equals(Request["password"]))
        {
            tokenNum = sparrowFeApp.visdownload.EncryptLast(ticks.ToString());
            isTokenGenrated = 1;
        }
        else
        {
            msg = "Username or Password is Invalid";
        }
        string tokenJson = "";
        var token = new { token = tokenNum, message = msg, code = isTokenGenrated };
        tokenJson = Newtonsoft.Json.JsonConvert.SerializeObject(token);

        Response.Clear();
        Response.ContentType = "application/json; charset=utf-8";
        Response.Write(tokenJson);
        Response.End();
    }

    public void Download()
    {
        string data = Request["data"].ToString().Replace("\\", "\\\\");
        var jsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        string Key = sparrowFeApp.core.Constant.FeAppApiKey;
        string requestURL = ConfigurationManager.AppSettings["FEService"].ToString();
        var client = new RestClient(requestURL);
        var request = new RestRequest(Method.POST);
        request.AddHeader("Accept", "*/*");
        request.AddHeader("cache-control", "no-cache");
        string postdata = "";
        foreach (var keyvalue in jsonData)
        {
            postdata = postdata + keyvalue.Key + "=" + Server.UrlEncode(keyvalue.Value)  + "&";
        }
        postdata = postdata.TrimEnd('&');
        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        request.AddParameter("undefined", postdata, ParameterType.RequestBody);
        IRestResponse response = client.Execute(request);
        Response.BinaryWrite(((RestSharp.RestResponseBase)response).RawBytes);
        Response.End();
    }
</script>

