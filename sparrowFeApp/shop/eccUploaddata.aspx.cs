﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using System.Configuration;



namespace sparrowFeApp.shop
{
    public partial class eccUploaddata : core.BasePage
    {
        const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
        ecgdService.gdSoapClient objgd = new ecgdService.gdSoapClient();

        protected void Page_Load(object sender, EventArgs e)
        {
            ResourceId = 66;
            core.ConfiguratorCl conf = (core.ConfiguratorCl)Session["configurator"];
            string err = "";
            try
            {
                if (!IsPostBack)
                {
                    if (conf.IsFromECC == null)
                    {
                        Session["configurator"] = null;
                    }
                    err += "page load |";
                    string DeliveryAddressId = null, InvoiceAddressId = null, LanguageCode = null;
                    long LanguageId = 493, HandlingCompanyID = 0, RootCompanyID = 0;
                    //objException.LogExceptiondata(conf.type, 355, "conf.type");
                    //objException.LogExceptiondata(Convert.ToString(Request.QueryString["type"]), 355, Convert.ToString(conf.custid));

                    string strsql = string.Format("select * from genSearchCustomer where UserId ={0} and CustomerID={1}", conf.ECCCustUserId, conf.custid);
                    DataSet objCustomerDetail = objgd.GetData(strsql, Keydata);
                    err += "getting cust data |";
                    if (objCustomerDetail != null && objCustomerDetail.Tables[0].Rows.Count > 0)
                    {
                        DeliveryAddressId = Convert.ToString(objCustomerDetail.Tables[0].Rows[0]["DeliveryAddressId"]);
                        InvoiceAddressId = Convert.ToString(objCustomerDetail.Tables[0].Rows[0]["InvoiceAddressId"]);
                        LanguageId = Convert.ToInt64(objCustomerDetail.Tables[0].Rows[0]["LanguageId"]);
                        HandlingCompanyID = Convert.ToInt64(objCustomerDetail.Tables[0].Rows[0]["HandlingCompanyID"]);
                        RootCompanyID = Convert.ToInt64(objCustomerDetail.Tables[0].Rows[0]["RootCompanyID"]);
                    }
                    //objException.LogExceptiondata("done2", 355, Convert.ToString(conf.custid));
                    //Logic for set delivery date in page load...
                    //long ExceptionId = 0;
                    //long LandId = 0;
                    //DataSet ds_lang = new DataSet();
                    //string Sql_invoiceId = "SELECT  LanguageID FROM genSearchCustomer with(nolock) where CustomerID=" + conf.custid + "  AND IsActive=1 ";
                    //DataSet DS_customer = objgd.GetData(Sql_invoiceId, Keydata);
                    //if (DS_customer != null)
                    //{
                    //    if (DS_customer.Tables[0].Rows.Count > 0)
                    //    {
                    //        LandId = Convert.ToInt32(DS_customer.Tables[0].Rows[0]["LanguageID"]);
                    //        ds_lang = objgd.GetData("select Code as Code from admCodes where  CodeId =" + LandId + " and IsActive=1 ", Keydata);
                    //    }
                    //}
                    //long OrderId = 0;
                    //string CustLangCode = "en";
                    //if (ds_lang.Tables[0].Rows.Count > 0)
                    //{
                    //    CustLangCode = Convert.ToString(ds_lang.Tables[0].Rows[0]["Code"]);
                    //}
                    err += "check url and set value |";
                    if (Request["BoardDeliveryTerm"] != null)
                    {
                        string BoardDeliveryTerm = Convert.ToString(Request["BoardDeliveryTerm"]);
                        //string query1 = string.Format("exec ops_getdeliverydate_Visxml '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'", conf.custid, OrderId, BoardDeliveryTerm,
                        //                                                               HandlingCompanyID, RootCompanyID, ExceptionId, Convert.ToString(Request.QueryString["type"]), CustLangCode);
                        //DataSet ds_date = objgd.GetData(query1, Keydata);
                        //if (ds_date.Tables[0].Rows.Count != 0)
                        //{
                        WUCuploaddata.Deliveryterm = Convert.ToInt32(BoardDeliveryTerm);
                        //WUCuploaddata.Deliverydate = Convert.ToDateTime(ds_date.Tables[0].Rows[0]["DeliveryDate"]).ToString("dd MMM yyyy");
                        //}
                    }
                    string BoardDeliveryDate = DateTime.Now.ToString("dd MMM yyyy");
                    if (Request["BoardDeliveryDate"] != null)
                    {
                        try
                        {
                            DateTime dttime = DateTime.Parse(Convertdateformate(Request["BoardDeliveryDate"].ToString()));
                            BoardDeliveryDate = dttime.ToString("dd MMM yyyy");
                            WUCuploaddata.Deliverydate = BoardDeliveryDate;
                        }
                        catch (Exception)
                        {
                            WUCuploaddata.Deliverydate = BoardDeliveryDate;
                        }
                    }

                    //if (dtGetXmlString != null && dtGetXmlString.Rows.Count > 0)
                    //{
                    //    xmlString = dtGetXmlString.Rows[0]["XMLString"].ToString();
                    //}




                    WUCuploaddata.UserId = conf.ECCCustUserId;
                    WUCuploaddata.LanguageId = Convert.ToInt32(LanguageId);
                    WUCuploaddata.HCompnyID = HandlingCompanyID;
                    WUCuploaddata.RCompnyID = RootCompanyID;

                    LanguageCode = "en";
                    WUCuploaddata.LanguageCode = LanguageCode;

                    WUCuploaddata.DeliveryAddressId = Convert.ToInt64(DeliveryAddressId);
                    WUCuploaddata.InvoiceAddressId = Convert.ToInt64(InvoiceAddressId);
                    WUCuploaddata.Company_id = Convert.ToInt64(conf.custid);
                    WUCuploaddata.eccUserId = Convert.ToInt64(conf.ECCUserId);
                    err += "check url and set value done |";
                    if (Convert.ToString(Request.QueryString["type"]).ToLower() == "newOrder".ToLower())
                    {
                        err += "type-newOrder |";
                        WUCuploaddata.SessionId = Request["sessionid"];
                        WUCuploaddata.UserId = conf.ECCCustUserId;
                        WUCuploaddata.Company_id = Convert.ToInt64(conf.custid);
                        WUCuploaddata.serviceName = conf.service;
                        WUCuploaddata.action = "uploaddata";
                        WUCuploaddata.Status = conf.status;
                        WUCuploaddata.Type = "newOrder";
                        WUCuploaddata.EntityNo = "Temp";
                        WUCuploaddata.PerentBasketId = conf.PerentBasketId;
                        WUCuploaddata.ddlDeliveryAddress = true;
                    }

                    else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "inquiry".ToLower())
                    {
                        err += "type-inquiry |";
                        WUCuploaddata.SessionId = Request["sessionid"];
                        err += "SessionId-inquiry =" + Convert.ToString(WUCuploaddata.SessionId) + "|";
                        WUCuploaddata.UserId = conf.ECCCustUserId;
                        err += "WUCuploaddata.UserId =" + Convert.ToString(WUCuploaddata.UserId) + "|";
                        WUCuploaddata.Company_id = Convert.ToInt64(conf.custid);
                        err += "WUCuploaddata.Company_id =" + Convert.ToString(WUCuploaddata.Company_id) + "|";
                        WUCuploaddata.serviceName = conf.service;
                        err += "WUCuploaddata.serviceName =" + Convert.ToString(WUCuploaddata.serviceName) + "|";
                        WUCuploaddata.action = "uploaddata";
                        WUCuploaddata.Status = conf.status;
                        err += "WUCuploaddata.Status =" + Convert.ToString(WUCuploaddata.Status) + "|";
                        WUCuploaddata.Type = "inquiry";
                        WUCuploaddata.EntityNo = "Temp";
                        WUCuploaddata.PerentBasketId = conf.PerentBasketId;
                        err += "WUCuploaddata.Status =" + Convert.ToString(WUCuploaddata.PerentBasketId) + "|";
                        WUCuploaddata.ddlDeliveryAddress = true;
                        if (Convert.ToString(conf.service).Trim().ToLower() == "stencilservice")
                        {
                            WUCuploaddata.IsVisibleLbtnAdd01 = false;
                            WUCuploaddata.IsStencilOnly = true;
                        }
                        if (Request.QueryString["InqOrder"] != null && Request.QueryString["InqOrder"] != "")
                        {
                            err += "InqOrder =" + Convert.ToString(Request.QueryString["InqOrder"]) + "|";
                            WUCuploaddata.InqOrderNumber = Request.QueryString["InqOrder"].ToString();
                            if (Request.QueryString["InqOrder"].ToString().ToUpper().StartsWith("B"))
                            {
                                string qury = String.Format("SELECT * from BasketItems where BasketNo = '{0}'", Convert.ToString(Request.QueryString["InqOrder"]));
                                DataSet ds = objgd.GetData(qury, Keydata);
                                err += " get basket data |";
                                WUCuploaddata.PCBName = Convert.ToString(ds.Tables[0].Rows[0]["PCB_Name"]);
                                WUCuploaddata.ArticalRef = Convert.ToString(ds.Tables[0].Rows[0]["Article_Reference"]);
                                WUCuploaddata.PurchaseRef = Convert.ToString(ds.Tables[0].Rows[0]["Purchase_Reference"]);
                                WUCuploaddata.ProjectRef = Convert.ToString(ds.Tables[0].Rows[0]["Project_Reference"]);
                            }
                            else
                            {
                                err += " get order data |";
                                string qury = String.Format("SELECT PCBName,PurchaseRef,ArticleRef,ProjectRef from genSearchOrders where OrderNumber = '{0}' ", Convert.ToString(Request.QueryString["InqOrder"]));
                                DataSet ds = objgd.GetData(qury, Keydata);
                                WUCuploaddata.PCBName = Convert.ToString(ds.Tables[0].Rows[0][0]);
                                WUCuploaddata.ArticalRef = Convert.ToString(ds.Tables[0].Rows[0][2]);
                                WUCuploaddata.PurchaseRef = Convert.ToString(ds.Tables[0].Rows[0][1]);
                                WUCuploaddata.ProjectRef = Convert.ToString(ds.Tables[0].Rows[0][3]);
                            }
                        }
                        else
                        {
                            err += " NO InqOrderNumber |";
                            WUCuploaddata.InqOrderNumber = "";
                        }

                        //bool StencilOnlyInq = false;
                        //bool.TryParse(Convert.ToString(conf.IsStencilOnly), out StencilOnlyInq);
                        //WUCuploaddata.IsStencilOnly = StencilOnlyInq;

                        if (conf.ArticalId != 0)
                            WUCuploaddata.parentarticalId = conf.ArticalId;
                    }

                    else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "newOffer".ToLower())
                    {
                        err += " type - newOffer|";
                        WUCuploaddata.SessionId = Request["sessionid"];
                        WUCuploaddata.UserId = conf.ECCCustUserId;
                        WUCuploaddata.Company_id = Convert.ToInt64(conf.custid);
                        WUCuploaddata.serviceName = conf.service;
                        WUCuploaddata.action = "uploaddata";
                        WUCuploaddata.Status = conf.status;
                        WUCuploaddata.Type = "newOffer";
                        WUCuploaddata.EntityNo = "Temp";
                        WUCuploaddata.PerentBasketId = conf.PerentBasketId;
                    }
                    //else if (Convert.ToString(Request.QueryString["type"]) == "modifyOrder")
                    //{
                    //    WUCuploaddata.SessionId = Request["sessionid"];
                    //    WUCuploaddata.UserId = conf.ECCCustUserId;
                    //    WUCuploaddata.Company_id = Convert.ToInt64(conf.custid);
                    //    WUCuploaddata.serviceName = conf.service;
                    //    WUCuploaddata.action = "uploaddata";
                    //    WUCuploaddata.Status = conf.status;
                    //    WUCuploaddata.Type = "modifyOrder";
                    //    WUCuploaddata.EntityNo = "Temp";
                    //    WUCuploaddata.PerentBasketId = conf.PerentBasketId;

                    //}
                    else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "modifyINQ".ToLower())
                    {
                        err += " type - modifyINQ|";
                        string BasketNo = "";
                        long orderdeliveryAddressid = 0;
                        WUCuploaddata.SessionId = Convert.ToString(Request.QueryString["sessionid"]);
                        WUCuploaddata.Type = "modifyINQ";
                        if (Request.QueryString["orderNum"] != null)
                        {
                            string qury = String.Format("select PCB_Name,Purchase_Reference,Article_Reference,Project_Reference,isnull(Is_Stencil_Fixed,0),Estimate_Delivery_Date,BasketNo from BasketItems where Basket_Id = '{0}'", Convert.ToString(Request.QueryString["orderNum"]));
                            DataSet ds = objgd.GetData(qury, Keydata);
                            WUCuploaddata.FilteredPcbName = false;
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                BasketNo = Convert.ToString(ds.Tables[0].Rows[0]["BasketNo"]);
                                WUCuploaddata.PCBName = Convert.ToString(ds.Tables[0].Rows[0][0]);
                                WUCuploaddata.ArticalRef = Convert.ToString(ds.Tables[0].Rows[0][2]);
                                WUCuploaddata.PurchaseRef = Convert.ToString(ds.Tables[0].Rows[0][1]);
                                WUCuploaddata.ProjectRef = Convert.ToString(ds.Tables[0].Rows[0][3]);
                                WUCuploaddata.PerentBasketId = Convert.ToString(Request.QueryString["orderNum"]);
                                WUCuploaddata.IsStencilFixed = Convert.ToBoolean(ds.Tables[0].Rows[0][4]);

                                string sql_inq = "SELECT DeliveryAddressId from genInqOffers with(nolock) where InquiryNo='" + BasketNo + "'";
                                DataSet ds_ing = objgd.GetData(sql_inq, Keydata);
                                if (ds_ing != null && ds_ing.Tables[0].Rows.Count > 0)
                                {
                                    long.TryParse(Convert.ToString(ds_ing.Tables[0].Rows[0]["DeliveryAddressId"]), out orderdeliveryAddressid);
                                }

                            }
                            if (conf.service != null)
                            {
                                if (Convert.ToString(conf.service).Trim().ToLower() == "stencilservice")
                                {
                                    WUCuploaddata.IsVisibleLbtnAdd01 = false;
                                }
                            }
                            WUCuploaddata.ddlDeliveryAddress = true;
                            WUCuploaddata.OrderDeliveryAddressId = orderdeliveryAddressid;
                            if (conf.ArticalId != 0)
                                WUCuploaddata.parentarticalId = conf.ArticalId;
                        }
                        WUCuploaddata.action = "uploaddata";
                        WUCuploaddata.Status = "BasketInquiry";
                    }
                    else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "modifyOrder".ToLower())
                    {
                        err += " type - modifyOrder|";
                        long OrderDeliveryAddress = 0;
                        long OrderInvoiceAddress = 0;
                        WUCuploaddata.SessionId = Convert.ToString(Request.QueryString["sessionid"]);
                        WUCuploaddata.Type = "modifyOrder";
                        if (Request.QueryString["orderNum"] != null)
                        {
                            string qury = String.Format("SELECT YourRefNo as PurchaseRef,OrderRef3 as ArticleRef,OrderRef as ProjectRef,PCBName,DeliveryAddressId,isnull(IsStencilFixed,0) as  IsStencilFixed,InvoiceAddressId  from genOrders with(NOLOCK) where OrderNumber= '{0}' ", Convert.ToString(Request.QueryString["orderNum"]));
                            DataSet ds = objgd.GetData(qury, Keydata);
                            WUCuploaddata.FilteredPcbName = false;
                            bool IsStencilFixed = false;
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                WUCuploaddata.PCBName = Convert.ToString(ds.Tables[0].Rows[0]["PCBName"]);
                                WUCuploaddata.ArticalRef = Convert.ToString(ds.Tables[0].Rows[0]["ArticleRef"]);
                                WUCuploaddata.PurchaseRef = Convert.ToString(ds.Tables[0].Rows[0]["PurchaseRef"]);
                                WUCuploaddata.ProjectRef = Convert.ToString(ds.Tables[0].Rows[0]["ProjectRef"]);
                                long.TryParse(Convert.ToString(ds.Tables[0].Rows[0]["DeliveryAddressId"]), out OrderDeliveryAddress);
                                long.TryParse(Convert.ToString(ds.Tables[0].Rows[0]["InvoiceAddressId"]), out OrderInvoiceAddress);
                                bool.TryParse(Convert.ToString(ds.Tables[0].Rows[0]["IsStencilFixed"]), out IsStencilFixed);
                            }

                            WUCuploaddata.PerentBasketId = Convert.ToString(Request.QueryString["orderNum"]);

                            //if (ds_basket != null && ds_basket.Tables[0].Rows.Count != 0)
                            //{
                            WUCuploaddata.IsStencilFixed = IsStencilFixed;
                            //}
                            //else
                            //{ WUCuploaddata.IsStencilFixed = false; }

                            //if (Convert.ToString(ds_basket.Tables[0].Rows[0]["Estimate_Delivery_Date"]) != "")
                            //{
                            //    WUCuploaddata.Deliverydate = Convert.ToDateTime(ds_basket.Tables[0].Rows[0]["Estimate_Delivery_Date"]).ToString("dd MMM yyyy");
                            //}
                        }
                        WUCuploaddata.ddlDeliveryAddress = true;
                        WUCuploaddata.OrderDeliveryAddressId = OrderDeliveryAddress;
                        WUCuploaddata.OrderInvoiceAddressId = OrderInvoiceAddress;
                        WUCuploaddata.Status = "BasketInquiry";
                    }
                    else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "PPAtoSI".ToLower() || Convert.ToString(Request.QueryString["type"]).ToLower() == "releasePPA".ToLower() || Convert.ToString(Request.QueryString["type"]).ToLower() == "releaseConfirmPPA".ToLower())
                    {
                        err += " type - releasePPA|";
                        long OrderDeliveryAddress = 0;
                        long OrderInvoiceAddress = 0;
                        WUCuploaddata.SessionId = Convert.ToString(Request.QueryString["sessionid"]);
                        if (Convert.ToString(Request.QueryString["type"]).ToLower() == "releasePPA".ToLower())
                        {
                            WUCuploaddata.Type = "releasePPA";
                            WUCuploaddata.action = "processPPA";
                        }
                        else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "PPAtoSI".ToLower())
                        {
                            WUCuploaddata.Type = "PPAtoSI";
                            WUCuploaddata.action = "processPPA";
                        }
                        else
                        {
                            WUCuploaddata.Type = "releaseConfirmPPA";
                            WUCuploaddata.action = "processPPA";
                        }
                        if (Request.QueryString["orderNum"] != null)
                        {
                            string qury = String.Format("SELECT YourRefNo as PurchaseRef,OrderRef3 as ArticleRef,OrderRef as ProjectRef,PCBName,DeliveryAddressId,isnull(IsStencilFixed,0) as  IsStencilFixed,InvoiceAddressId  from genOrders with(NOLOCK) where OrderNumber= '{0}' ", Convert.ToString(Request.QueryString["orderNum"]));
                            DataSet ds = objgd.GetData(qury, Keydata);
                            WUCuploaddata.FilteredPcbName = false;
                            bool IsStencilFixed = false;
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                WUCuploaddata.PCBName = Convert.ToString(ds.Tables[0].Rows[0]["PCBName"]);
                                WUCuploaddata.ArticalRef = Convert.ToString(ds.Tables[0].Rows[0]["ArticleRef"]);
                                WUCuploaddata.PurchaseRef = Convert.ToString(ds.Tables[0].Rows[0]["PurchaseRef"]);
                                WUCuploaddata.ProjectRef = Convert.ToString(ds.Tables[0].Rows[0]["ProjectRef"]);
                                long.TryParse(Convert.ToString(ds.Tables[0].Rows[0]["DeliveryAddressId"]), out OrderDeliveryAddress);
                                long.TryParse(Convert.ToString(ds.Tables[0].Rows[0]["InvoiceAddressId"]), out OrderInvoiceAddress);
                                bool.TryParse(Convert.ToString(ds.Tables[0].Rows[0]["IsStencilFixed"]), out IsStencilFixed);
                            }
                            WUCuploaddata.PerentBasketId = Convert.ToString(Request.QueryString["orderNum"]);
                            WUCuploaddata.IsStencilFixed = IsStencilFixed;
                        }
                        WUCuploaddata.ddlDeliveryAddress = false;
                        WUCuploaddata.OrderDeliveryAddressId = OrderDeliveryAddress;
                        WUCuploaddata.OrderInvoiceAddressId = OrderInvoiceAddress;

                        WUCuploaddata.Status = conf.status;
                    }
                    else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "repeatOrder".ToLower())
                    {
                        err += " type - repeatOrder|";
                        long OrderDeliveryAddress = 0;
                        WUCuploaddata.SessionId = Convert.ToString(Request.QueryString["sessionid"]);
                        WUCuploaddata.Type = "repeatOrder";
                        if (Request.QueryString["orderNum"] != null)
                        {
                            string qury = String.Format("SELECT PCBName,PurchaseRef,ArticleRef,ProjectRef,DeliveryAddressId from genSearchOrders where OrderNumber = '{0}' ", Convert.ToString(Request.QueryString["orderNum"]));
                            //string query_basket = String.Format("select Is_Stencil_Fixed,Estimate_Delivery_Date from BasketItems where Order_Number= '{0}' ", Convert.ToString(Request.QueryString["orderNum"]));
                            string query_basket = String.Format("EXEC usp_GetOrderStencilFix '{0}'", Convert.ToString(Request.QueryString["orderNum"]));
                            DataSet ds = objgd.GetData(qury, Keydata);
                            DataSet ds_basket = objgd.GetData(query_basket, Keydata);
                            WUCuploaddata.FilteredPcbName = false;
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                WUCuploaddata.PCBName = Convert.ToString(ds.Tables[0].Rows[0][0]);
                                WUCuploaddata.ArticalRef = Convert.ToString(ds.Tables[0].Rows[0][2]);
                                WUCuploaddata.PurchaseRef = Convert.ToString(ds.Tables[0].Rows[0][1]);
                                WUCuploaddata.ProjectRef = Convert.ToString(ds.Tables[0].Rows[0][3]);
                                long.TryParse(Convert.ToString(ds.Tables[0].Rows[0]["DeliveryAddressId"]), out OrderDeliveryAddress);
                            }
                            WUCuploaddata.PerentBasketId = Convert.ToString(Request.QueryString["orderNum"]);
                            WUCuploaddata.UserId = Convert.ToInt32(conf.ECCCustUserId);
                            if (ds_basket.Tables[0].Rows.Count != 0)
                            {
                                WUCuploaddata.IsStencilFixed = Convert.ToBoolean(ds_basket.Tables[0].Rows[0][0]);
                            }
                            else
                            {
                                WUCuploaddata.IsStencilFixed = Convert.ToBoolean(false);
                            }
                            //if (Convert.ToString(ds_basket.Tables[0].Rows[0][1]) != "")
                            //{
                            //    WUCuploaddata.Deliverydate = Convert.ToDateTime(ds_basket.Tables[0].Rows[0][1]).ToString("dd MMM yyyy");
                            //}

                        }
                        WUCuploaddata.ddlDeliveryAddress = true;

                        if (OrderDeliveryAddress != 0)
                        {
                            string strCheckDelAdd = string.Format("exec ECC_CheckOrderDeliveryAddress {0},{1}", OrderDeliveryAddress, Convert.ToInt64(conf.custid));
                            DataSet dsGetDelAdd = objgd.GetData(strCheckDelAdd, Keydata);
                            if (dsGetDelAdd != null && dsGetDelAdd.Tables.Count > 0)
                            {
                                if (dsGetDelAdd.Tables[0].Rows.Count > 0)
                                {
                                    long.TryParse(dsGetDelAdd.Tables[0].Rows[0]["ActiveDeliveryAddressId"].ToString(), out OrderDeliveryAddress);
                                }
                            }
                        }
                        WUCuploaddata.OrderDeliveryAddressId = OrderDeliveryAddress;
                        //WUCuploaddata.action = "uploaddata";
                        WUCuploaddata.Status = conf.status;
                    }
                    else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "newBasket".ToLower())
                    {
                        err += " type - newBasket|";
                        WUCuploaddata.SessionId = Request["sessionid"];
                        WUCuploaddata.UserId = conf.ECCCustUserId;
                        WUCuploaddata.Company_id = Convert.ToInt64(conf.custid);
                        WUCuploaddata.serviceName = conf.service;
                        WUCuploaddata.action = "uploaddata";
                        WUCuploaddata.Status = conf.status;
                        WUCuploaddata.Type = "newOrder";
                        WUCuploaddata.EntityNo = "Temp";
                        WUCuploaddata.PerentBasketId = conf.PerentBasketId;
                        WUCuploaddata.ddlDeliveryAddress = true;

                        bool StencilOnlyInq = false;
                        bool.TryParse(Convert.ToString(conf.IsStencilOnly), out StencilOnlyInq);
                        WUCuploaddata.IsStencilOnly = StencilOnlyInq;

                        if (Request.QueryString["orderNum"] != null)
                        {
                            string qury = String.Format("SELECT PCBName,PurchaseRef,ArticleRef,ProjectRef from genSearchOrders where OrderNumber = '{0}' ", Convert.ToString(conf.PerentBasketId));
                            string query_basket = String.Format("EXEC usp_GetOrderStencilFix '{0}'", Convert.ToString(Request.QueryString["orderNum"]));
                            DataSet ds = objgd.GetData(qury, Keydata);
                            DataSet ds_basket = objgd.GetData(query_basket, Keydata);
                            if (ds != null && ds.Tables[0].Rows.Count > 0)//
                            {
                                WUCuploaddata.PCBName = Convert.ToString(ds.Tables[0].Rows[0][0]);
                                WUCuploaddata.ArticalRef = Convert.ToString(ds.Tables[0].Rows[0][2]);
                                WUCuploaddata.PurchaseRef = Convert.ToString(ds.Tables[0].Rows[0][1]);
                                WUCuploaddata.ProjectRef = Convert.ToString(ds.Tables[0].Rows[0][3]);
                                string dataStatus = Request.QueryString["dataStatus"] ?? "0";
                                WUCuploaddata.dataStatus = dataStatus;
                            }
                            // WUCuploaddata.UserId = Convert.ToInt32(conf.ECCCustUserId);

                            if (ds_basket.Tables[0].Rows.Count != 0)
                            {
                                WUCuploaddata.IsStencilFixed = Convert.ToBoolean(ds_basket.Tables[0].Rows[0][0]);
                            }
                            else
                            {
                                WUCuploaddata.IsStencilFixed = Convert.ToBoolean(false);
                            }
                        }

                    }
                    else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "save".ToLower())
                    {
                        err += " type - save|";
                        WUCuploaddata.SessionId = Convert.ToString(Request.QueryString["sessionid"]);
                        WUCuploaddata.Type = "save";

                        WUCuploaddata.ddlDeliveryAddress = false;
                        WUCuploaddata.action = "uploaddata";
                        WUCuploaddata.Status = conf.status;
                    }
                    else if (Convert.ToString(Request.QueryString["type"]).ToLower() == "modifyBasket".ToLower())
                    {
                        err += " type - modifyBasket|";
                        ////////////Comment Explog entry///////
                        //genExceptionLog objException = new genExceptionLog();
                        //objException.LogExceptiondata("ModifyBasketWUCECCUploaddata.ascx-EccUploaddat.aspx", Convert.ToInt64(ViewState["UserId"]),"sessionid=" + Convert.ToString(Request.QueryString["sessionid"]) + ",orderNum=" + Convert.ToString(Request.QueryString["orderNum"]) + ",ECCUserId=" + Convert.ToString(conf.ECCUserId) + ",ECCCustUserId=" + Convert.ToString(conf.ECCCustUserId) + ",type=" + Convert.ToString(Request["type"]) + ",custid=" + Convert.ToString(conf.custid) + ",frompage=" + Convert.ToString(conf.FromPage) + ",PanelECRegComp=" + Convert.ToString(Request["PanelECRegComp"]) + ",StencilTopQty=" + Convert.ToString(Request["StencilTopQty"]) + ",StencilBotQty=" + Convert.ToString(Request["StencilBotQty"]) + ",BoardDeliveryTerm=" + Convert.ToString(Request["BoardDeliveryTerm"]) + ",BoardDeliveryDate=" + Convert.ToString(Request["BoardDeliveryDate"]) + ",backUrl=" + Convert.ToString(Request.UrlReferrer));

                        WUCuploaddata.Visible = false;
                        errormsg.Visible = true;
                        lblerror.Text = "Error occurred.";
                        //WUCuploaddata.SessionId = Convert.ToString(Request.QueryString["sessionid"]);
                        //WUCuploaddata.Type = "modifyBasket";
                        //if (Request.QueryString["orderNum"] != null)
                        //{
                        //    string qury = String.Format("select PCB_Name,Purchase_Reference,Article_Reference,Project_Reference,Is_Stencil_Fixed ,(SELECT code FROM admCodes where CodeId = Status_Id) as status,Estimate_Delivery_Date from BasketItems where Basket_Id = '{0}'", Convert.ToString(Request.QueryString["orderNum"]));
                        //    DataSet ds = objgd.GetData(qury, Keydata);
                        //    WUCuploaddata.FilteredPcbName = false;
                        //    WUCuploaddata.PCBName = Convert.ToString(ds.Tables[0].Rows[0][0]);
                        //    WUCuploaddata.ArticalRef = Convert.ToString(ds.Tables[0].Rows[0][2]);
                        //    WUCuploaddata.PurchaseRef = Convert.ToString(ds.Tables[0].Rows[0][1]);
                        //    WUCuploaddata.ProjectRef = Convert.ToString(ds.Tables[0].Rows[0][3]);
                        //    WUCuploaddata.PerentBasketId = Convert.ToString(Request.QueryString["orderNum"]);
                        //    WUCuploaddata.IsStencilFixed = Convert.ToBoolean(ds.Tables[0].Rows[0][4]);
                        //    WUCuploaddata.Status = Convert.ToString(ds.Tables[0].Rows[0]["status"]);
                        //    //if (Convert.ToString(ds.Tables[0].Rows[0][6]) != "")
                        //    //{
                        //    //    WUCuploaddata.Deliverydate = Convert.ToDateTime(ds.Tables[0].Rows[0][6]).ToString("dd MMM yyyy");
                        //    //}
                        //}
                        //WUCuploaddata.action = "uploaddata";

                    }
                    else if (Convert.ToString(Request.QueryString["type"]) == "")
                    {
                        err += "No type |";
                        string qury = "";
                        DataSet ds = null;
                        if (conf.IsException == "true")
                        {
                            WUCuploaddata.ExceptionId = conf.ExceptionId;
                            WUCuploaddata.IsException = "true";
                            qury = String.Format("select  XMLString from i8response where Sessionid = '{0}'", Convert.ToString(Request.QueryString["sessionid"]));
                            ds = objgd.GetData(qury, Keydata);
                            WUCuploaddata.EntityNo = conf.PerentBasketId;

                            WUCuploaddata.XML = Convert.ToString(ds.Tables[0].Rows[0]["XMLString"]);

                        }


                        qury = String.Format("select PCBName,PurchaseRef,ArticleRef from genSearchOrders where OrderNumber = '{0}'", Convert.ToString(Request.QueryString["orderNum"]));
                        ds = objgd.GetData(qury, Keydata);

                        WUCuploaddata.PCBName = Convert.ToString(ds.Tables[0].Rows[0][0]);
                        WUCuploaddata.ArticalRef = Convert.ToString(ds.Tables[0].Rows[0][2]);
                        WUCuploaddata.PurchaseRef = Convert.ToString(ds.Tables[0].Rows[0][1]);


                        conf.sessionid = Convert.ToString(Request.QueryString["sessionid"]);
                        conf.type = "modifyOrder";
                        Session["configurator"] = conf;

                        WUCuploaddata.SessionId = conf.sessionid;
                        WUCuploaddata.UserId = Convert.ToInt32(conf.ECCCustUserId);
                        WUCuploaddata.Company_id = Convert.ToInt32(conf.custid);
                        WUCuploaddata.serviceName = conf.service;
                        WUCuploaddata.action = "uploaddata";
                        WUCuploaddata.Type = conf.type;
                        WUCuploaddata.EntityNo = "Temp";
                        if (conf.PerentBasketId != null)
                        {
                            WUCuploaddata.PerentBasketId = conf.PerentBasketId.Replace("STENCIL", "");
                        }
                    }
                }
                err += "Complete";
            }
            catch (Exception ex)
            {
                try
                {
                    errormsg.Visible = true;
                    WUCuploaddata.Visible = false;
                }
                catch { }
            }
        }
        private string Convertdateformate(string deldate)
        {
            try
            {
                string[] del_date = deldate.Split('/');

                return Convert.ToString(del_date[2] + "-" + del_date[1] + "-" + del_date[0]);
            }
            catch (Exception)
            {
                return DateTime.Now.ToString("yyyy-MM-dd");
            }
        }
    }
}