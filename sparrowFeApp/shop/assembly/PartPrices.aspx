﻿<%@ Page Language="C#" AutoEventWireup="true" Debug="true" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net"%>


<script runat="server">    

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["EDAPART"] != null && Session["EDAPART"].ToString().ToUpper() == "true".ToUpper())
        {

        }
        else
        {
          Response.Write("Invalid request");
          Response.End();
          Response.Clear();
        }
    }
    [WebMethod]
    public static string GetPartPrices(string mpn)
    {
        string json = "";
        try
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("mpn", mpn);
            json = sparrowFeApp.shop.assembly.Component.GetDataFromSparrow("get_part_prices", param);
        }
        catch (Exception ex)
        {
            json = "{\"error\": \"" + ex.Message + "\"}";
        }
        return json;
    }

    [WebMethod]
    public static string SavePrices(string data)
    {
        string json = "";
        try
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("data", data);
            json = sparrowFeApp.shop.assembly.Component.GetDataFromSparrow("insert_part_prices", param);
        }
        catch (Exception ex)
        {
            json = "{\"error\": \"" + ex.Message + "\"}";
        }
        return json;
    }
</script>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>    
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />    
    
    <link href="resources/flatpickr.min.css" rel="stylesheet" />

    <script src="resources/es6-promise.min.js"></script>    
    <script src="resources/axios.min.js"></script>    
    <script src="resources/flatpickr.js"></script>
    <!-- <script src="resources/vue.js"></script> -->
    <script src="resources/vue.min.js"></script>
    <style>
        body, table td, select, input, textarea, span, a span, li a span, keygen, select, button, input[type="text"], li, ul, a, a:visited, a:hover, div, table td div {
            font-family: Helvetica !important;
            font-size: 98%;
        }

        .edit-pricebreak {
            height: 15px;
        }

        .tbl-price-break tbody tr:hover .edit-pricebreak {
            /*display: inline-block;*/
            visibility: visible;
        }

        /*.tbl-price-break tbody tr {
            cursor: pointer;
        }*/

        table {
            border-collapse: collapse;
        }

        .tbl-price-break td, .tbl-price-break th {
            border-bottom: 1px solid #EEEEEE;
            padding: 5px;
        }

        .tbl-price-break th {
            text-align: left;
        }

        .tbl-price-break {
            margin-top: 20px;
        }

        .th-qty, .th-price {
            min-width: 90px;
        }

        .tbl-price-break input[type='text'] {
            width: 75px;
        }

        .edit-on input, .edit-on .break-curr {
            display: inline-block;
        }

        .edit-off input, .edit-off .break-curr {
            display: none;
        }

        .edit-on .break-val {
            display: none !important;
        }

        .edit-on .edit-pricebreak {
            visibility: hidden !important;
        }

        .edit-off .edit-pricebreak {
            visibility: hidden;
        }

        .add-price .edit-on {
            display: none;
        }

        .add-price .edit-off {
            display: block;
        }

        .rm-price {
            height: 10px;
            visibility: hidden;
        }

        .tbl-price-break tr:hover .rm-price {
            visibility: visible;
        }

        .supp-opt-on .supp-opt {
            display: inline-block;
        }

        .supp-opt-on .supp-opt-a {
            display: none;
        }

        .supp-opt-off .supp-opt {
            display: none;
        }

        #txtStock {
            width: 50px;
        }

        #header {
            position: fixed;
            top: 0px;
            left: 0px;
            width: 100%;
            background: #E3E3E3;
            padding: 10px;
        }

        #footer {
            position: fixed;
            bottom: 0px;
            left: 0px;
            width: 100%;
            background: #E3E3E3;
            padding: 8px;
            text-align: center;
        }

        .body-container {
            position: absolute;
            width: 99%;
            overflow: auto;
            top: 45px;
            bottom: 50px;
            color: #717171;
        }

        .container {
            width: 99%;
            overflow: auto;
        }

        .buttonOrange {
            color: #fff;
            background-color: #e8791c;
            border-color: #c75d05;
            padding: 3px 10px;
            border-radius: 3px;
            border-width: 1px;
            border-style: solid;
            font-size: 13px;
            cursor: pointer;
        }

        .head-mpn {
            font-weight: bold;
        }

        .supplier {
            padding-bottom: 10px;
            margin-bottom: 10px;
            display: inline-flex;
        }

        .supp-spn {
            font-weight: bold;
            font-size: 16px;
        }

        .add-price-break {
            margin-top: 10px;
        }

        .pb-container {
            margin-top: 15px;
            margin-right: 45px;
            display: inline-block;
        }

        a:link, a:visited {
            color: #2a9d00;
            text-decoration: none;
            outline: none;
        }

        .price-block {
            overflow-x: auto;
            white-space: nowrap;
        }
        /* .manual-block {
            border-top: dotted 1px #ccc;
        } */
        .label {
            font-weight: bold;
            display: inline-block;
            width: 60px;
        }

        input[type="text"], select {
            border: 1px solid #c3c3c3;
            padding: 3px;
            border-radius: 3px;
            background: #fff;
        }

        select {
            padding: 2px;
        }

        .expire-on {
            width: 125px;
        }

        .title {
            border-bottom: 1px #479347 solid;
            margin: 0;
            padding: 0.5em 0 0.5em;
            color: #479347;
        }

        .divider {
            display: inline-flex;
            width: 1px;
            border-left: 1px dotted #ccc;
            padding: 20px;
            margin-top: 10px;
        }

        .price-exp {
            color: red;
        }

        .pageloader {
            position: absolute;
            left: 45%;
            top: 50%;
            z-index: 20;
            background: #e8791c none repeat scroll 0 0;
            border-radius: 1px;
            color: #fff;
            font-size: 12px;
            padding: 5px 7px;
            position: fixed;
            z-index: 100000;
        }

        .hide {
            display: none;
        }

        .input-exp {
            border: none !important;
        }
    </style>
</head>

<body>

<div id="app">    
        <div id="header" class="add-price"             
            v-bind:class="[toggleSupplierSelection ? 'supp-opt-on' : 'supp-opt-off']">
            <div class="container">
                <div style="float:right">
                   <div>MPN: <span class="head-mpn">{{mfrPartNumber}}</span></div>
                </div>
                <div style="float:left" v-if="!isGenPart">
                    <button class="supp-opt-a buttonOrange" v-if="!isReadOnly" @click="viewSupplierSelection()"> Add new price</button>
                    <div class="supp-opt">
                        <select id="ddlSupplier" v-model="newSupplier">
                            <option value="0">--Select supplier--</option>
                            <option value="1">Arrow</option>
                            <option value="2">Avnet</option>
                            <option value="9">Conrad</option>
                            <option value="3">Digi-Key</option>
                            <option value="4">Element14</option>                
                            <option value="5">Mouser</option>
                            <option value="6">RS-Component</option>
                            <option value="7">Rutronik</option>                
                            <option value="10">TME</option>
                            <option value="8">Verical</option>            
                        </select>

                        <span>
                            Currency:
                            <select id="ddlCurrency" style="width:60px;" v-model="newCurrency">
                                <option value="EUR">EUR</option>
                                <option value="USD">USD</option>
                            </select>
                        </span>
                        <span>
                            SKU: <input id="txtSKU" type="text" v-model="newSKU"/>
                        </span>
                        <span>
                            Stock: <input id="txtStock" type="text" v-model="newStock"/>
                        </span>
                        <input type="button" class="buttonOrange" value="Add supplier"  @click="addSupplier()"/>
                    </div>            
            </div> 
            </div>                      
        </div>

        <div class="body-container container">            

            <div v-bind:class="[showLoading ? 'pageloader' : 'hide']">
                <div style="display: inline-block;">
                    <img style="vertical-align: middle;" src="/shop/images/Small_Loading.gif" />
                </div>
                <div style="display: inline-block; vertical-align: top;">
                    <span>{{waitMessage}}</span>
                </div>
            </div>

            <div v-if="showLoading == false && suppliers.length == 0" style="text-align:center;margin-top:10px;">              
                <div>
                    <span v-if="!isGenPart">Price is not available for {{mfrPartNumber}}</span>
                    <span v-if="isGenPart">Extra price cannot be added for generic part.</span>                    
                </div>                                
            </div>           
            
            <div class="price-block">        
                <div v-if="hasAPIPrice" ><h3 class="title">Supplier prices</h3></div>

                <div class="api-block">                    
                    <div v-for="supplier in suppliers" v-if="!supplier.manual && !supplier.removed" class="supplier">
                        <pricebreak  :supplier="supplier"></pricebreak>                                        
                        <!-- <div class="divider"></div> -->
                    </div>
                </div>                                        

                 <div class="manual-block" v-if="hasManualPrice">
                    <div><h3 class="title">Extra prices</h3></div>                    
                    <div v-for="supplier in suppliers" v-if="supplier.manual && !supplier.removed" class="supplier">
                        <pricebreak :supplier="supplier"></pricebreak>                                        
                        <!-- <div class="divider"></div> -->
                    </div>
                </div>   
            </div>
        </div>        

        <div id="footer">
            <a href="#" @click="closeWindow(false)">Cancel</a>&nbsp;&nbsp;
            <input type="button" value="Save & Close" class="buttonOrange" v-if="!isGenPart && !isReadOnly"  @click="savePrices()"/>
        </div>
    </div>    

<template id="pricebreak-template" style="display: none;">            
    <div style="display:inline-flex;">            
        <div class="pb-container" v-for="product in supplier.products">                           
            <div>
                <div>
                    <span class="supp-spn">{{supplier.supplier}} </span> 
                     <div style="display:inline-block;margin-left:10px;">
                          <img src="resources/edit.png" class="edit-pricebreak" @click="$parent.editStockSKU(product)" v-if="supplier.manual && !$parent.isReadOnly" style="cursor:pointer;height:12px;"/>
                          <img src="resources/close-button.png" @click="$parent.removeSupplier(supplier)" style="margin-left:5px;height:10px;cursor:pointer;" v-if="supplier.manual && !$parent.isReadOnly"/> 
                     </div>
                    <span>
                        <a href="javascript:void(0)" @click="$parent.copySupplierToEdit(supplier,product)"  v-if="!supplier.manual && !$parent.isReadOnly">
                            <img src="resources/copy-documents-option.png" title="Copy supplier prices to the extra prices" />
                        </a>
                    </span>
                    <!-- <img v-if="supplier.manual" src="resources/close-button.png"  
                        title ="Remove supplier price"
                                height="10"
                                @click="$parent.removeSupplierPrice(supplier)" style="margin-left:10px;"/>  -->
                </div>
                <div style="margin-top: 10px;">
                    <div v-bind:class="[product == $parent.editedSupplier ? 'edit-on' : 'edit-off']">
                        <span class="label">SKU: </span>
                        <span class="break-val">{{product.sku}}</span>
                          <input type="text"  v-model="product.sku"  
                                @blur="$parent.doneSupplierEdit()"
                                @keyup.enter="$parent.doneSupplierEdit()"/>      
                        <span><a v-bind:href="product.url" target="_blank"><img src="resources/external-link.png" /></a></span>
                    </div>
                    <div style="margin-top:5px;" v-bind:class="[product == $parent.editedSupplier ? 'edit-on' : 'edit-off']">
                        <span class="label">Stock:</span>  
                        <span  v-bind:class="[product.stock == 0 ? 'price-exp break-val' : 'break-val']">{{product.stock}}</span>     
                        <input type="text"  v-model="product.stock"
                                @blur="$parent.doneSupplierEdit()"
                                @keyup.enter="$parent.doneSupplierEdit()"/>                                              
                    </div>
                    <div style="margin-top:5px;">
                        <span class="label">Valid till:</span> 
                        <span>
                            <input type="text" v-if="!supplier.manual"  disabled = 'disabled'  class="expire-on input-exp" ref="dateExpire"/>
                            <input type="text" v-if="supplier.manual"  class="expire-on" ref="dateExpire"/>
                            <!-- <span v-if="!supplier.manual">{{product.expired_on}}</span> -->
                        </span>                           
                    </div>
                    <div style="margin-top:5px;" v-if="product.priceExpired">
                        <span class="label"></span> 
                        <span class="price-exp">Price is expired.</span>                     
                    </div>
                </div>
            </div>
            <table class="tbl-price-break">                    
                <thead>
                    <tr>
                        <th class="th-qty">Qty</th>
                        <th class="th-price">Price(EUR)</th>
                        <th v-if="product.hasDiffCurrencyPrice">Price({{product.currency}})</th>
                        <th></th>                        
                    </tr>
                </thead>
                <tbody v-bind:class="[(product.priceExpired || product.stock == 0) == true ? 'price-exp' : '']">
                    <tr v-for="priceBreak in product.priceBreaks">
                        <td v-bind:class="[priceBreak == $parent.editedPriceBreak ? 'edit-on' : 'edit-off']">
                            <span class="break-val">{{priceBreak.moq}}</span>

                            <input type="text"
                                v-model="priceBreak.moq"                                
                                @blur="$parent.donePricebreakEdit(priceBreak, product.currency)"
                                @keyup.enter="$parent.donePricebreakEdit(priceBreak, product.currency)">                                    
                        </td>
                        <td v-bind:class="[priceBreak == $parent.editedPriceBreak ? 'edit-on' : 'edit-off']">
                            <span class="break-val">{{priceBreak.price}}</span>

                            <input type="text"                                                      
                                v-if="!product.hasDiffCurrencyPrice"
                                v-model="priceBreak.price"
                                @blur="$parent.donePricebreakEdit(priceBreak, product.currency)"
                                @keyup.enter="$parent.donePricebreakEdit(priceBreak, product.currency)">                            
                        </td>
                        <td v-if="product.hasDiffCurrencyPrice" v-bind:class="[priceBreak == $parent.editedPriceBreak ? 'edit-on' : 'edit-off']">
                            <span class="break-val">{{priceBreak.currency_price}}</span>
                            <input type="text"                                                      
                                v-model="priceBreak.currency_price"
                                @blur="$parent.donePricebreakEdit(priceBreak, product.currency)"
                                @keyup.enter="$parent.donePricebreakEdit(priceBreak, product.currency)">                            
                        </td>                           
                        <td v-if="supplier.manual && !$parent.isReadOnly" v-bind:class="[priceBreak == $parent.editedPriceBreak ? 'edit-on' : 'edit-off']">
                            <img src="resources/edit.png" 
                                    class="edit-pricebreak"                                 
                                    @click="$parent.editPricebreak(priceBreak)"/>

                            <img src="resources/close-button.png"  
                                class="rm-price"                                
                                @click="$parent.removePricebreak(product.priceBreaks, priceBreak)" style="margin-left:10px;"/> 
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="add-price-break" v-if="supplier.manual && !$parent.isReadOnly">                        
                <a href="#"  @click="$parent.addPrice(product.priceBreaks)"><img src="resources/add-circular.png" style="vertical-align:middle;"/> Add price</a>
            </div>
        </div>        
    </div>
</template>

    <script>
        Array.prototype.remove = function (v) {
            if (this.indexOf(v) != -1) {
                this.splice(this.indexOf(v), 1);
                return true;
            }
            return false;
        }

        Vue.component('pricebreak', {
            template: "#pricebreak-template",
            props: ['supplier'],
            mounted: function () {
                for (var i = 0; i < this.$props.supplier.products.length; i++) {
                    var dateEle = this.$refs.dateExpire[i];
                    var product = this.$props.supplier.products[i];

                    if (dateEle) {
                        //Convert ISO date to client timezone date
                        if (product.expired_on.indexOf('Z') == -1) {
                            product.expired_on = product.expired_on + '.000Z';
                        }
                        var expiredNative = new Date(product.expired_on);

                        flatpickr(dateEle, {
                            dateFormat: 'd-m-Y H:i',
                            time_24hr: true,
                            defaultDate: expiredNative,
                            maxDate: new Date().fp_incr(16),
                            enableTime: true,
                            onReady: function (selectedDates, dateStr, instance) {
                                product.expired_on = selectedDates[0];
                            },
                            onChange: function (selectedDates, dateStr, instance) {
                                product.expired_on = selectedDates[0];

                                var nowUTC = app.getUTCDate();
                                product.priceExpired = (selectedDates[0].getTime() < nowUTC.getTime());
                            }
                        });
                    }
                }
            }
        });

        var app = new Vue({
            el: '#app',
            data: {
                editedPriceBreak: null,
                editedSupplier: null,
                isLastEditedValidPrice: false,
                toggleSupplierSelection: false,
                mfrPartNumber: '',
                isReadOnly: false,
                suppliers: [],
                hasManualPrice: false,
                hasAPIPrice: false,
                baseCurrency: 'EUR',
                usdFactor: 0,
                newSupplier: "0",
                newCurrency: "EUR",
                newSKU: "",
                newStock: "",
                showLoading: false,
                waitMessage: "Loading...",
                isGenPart: false
            },
            mounted: function () {
                ES6Promise.polyfill();

                var mpnVal = this.getParameterByName("mpn");
                var isReadOnly = (this.getParameterByName("readOnly") == null) ? 0 : this.getParameterByName("readOnly");
                isReadOnly = (isReadOnly == 0) ? false : true;
                this.isReadOnly = isReadOnly;

                if (!mpnVal) {
                    alert("MPN value is not set.");
                }
                else {
                    this.mfrPartNumber = mpnVal;
                    if (this.mfrPartNumber.indexOf('GPN0') != -1) {
                        this.isGenPart = true;
                    }
                    else {
                        this.getPartPrices();
                    }
                }
            },
            methods: {
                getParameterByName: function (key) {
                    return decodeURIComponent(window.location.href.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
                },
                viewSupplierSelection: function () {
                    this.toggleSupplierSelection = true;
                },
                closeWindow: function (partUpdated) {
                    parent.partPriceDefined(partUpdated);
                },
                getUTCDate: function () {
                    var now = new Date();
                    var nowUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
                    return nowUTC;
                },
                getPartPrices: function () {
                    this.showLoading = true;
                    axios.post('/shop/assembly/partprices.aspx/GetPartPrices', {
                        mpn: this.mfrPartNumber
                    })
                    .then(function (response) {
                        app.showLoading = false;

                        var data = JSON.parse(response.data.d);

                        if (data.error) {
                            alert("Error occrred.\n" + data.error);
                            return
                        }

                        app.usdFactor = parseFloat(data.usd_factor);

                        for (var i = 0; i < data.suppliers.length; i++) {
                            //Set intial value for removed prop
                            data.suppliers[i]['removed'] = false;

                            app.suppliers.push(data.suppliers[i]);

                            //Set manual and API prices flag
                            if (!app.hasManualPrice) {
                                if (data.suppliers[i].manual) {
                                    app.hasManualPrice = true;
                                }
                            }
                            if (!app.hasAPIPrice) {
                                app.hasAPIPrice = !data.suppliers[i].manual;
                            }

                            //Set expired price flag
                            for (var j = 0; j < data.suppliers[i].products.length; j++) {
                                var expireUTC = new Date(data.suppliers[i].products[j].expired_on);
                                var nowUTC = app.getUTCDate();
                                data.suppliers[i].products[j]['priceExpired'] = (expireUTC.getTime() < nowUTC.getTime());
                            }
                        }
                    })
                    .catch(function (error) {
                        app.showLoading = false;
                        console.log(error);
                    });
                },
                addSupplier: function () {
                    var supplier = document.getElementById("ddlSupplier").options[document.getElementById('ddlSupplier').selectedIndex].text;
                    var sku = this.newSKU.trim();
                    var stock = parseInt(this.newStock.trim()) || 0;
                    var currency = this.newCurrency;

                    var validInput = true;
                    if (sku === "" || stock === 0 || supplier.indexOf("supplier") > 0) {
                        validInput = false;
                    }

                    if (!validInput) {
                        alert("Please enter or select valid input.")
                        return;
                    }

                    // to validate duplicate supplier
                    var isValidaSupplier = this.validateSupplierExtraPrices(supplier, sku)
                    if (isValidaSupplier) {
                        var supplierObject = this.createSupplierObject(supplier, this.mfrPartNumber, sku, stock, currency);
                        this.suppliers.push(supplierObject);

                        var priceBreack = supplierObject.products[0].priceBreaks[0];
                        app.editPricebreak(priceBreack)

                        this.toggleSupplierSelection = false
                        this.hasManualPrice = true;

                        //Set default input values
                        this.newSupplier = 0;
                        this.newSKU = "";
                        this.newCurrency = "EUR";
                        this.newStock = "";
                    }
                },
                copySupplierToEdit: function (supplier, product) {
                    var supplierName = supplier.supplier;
                    var sku = product.sku;
                    var stock = product.stock;
                    var priceBreaks = product.priceBreaks;
                    var currency = product.currency;
                    var supplierObject = this.createSupplierObject(supplierName, this.mfrPartNumber, sku, stock, currency);

                    // to validate duplicate supplier

                    var isValidaSupplier = this.validateSupplierExtraPrices(supplierName, sku)

                    if (isValidaSupplier) {
                        supplierObject.products[0].priceBreaks.pop();
                        for (var i = 0; i < priceBreaks.length; i++) {
                            var priceBreak = { moq: priceBreaks[i].moq, price: priceBreaks[i].price, currency_price: priceBreaks[i].currency_price };

                            supplierObject.products[0].priceBreaks.push(priceBreak);
                        }
                        this.suppliers.push(supplierObject);
                        var priceBreack = supplierObject.products[0].priceBreaks[0];
                        app.editPricebreak(priceBreack)

                        this.toggleSupplierSelection = false
                        this.hasManualPrice = true;
                    }
                },

                validateSupplierExtraPrices: function (supplierName, sku) {
                    var isPriceExist = false;
                    for (var i = 0; i < this.suppliers.length; i++) {
                        if (this.suppliers[i].manual && this.suppliers[i].supplier === supplierName) {
                            for (var j = 0; j < this.suppliers[i].products.length; j++) {
                                if (sku.toLowerCase() === this.suppliers[i].products[j].sku.toLowerCase()) {
                                    isPriceExist = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (isPriceExist) {
                        alert("Extra price is already exist for SKU " + sku + " of " + supplierName + ".")
                        return false;
                    }
                    else {
                        return true;
                    }
                },

                editStockSKU: function (product) {
                    this.editedSupplier = product;
                },
                createSupplierObject: function (supplierName, mpn, sku, stock, currency) {
                    var expiredOn = new Date();
                    expiredOn.setDate(expiredOn.getDate() + 15);

                    var supplier = {
                        supplier: supplierName,
                        mpn: mpn,
                        manual: true,
                        removed: false,
                        products: [{
                            sku: sku,
                            stock: stock,
                            currency: currency,
                            expired_on: expiredOn.toISOString(),
                            hasDiffCurrencyPrice: (app.baseCurrency !== currency),
                            priceBreaks: [{
                                moq: "",
                                price: "",
                                currency_price: ""
                            }]
                        }]
                    }
                    return supplier;
                },
                addPrice: function (priceBreaks) {
                    var priceBreak = { moq: '', price: '', currency_price: "" };
                    priceBreaks.push(priceBreak);
                    this.editedPriceBreak = priceBreak;
                },
                savePrices: function () {
                    if (this.suppliers.length == 0) {
                        alert("Incorrect price data.");
                        return;
                    }
                    var isValidPrice = true;
                    for (var i=0; this.suppliers.length > i ; i++) {
                        for (var j = 0; this.suppliers[i].products.length > j ; j++) {
                            for (var k = 0; this.suppliers[i].products[j].priceBreaks.length > k; k++) {
                                if (!this.isValidPriceBreak(this.suppliers[i].products[j].priceBreaks[k])) {
                                    alert('Invalid price break for supplier ' + this.suppliers[i].supplier);
                                    isValidPrice = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (!isValidPrice)
                    {
                        return;
                    }
                    var priceData = JSON.stringify(this.suppliers);

                    app.waitMessage = "Saving..."
                    app.showLoading = true;
                    axios.post('/shop/assembly/partprices.aspx/SavePrices', {
                        data: priceData
                    })
                    .then(function (response) {
                        app.showLoading = false;
                        var data = JSON.parse(response.data.d);

                        if (data.error) {
                            alert("Error occrred.\n" + data.error);
                        }
                        else {
                            app.closeWindow(true);
                        }
                    })
                    .catch(function (error) {
                        app.showLoading = false;
                        console.log(error);
                    });
                },
                editPricebreak: function (priceBreak) {
                    this.editedPriceBreak = priceBreak;
                },
                donePricebreakEdit: function (priceBreak, currency) {

                    if (!this.isValidPriceBreak(priceBreak)) {
                        return;
                    }
                    //Convert differnt currency price to the base currency price                    
                    if (currency === "USD") {
                        priceBreak.price = (parseFloat(priceBreak.currency_price) * app.usdFactor).toFixed(2);
                    }
                    else {
                        priceBreak.currency_price = priceBreak.price;
                    }
                    this.editedPriceBreak = null;
                },
                doneSupplierEdit: function () {
                    this.editeSupplier = null;
                },
                removeSupplier: function (supplier) {
                    if (confirm("Are you sure you want to delete extra prices?")) {
                        var index = this.suppliers.indexOf(supplier);
                        this.suppliers.splice(index, 1);


                    }
                },
                removePricebreak: function (priceBreaks, priceBreak) {
                    if (confirm("Are you sure you want to remove current price break?")) {
                        priceBreaks.splice(priceBreaks.indexOf(priceBreak), 1)
                    }
                },
                removeSupplierPrice: function (supplier) {
                    if (confirm("Are you sure you want to remove supplier prices?")) {
                        supplier.removed = true;
                    }
                },
                isValidPriceBreak: function (priceBreak) {
                    var moq = parseFloat(priceBreak.moq);
                    var price = parseFloat(priceBreak.price);

                    if (isNaN(moq) || isNaN(price) || price <=0 || moq === 0) {
                        this.isLastEditedValidPrice = false;
                    }
                    else {
                        this.isLastEditedValidPrice = true;
                    }
                    return this.isLastEditedValidPrice;
                }
            }
        });

    </script>
</body>
</html>

