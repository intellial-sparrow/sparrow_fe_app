﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml;
using System.Configuration;
//using EC09.Data;
using System.Security.Principal;
using System.IO;
using System.Web.Caching;
using EC09WebApp.shop.rb;

namespace sparrowFeApp.shop.assembly
{
    public class ComponentStatus
    {

        public class Part
        {
            public string MPN { get; set; }

            public string Manufacturer { get; set; }

            public string Category { get; set; }

            public string IPC { get; set; }

            public string Description { get; set; }

            public string Datasheet { get; set; }

            public string ImageUrl { get; set; }

            public string Generic { get; set; }

            public string External { get; set; }

            public string Verified { get; set; }

            public string MoutingType { get; set; }
            public string Sku { get; set; }
            public string Supplier { get; set; }
            public string   PurchQty { get; set; }
            public string ProdQty { get; set; }

            public string Supply { get; set; }
            public string OrdQty { get; set; }

            public string PcbQty { get; set; }

            public string price { get; set; }

            private List<PartPlacement> _Placements;
            public List<PartPlacement> Placements
            {
                get { return _Placements ?? (_Placements = new List<PartPlacement>()); }
                set { _Placements = value; }
            }
        }

        public class PartPlacement
        {
            public double X { get; set; }

            public double Y { get; set; }

            public double Rotation { get; set; }

            public string Side { get; set; }

            public string Designator { get; set; }
        } 

        public class CustomerPart
        {
            public Part Part { get; set;}

            public int  OrderQty { get; set; }
        }

        public enum PartSupplyType
        {
            ByAssembler = 0,
            ByCustomer = 1,
            NotPlaced = 2,
        }

        public enum LayerType
        {
            Top = 0,
            Bottom = 1,
            Both = 2,
            None = 3
        }

        public ComponentStatus(string orderNumber)
        {
            this.orderNumber = orderNumber;
        }

        private string orderNumber;

        /// <summary>
        /// Returns count of approved component for placement.
        /// <para>Only defined part is taken into account.</para>
        /// </summary>        
        public int PlacementAppr { get; private set; }

        /// <summary>
        /// Returns count of not approved components for placement.
        /// <para>Only defined part is taken into account.</para>
        /// </summary>     
        public int PlacementToBeAppr { get; private set; }

        /// <summary>
        /// Returns count of defined parts in BOM.
        /// <para>Ignore and not placed parts are not checked.</para>
        /// </summary>
        public int PartsDefined { get; private set; }

        /// <summary>
        /// Returns count of not defined parts in BOM.
        /// <para>Ignore and not placed parts are not checked.</para>
        /// </summary>
        public int PartsNotDefined { get; private set; }

        /// <summary>
        /// Returns count of parts that need to be approve by customer.
        /// </summary>
        public int PartsToBeAppr { get; private set; }

        /// <summary>
        /// Returns count of not defined parts in BOM but has zero qty set.
        /// <para>Ignore and not placed parts are not checked.</para>
        /// </summary>
        public int ZeroOrderQtyParts { get; private set; }

        /// <summary>
        /// <para>value = 0, All the parts will be mounted on top layer.</para>
        /// <para>value = 1, All the parts will be mounted on bottom layer.</para>
        /// <para>value = 2, Parts will be mounted on both side.</para>
        /// <para>value = 3, No SMD part is available.</para>
        /// </summary>
        public int Layer { get; private set; }
        
        private List<CustomerPart> customerSuppliedParts;

        private List<Part> uniqueMatchParts = null;

        private List<Part> nonidentifiedParts = null;

        public bool IsValidBOM()
        {
            return (PartsNotDefined == 0);
        }

        private bool hasLoadComponentStateCalled = false;

        private Part GetPartObject(XmlElement partEle)
        {
            Part part = new Part()
            {
                Category = partEle.GetAttribute("cat"),
                Datasheet = partEle.GetAttribute("url"),
                Description = partEle.GetAttribute("desc"),
                External = partEle.GetAttribute("external"),
                Generic = partEle.GetAttribute("gpn"),                
                ImageUrl = partEle.GetAttribute("img"),
                IPC = partEle.GetAttribute("ipc"),
                MPN = partEle.GetAttribute("mpn"),
                Manufacturer = partEle.GetAttribute("manuf"),
                MoutingType = partEle.GetAttribute("type"),
                Verified = partEle.GetAttribute("ver"),
            };
            return part;
        }

        private PartPlacement GetPlacementObject(XmlElement placementEle)
        {
            double x = 0, y , rotation;
            Double.TryParse(placementEle.GetAttribute("x"), out x);
            Double.TryParse(placementEle.GetAttribute("y"), out y);
            Double.TryParse(placementEle.GetAttribute("rot"), out rotation);

            PartPlacement placement = new PartPlacement()
            {
                Designator = placementEle.GetAttribute("refdes"),
                Side = placementEle.GetAttribute("layer"),
                Rotation = rotation,
                X = x,
                Y = y,               
            };
            return placement;
        }

        /// <summary>
        /// Returns list of customer supplied parts
        /// <para>Ignore and not placed parts are not checked.</para>
        /// </summary>
        public List<CustomerPart> GetCustomerSuppliedParts()
        {
            if (!hasLoadComponentStateCalled)
            {
                throw new Exception("LoadComponentState is not called");
            }

            return customerSuppliedParts;
        }

        /// <summary>
        /// Returns list of not verifed parts by EDA team.
        /// <para>Ignore and not placed parts are not checked.</para>
        /// </summary>
        public List<Part> GetNotVerifedParts()
        {
            if(!hasLoadComponentStateCalled)
            {
                throw new Exception("LoadComponentState is not called");
            }

            return uniqueMatchParts.FindAll(part => part.Verified == "0");
        }

        /// <summary>
        /// Returns list external parts that is searched from different suppliers API.
        /// <para>Ignore and not placed parts are not checked.</para>
        /// </summary>
        public List<Part> GetExternalParts()
        {
            if (!hasLoadComponentStateCalled)
            {
                throw new Exception("LoadComponentState is not called");
            }

            return uniqueMatchParts.FindAll(part => part.External == "1");
        }

        public List<Part> GetParts()
        {
            if (!hasLoadComponentStateCalled)
            {
                throw new Exception("LoadComponentState is not called");
            }

            return uniqueMatchParts;
        }

        public bool HasAnyGenericPart()
        {
            return uniqueMatchParts.FindAll(part => part.Generic == "1").Count > 0;
        }

        public int GenericParts()
        {
            return uniqueMatchParts.FindAll(part => part.Generic == "1").Count; 
        }

        public List<Part> GetNonIdentifiedPart()
        {
            return nonidentifiedParts;
        }

        public void LoadComponentState(bool isForComponentPDF = false, int qty = 0, bool unIdentifiedPart = false)
        {
            hasLoadComponentStateCalled = true;

            //EC09WebApp.OrderDetail orderDetail = new EC09WebApp.OrderDetail();
            visdownload visd = new visdownload();
            string compXMLPath = visd.getVisPath(orderNumber, "comp", "", ""); 

            if (isForComponentPDF && qty != 0 && orderNumber.StartsWith("B"))
            {
                compXMLPath = compXMLPath.ToLower().Replace(".xml", "_" + qty + ".xml");
            }

            //genFileServer fileServer = EC09WebService.BPO.GetActiveFileServer();
            compXMLPath = ConfigurationManager.AppSettings["FileServerPath"].ToString() + compXMLPath;

            using (WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(ConfigurationManager.AppSettings["FileServerUsername"].ToString(), ConfigurationManager.AppSettings["FileServerPassword"].ToString(), ConfigurationManager.AppSettings["FileServerDomain"].ToString()))
            {
                if (!File.Exists(compXMLPath))
                {
                    return;
                }
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(compXMLPath);

                XmlElement asmEle = (XmlElement)xmlDoc.SelectNodes("AssemblyData")[0];
                string xmlVersion = asmEle.GetAttribute("version");
                if (xmlVersion != "2")
                {
                    throw new Exception("Component XML version 2.0 is supported.");
                }

                XmlNodeList lineNodes = xmlDoc.SelectNodes("AssemblyData/BOM/Line");
                bool hasTopParts = false;
                bool hasBottomParts = false;

                this.customerSuppliedParts = new List<CustomerPart>();
                this.uniqueMatchParts = new List<Part>();
                this.nonidentifiedParts = new List<Part>();

                foreach (XmlNode lineNode in lineNodes)
                {
                    XmlElement lineEle = (XmlElement)lineNode;
                    string ignored = lineEle.GetAttribute("ign");
                    string supply = lineEle.GetAttribute("supply");

                    if (ignored == "0")
                    {
                        if (isForComponentPDF == false)//When Component pdf generated then display "Not placed" part in pdf
                        {
                            if (supply == Convert.ToInt32(PartSupplyType.NotPlaced).ToString())
                            {
                                continue;
                            }
                        }
                        XmlNodeList mpnNodes = lineNode.SelectNodes("MPN");
                        if(mpnNodes.Count == 1)
                        {
                            this.PartsDefined++;
                            string mpn = mpnNodes[0].Attributes["mpn"].Value;

                            XmlNodeList partNodes = xmlDoc.SelectNodes("AssemblyData/Parts/Part[@mpn='" + mpn + "']");                            
                            if(partNodes.Count > 0)
                            {
                                XmlElement partEle = (XmlElement)partNodes[0];
                                Part partObj = GetPartObject(partEle);
                                partObj.Supply = supply;
                                // for get sku and Supplier details from price node 
                                XmlNodeList priceNodes = xmlDoc.SelectNodes("AssemblyData/Prices/Price[@mpn='" + mpn + "']");
                                int custSuppyPurchaseQty = 0;
                                if (priceNodes.Count > 0)
                                {
                                    XmlElement priceEle = (XmlElement)priceNodes[0];
                                    partObj.Sku = priceEle.GetAttribute("sku")??"";
                                    partObj.Supplier = priceEle.GetAttribute("suppl") ?? "";
                                    partObj.PurchQty = priceEle.GetAttribute("purchQty") ?? "0";
                                    partObj.ProdQty = priceEle.GetAttribute("prodQty") ?? "0"; 
                                    partObj.OrdQty = priceEle.GetAttribute("ordQty") ?? " ";
                                    partObj.PcbQty = priceEle.GetAttribute("pcbQty") ?? " ";
                                    string manualPrice = priceEle.GetAttribute("manPrice");
                                    string price = priceEle.GetAttribute("price");
                                    if (manualPrice == "-1" || manualPrice == "-1.0")
                                    {
                                        partObj.price = price;
                                    }
                                    else {
                                        partObj.price = manualPrice;
                                    }

                                    if (partObj.price == "-1" || partObj.price == "-1.0")
                                    {
                                        partObj.price = "Not available";
                                    }

                                    string custsupplypart= priceEle.GetAttribute("manProdQty") ?? "-1"; 
                                        int.TryParse(custsupplypart, out custSuppyPurchaseQty);
                                    if (custSuppyPurchaseQty <=0)
                                    {
                                        custsupplypart = priceEle.GetAttribute("ProdQty") ?? "-1";
                                        int.TryParse(custsupplypart, out custSuppyPurchaseQty);
                                        if (custSuppyPurchaseQty <= 0)
                                        {
                                            custsupplypart = priceEle.GetAttribute("ordQty") ?? "-1";
                                            int.TryParse(custsupplypart, out custSuppyPurchaseQty);
                                        }
                                   }

                                }

                                //Generate unique part list
                                if (uniqueMatchParts.Find(prt => prt.MPN == mpn) == null)
                                {
                                    uniqueMatchParts.Add(partObj);
                                }

                              
                                //Make list of customer supplied parts
                                if (supply == Convert.ToInt32(PartSupplyType.ByCustomer).ToString())
                                {
                                    //int custSuppyPurchaseQty = lineNode.SelectNodes("Comp").Count;

                                    CustomerPart customerPart = new CustomerPart()
                                    {
                                        OrderQty = custSuppyPurchaseQty,
                                        Part = partObj
                                    };
                                    this.customerSuppliedParts.Add(customerPart);
                                }
                            }

                            this.ZeroOrderQtyParts += xmlDoc.SelectNodes("AssemblyData/Prices/Price[@mpn='" + mpn + "' and @ordQty='0']").Count;

                            string approvePart = lineEle.GetAttribute("appr");
                            if (approvePart == "0")
                            {
                                this.PartsToBeAppr++;
                            }

                            XmlNodeList compNodes = lineNode.SelectNodes("Comp");
                            Part uniqueMatchPart = uniqueMatchParts.Find(prt => prt.MPN == mpn);                            

                            foreach (XmlNode compNode in compNodes)
                            {
                                XmlElement compEle = (XmlElement)compNode;

                                //Add CPL info
                                if(uniqueMatchPart != null)
                                {
                                    PartPlacement placement = GetPlacementObject(compEle);
                                    uniqueMatchPart.Placements.Add(placement);
                                }

                                string approvePlacement = compEle.GetAttribute("appr");
                                if (approvePlacement == "1")
                                {
                                    this.PlacementAppr++;
                                }
                                else
                                {
                                    this.PlacementToBeAppr++;
                                }

                                //Only set layer if MPN is SMD ,QFN ,BGA,Finepitch type
                                XmlNodeList smdNodes = xmlDoc.SelectNodes("AssemblyData/Parts/Part[@mpn='" + mpn + "']");
                                if (smdNodes.Count > 0)
                                {
                                    foreach (XmlNode smdNode in smdNodes)
                                    {
                                        XmlElement smdEle = (XmlElement)smdNode;
                                       string type=  smdEle.GetAttribute("type");
                                        if (type.ToUpper()== "LGA" || type.ToUpper() == "LGA FINEPITCH" || type.ToUpper()== "MIXED" || type.ToUpper()== "SMD" || type.ToUpper() == "BGA" || type.ToUpper() == "QFN" || type.ToUpper() == "FINEPITCH" || type.ToUpper() == "SMD FINEPITCH" || type.ToUpper() == "BGA FINEPITCH" || type.ToUpper() == "QFN FINEPITCH")
                                        {
                                            string layer = compEle.GetAttribute("layer");
                                            if (layer == "top")
                                            {
                                                hasTopParts = true;
                                            }
                                            else
                                            {
                                                hasBottomParts = true;
                                            }
                                        }
                                      
                                    }
                                }                                
                            }
                        }
                        else
                        {
                            this.PartsNotDefined++;
                            try
                            {
                                if(isForComponentPDF == true)
                                {
                                    string Refdesc = lineEle.GetAttribute("refdes");
                                    XmlNodeList compNodes = lineNode.SelectNodes("Comp");
                                    long ComponentQty = 0;
                                    ComponentQty = compNodes.Count;
                                     
                                    Part part = new Part(); 
                                    part.MPN = "Part not identified"; 
                                    part.Supply = supply;
                                    part.PcbQty = ComponentQty.ToString();
                                    part.OrdQty = ComponentQty.ToString();
                                    part.Description = "";
                                    part.MoutingType = "Unknown";

                                    if (lineNode.Attributes != null && lineNode.Attributes["partType"] != null)
                                    {
                                        part.MoutingType = lineNode.Attributes["partType"].Value;
                                    } 

                                    foreach (XmlNode compNode in compNodes)
                                    {
                                        XmlElement compEle = (XmlElement)compNode;
                                        PartPlacement placement = GetPlacementObject(compEle);
                                        if (nonidentifiedParts != null)
                                        {
                                            PartPlacement ppnonidentifiedparts = GetPlacementObject(compEle);
                                            part.Placements.Add(ppnonidentifiedparts);
                                        }
                                    } 

                                    nonidentifiedParts.Add(part);  
                                }
                                if (unIdentifiedPart && mpnNodes.Count == 0)
                                {
                                    string mpn = lineEle.GetAttribute("mpn") ?? "";
                                    string desc = lineEle.GetAttribute("desc") ?? "";
                                    string manuf = lineEle.GetAttribute("manuf") ?? "";
                                    string url = lineEle.GetAttribute("url") ?? "";
                                    if (mpn != "" && mpn.Trim().Length > 2)
                                    {
                                        Part part = new Part();
                                        part.MPN = mpn;
                                        part.Description = desc;
                                        part.Manufacturer = manuf;
                                        part.Datasheet = url;
                                        nonidentifiedParts.Add(part);
                                    }
                                }
                            }
                            catch(Exception ex) { string str = ex.Message.ToString(); }
                        }                       
                    }
                }

                //Set layer info
                if(hasTopParts && hasBottomParts)
                {
                    this.Layer = Convert.ToInt32(LayerType.Both);
                }
                else if(hasTopParts)
                {
                    this.Layer = Convert.ToInt32(LayerType.Top);
                }
                else if (hasBottomParts)
                {
                    this.Layer = Convert.ToInt32(LayerType.Bottom);
                }
                else
                {
                    this.Layer = Convert.ToInt32(LayerType.None);
                }
            }
        }
    }

    public class Component
    {
        const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
        public static string GetDataFromSparrow(string apiName, Dictionary<string, object> parameters)
        {
            try
            {
                var baseAddress = ConfigurationManager.AppSettings["SparrowAPI"].ToString() + apiName + "/";
                var client = new RestSharp.RestClient(baseAddress);
                var restRequest = new RestRequest(Method.POST);
                restRequest.Timeout = 300000; //5min

                if (parameters == null)
                {
                    parameters = new Dictionary<string, object>();
                }

                foreach (KeyValuePair<string, object> entry in parameters)
                {
                    restRequest.AddParameter(entry.Key, entry.Value);
                }

                restRequest.AddParameter("key", "yhlC0v6OTSY0lys9MHmFWkhYyzyaEG09");
                string result = client.Execute(restRequest).Content;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string DownloadPackageFile(string uid ,out string fileName)
        {
            try
            {
                var baseAddress = ConfigurationManager.AppSettings["SparrowAPI"].ToString() + "download_package_file/";
               

                var client = new RestClient(baseAddress);
                var request = new RestRequest(Method.POST);
                request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"key\"\r\n\r\nyhlC0v6OTSY0lys9MHmFWkhYyzyaEG09\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"uid\"\r\n\r\n"+ uid + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var result = response.Content;
                 fileName = response.Headers.ToList().Find(x => x.Name == "Content-Disposition").Value.ToString();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void CovertCompXMLToNewFormat(string orderNumber)
        {
            //EC09WebApp.OrderDetail orderDetail = new EC09WebApp.OrderDetail();
            visdownload visd = new visdownload();
            string compXMLPath = visd.getVisPath(orderNumber, "comp", "", "");

            //genFileServer fileServer = EC09WebService.BPO.GetActiveFileServer();
            compXMLPath = ConfigurationManager.AppSettings["FileServerPath"].ToString() + compXMLPath;

            using (WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(ConfigurationManager.AppSettings["FileServerUsername"].ToString(), ConfigurationManager.AppSettings["FileServerPassword"].ToString(), ConfigurationManager.AppSettings["FileServerDomain"].ToString()))
            {
                if (!File.Exists(compXMLPath))
                {
                    return;
                }
                XmlDocument oldXmlDoc = new XmlDocument();
                oldXmlDoc.Load(compXMLPath);
                
                XmlElement asmEle = (XmlElement)oldXmlDoc.SelectNodes("AssemblyData")[0];
                string xmlVersion = asmEle.GetAttribute("version");
                if (xmlVersion == "2")
                {
                    return;
                }

                XmlDocument newDoc = new XmlDocument();
                XmlElement assemblyEle = newDoc.CreateElement("AssemblyData");                
                assemblyEle.SetAttribute("version", "2");
                newDoc.AppendChild(assemblyEle);

                XmlNodeList brdTopNodes = oldXmlDoc.SelectNodes("AssemblyData/BoardTop");
                if (brdTopNodes.Count > 0)
                {
                    XmlNode brdTopNode = assemblyEle.OwnerDocument.ImportNode(brdTopNodes[0], true);
                    assemblyEle.AppendChild(brdTopNode);
                }

                XmlNodeList brdBottomNodes = oldXmlDoc.SelectNodes("AssemblyData/BoardBottom");
                if (brdBottomNodes.Count > 0)
                {
                    XmlNode brdBottomNode = assemblyEle.OwnerDocument.ImportNode(brdBottomNodes[0], true);
                    assemblyEle.AppendChild(brdBottomNode);
                }

                //Create BOM node
                XmlElement bomEle = newDoc.CreateElement("BOM");
                assemblyEle.AppendChild(bomEle);

                //Create Parts node
                XmlElement partsEle = newDoc.CreateElement("Parts");
                assemblyEle.AppendChild(partsEle);

                //Create price node                
                XmlNodeList matchNodesWithQty = oldXmlDoc.SelectNodes("AssemblyData/part/match[@pcbQty!='0' and @ordQty !='0']");
                int numPCBs = 0;
                if (matchNodesWithQty.Count > 0)
                {
                    numPCBs = Convert.ToInt32(matchNodesWithQty[0].Attributes["ordQty"].Value) / Convert.ToInt32(matchNodesWithQty[0].Attributes["pcbQty"].Value);
                }

                //Only create pricesEle if numPBC > 0
                XmlElement pricesEle = null;
                if (numPCBs != 0)
                {
                    pricesEle = newDoc.CreateElement("Prices");
                    pricesEle.SetAttribute("numPCBs", numPCBs.ToString());
                    assemblyEle.AppendChild(pricesEle);
                }               

                XmlNodeList partNodes = oldXmlDoc.SelectNodes("AssemblyData/part");
                foreach (XmlNode partNode in partNodes)
                {   
                    //Create Line node
                    XmlElement lineEle = newDoc.CreateElement("Line");
                    bomEle.AppendChild(lineEle);

                    foreach (XmlAttribute attribute in partNode.Attributes)
                    {
                        lineEle.SetAttribute(attribute.Name, attribute.Value);
                    }

                    //Create MPN node
                    XmlNodeList matchNodes = partNode.SelectNodes("match");
                    foreach (XmlNode matchNode in matchNodes)
                    {
                        XmlElement mpnEle = newDoc.CreateElement("MPN");

                        XmlElement matchELE = (XmlElement)matchNode;
                        mpnEle.SetAttribute("mpn", matchELE.GetAttribute("mpn"));

                        lineEle.AppendChild(mpnEle);

                        //Create part node
                        XmlElement partEle = newDoc.CreateElement("Part");
                        partsEle.AppendChild(partEle);

                        XmlElement priceEle = null;
                        string ordQty = matchELE.GetAttribute("ordQty");

                        //Only create pricesEle if ordQty > 0
                        if (pricesEle != null && ordQty != "0")
                        {
                            //Create price node
                            priceEle = newDoc.CreateElement("Price");
                            pricesEle.AppendChild(priceEle);
                        }                        

                        foreach (XmlAttribute attribute in matchNode.Attributes)
                        {
                            if(attribute.Name == "manPrice" || attribute.Name == "manQty" || attribute.Name == "manPriceQty" || attribute.Name == "moq" 
                                || attribute.Name == "manURL" || attribute.Name == "mpn" || attribute.Name == "ordQty" || attribute.Name == "price"
                                || attribute.Name == "pcbQty" || attribute.Name == "prodQty" || attribute.Name == "purchQty" || attribute.Name == "suppl")
                            {
                                if(pricesEle != null && priceEle != null)
                                {
                                    priceEle.SetAttribute(attribute.Name, attribute.Value);
                                }                                
                            }
                            else
                            {
                                if(attribute.Name != "custSuppl" && attribute.Name != "suppl" && attribute.Name != "spn"
                                   && attribute.Name != "pins" && attribute.Name != "stock")
                                {
                                    partEle.SetAttribute(attribute.Name, attribute.Value);
                                }                                
                            }

                            if(attribute.Name == "mpn")
                            {
                                partEle.SetAttribute(attribute.Name, attribute.Value);
                            }
                        }
                    }

                    //Create comp node
                    XmlNodeList compNodes = partNode.SelectNodes("comp");
                    foreach (XmlNode compNode in compNodes)
                    {
                        XmlElement compEle = newDoc.CreateElement("Comp");
                        foreach (XmlAttribute attribute in compNode.Attributes)
                        {
                            compEle.SetAttribute(attribute.Name, attribute.Value);
                        }
                        lineEle.AppendChild(compEle);
                    }                    
                }
                
                string newFileName = Path.GetFileNameWithoutExtension(compXMLPath);
                string backFilePath = compXMLPath.Replace(newFileName, newFileName + "_back");
                if(!File.Exists(backFilePath))
                {
                    File.Move(compXMLPath, backFilePath);
                }                
                
                newDoc.Save(compXMLPath);
            }
        }        

        public static XmlDocument UpdateXMLPartAttr(XmlDocument xmlDoc)
        {
            List<object> searchList = new List<object>();
            
            XmlNodeList partNodes = xmlDoc.SelectNodes("AssemblyData/Parts/Part");

            for (int i = 0; i < partNodes.Count; i++)
            {
                string mpn = partNodes[i].Attributes["mpn"].Value;
                var searchData = new { index = i, mpn = mpn, spn = "", package = "", descr = "", specs = "[]" };
                searchList.Add(searchData);
            }

            if (searchList.Count == 0)
            {
                return xmlDoc;
            }

            string searchJson = JsonConvert.SerializeObject(searchList);

            DataTable dtRevised = new DataTable();
            dtRevised = CmBomImport.GetMatchedParts(searchJson, 1, 0, "0");

            if (dtRevised != null && dtRevised.Rows.Count > 1)
            {
                for (int i = 0; i < partNodes.Count; i++)
                {
                    string mpn = partNodes[i].Attributes["mpn"].Value;

                    DataRow[] drMatches = dtRevised.Select("mpn = '" + mpn + "'");
                    if (drMatches.Length > 0)
                    {
                        XmlElement element = (XmlElement)partNodes[i];                                                
                        element.SetAttribute("ver", drMatches[0]["verified"].ToString());
                        element.SetAttribute("desc", drMatches[0]["descr"].ToString());
                        element.SetAttribute("ipc", drMatches[0]["ipc_name"].ToString());
                        element.SetAttribute("url", drMatches[0]["data_url"].ToString());
                        element.SetAttribute("img", drMatches[0]["image"].ToString());
                        element.SetAttribute("manuf", drMatches[0]["manuf"].ToString());
                        element.SetAttribute("gpn", drMatches[0]["is_generic"].ToString());
                        element.SetAttribute("pol", drMatches[0]["polarized"].ToString());
                        element.SetAttribute("cat", drMatches[0]["category"].ToString());
                        element.SetAttribute("type", drMatches[0]["mounting"].ToString());
                        element.SetAttribute("marking", drMatches[0]["marking"].ToString());
                        element.SetAttribute("remark", drMatches[0]["remark"].ToString());
                        element.SetAttribute("partId", drMatches[0]["rel_id"].ToString());
                        element.SetAttribute("external", "0");
                    }
                }
            }

            return xmlDoc;
        }

        public static void UpdateManualPrices(string orderNumber)
        {
            //EC09WebApp.OrderDetail orderDetail = new EC09WebApp.OrderDetail();
            visdownload visd = new visdownload();
            string compXMLPath = visd.getVisPath(orderNumber, "comp", "", "");

            //genFileServer fileServer = EC09WebService.BPO.GetActiveFileServer();
            compXMLPath = ConfigurationManager.AppSettings["FileServerPath"].ToString() + compXMLPath;

            using (WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(ConfigurationManager.AppSettings["FileServerUsername"].ToString(), ConfigurationManager.AppSettings["FileServerPassword"].ToString(), ConfigurationManager.AppSettings["FileServerDomain"].ToString()))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(compXMLPath);

                XmlNodeList priceNodes = xmlDoc.SelectNodes("AssemblyData/Prices/Price");

                for (int i = 0; i < priceNodes.Count; i++)
                {
                    XmlElement priceElement = (XmlElement)priceNodes[i];
                    string mpn = priceElement.GetAttribute("mpn");
                    
                    XmlNodeList assemblerMPNNodes = xmlDoc.SelectNodes("AssemblyData/BOM/Line[@ign='0' and @supply='0']/MPN[@mpn='" + mpn + "']");                    

                    //Only update prices for tha parts which are supplied by assemblers.
                    if (assemblerMPNNodes.Count > 0)
                    {                        
                        //Update manual price tag which are not modified.
                        if (priceElement.GetAttribute("manPrice") == "-1")
                        {
                            string price = priceElement.GetAttribute("price");

                            //Supplier price should avaialable
                            if (priceElement.GetAttribute("price") != "-1")
                            {
                                string purchQty = priceElement.GetAttribute("purchQty");
                                string ordQty = priceElement.GetAttribute("ordQty");

                                priceElement.SetAttribute("manPrice", price);
                                priceElement.SetAttribute("manQty", purchQty);
                                priceElement.SetAttribute("manPriceQty", ordQty);
                            }
                        }
                    }
                }

                xmlDoc.Save(compXMLPath);
            }                                    
        }

        public static void UpdateManualPricesToZero(string orderNumber)
        {
            //EC09WebApp.OrderDetail orderDetail = new EC09WebApp.OrderDetail();
            visdownload visd = new visdownload();
            string compXMLPath = visd.getVisPath(orderNumber, "comp", "", "");

            //genFileServer fileServer = EC09WebService.BPO.GetActiveFileServer();
            compXMLPath = ConfigurationManager.AppSettings["FileServerPath"].ToString() + compXMLPath;

            using (WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(ConfigurationManager.AppSettings["FileServerUsername"].ToString(), ConfigurationManager.AppSettings["FileServerPassword"].ToString(), ConfigurationManager.AppSettings["FileServerDomain"].ToString()))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(compXMLPath);

                XmlNodeList priceNodes = xmlDoc.SelectNodes("AssemblyData/Prices/Price");

                for (int i = 0; i < priceNodes.Count; i++)
                {
                    XmlElement priceElement = (XmlElement)priceNodes[i];
                    priceElement.SetAttribute("manPrice", "-1");
                    priceElement.SetAttribute("manQty", "-1");
                    priceElement.SetAttribute("manPriceQty", "-1");
                }

                xmlDoc.Save(compXMLPath);
            }
        }

        public static string GetRootCompany(string orderNumber)
        {

            string rootCompany = "";
            if (HttpContext.Current.Cache["ORDERRC" + orderNumber] != null)
            {
                rootCompany = HttpContext.Current.Cache["ORDERRC" + orderNumber].ToString();
            }
            else
            {
                DataSet dsOrderRC =  (new ecgdService.gdSoapClient()).GetData(string.Format("exec SP_getRootCompany '{0}'",orderNumber),Keydata);
                if (dsOrderRC != null && dsOrderRC.Tables.Count > 0 && dsOrderRC.Tables[0].Rows.Count > 0)
                {
                    rootCompany = dsOrderRC.Tables[0].Rows[0]["RC"].ToString();
                    if (rootCompany != "")
                    {
                        string DocumentKey = "ORDERRC" + orderNumber;
                        HttpContext.Current.Cache.Add(DocumentKey, rootCompany,null, DateTime.UtcNow.AddHours(1), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                    }
                }
            }
            return rootCompany;
        }
    }
}