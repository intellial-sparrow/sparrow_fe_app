﻿using EC09WebApp.shop.rb;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace sparrowFeApp.shop.assembly
{
    public class TextToDataSet
    {
        private static string doubleQuotes = "(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))";
        public TextToDataSet()
        { }
        private static long bomLimit = Convert.ToInt64(ConfigurationManager.AppSettings["bom_limit"].ToString());
        private static bool ContainColumn(string columnName, DataTable table)
        {
            bool result = false;
            DataColumnCollection columns = table.Columns;
            if (columns.Contains(columnName))
            {
                result = true;
            }
            return result;
        }

        public static DataTable ConvertCSVtoDataTable(string filePath, int headerRow, string isHeader, string delimiter)
        {
            //initializing a StreamReader type variable and will pass the file location
            StreamReader oStreamReader = null;
            DataTable oDataTable = new DataTable();
            try
            {
                //EC09.Data.genFileServer objFileServer = EC09WebApp.EC09WebService.BPO.GetActiveFileServer();
                //if (objFileServer.IsRequireImpersonation == true)
                //{
                    System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(ConfigurationManager.AppSettings["FileServerUsername"].ToString(), ConfigurationManager.AppSettings["FileServerPassword"].ToString(), ConfigurationManager.AppSettings["FileServerDomain"].ToString());
                if (impersonateOnFileServer != null)
                    {
                        using (oStreamReader = new StreamReader(filePath))
                        {
                            string[] ColumnNames = null;
                            string[] oStreamDataValues = null;

                            delimiter = GetDelimiter(delimiter);

                            ColumnNames = CreateHeader(filePath, delimiter, isHeader, headerRow, true, "");

                            //Create column using for loop through all the column names
                            for (int index = 0; index < ColumnNames.Length; index++)
                            {
                                string colName = ColumnNames[index].ToString();
                                colName = ReplaceFirstOccurrence(colName, "\"", "");
                                colName = ReplaceLastOccurrence(colName, "\"", "");
                                if (String.IsNullOrEmpty(colName) == false)
                                {
                                    colName = TextToDataSet.GetColumnName(colName);
                                }
                                if (String.IsNullOrEmpty(colName) || ContainColumn(colName, oDataTable))
                                {
                                    colName = "Column" + index;
                                }
                                ColumnNames[index] = colName;
                                DataColumn oDataColumn = new DataColumn(colName, typeof(string));

                                //setting the default value of empty.string to newly created column
                                oDataColumn.DefaultValue = string.Empty;

                                //adding the newly created column to the table
                                oDataTable.Columns.Add(oDataColumn);
                            }

                            //using while loop read the stream data till end
                            int rowIndex = 0;
                            while (!oStreamReader.EndOfStream)
                            {
                                String oStreamRowData = oStreamReader.ReadLine().Trim();
                                if (oStreamRowData.EndsWith("\"\"\";"))
                                {
                                    oStreamRowData = oStreamRowData.Replace("\"\"\"", "\"");
                                    oStreamRowData = oStreamRowData.Replace("\"\"", "\""); 
                                    oStreamRowData = oStreamRowData.TrimStart('\"');
                                }

                                //creates a new DataRow with the same schema as of the oDataTable            
                                DataRow oDataRow = oDataTable.NewRow();
                                if (oStreamRowData.Length > 0)
                                {

                                    string regEx = delimiter + doubleQuotes;
                                    if (delimiter == "|")
                                    {
                                        regEx = "\\" + regEx;
                                    }

                                    Regex CSVDoubleQuotes = new Regex(regEx.Trim());

                                    //oStreamDataValues = oStreamRowData.Split(delimiter.ToCharArray());
                                    oStreamDataValues = CSVDoubleQuotes.Split(oStreamRowData);

                                    int dataCount = oStreamDataValues.Length;

                                    //using for-each looping through all the column names
                                    for (int i = 0; i < ColumnNames.Length; i++)
                                    {
                                        string colValue = null;
                                        if (dataCount > i)
                                        {
                                            colValue = oStreamDataValues[i].ToString();
                                        }
                                        colValue = ReplaceFirstOccurrence(colValue, "\"", "");
                                        colValue = ReplaceLastOccurrence(colValue, "\"", "");
                                        oDataRow[ColumnNames[i]] = colValue == null ? string.Empty : colValue;
                                    }
                                }
                                else // add empty row
                                {
                                    //using for-each looping through all the column names
                                    for (int i = 0; i < ColumnNames.Length; i++)
                                    {
                                        oDataRow[ColumnNames[i]] = string.Empty;
                                    }
                                }

                                //adding the newly created row with data to the oDataTable       
                                oDataTable.Rows.Add(oDataRow);
                                rowIndex++;
                                if (bomLimit == rowIndex)
                                {
                                    break;
                                }
                            }
                        }
                    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oStreamReader != null)
                {
                    //close the oStreamReader object
                    oStreamReader.Close();
                    //release all the resources used by the oStreamReader object
                    oStreamReader.Dispose();
                }
            }

            return oDataTable;
        }

        /// <summary>
        /// Converts a given delimited file into a dataset. 
        /// Assumes that the first line    
        /// of the text file contains the column names.
        /// </summary>
        /// <param name="File">The name of the file to open</param>    
        /// <param name="TableName">The name of the 
        /// Table to be made within the DataSet returned</param>
        /// <param name="delimiter">The string to delimit by</param>
        /// <returns></returns>  
        public static DataTable ConvertTextFiletoDataTable(string File, string isHeader, string TableName, string delimiter, int headerRow, string fixdlenthcolumn)
        {
            StreamReader sReader = null;
            int test = 0;
            DataTable result = new DataTable(TableName);
            delimiter = GetDelimiter(delimiter);
            try
            {
                using (sReader = new StreamReader(File))
                {
                    string[] columns;

                    columns = CreateHeader(File, delimiter, isHeader, headerRow, false, fixdlenthcolumn); // TODO

                    //Cycle the columns, adding those that don't exist yet 
                    //and sequencing the one that do.
                    foreach (string col in columns)
                    {
                        bool added = false;
                        string next = "";
                        int i = 0;
                        while (!added)
                        {
                            //Build the column name and remove any unwanted characters.
                            string columnname = col + next;
                            columnname = GetColumnName(columnname);
                            columnname = columnname.Replace("#", "");
                            columnname = columnname.Replace("'", "");
                            columnname = columnname.Replace("&", "");

                            if (String.IsNullOrEmpty(columnname) == false)
                            {
                                columnname = TextToDataSet.GetColumnName(columnname);
                            }
                            //See if the column already exists
                            if (!result.Columns.Contains(columnname))
                            {
                                //if it doesn't then we add it here and mark it as added
                                result.Columns.Add(columnname);
                                added = true;
                            }
                            else
                            {
                                //if it did exist then we increment the sequencer and try again.
                                i++;
                                next = "_" + i.ToString();
                            }
                        }
                    }

                    //Read the rest of the data in the file.        
                    string AllData = sReader.ReadToEnd();

                    //Split off each row at the Carriage Return/Line Feed
                    //Default line ending in most windows exports.  
                    //You may have to edit this to match your particular file.
                    //This will work for Excel, Access, etc. default exports.
                    //string[] rows = AllData.Split("\r\n".ToCharArray());
                    string[] rows = AllData.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

                    int ctr = 0;
                    int rowIndex = 0;
                    for (int index = 0; index < rows.Length; index++)
                    {
                        if (rows[index].Length > ctr)
                        {
                            rowIndex = index;
                            ctr = rows[index].Length;
                        }
                    }
                    //rows = rows.Where(row => !string.IsNullOrEmpty(row)).ToArray();
                    int FirstRow = 0;
                    int statrRow = 0;
                    //Now add each row to the DataSet        
                    foreach (string r in rows)
                    {
                        string[] items = new string[] { r };

                        if (delimiter == "none")
                        {
                            items[0] = r;
                        }
                        else if (delimiter == "FixedWidth")
                        {
                            string row = r.Replace("\t", "   ");
                            string[] colsSeparatedAt = fixdlenthcolumn.Split('/');
                            items = new string[colsSeparatedAt.Length];
                            int startIndex = 0;
                            int i = 0;
                            int colSeparateAt = 0;
                            int substringLength = 0;

                            foreach (string colSeparateAtStr in colsSeparatedAt)
                            {
                                int.TryParse(colSeparateAtStr, out colSeparateAt);

                                if (colSeparateAt == 0)
                                {
                                    colSeparateAt = row.Length;
                                }

                                substringLength = colSeparateAt - startIndex;
                                //Set data into cell according to separtion
                                if (colSeparateAt != 0 && (startIndex + substringLength) <= row.Length)
                                {
                                    items[i] = row.Substring(startIndex, substringLength);
                                }
                                else
                                {
                                    //Set all remaining data in last cell       
                                    items[i] = row.Substring(startIndex, (row.Length - startIndex));
                                    break;
                                }

                                startIndex = colSeparateAt;
                                i++;
                            }
                        }
                        else if (delimiter != "")
                        {
                            if (delimiter == "*")
                            {
                                delimiter = "\\*";
                            }
                            if (delimiter == "|")
                            {
                                delimiter = "\\|";
                            }

                            Regex rejex = new Regex(delimiter + doubleQuotes);
                            //Split the row at the delimiter.
                            items = rejex.Split(r.Trim());
                        }

                     

                        for (int index = 0; index < items.Length; index++)
                        {
                            if (result.Columns.Count < items.Length & result.Columns.Count < index + 1)
                            {
                                result.Columns.Add("columnn" + index);
                            }

                            if (items[index] == null || items[index].Trim() == "")
                            {
                                continue;
                            }

                            //items[index] = ReplaceFirstOccurrence(items[index], "\"", "");
                            //items[index] = ReplaceLastOccurrence(items[index], "\"", "");

                            if (delimiter == "FixedWidth" && items.Length == 1)
                            {

                                if (rowIndex == FirstRow)
                                {
                                    int count = 0;
                                    string newword = "";
                                    foreach (char c in items[index])
                                    {
                                        if (statrRow == 0 && items[index].Length>40)
                                        {
                                            newword = newword + "<Span data-charnumber='" + count.ToString() + "' Class='SpanIndex fwseparaterfirst'>" + c + "</Span>";
                                        }
                                        else {
                                            newword = newword + "<Span data-charnumber='" + count.ToString() + "' Class='SpanIndex'>" + c + "</Span>";
                                        }
                                        count++;
                                    }
                                    items[index] = newword;
                                    if (statrRow == 0 && items[index].Length > 40)
                                    {
                                        statrRow++;
                                    }
                                 }
                                else
                                {
                                    int count = 0;
                                    string newword = "";
                                    foreach (char c in items[index])
                                    {
                                        newword = newword + "<Span data-charnumber='" + count.ToString() + "' Class='SpanCol'>" + c + "</Span>";
                                        count++;
                                    }
                                    items[index] = newword;
                                }
                            }

                        }
                        FirstRow++;

                        result.Rows.Add(items);
                        test++;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sReader != null)
                {
                    sReader.Dispose();
                }
            }
            //Return the imported data.        
            //return result;
            return result;
        }

        private static string[] CreateHeader(string File, string delimiter, string isHeader, int headerRow, bool isCSV, string fixdlenthcolumn)
        {
            string regEx = delimiter;
            if (regEx == "|")
            {
                regEx = "\\|";
            }
            if (regEx == "*")
            {
                regEx = "\\*";
            }

            if (isCSV)
            {
                regEx += doubleQuotes;
            }
            Regex CSVDoubleQuotes = new Regex(regEx);
            if (isHeader.ToLower() == "no")
            {
                int counter = 0;
                int maxColumn = 0;
                string line;
                using (StreamReader sReader = new StreamReader(File))
                {

                    int count = 0;
                    if (delimiter == "FixedWidth")
                    {
                        maxColumn = fixdlenthcolumn.Split('/').Length;

                    }
                    else if (delimiter != "none")
                    {
                        while ((line = sReader.ReadLine()) != null)
                        {
                            if (isCSV)
                            {
                                if (line.EndsWith("\"\"\";"))
                                {
                                    line = line.Replace("\"\"\"", "\"");
                                    line = line.Replace("\"\"", "\"");
                                    line = line.TrimStart('\"');
                                }
                            }
                            count = CSVDoubleQuotes.Split(line.Trim()).Length;
                            if (maxColumn < count)
                            {
                                maxColumn = count;
                            }
                            counter++;
                        }
                    }
                    else
                    {
                        maxColumn = 1;
                    }
                }

                string[] header = new string[maxColumn];
                for (int index = 0; index < maxColumn; index++)
                {
                    header[index] = "Column" + (index + 1);
                }
                return header;
            }
            else
            {
                string headerLine = "";
                using (StreamReader sReader = new StreamReader(File))
                {
                    for (int i = 0; i < headerRow; i++)
                        headerLine = sReader.ReadLine();
                }

                string[] header = new string[1];
                if (delimiter == "none")
                {
                    header[0] = headerLine.ToString();
                }
                else if (delimiter == "FixedWidth")
                {
                    string row = headerLine.Replace("\t", "   ");
                    string[] ColumanWith = fixdlenthcolumn.Split('/');
                    header = new string[ColumanWith.Length];
                    int StartIndex = 0;
                    int i = 0;
                    bool IsDone = false;
                    foreach (string str in ColumanWith)
                    {
                        int with = 0;
                        int.TryParse(str, out with);

                        int lenth = StartIndex + with;

                        if (with == 0)
                        {
                            with = row.Length;
                        }
                        if (!IsDone && with != 0 && with <= row.Length)
                        {
                            header[i] = row.Substring(StartIndex, with - StartIndex).TrimEnd().TrimStart();
                        }
                        else {
                            header[i] = "";
                        }
                        StartIndex = with;
                        i++;
                    }
                }
                else
                {
                    //int count = headerLine.Split(delimiter.ToCharArray()).Length;
                    int count = 0;
                    if (isCSV)
                    {
                        if (headerLine.EndsWith("\"\"\";"))
                        {
                            headerLine = headerLine.Replace("\"\"\"", "\"");
                            headerLine = headerLine.Replace("\"\"", "\"");
                            headerLine = headerLine.TrimStart('\"');
                        }
                    }

                    count = CSVDoubleQuotes.Split(headerLine).Length;

                    header = new string[count];
                    //header = headerLine.Split(delimiter.ToCharArray());

                    header = CSVDoubleQuotes.Split(headerLine);
                }
                //string[] header = headerLine.Split(delimiter.ToCharArray());
                return header;
            }

        }

        public static string GetColumnName(string name)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            name = r.Replace(name, "");
            name = Regex.Replace(name, "[^a-zA-Z0-9_]+", " ");
            name = name.Trim().Replace(" ", "_");
            return name;
        }

        private static string GetDelimiter(string delimiter)
        {
            string del = delimiter;
            switch (delimiter)
            {
                case "Semicolon":
                    delimiter = ";";
                    break;
                case "Pipe":
                    delimiter = "|";
                    break;
                case "Comma":
                    delimiter = ",";
                    break;
                case "Tab":
                    delimiter = "\t";
                    break;
                case "space":
                    delimiter = "\\s+";
                    break;
                default:
                    delimiter = del;
                    break;
            }
            return delimiter;
        }

        private static string GetLine(StreamReader sReader, int line)
        {
            for (int i = 1; i < line; i++)
                sReader.ReadLine();
            return sReader.ReadLine();
        }

        public static string ReplaceFirstOccurrence(string Source, string Find, string Replace)
        {
            if (Source == null)
                return "";
            int Place = Source.IndexOf(Find);
            if (Place < 0)
            {
                return Source;
            }

            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }

        public static string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            if (Source == null)
                return "";
            int Place = Source.LastIndexOf(Find);
            if (Place < 0)
            {
                return Source;
            }

            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }

    }
}