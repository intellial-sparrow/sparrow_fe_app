﻿<%@ Page Language="C#" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["EDAPART"] != null && Session["EDAPART"].ToString().ToUpper() == "TRUE")
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            string userName = ConfigurationManager.AppSettings["SparrowAPIUser"].ToString();
            string password = ConfigurationManager.AppSettings["SparrowAPIpassword"].ToString();
            parameters.Add("username", userName);
            parameters.Add("password", password);
            string token = sparrowFeApp.shop.assembly.Component.GetDataFromSparrow("generate_token", parameters);//EC09WebApp.shop.assembly.Component.GetDataFromSparrow("generate_token", parameters);
            string partID = Request["partID"] ?? "";
            string readOnly = Request["readOnly"] ?? "0";
            string readOnlytype = "w";
            if (readOnly == "1")
            {
                readOnlytype = "r";
            }
            var res = new { code = "0", token = "", error = "" };
            var responces = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(token.ToString(), res);
            if (responces.token != null && responces.token != "")
            {
                string eccuser = "0";
                if (sparrowFeApp.core.AppSessions.EccUserId != null)
                {
                    eccuser = sparrowFeApp.core.AppSessions.EccUserId;
                }
                edaframe.Src = ConfigurationManager.AppSettings["SparrowAPIPage"].ToString() + "/" + partID + "/" + responces.token + "/"+readOnlytype +"?source=ecc&email="+eccuser;
            }
            else
            {
                Response.Clear();
                Response.Write("Invalid request " + responces.error);
                Response.End();
            }
        }
        else
        {
            Response.Clear();
            Response.Write("Invalid request");
            Response.End();

        }
    }
</script>

<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body style="height: 97%;">
    <form id="form1" runat="server">
        <div>
            <iframe id="edaframe" style="height: 96%; width: 97%; border: 0;" src="#" runat="server"></iframe>
        </div>
    </form>
</body>
</html>
