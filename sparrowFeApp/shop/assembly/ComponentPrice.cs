﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml;
using System.Configuration;

namespace sparrowFeApp.shop.assembly
{   public class ComponentPrice
    {
        private List<Dictionary<string, object>> reqPriceList = new List<Dictionary<string, object>>();

        public string ComponentXMLPath { get; set; }

        public Exception Exception { get; set; }

        public dynamic PriceObj { get; set; }

        public string latestPrice { get; set; }

        public ComponentPrice(string latestPrice)
        {
            this.latestPrice = latestPrice;
        }

        public void AddComponent(string mpn, int orderQty, int prodQty, string isGeneric, string category)
        {
            Dictionary<string, object> reqPriceData = new Dictionary<string, object>();
            reqPriceData.Add("mpn", mpn);
            reqPriceData.Add("ordQty", orderQty);
            reqPriceData.Add("prodQty", prodQty);
            reqPriceData.Add("isGen", isGeneric);
            reqPriceData.Add("cat", category);
            reqPriceList.Add(reqPriceData);
        }

        public XmlDocument UpdatePrice(XmlDocument xmlDoc, string partSelector, string preferedSupplier, string orderNumber)
        {
            if (reqPriceList.Count == 0)
            {
                return xmlDoc;
            }
            
            string reqPriceJson = JsonConvert.SerializeObject(reqPriceList);
            Dictionary<string, object> priceParameters = new Dictionary<string, object>();
            priceParameters.Add("data", reqPriceJson);
            priceParameters.Add("latest_price", latestPrice);
            priceParameters.Add("source_ref", orderNumber);
            priceParameters.Add("supplier_preference", preferedSupplier);

            string priceResult = Component.GetDataFromSparrow("get_parts_price", priceParameters);
            dynamic priceObj = JsonConvert.DeserializeObject(priceResult);


            xmlDoc = UpdateXMLDoc(xmlDoc, priceObj, partSelector);
            return xmlDoc;
        }
        
        public XmlDocument UpdateXMLDoc(XmlDocument xmlDoc, dynamic priceObj, string partSelector)
        {
            foreach (dynamic item in priceObj.data)
            {
                string mpn = item.mpn;
                string orderQty = item.ord_qty;
                XmlNodeList nodes = xmlDoc.SelectNodes(partSelector + "[@mpn='" + mpn + "' and @ordQty='" + orderQty + "']");
                foreach (XmlNode node in nodes)
                {
                    XmlElement xmlElement = (XmlElement)node;
                    xmlElement.SetAttribute("prodQty", item.prod_qty.ToString());
                    xmlElement.SetAttribute("purchQty", item.purch_qty.ToString());
                    xmlElement.SetAttribute("moq", item.moq.ToString());
                    xmlElement.SetAttribute("price", item.price.ToString());
                    xmlElement.SetAttribute("suppl", item.suppl.ToString());
                    xmlElement.SetAttribute("sku", item.sku.ToString());
                    xmlElement.SetAttribute("val", item.expired_on.ToString());
                    xmlElement.SetAttribute("stock", item.stock.ToString());
                    xmlElement.SetAttribute("havePrice", "1");
                }
            }
            
            XmlNodeList matchNodes = xmlDoc.SelectNodes(partSelector);
            foreach (XmlNode node in matchNodes)
            {
                XmlElement xmlElement = (XmlElement)node;
                if(xmlElement.GetAttribute("havePrice") == "1")
                {
                    xmlElement.RemoveAttribute("havePrice");                    
                }
                else
                {                    
                    xmlElement.SetAttribute("price", "0");                    
                }
            }

            return xmlDoc;
        }
    }
};