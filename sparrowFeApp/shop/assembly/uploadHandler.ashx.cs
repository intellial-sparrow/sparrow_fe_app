﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using EC09WebApp.shop.rb;
using NPOI.SS.UserModel;
using sparrowFeApp.core;
namespace sparrowFeApp.shop.assembly
{
    public class uploadHandler : IHttpHandler,IRequiresSessionState
    {
        //const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
        private string Excel07ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
        private DataFormatter dataFormatter;
        private IFormulaEvaluator formulaEvaluator;
        private long bomLimit = Convert.ToInt64(Constant.BomLimit); 
        private long bomcellLimit = Convert.ToInt64(Constant.BomCellLimit);




        private enum Status
        {
            Error = 0,
            BOMExist = 2,
            UnknownFile = 3,
            EditBOM = 4,
            BlankFilePath = 5
        }
        public bool IsImpersonation()
        {
            WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);
            if (impersonateOnFileServer != null)
            {
                return true;
            }
            return false;
        }
        public void DownloadFile(HttpContext context, string filePath)
        {
            if (IsImpersonation())
            {
                string fileName = Path.GetFileName(filePath);
                Stream stream = null;
                int bytesToRead = 10000;
                byte[] buffer = new Byte[bytesToRead];
                try
                {
                    stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    var resp = HttpContext.Current.Response;
                    resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                    resp.AddHeader("Content-Length", stream.Length.ToString());

                    int length;
                    do
                    {
                        if (resp.IsClientConnected)
                        {
                            length = stream.Read(buffer, 0, bytesToRead);
                            resp.OutputStream.Write(buffer, 0, length);
                            resp.Flush();
                            buffer = new Byte[bytesToRead];
                        }
                        else
                        {
                            length = -1;
                        }
                    } while (length > 0);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Close();
                    }
                }
            }
        }
        public string GetFolderPath(string entitynr)
        {
            string FolderPath = "";
            //test
            if (IsImpersonation())
            {
                FolderPath = Common.getFilePath(entitynr);
            }
            return FolderPath;
        }
        public string GetFilePath(string entitynr, string fileType, bool checkCustFile)
        {
            string RootPath = Constant.FileServerPath;
            string FolderPath = "";
            string filePath = "";
            
            
            if (IsImpersonation())
            {
                FolderPath = Common.getFilePath(entitynr);
                string[] filePaths = Directory.GetFiles(FolderPath);
                if (Directory.Exists(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType))
                {
                    filePaths = Directory.GetFiles(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType);
                    if (filePaths != null)
                    {
                        if (fileType == "CM")
                        {
                            if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_"))[0] ?? null;
                            }
                            else if (Array.FindAll(filePaths, x => x.Contains("SUBMITTED_BOM_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("SUBMITTED_BOM_"))[0] ?? null;
                            }
                            else if (Array.FindAll(filePaths, x => x.Contains("BOM_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("BOM_"))[0] ?? null;
                            }
                        }
                        else
                        {
                            if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_"))[0] ?? null;
                            }
                            else if (Array.FindAll(filePaths, x => x.Contains("SUBMITTED_CPL_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("SUBMITTED_CPL_"))[0] ?? null;
                            }
                            else if (Array.FindAll(filePaths, x => x.Contains("CPL_")).Count() > 0)
                            {
                                filePath = Array.FindAll(filePaths, x => x.Contains("CPL_"))[0] ?? null;
                            }
                        }
                    }
                }
                else
                {
                    if (fileType == "CM")
                    {
                        if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_BOM_"))[0] ?? null;
                        }
                        else if (Array.FindAll(filePaths, x => x.Contains("SUBMITTED_BOM_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("SUBMITTED_BOM_"))[0] ?? null;
                        }
                        else if (Array.FindAll(filePaths, x => x.Contains("BOM_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("BOM_"))[0] ?? null;
                        }
                    }
                    else
                    {
                        if (Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("UPLOADED_CPL_"))[0] ?? null;
                        }
                        else if (Array.FindAll(filePaths, x => x.Contains("SUBMITTED_CPL_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("SUBMITTED_CPL_"))[0] ?? null;
                        }
                        else if (Array.FindAll(filePaths, x => x.Contains("CPL_")).Count() > 0)
                        {
                            filePath = Array.FindAll(filePaths, x => x.Contains("CPL_"))[0] ?? null;
                        }
                    }
                }
            }
            
            return filePath;
        }

        public string GetBOMFileUploadPath(string entitynr, string fileType)
        {
            string FolderPath = ""; string filePath = "";
             //test
            
            if (IsImpersonation())
            {
                FolderPath = Common.getFilePath(entitynr);
                string[] filePaths = Directory.GetFiles(FolderPath);
                string fileName = fileType == "CM" ? "BOM_" : "CPL_";
                if (Array.FindAll(filePaths, x => x.Contains(fileName)).Count() > 0)
                {
                    filePath = Array.FindAll(filePaths, x => x.Contains(fileName))[0] ?? null;
                }
            }
           
            return filePath;
        }
        private string UploadFile(HttpPostedFile file, string entitynr, string fileName, string fileType)
        {
            string FolderPath = "";
            string filePath = "";
            if (entitynr.Contains("-A"))
            {
                entitynr = entitynr.Split('-')[0].ToString();
            }
            //test
            
            if (IsImpersonation())
            {
                FolderPath = Common.getFilePath(entitynr);
                if (!Directory.Exists(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType))
                {
                    Directory.CreateDirectory(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType);
                }
                else
                {
                    string[] fileList = System.IO.Directory.GetFiles(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType);
                    foreach (string files in fileList)
                    {
                        string name = Path.GetFileName(files);
                        if (!name.ToUpper().StartsWith("IMPORTED_") && !name.ToUpper().StartsWith("SUBMITTED_"))
                        {
                            System.IO.File.Delete(files);
                        }
                    }
                }
              
                FolderPath = FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType;
                filePath = FolderPath + "\\" + fileName;
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                string strUserid = "0";
                string strFrom = "";
                    
                //if (EC09Sessions.EccUserId != null && EC09Sessions.EccUserId.ToString().Trim() != "" && EC09Sessions.EccUserId != "0")
                //{
                //    strUserid = EC09Sessions.EccUserId.ToString().Trim();
                //    strFrom = "ECC";
                //}
                //else if (EC09Sessions.UserId != null)
                //{
                //    strUserid = EC09Sessions.UserId.ToString();
                //    strFrom = "ECWEB";
                //}

                if (HttpContext.Current.Session["UserId"] != null && HttpContext.Current.Session["UserId"].ToString().Trim() != "" && HttpContext.Current.Session["UserId"].ToString() != "0")
                {
                    strUserid = HttpContext.Current.Session["UserId"].ToString().Trim();
                    strFrom = "ECC";
                }
                else if (HttpContext.Current.Session["UserId"] != null)
                {
                    strUserid = HttpContext.Current.Session["UserId"].ToString();
                    strFrom = "ECWEB";
                }

                //EC09WebApp.EC09WebService.BPO bpo = new EC09WebService.BPO();  //Pending
                //bpo.InsertBOMCPLUploadHistory(entitynr, Path.GetFileName(file.FileName), FolderPath, fileType, strUserid, strFrom);

                file.SaveAs(filePath);
            }
           
            fileName = Path.GetFileName(filePath);
            //\\WINSERVER1\ec_fileserver\fe_fileserver\ //test
            //string filePathnw = filePath.Remove(0, 40).Replace(fileName, "");
            string filePathnw = filePath.Replace(Constant.FileServerPath, "").Replace(fileName, "");
            if (filePathnw.Trim() != "" && filePathnw.Contains(entitynr))
            { 
                //'fun_name': 'file_sync', 'entity_number': 'E0660308', 'file_name': 'E0660308_components.xml','file_type': 'assembly_editor','file_path': '\\Order_Files\\2019\\8\\1\\E0660308\\assembly\\E0660308\','crm_rel_id': 167403 ,'source':'FE','target': 'EC'} 
                var feApp = new { fun_name = "file_sync", entity_number = entitynr, file_name = fileName, file_type = "assembly_editor", file_path = filePathnw, crm_rel_id = AppSessions.EccUserId, source = "FE", target = "EC" };
                string jsonFeApp = JsonConvert.SerializeObject(feApp); 
                Common feAppData = new Common();
                feAppData.FeAppAPI(jsonFeApp);
            } 

            return filePath;
        } 

        public bool IsBOMXmlExist(string folderPath, string entityNo, string fileType)
        {
            string RootPath = Constant.FileServerPath; 
            folderPath = RootPath + folderPath;
            if (Directory.Exists(folderPath + "\\assembly\\" + entityNo + "\\BOM\\"))
            {
                DirectoryInfo dir = new DirectoryInfo(folderPath + "\\assembly\\" + entityNo + "\\BOM\\");
                var files = dir.GetFiles("*cpl.xml");
                if (fileType == "CM")
                {
                    if (File.Exists(folderPath + "\\assembly\\" + entityNo + "\\BOM\\" + "bom.xml"))
                    {
                        return true;
                    }
                }
                else
                {
                    if (files.Length > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void DeleteFileRow(string filePath, string sheetName, string rowIndexes)
        {
            IWorkbook workbook = null;
            try
            {
                rowIndexes = rowIndexes.TrimEnd(',');
                for (int i = rowIndexes.Split(',').Length - 1; i >= 0; i--)
                {
                    int rowIndex = Convert.ToInt16(rowIndexes.Split(',')[i]);
                    string extension = Path.GetExtension(filePath).ToLower();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                        {
                            workbook = WorkbookFactory.Create(file);
                            ISheet sheet;
                            if (sheetName == "")
                            {
                                sheetName = workbook.GetSheetAt(0).SheetName;
                            }
                            sheet = workbook.GetSheet(sheetName);

                            int lastRowNum = sheet.LastRowNum;

                            //rowIndex is reduced by one because LastRowNum is calculated from zero index and our rowindex caluculated from one index. 
                            if (rowIndex != 0)
                            {
                                rowIndex = rowIndex - 1;
                            }

                            //if (rowIndex >= 0 && rowIndex <= lastRowNum)
                            //{
                            //    sheet.ShiftRows(rowIndex, lastRowNum, -1);
                            //}

                            //if (rowIndex == lastRowNum)
                            //{
                            //    IRow removingRow = sheet.GetRow(rowIndex);
                            //    if (removingRow != null)
                            //    {
                            //        sheet.RemoveRow(removingRow);
                            //    }
                            //}

                            IRow row = sheet.GetRow(rowIndex);
                            if (row != null)
                            {
                                sheet.RemoveRow(row);
                                //sheet.ShiftRows(rowIndex + 1, sheet.LastRowNum, -1);
                            }
                        }

                        using (FileStream file = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                        {
                            workbook.Write(file);
                        }
                    }
                    else if (extension == ".ods")
                    {
                        ODSToDataTable ODSTable = new ODSToDataTable();
                        ODSTable.ODSDeleteRow(filePath, rowIndexes, sheetName);
                        break;
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        using (StreamReader sr = new StreamReader(filePath))
                        {
                            int count = 0;
                            while (!sr.EndOfStream)
                            {
                                count++;
                                if (count != rowIndex)
                                {
                                    using (StringWriter sw = new StringWriter(sb))
                                    {
                                        sw.WriteLine(sr.ReadLine());
                                    }
                                }
                                else
                                {
                                    sr.ReadLine();
                                }
                            }
                            sr.Dispose();
                        }
                        using (StreamWriter sw = new StreamWriter(filePath))
                        {
                            sw.Write(sb.ToString().TrimEnd(Environment.NewLine.ToCharArray()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                workbook = null;
            }
        }

        private DataTable ExcelToDataTable(string filePath, int headerRow, string sheetname)
        {
            DataTable dataTable = new DataTable();
            IWorkbook workbook = null;
            try
            {
                if (IsImpersonation())
                {
                    ISheet sheet;
                    String sheetName = sheetname;
                    using (FileStream outFile = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite))
                    {
                        workbook = WorkbookFactory.Create(outFile);
                    }
                    dataFormatter = new DataFormatter(CultureInfo.CurrentCulture);
                    formulaEvaluator = WorkbookFactory.CreateFormulaEvaluator(workbook);

                    if (sheetname == "")
                    {
                        sheetName = workbook.GetSheetAt(0).SheetName;
                    }
                    sheet = workbook.GetSheet(sheetName);
                    dataTable.Rows.Clear();
                    dataTable.Columns.Clear();
                    bool setHeader = false;
                    headerRow = headerRow - 1;
                    // add header
                    if (headerRow >= 0)
                    {
                        if (sheet.GetRow(headerRow) != null)
                        {
                            //sheet.GetRow(headerRow).Cells.Count
                            if (dataTable.Columns.Count < sheet.GetRow(headerRow).LastCellNum)
                            {
                                for (int j = 0; j < sheet.GetRow(headerRow).LastCellNum; j++)
                                {
                                    string colName = (GetCellValue(sheet.GetRow(headerRow).GetCell(j)) ?? "");
                                    if (String.IsNullOrEmpty(colName) == false)
                                    {
                                        colName = TextToDataSet.GetColumnName(colName);
                                    }
                                        
                                    if (String.IsNullOrEmpty(colName))
                                    {
                                        colName = "Column" + j.ToString();
                                    }
                                    else if (dataTable.Columns.Contains(colName))
                                    {
                                        colName = colName + j.ToString();
                                    }                                    
                                    dataTable.Columns.Add(colName, typeof(string));
                                    if (j == bomcellLimit)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                        setHeader = true;
                    }

                    int rowIndex = 0;
                    int emptyRow = 0;

                    while (rowIndex <= sheet.LastRowNum)
                    {
                        if (sheet.GetRow(rowIndex) == null)
                        {
                            dataTable.Rows.Add();
                            rowIndex++;

                            emptyRow++;
                            if (emptyRow == 200)
                            {
                                break;
                            }
                            continue;
                        }
                        emptyRow = 0;

                        int currentRowColDiff = sheet.GetRow(rowIndex).LastCellNum - dataTable.Columns.Count;
                        if (!setHeader || currentRowColDiff > 0)
                        {
                            // add necessary columns
                            for (int j = 0; j < currentRowColDiff; j++)
                            {
                                dataTable.Columns.Add("Column" + (dataTable.Columns.Count + 1).ToString(), typeof(string));
                                if (j == bomcellLimit)
                                {
                                    break;
                                }
                            }
                        }

                        // add row
                        dataTable.Rows.Add();

                        // write row value
                        for (int j = 0; j < sheet.GetRow(rowIndex).LastCellNum; j++)
                        {
                            var cell = sheet.GetRow(rowIndex).GetCell(j);
                            if (cell != null)
                            {
                                dataTable.Rows[rowIndex][j] = GetCellValue(cell);
                            }
                            if (j == bomcellLimit)
                            {
                                break;
                            }
                        }

                        rowIndex++;
                        if (bomLimit == rowIndex)
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                workbook = null;
            }

            return dataTable;
        }

        private string GetCellValue(ICell cell)
        {
            string value = "";
            if (cell == null)
            {
                return value;
            }

            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.Numeric:
                    value = cell.NumericCellValue.ToString();
                    //value = dataFormatter.FormatRawCellContents(cell.NumericCellValue, 0, cell.CellStyle.GetDataFormatString());
                    break;
                case NPOI.SS.UserModel.CellType.String:
                    value = cell.StringCellValue;
                    break;
                case NPOI.SS.UserModel.CellType.Boolean:
                    value = cell.BooleanCellValue.ToString();
                    break;
                case NPOI.SS.UserModel.CellType.Formula:
                    value = GetFormattedValue(cell);
                    break;

            }
            return value;
        }

        protected string GetFormattedValue(ICell cell)
        {
            string returnValue = string.Empty;
            if (cell != null)
            {
                try
                {
                    // Get evaluated and formatted cell value
                    returnValue = dataFormatter.FormatCellValue(cell, this.formulaEvaluator);
                }
                catch
                {
                    // When failed in evaluating the formula, use stored values instead...
                    // and set cell value for reference from formulae in other cells...
                    if (cell.CellType == CellType.Formula)
                    {
                        switch (cell.CachedFormulaResultType)
                        {
                            case CellType.String:
                                returnValue = cell.StringCellValue;
                                cell.SetCellValue(cell.StringCellValue);
                                break;
                            case CellType.Numeric:
                                returnValue =  cell.NumericCellValue.ToString();

                                //dataFormatter.FormatRawCellContents(cell.NumericCellValue, 0, cell.CellStyle.GetDataFormatString());   
                                cell.SetCellValue(cell.NumericCellValue);
                                break;
                            case CellType.Boolean:
                                returnValue = cell.BooleanCellValue.ToString();
                                cell.SetCellValue(cell.BooleanCellValue);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            return (returnValue ?? string.Empty).Trim();
        }

        private DataTable FilterData(DataTable dtData, int headerLine)
        {
            try
            {
                if (dtData.Rows.Count > 0)
                {
                    List<DataRow> rowsToRemove = new List<DataRow>();
                    dtData = IsContainColumn("Index", "", dtData);

                    dtData = IsContainColumn("IsVisible", "", dtData);
                    string valuesarr = string.Empty;
                    int rowNumber = 1;
                    for (int i = 0; i <= dtData.Rows.Count - 1; i++)
                    {
                        dtData.Rows[i]["IsVisible"] = "true";
                        dtData.Rows[i]["index"] = rowNumber;
                        rowNumber++;
                    }
                    dtData.AcceptChanges();

                    foreach (var dr in rowsToRemove)
                    {
                        dtData.Rows.Remove(dr);
                    }

                    dtData = SetColumnName(dtData);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dtData;
        }

        private DataTable IsContainColumn(string columnName, string defaultValue, DataTable table)
        {
            DataColumnCollection columns = table.Columns;
            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            columnName = TextToDataSet.GetColumnName(columnName);
            columnName = textInfo.ToTitleCase(columnName);
            if (!columns.Contains(columnName))
            {
                System.Data.DataColumn newColumn = new System.Data.DataColumn(columnName, typeof(System.String));
                if (columnName.ToLower() == "index")
                {
                    DataTable resultTable = new DataTable();
                    newColumn.AutoIncrement = true;
                    newColumn.AutoIncrementSeed = 1;
                    newColumn.AutoIncrementStep = 1;
                    resultTable.Columns.Add(newColumn);
                    resultTable.Merge(table);
                    table = resultTable;
                }
                else
                {
                    newColumn.DefaultValue = defaultValue;
                    table.Columns.Add(newColumn);
                }
            }
            return table;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                DataTable dtData = null;
                List<object> headers = null;
                List<WorkSheet> excelSheets = new List<WorkSheet>();
                bool isDownload = Convert.ToBoolean(context.Request.QueryString["isDownload"] ?? "false");
                string entitynr = Convert.ToString(context.Request.Params["entitynr"]);
                string bomFileType = Convert.ToString(context.Request.Params["bomfiletype"]);
                if (isDownload)
                {
                    string downloadFilePath = GetFilePath(entitynr, bomFileType, true);
                    DownloadFile(context, downloadFilePath);
                }
                string uploadPath = Convert.ToString(context.Request.Params["path"]);
                string isHeader = Convert.ToString(context.Request.Params["isHeader"]);
                int headerLine = Convert.ToInt32(context.Request.Params["hRow"].ToString());
                string slectedDelimiter = Convert.ToString(context.Request.Params["separator"]);
                string fixdlenthColumn = Convert.ToString(context.Request.Params["fixdlenthcolumn"]);

                bool forceColumnsMap = Convert.ToBoolean(context.Request.Params["forceColumnsMap"] ?? "false");
                string sheet = Convert.ToString(context.Request.Params["sheet"]);
                bool checkCustFile = Convert.ToBoolean(context.Request.Params["checkCustFile"] ?? "false");
                string clientData = context.Request.Params["clientData"] ?? "";
                bool isReadData = Convert.ToBoolean(context.Request.Params["isReadData"] ?? "false");// when click on editBOM all data fill up to textarea
                bool isMultipleExcelChange = Convert.ToBoolean(context.Request.Params["isMultipleExcelChange"] ?? "false");
                string deleteRowIndexes = Convert.ToString(context.Request.Params["deleteRowIndex"]) ?? "";
                HttpFileCollection files = context.Request.Files;
                if (entitynr.Contains("-A"))
                {
                    entitynr = entitynr.Split('-')[0].ToString();
                }
                string filePath = "";
                if (clientData != "")
                {
                    if (IsImpersonation())
                    {
                        filePath = GetFilePath(entitynr, bomFileType, checkCustFile);
                        string fileNameWithExt = Path.GetFileName(filePath);
                        string fileNameWithoutExt = Path.GetFileNameWithoutExtension(filePath);
                        if (fileNameWithoutExt.Contains("UPLOADED_") || fileNameWithoutExt.Contains("SUBMITTED_"))
                        {
                            fileNameWithoutExt = fileNameWithoutExt.Replace("UPLOADED_", "").Replace("SUBMITTED_", "");
                        }
                        string assemblyBOMPath = "";
                        if (filePath.Contains("\\assembly\\" + entitynr + "\\"))
                        {
                            assemblyBOMPath = filePath.Replace(fileNameWithExt, "");
                            Directory.Delete(assemblyBOMPath, true);
                        }
                        else
                        {
                            assemblyBOMPath = filePath.Replace(fileNameWithExt, "") + "\\assembly\\" + entitynr + "\\BOM\\" + bomFileType;
                        }

                        fileNameWithoutExt = SubstringFileName(bomFileType, fileNameWithoutExt, "");
                        string fileName = (bomFileType == "CM" ? "UPLOADED_BOM_" : "UPLOADED_CPL_") + fileNameWithoutExt + ".txt";

                        Directory.CreateDirectory(assemblyBOMPath);
                        filePath = assemblyBOMPath + "\\" + fileName;
                        File.WriteAllText(filePath, clientData);
                    }
                }
                else
                {
                    if (files.Count > 0)
                    {
                        string fname = GetFileName(files[0].FileName);
                        fname= fname.Replace("UPLOADED_", "").Replace("SUBMITTED_", "");
                        string fileName = (bomFileType == "CM" ? "UPLOADED_BOM_" : "UPLOADED_CPL_") + fname;//entitynr + Path.GetExtension(files[0].FileName);
                        filePath = UploadFile(files[0], entitynr, fileName, bomFileType);
                        if (bomFileType == "CM")
                        {
                            HttpContext.Current.Session["bomcplUpload"] = "yes";
                        }
                    }
                    else
                    {
                        filePath = GetFilePath(entitynr, bomFileType, checkCustFile);
                    }
                }
                if (filePath != "" && deleteRowIndexes != "")
                {
                    DeleteFileRow(filePath, sheet, deleteRowIndexes);
                }
                if (isReadData)
                {
                    string fileData = File.ReadAllText(filePath); // relative path
                    context.Response.Write(JsonConvert.SerializeObject(new { Status = Status.EditBOM, data = fileData }));
                    return;
                }

                if (filePath == "")
                {
                    context.Response.Write(JsonConvert.SerializeObject(new { Status = Status.BlankFilePath, data = dtData, headers = headers, excelSheets = excelSheets }));
                    return;
                }


                string extension = Path.GetExtension(filePath).ToLower();
                if (extension.ToLower() == ".pdf" || extension.ToLower() == ".doc" || extension.ToLower() == ".docx" || extension.ToLower() == ".zip" || extension.ToLower() == ".rar" || extension.ToLower() == ".brd")
                {
                    string fileName = GetFileNameFromPath(filePath);
                    fileName = SubstringFileName(bomFileType, fileName, "");
                    context.Response.Write(JsonConvert.SerializeObject(new { Status = Status.UnknownFile, data = dtData, headers = headers, excelSheets = excelSheets, extension = extension, fileName = fileName.Replace("SUBMITTED_BOM_", "").Replace("UPLOADED_BOM_", "").Replace("IMPORTED_BOM_", "") }));
                    return;
                }

                if (files.Count > 0 && (extension == ".xls" || extension == ".xlsx" || extension == ".ods") || checkCustFile == true && (extension == ".xls" || extension == ".xlsx" || extension == ".ods"))
                {

                    if (extension == ".ods")
                    {
                        ODSToDataTable excelSheet = new ODSToDataTable();
                        excelSheets = excelSheet.GetSheetList(filePath);
                    }
                    else
                    {
                        excelSheets = GetSheet(filePath);
                    }
                    //If exception occurred in raeding excel file
                    if (excelSheets == null)
                    {
                        context.Response.Write(JsonConvert.SerializeObject(new { Status = Status.UnknownFile, data = dtData, headers = headers, excelSheets = excelSheets }));
                        return;
                    }
                }

                dtData = ReadBOM(filePath, extension, isHeader, headerLine, slectedDelimiter, fixdlenthColumn, sheet);
                if (files.Count > 0 || forceColumnsMap || checkCustFile == true)
                {
                    Dictionary<string, string> mapColumns = null;
                    string delimeter = "";

                    dtData = FindMappingColumns(bomFileType, filePath, extension, dtData, out headerLine, out delimeter, slectedDelimiter, fixdlenthColumn, sheet, out mapColumns);
                    dtData = RemoveDblQuotes(extension, dtData);
                    headers = new List<object>();
                    headers.Add(mapColumns);
                    headers.Add(headerLine);
                    headers.Add(delimeter);
                }
                if (dtData != null && dtData.Rows.Count > 0)
                {
                    dtData = SupressEmptyColumns(dtData);
                }
                if (checkCustFile)
                {
                    if (!isMultipleExcelChange)
                    {
                        bool isBOMXmlExist = IsBOMXmlExist(GetFolderPath(entitynr), entitynr, bomFileType);
                        if (isBOMXmlExist)
                        {
                            string fileName = GetFileNameFromPath(filePath);
                            fileName = SubstringFileName(bomFileType, fileName, "");
                            if (!fileName.ToUpper().StartsWith("SUBMITTED_BOM_") && !fileName.ToUpper().StartsWith("UPLOADED_BOM_"))
                            {
                                context.Response.Write(JsonConvert.SerializeObject(new { Status = Status.BOMExist, data = dtData, headers = headers, excelSheets = excelSheets, extension = extension, fileName = fileName.Replace("SUBMITTED_BOM_", "").Replace("UPLOADED_BOM_", "").Replace("IMPORTED_BOM_", "") }));
                                return;
                            }
                        }
                    }
                }
                context.Response.Write(JsonConvert.SerializeObject(new { data = dtData, headers = headers, excelSheets = excelSheets, extension = extension }));
            }
            catch (Exception ex)
            {
                context.Response.Write(JsonConvert.SerializeObject(new { Status = Status.Error, Msg = ex.Message }));
            }
        }
        public string SubstringFileName(string bomFileType, string fileName, string extension)
        {
            string bomStartWith = bomFileType == "CM" ? "BOM_" : "CPL_";
            if (fileName != "" && fileName.StartsWith(bomStartWith))
            {
                fileName = fileName.Substring(4) + extension;
            }
            return fileName;
        }
        public static string GetFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);
            fileName = Path.GetFileNameWithoutExtension(fileName);
            string regexSearch = new string(Path.GetInvalidFileNameChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            fileName = r.Replace(fileName, "");
            if (fileName.Length > 50)
            {
                fileName = fileName.Substring(0, 50);
            }
            return fileName + extension;
        }
        public string GetFileNameFromPath(string filePath)
        {
            return Path.GetFileName(filePath) ?? "";
        }

        private DataTable FindMappingColumns(string bomFileType, string filePath, string extension, DataTable dtData, out int headerLine, out string delimeter, string selectedDelimiter, string fixdlenthColumn, string sheet, out Dictionary<string, string> mapColumn)
        {
            headerLine = 0;
            mapColumn = null;
            delimeter = "";

            Dictionary<int, Dictionary<string, string>> mapColumns = new Dictionary<int, Dictionary<string, string>>();
            Dictionary<string, int> mappedColWeight;
            string sysColumn = "";
            string cellValue = "";
            int columnWeight = 0;

            //Find csv header and delimeter
            if (extension != ".xls" && extension != ".xlsx" && extension != ".ods")
            {
                delimeter = selectedDelimiter;
                if (delimeter == "" || delimeter == "none")
                {
                    delimeter = FindDelimiter(dtData).ToString().Replace("\0", "");
                }

                if (delimeter != "")
                {
                    dtData = ReadBOM(filePath, extension, "no", 0, delimeter.ToString(), fixdlenthColumn, sheet);
                }
            }

            
            //EC09WebApp.API.gd gd = new EC09WebApp.API.gd();
            int GetMappedColumnsByWeight = bomFileType == "CPL" ? 2 : 1;
   
            DataTable dtSysCol = sparrowFeApp.core.Common.GetMappedColumnsByWeight(GetMappedColumnsByWeight);
            if (dtSysCol.Rows.Count == 0)
            {
                return dtData;
            }

            DataTable dtScan = null;
            if (dtData.Rows.Count > 30)
            {
                dtScan = dtData.AsEnumerable().OrderBy(x => x["index"]).Take(50).CopyToDataTable();
            }
            else
            {
                dtScan = dtData;
            }

            foreach (DataRow drScan in dtScan.Rows)
            {
                mapColumn = new Dictionary<string, string>();
                mappedColWeight = new Dictionary<string, int>();

                foreach (DataColumn scanCol in dtScan.Columns)
                {
                    if (scanCol.ColumnName.ToLower() == "index" || scanCol.ColumnName.ToLower() == "isvisible")
                    {
                        continue;
                    }

                    cellValue = drScan[scanCol].ToString().ToLower().Trim();
                    if (cellValue == "")
                    {
                        continue;
                    }

                    cellValue = Regex.Replace(cellValue.ToLower(), @"\s+", "_");
                    foreach (DataRow drBOMCol in dtSysCol.Rows)
                    {
                        if (cellValue == drBOMCol["user_colname"].ToString())
                        {
                            sysColumn = drBOMCol["name"].ToString();
                            columnWeight = (int)drBOMCol["weight"];

                            //Get old sys column from cell value
                            string oldSysColumn = mapColumn.FirstOrDefault(x => x.Value == cellValue).Key;
                            if (oldSysColumn != null && mappedColWeight.ContainsKey(oldSysColumn) && columnWeight > mappedColWeight[oldSysColumn])
                            {
                                //Remove old sys column
                                mapColumn.Remove(oldSysColumn);
                                mappedColWeight.Remove(oldSysColumn);

                                //Remove if new sys column already exist in dictionary
                                if (mapColumn.ContainsKey(sysColumn))
                                {
                                    mapColumn.Remove(sysColumn);
                                    mappedColWeight.Remove(sysColumn);
                                }

                                //Add new sys column
                                mapColumn.Add(sysColumn, cellValue);
                                mappedColWeight.Add(sysColumn, columnWeight);
                            }
                            else if (!mapColumn.ContainsValue(cellValue) && !mapColumn.ContainsKey(sysColumn))
                            {
                                mapColumn.Add(sysColumn, cellValue);
                                mappedColWeight.Add(sysColumn, columnWeight);
                            }
                        }
                    }
                }

                if (mapColumn.Count > 0)
                {
                    mapColumns.Add(Convert.ToInt32(drScan["index"]), mapColumn);
                }
            }

            //Find the best match header row
            mapColumn = new Dictionary<string, string>();
            foreach (var data in mapColumns)
            {
                if (mapColumn.Count < data.Value.Count)
                {
                    headerLine = data.Key;
                    mapColumn = data.Value;
                }
            }

            mapColumn = mapColumn.Count == 0 ? null : mapColumn;

            //Read data again with header line.
            if (headerLine != 0)
            {
                if (extension == ".xls" || extension == ".xlsx" || extension == ".ods")
                {
                    dtData = ReadBOM(filePath, extension, "yes", headerLine, "", "", sheet);
                }
                else
                {
                    if (delimeter != "")
                    {
                        dtData = ReadBOM(filePath, extension, "yes", headerLine, delimeter.ToString(), fixdlenthColumn, sheet);
                    }
                }
            }

            return dtData;
        }

        private char FindDelimiter(DataTable data)
        {
            char delimeter = '\0';

            List<char> delimiters = new List<char> { ',', ';', '|', '\t' };
            List<Delimiter> delimiterCounts;
            List<Delimiter> maxDelimiterCounts = new List<Delimiter>();
            Delimiter delimiterObj;

            int delimiterWeight = 0;

            //Generate list of max repeated delimited per row
            foreach (DataRow dr in data.Rows)
            {
                delimiterCounts = new List<Delimiter>();

                foreach (char c in delimiters)
                {
                    delimiterWeight = 0;
                    foreach (DataColumn column in data.Columns)
                    {
                        if (column.ColumnName.ToLower() == "index" || column.ColumnName.ToLower() == "isvisible")
                        {
                            continue;
                        }

                        string col = Regex.Replace(dr[column].ToString(), "\".*\"", "");

                        delimiterWeight += col.ToCharArray().Count(t => t == c);
                    }

                    if (delimiterWeight != 0)
                    {
                        delimiterObj = new Delimiter() { Separator = c, Count = delimiterWeight };
                        delimiterCounts.Add(delimiterObj);
                    }
                }

                if (delimiterCounts.Count != 0)
                {
                    Delimiter maxDelimiter = delimiterCounts.First(x => x.Count == delimiterCounts.Max(i => i.Count));
                    if (maxDelimiter != null)
                    {
                        maxDelimiterCounts.Add(maxDelimiter);
                    }
                }
            }

            //Find delimiter based on used of same amount in every row.
            if (maxDelimiterCounts.Count != 0)
            {
                int sameFrequencyCount = maxDelimiterCounts.GroupBy(x => x.Count).OrderByDescending(x => x.Count()).First().Key;
                delimiterObj = maxDelimiterCounts.Find(x => x.Count == sameFrequencyCount);
                delimeter = delimiterObj.Separator;
            }

            return delimeter;
        }

        private DataTable ReadBOM(string filePath, string extension, string isHeader, int headerLine, string separator, string fixdlenthColumn, string sheet)
        {
            DataTable dtData = null;
            string conStr = string.Empty;
            switch (extension)
            {
                case ".xls":
                    conStr = string.Format(Excel07ConString, filePath, isHeader);
                    dtData = ReadDataFromExcel(conStr, headerLine, filePath, sheet);
                    break;
                case ".xlsx":
                    conStr = string.Format(Excel07ConString, filePath, isHeader);
                    dtData = ReadDataFromExcel(conStr, headerLine, filePath, sheet);
                    break;
                case ".csv":
                    dtData = ReadDataFromCSV(filePath, isHeader, separator, headerLine);
                    break;
                case ".ods":
                    dtData = ReadDataFromODS(filePath, headerLine, sheet);
                    break;
                default:
                    dtData = ReadDataFromText(filePath, isHeader, separator, headerLine, fixdlenthColumn);
                    break;
            }

            return dtData;
        }

        private DataTable ReadDataFromCSV(string fname, string isHeader, string separator, int headerLine)
        {
            try
            {
                DataTable dtData = TextToDataSet.ConvertCSVtoDataTable(fname, headerLine, isHeader, separator);
                dtData = FilterData(dtData, headerLine);
                return dtData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable ReadDataFromExcel(string conStr, int headerLine, string filePath, string sheet)
        {
            try
            {
                DataTable dtData = ExcelToDataTable(filePath, headerLine, sheet);
                dtData = FilterData(dtData, headerLine);
                return dtData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable ReadDataFromText(String fname, string isHeader, string separator, int headerLine, string fixdlenthcolumn)
        {
            try
            {
                if (fname == "")
                {
                    return null;
                }
                DataTable dtData = TextToDataSet.ConvertTextFiletoDataTable(fname, isHeader, "TextFile", separator, headerLine, fixdlenthcolumn);
                dtData = FilterData(dtData, headerLine);
                return dtData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private DataTable ReadDataFromODS(String filepath, int headerLine, string sheet)
        {
            try
            {
                ODSToDataTable datatable = new ODSToDataTable();
                DataTable dtData = datatable.ReadODSFile(filepath, headerLine, sheet);
                dtData = FilterData(dtData, headerLine);
                return dtData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private DataTable SetColumnName(DataTable table)
        {
            for (int index = 0; index < table.Columns.Count; index++)
            {
                string name = table.Columns[index].ColumnName.ToLower();
                System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
                name = TextToDataSet.GetColumnName(name);
                if (name == "")
                {
                    name = "Column" + index;
                    table.Columns[index].ColumnName = name;
                }
            }
            return table;
        }

        private DataTable SupressEmptyColumns(DataTable dtSource)
        {
            //the DataTable is dynamic, loop threw each col and threw each row to
            // determine if that column is empty.

            System.Collections.ArrayList columnsToRemove = new System.Collections.ArrayList();

            foreach (DataColumn dc in dtSource.Columns)
            {
                if (dc.ColumnName.ToLower() == "index") continue;

                bool colEmpty = true;
                foreach (DataRow dr in dtSource.Rows)
                {
                    if (Convert.ToString(dr[dc.ColumnName]) != string.Empty)
                    {
                        colEmpty = false;
                    }
                }

                if (colEmpty == true)
                {
                    columnsToRemove.Add(dc.ColumnName);
                }
            }

            //remove all columns that are empty
            foreach (string columnName in columnsToRemove)
            {
                dtSource.Columns.Remove(columnName);
            }

            return dtSource;
        }
        private List<WorkSheet> GetSheet(string filePath)
        {
            List<WorkSheet> excelSheets = new List<WorkSheet>();
            IWorkbook workbook = null;
            try
            {
                if (IsImpersonation())
                {
                    using (FileStream outFile = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite))
                    {
                        workbook = WorkbookFactory.Create(outFile);
                    }
                    for (int i = 0; i < workbook.NumberOfSheets; i++)
                    {
                        WorkSheet objSheet = new WorkSheet();
                        objSheet.name = workbook.GetSheetAt(i).SheetName;
                        objSheet.number = i;
                        excelSheets.Add(objSheet);
                    }
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                workbook = null;
            }
            return excelSheets;
        }
        public DataTable RemoveDblQuotes(string extension, DataTable dtData)
        {
            if (dtData != null)
            {
                if (extension != ".xls" || extension != ".xlsx"  || extension != ".ods")
                {
                    for (int i = 0; i < dtData.Rows.Count; i++)
                    {
                        DataRow row = dtData.Rows[i];
                        for (int j = 0; j < row.ItemArray.Length; j++)
                        {
                            string ItemArrayValue = row.ItemArray[j].ToString();
                            dtData.Rows[i][j] = ItemArrayValue.Replace("\"", "");
                        }
                    }
                }
            }
            return dtData;
        }
    }



    class Delimiter
    {
        public char Separator { get; set; }
        public int Count { get; set; }
    }

    public class WorkSheet
    {
        public string name { get; set; }
        public int number { get; set; }
    }
}
