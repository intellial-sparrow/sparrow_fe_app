﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CMBomImport.aspx.cs" Inherits="sparrowFeApp.shop.assembly.CmBomImport" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BOM Import</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="resources/all.min.css?v=0.2.9" rel="stylesheet" />
    <style>
        .ec-modal {
            font-size: 14px;
        }

            .ec-modal .btn-warning {
                margin-left: 0px;
            }
    </style>
</head>
<body class="bom-body">
    <form id="form1" runat="server">
        <div class="ec-modal confirmModal hide" id="modal" style="position: fixed; z-index: 3; top: 0; padding-top: 15px; width: 100%; height: 100%; background-color: #000; background-color: rgba(0,0,0,.4)">
            <div class="ec-content" style="position: relative; background-color: #fefefe; margin: auto; padding: 10px; border: 1px solid #888; width: 35%; height: 140px; box-shadow: 0 4px 8px 0 rgba(0,0,0,.2),0 6px 20px 0 rgba(0,0,0,.19)">
                <div class="ec-header" style="padding: 2px 16px;">
                    <span class="ec-close" style="float: right; font-size: 28px; font-weight: 700; cursor: pointer;">×</span>
                    <h2></h2>
                </div>
                <div class="ec-body" style="height: 50%; margin-top: 15px;">
                    <div>
                        <span id="confirmFileMsg" runat="server"></span>
                    </div>
                </div>
                <div class="ec-footer" style="padding-right: 22px; float: right;">
                    <input type="button" class="btn btn-sm btn-warning bom_reload" value="Reload and overwrite	BOM" id="btnOverwrite" runat="server" />
                    <span id="btnCancel" class="btn btn-sm btn-warning bom_btnuploaddata" runat="server">Cancel</span>
                </div>
            </div>
        </div>
        <div id="blockLoading" class="divBlockLoading">
        </div>
        <div class="loaderBG hide"></div>
        <div class="pageloader hide">
            <div style="display: inline-block;">
                <img src="/shop/images/Small_Loading.gif" />
            </div>
            <div style="display: inline-block; vertical-align: top;">
                <span id="ctl00_lblLoading" runat="server">Loading...</span>
            </div>
        </div>

        <div class="header md-header">
            <div class="bom-step stepMultipleSheet hide">
                <div class="input-group ig-block">
                    <h3 class="title" runat="server" id="h3BomWorkSheet">Select the BOM worksheet</h3>
                </div>
                <div class="singleLine">

                    <div class="input-group">
                        <div>
                            <select id="drpWorkShee" style="width: 185px; height: 27px;"></select>
                        </div>
                        <div>
                            <div class="txtTitle" style="margin-top: 5px; padding-top: 1px;" runat="server" id="divMulWorkSheet">Multiple worksheets found, please select the worksheet that contains the part information.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bom-step step1">
                <div class="input-group ig-block">
                    <h3 class="title" runat="server" id="h3BomDataFile">Select your BOM data file</h3>
                </div>
                <div class="singleLine">
                    <div class="input-group browse">
                        <div class="form-control file-caption ">
                            <div class="file-caption-name" runat="server" id="divNoFileChoosen">No file chosen</div>
                        </div>
                        <div class="input-group-btn">
                            <div tabindex="1" class="btn btn-sm btn-primary btn-file" style="display: inherit; width: auto;">
                                <span class="hidden-xs" runat="server" id="spnBrowse">Browse</span>
                                <input id="fileUploader" type="file" class="file fileUploader" />
                            </div>
                        </div>
                        <div style="display: none;">
                            <div class="txtTitle" style="margin-top: 5px; padding-top: 1px;" runat="server" id="divAccepFormat">Acceptable formats: XLS, XLSX, CSV, tab delimited.</div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="bom-step step2 hide">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <div class="input-group ig-block">
                                <h3 class="title" runat="server" id="h3ChooseFileType">Choose the file type that best describes your data</h3>
                            </div>
                            <div class="input-group ig-block">
                                <input type="radio" name="fileType" class="radio" id="rdoDelimited" value="Delimited" checked />
                                <label for="rdoDelimited" class="radio" runat="server" id="lblelimited">Delimited - Characters such as commas or tabs separate each field.</label>
                            </div>
                            <div class="input-group ig-block">
                                <input type="radio" name="fileType" class="radio" id="rdoFixedWidth" value="FixedWidth" />
                                <label for="rdoFixedWidth" class="radio" runat="server" id="lblFixedWidth">
                                    Fixed width - Each column has fixed width in characters,<br />
                                    &nbsp&nbsp&nbsp you have to manually define separator (in first 3 row). See video on the right.</label>
                            </div>
                        </td>
                        <td>
                            <div id="divdelimiters" class="singleLine">
                                <h3 class="title" runat="server" id="h3ChooseDel">Choose delimiters your data contains</h3>
                                <div class="input-group delimiter">
                                    <input type="radio" name="delimiter" class="radio" id="rdoTab" value="Tab" />
                                    <label for="rdoTab" class="radio" runat="server" id="lblTab">Tab</label>
                                </div>
                                <div class="input-group delimiter">
                                    <input type="radio" name="delimiter" class="radio" id="rdoSemicolon" value="Semicolon" />
                                    <label for="rdoSemicolon" class="radio" id="lblSemicolon" runat="server">Semicolon</label>
                                </div>
                                <div class="input-group delimiter">
                                    <input type="radio" name="delimiter" class="radio" id="rdoPipe" value="Pipe" />
                                    <label for="rdoPipe" class="radio" runat="server" id="lblPipe">Pipe</label>
                                </div>
                                <div class="input-group delimiter">
                                    <input type="radio" name="delimiter" class="radio" id="rdoComma" value="Comma" />
                                    <label for="rdoComma" class="radio" runat="server" id="lblComma">Comma</label>
                                </div>
                                <div class="input-group delimiter">
                                    <input type="radio" name="delimiter" class="radio" id="rdospace" value="space" />
                                    <label for="rdospace" class="radio" runat="server" id="lblSpace">Space</label>
                                </div>
                                <div class="input-group delimiter">
                                    <input type="radio" name="delimiter" class="radio" id="rdoOther" value="Other" />
                                    <label for="rdoOther" class="radio" runat="server" id="lblOther">Other</label>
                                    <input type="text" id="txtOther" class="form-control txtOther" placeholder="Eg: \n" autocomplete="off" runat="server" />
                                </div>

                            </div>
                            <div id="ImgGif1" class="input-group ig-block Fixedwidthimage" style="display: none;">
                                <img src="resources/bg_mlArrow_right1.gif?v=0.0.1" style="height: 40px; padding-left: 60px; padding-top: 30px;" />
                            </div>

                            <div id="ImgGiftext" class="input-group ig-block Fixedwidthimage" style="display: none;">
                                <label for="rdoFixedWidth" class="radio" runat="server" id="lblseeHow">See how it's done</label>
                            </div>
                        </td>
                        <td>
                            <div id="ImgGif" class="input-group ig-block Fixedwidthimage" style="display: none;">
                                <img src="resources/Fixed-width_op.gif" style="height: 93px" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="bom-step step3 hide">
                <div class="input-group ig-block">
                    <h3 class="title" runat="server" id="h3ChooseScope">Choose scope of your data </h3>
                </div>
                <div class="">
                    <div class="input-group">
                        <div class="txtTitle" runat="server" id="divDataHeader">Data Header row</div>
                        <input type="text" id="headerRow" class="form-control headerRow range" placeholder="Header row" autocomplete="off" runat="server" />
                    </div>
                    <div class="input-group">
                        <div class="txtTitle" runat="server" id="divDataStartFrom">Data start from row</div>
                        <input type="text" id="startRow" class="form-control startRow range" placeholder="Start row" autocomplete="off" runat="server" />
                    </div>
                    <div class="input-group">
                        <div class="txtTitle" runat="server" id="divDataEnds">Data ends at row</div>
                        <input type="text" id="endRow" class="form-control endRow range" placeholder="End row" autocomplete="off" runat="server" />
                    </div>
                </div>
                <div class="txtTitle" style="margin-top: 5px; padding-top: 1px;" runat="server" id="divDataHeaderAll">
                    <i>Data Header row</i> - normally contains name of your data column, <i>Data start from row</i> - is the first row where your data begins, <i>Data ends at row</i> - is the last row where your data ends.
                </div>

            </div>

            <div class="bom-step step4 hide">
                <div class="input-group ig-block">
                    <h3 class="title" runat="server" id="divChooseColumns">Choose columns to be used</h3>
                </div>
                <div class="txtTitle" style="margin-top: 5px; padding-top: 1px;" runat="server" id="divMapping">
                    Mapping of at least one column out of MPN, SPN, Description or Package name. And, one out of Quantity and Reference designators is mandatory.
                </div>
            </div>
            <div class="step-footer" style="width: 100%;">
                <div class="messageInfo txtTitle">
                    <div id="errorMsg" class="message error"></div>
                </div>
                <div id="stepActionbar" style="display: none;" class="input-group ig-block">
                    <input style="margin-bottom: 7px;" type="button" class="btn btn-sm btn-back btn-warning btnBack" value="Back" id="btnBack" runat="server" /><%--background:url('resources/upload.png');background-repeat:no-repeat;--%>
                    <input style="margin-bottom: 7px;" type="button" class="btn btn-sm btn-warning btnNext" value="Next" id="btnNext" runat="server" />
                    <%--<a href="#" class="alink" style="display:none;font-size: 14px;position:relative;top: -2px;text-decoration:underline;" id="editBOM" runat="server">Edit BOM</a>--%>
                    <div style="display: inline; float: right;">
                        <input style="display: none; margin-bottom: 7px; margin-right: 9px !important;" type="button" class="btn btn-sm btn-back bom_btntextedit" value="Next" id="editBOM" runat="server" />
                        <input style="margin-bottom: 7px; margin-right: 28px; float: right;" title="Delete" type="button" class="btn btn-sm btn-back bom_btndelete" value="Delete" id="btnDelete" runat="server" />
                    </div>
                    <span id="rowSelectMsg" class="error" style="display: none; font-size: 13px; margin-bottom: 7px; margin-left: 12px;">Please select at least one row.</span>
                </div>
            </div>
        </div>
        <div class="bom-step stepTextBoxData hide" style="margin-top: -6px;">
            <div class="input-group ig-block">
                <h5 class="title" runat="server" id="h5EnterDataPdfOrDoc" style="padding: 5px 10px; background-color: #eaeaea; font-weight: normal;">Uploaded file contains non-readable data, Put your data here in the textbox below and continue. </h5>
                <%--<a id="downloadFile" href="#">Download file</a>--%> <%--href="/shop/assembly/uploadHandler.ashx?isDownload=true"--%>
            </div>
        </div>
        <div class="arrow_box center_x_mm_th">
            <div class="mapHeader" runat="server" id="divUnitOfMeasureX">Unit of measurement</div>
            <div class="option">
                <div class="mapValueBlock">
                    <select class="center_x">
                        <option value="Inch" runat="server" id="optInch_X">Inch</option>
                        <option value="MM" selected runat="server" id="optMM_X">MM</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="arrow_box center_y_mm_th">
            <div class="mapHeader" runat="server" id="divUnitOfMeasureY">Unit of measurement</div>
            <div class="mapValueBlock">
                <div>
                    <select class="center_y">
                        <option value="Inch" runat="server" id="optInch_Y">Inch</option>
                        <option value="MM" selected runat="server" id="optMM_Y">MM</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="arrow_box mpn_th">
            <div class="mapHeader" runat="server" id="divMPN">MPN</div>
            <div class="mounting distinctOption">
            </div>
        </div>
        <div class="arrow_box Mounting_th">
            <div class="mapHeader" runat="server" id="divDefineMount">Define mounting</div>
            <div class="mounting distinctOption">
            </div>
        </div>
        <div class="arrow_box supply_th">
            <div class="mapHeader" runat="server" id="divsupply">Supply</div>
            <div class="supply distinctOption">
            </div>
        </div>
        <div class="arrow_box layer_th">
            <div class="mapHeader" runat="server" id="divDefineLayers">Define Layers</div>
            <div class="layerOp distinctOption">
            </div>
        </div>
        <div class="arrow_box lead_time_th">
            <div class="mapHeader" runat="server" id="divDefinePeriod">Period in</div>
            <div class="option">
                <div class="mapValueBlock">
                    <select class="lead_time">
                        <option value="Days" selected runat="server" id="optDays">Days</option>
                        <option value="Weeks" runat="server" id="optWeeks">Weeks</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="divTable hide">
            <table id="bomTable" class="importTable">
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="margin-top: 6px;">
            <textarea id="textAreaPdf" style="display: none; border: 1px solid #666; width: 99.5%; font-family: 'monospace',monospace !important;"></textarea>
            <%--height:438px;--%>
            <input type="button" runat="server" id="btnSubmitPdfDoc" class="btn btn-sm btn-warning bom_save" style="display: none; margin-top: 7px; margin-left: 46%;" />
            <input type="button" runat="server" id="btnCancelPdfDoc" value="Cancel" class="btn btn-sm btn-warning bom_cancel" style="display: none; margin-top: 7px;" />
        </div>
        <asp:HiddenField ID="txtData" runat="server" />
    </form>

    <script src="resources/all.min.js?v=0.1.6"></script>
    <script src="resources/CMBomImport.js?v=1.7"></script>
    <%--<script src="../JS/Common.js?v=0.3.8"></script>--%>
    <script src="../../JS/Common.js"></script>
</body>
</html>
