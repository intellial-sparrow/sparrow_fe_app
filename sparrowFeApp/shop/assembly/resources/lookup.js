﻿var PartLookup = {
    customerData: null,
    serverData: '',
    captions: '',
    Messages: {
        addEmptyTable: 'Please, search a part to add in your BOM.',
        editEmptyTable: 'Sorry, no matches found.'
    },
    init: function () {
        this.events();
        this.onLoad();
    },
    events: function () {
        $('.matchedParts tbody').on('click', '.chkPart', function () {
            $('tr input[type="checkbox"]').not($(this)).prop('checked', false);
            if (this.checked) {
                $('tr.selected').removeClass('selected');
                $(this).parent().parent().addClass('selected');
            }
            else {
                $(this).parent().parent().removeClass('selected');
            }
        });

        $('#btnSearch').on('click', function (e) {
            var mpn = $.trim($('#txtMPN').val());
            var spn = $.trim($('#txtSPN').val());
            var pack = $.trim($('#txtPackage').val());
            var desc = $.trim($('#txtDes').val());

            if (mpn == "" && spn == "" && pack == "" && desc == "") {
                return;
            }

            var searchNode = [{
                index: 1,
                mpn: mpn,
                spn: spn,
                package: pack,
                descr: desc
            }];

            PartLookup.searchPart(searchNode, false, function (parts) {
                PartLookup.setMatchParts(parts);
            });
        });

        $('#btnAddPart').on('click', function (e) {
            PartLookup.sendData();
        });

        $('#btnUpdatePart').on('click', function (e) {
            PartLookup.sendData();
        });
    },
    sendData: function () {
        var table = $('.matchedParts').DataTable();
        var data = table.rows('.selected').data()[0];
        if (!data) {
            Core.showMessage("ftMsg", true, PartLookup.captions.RequiredPartInBOM, 7);
            return;
        }

        if (data) {
            var matchedPart = data[this.colIndex.MPN];            
            var manufacturer = data[this.colIndex.Manufacturer];
            var description = data[this.colIndex.Description];            
            var ipcPackage = data[this.colIndex.IPC];            
            var dataSheetURL = $('.matchedParts tbody .selected .td-sheet a').attr('href') || '';
            //var dataSheetURL = data[this.colIndex.Datasheet]
            
            var price = parseFloat(data[this.colIndex.Price]);
            var stock = parseInt(data[this.colIndex.Stock]);
            var isVerified = data[this.colIndex.Verified] == "Yes" ? true : false;
            var mounting = data[this.colIndex.Mounting];

            console.log(data);
            console.log("matchedPart:" + matchedPart);
            console.log("ipcPackage:" + ipcPackage);
            console.log("description:" + description);
            console.log("manufacturer:" + manufacturer);
            console.log("dataSheetURL:" + dataSheetURL);
            console.log("isVerified:" + isVerified);
            console.log("price:" + price);
            console.log("stock:" + stock);
            console.log("mounting:" + mounting);

            var partData = [];
            if (this.serverData.EditMode) {
                if (this.customerData.mpn != matchedPart) {
                    partData.push({ index: 0, mpn: matchedPart, package: "", descr: description });
                    partData.push({ index: 1, mpn: this.customerData.mpn, package: this.customerData.package, descr: this.customerData.description });
                }                
            }
            else {
                partData.push({ index: 0, mpn: matchedPart, package: "", descr: description });
            }

            if (partData.length > 0) {
                this.sendUpdatedPart(partData);
            }            

            parent.updatePart(matchedPart, ipcPackage, description, manufacturer, dataSheetURL, isVerified, price, stock);
        }
    },
    onLoad: function () {        
        this.serverData = $.parseJSON($('#txtData').val());
        debugger
        PartLookup.captions = this.serverData.Captions;
        this.loadMatchParts();
        if (this.serverData.EditMode) {            
            this.setCustomerPart();
        }
        else {
            $('.match-parts').css('top', '55px');
        }

        //Set table height
        //var offset1 = $(".match-parts").offset();
        //var offset2 = $("#part-footer").offset();
        //var height = offset2.top - offset1.top;
        //$('.match-parts').css('height', '' + height + 'px');
    },
    loadMatchParts: function () {
        var mpn = Core.getParameterByName('MP') || "";
        var search = false;
        var searchOnMPN = false;
        var searchData;

        if (mpn != "") {
            search = true;
            searchOnMPN = true;
            searchData = [{ mpn: mpn }]
        }
        else {            
            var mpn = Core.getParameterByName('mpn') || "";
            var spn = Core.getParameterByName('spn') || "";
            var pack = Core.getParameterByName('pack') || "";
            var desc = Core.getParameterByName('desc') || "";

            if (mpn != "" || spn != "" || pack != "" || desc != "") {
                search = true;
                searchData = [{
                    index: 1,
                    mpn: mpn,
                    spn: spn,
                    package: pack,
                    descr: desc
                }];
            }
        }

        if (search) {            
            this.searchPart(searchData, searchOnMPN, function (parts) {
                PartLookup.setMatchParts(parts);
                if (searchOnMPN) {
                    $('.chkPart').click();
                }
            });
        }
        else {
            this.updatePartTable([], [], this.Messages.addEmptyTable);
        }
    },
    setMatchParts: function (parts) {
        var edaData = [];
        var octaData = [];                

        for (i = 0; i < parts.length; i++) {
            if (parts[i].code == 0) {
                Core.showMessage("hdMsg", true, PartLookup.captions.ErrorInSearchingParts, 10);
                return;
            }

            if (parts[i].code == 1) {
                continue;
            }

            if (parts[i].is_generic == 1) {
                continue;
            }            
            
            var rowData = this.getRowData(parts[i]);

            if (parts[i].octopart == "1") {                
                octaData.push(rowData);
            }
            else {
                edaData.push(rowData);
                
                var genericPart = $.grep(parts, function (element, index) {
                    return element.is_generic == 1 && element.rel_id == parts[i].rel_id;
                });

                for (j = 0; j < genericPart.length; j++) {
                    var genericPartRowData = this.getRowData(genericPart[j])
                    edaData.push(genericPartRowData);
                }                
            }
        }

        this.updatePartTable(edaData, octaData, this.Messages.editEmptyTable);
    },
    getRowData: function (part) {
        var mpn, gnpImg, desc, ipcName, price, stock, verified, supplier, mounting, datasheet, image;

        mpn = part.mpn;

        gnpImg = "";
        if (part.is_generic == 1) {
            gnpImg = "<img src='resources/gen-comp-found.png' alt='Generic part is available' />"
        }
        else {
            gnpImg = "<img src='resources/phy-comp-found.png' />"
        }

        manuf = part.manuf;
        desc = part.descr;
        ipcName = part.ipc_name;
        datasheet = $.trim(part.data_url);
        if (datasheet == "#") {
            datasheet = "";
        }

        if (datasheet != "") {            
            datasheet = "<a href=" + datasheet + " target='_blank'><img src='resources/pdf.png' alt='Datasheet' /></a>";
        }

        image = $.trim(part.image);
        if (image == "#") {
            image = "";
        }
        if (image != "") {            
            image = "<img src='" + image + "' />"
        }

        price = part.price;
        stock = part.stock;
        verified = part.verified == "0" ? "No" : "Yes";
        supplier = part.supplier;
        mounting = part.mounting;

        var rowData = [PartLookup.getCheckbox(), gnpImg, part.is_generic, mpn, datasheet, image, mounting, ipcName, price, stock, manuf, desc, supplier, verified];
        return rowData;
    },
    colIndex: {
        GNPImg: 1,
        IsGenericPart: 2,
        MPN: 3,        
        Datasheet: 4,
        PartImg: 5,
        Mounting: 6,        
        IPC: 7,
        Price: 8,
        Stock: 9,
        Manufacturer: 10,
        Description: 11,        
        Supplier: 12,        
        Verified: 13        
    },
    updatePartTable: function (edaData, octaData, emptyTableMsg) {
        var columns = [
            { title: "", className: "td-chk" },
            { title: "", className: "td-gpn-img" },
            { title: "", className: "td-generic" },
            { title: "MPN", className: "td-mpn" },            
            { title: "Datasheet", className: "td-sheet" },
            { title: "Image", className: "td-img" },
            { title: "Type", className: "td-mnt" },
            { title: "IPC", className: "td-ipc" },
            { title: "Price", className: "td-price" },
            { title: "Stock", className: "td-stock" },
            { title: "Manufacturer", className: "td-manuf" },
            { title: "Description", className: "td-desc" },                        
            { title: "Supplier", className: "td-sup" },            
            { title: "Verified", className: "td-ver" },            
        ];

        var columnDefs = [
            {
                render: function (data, type, row) {
                    var mpnCell = '<div><div><span class="cell-mpn">' + row[PartLookup.colIndex.MPN] + '</span>';
                    if (row[PartLookup.colIndex.Manufacturer] != "") {
                        mpnCell += ' - <span>' + row[PartLookup.colIndex.Manufacturer] + '</span></div>';
                    }
                    if (row[PartLookup.colIndex.Description] != "") {
                        mpnCell += '<div class="cell-desc">' + row[PartLookup.colIndex.Description] + '</div>';
                    }
                    //mpnCell += '<div class="cell-mnf">';
                    //if (row[PartLookup.colIndex.Datasheet] != "") {
                    //    mpnCell += '<span class="cell-pdf"><a href="' + row[PartLookup.colIndex.Datasheet] + '" target="_blank"><img src="resources/pdf.png" alt="Datasheet" /></a></span>&nbsp;';
                    //    mpnCell += '<span><a href="' + row[PartLookup.colIndex.Datasheet] + '" target="_blank">' + row[PartLookup.colIndex.Manufacturer] + '</a></span>'
                    //}
                    mpnCell += '</div>';                    

                    return mpnCell;
                },
                targets: this.colIndex.MPN
            },
            {
                visible: false,
                targets: [this.colIndex.Manufacturer, this.colIndex.Supplier, this.colIndex.Description, this.colIndex.IsGenericPart, this.colIndex.Verified]
            }
        ];

        $('#tableMatchParts').DataTable({
            destroy: true,
            data: edaData,
            paging: false,
            ordering: false,
            searching: false,
            info: false,
            language: {
                emptyTable: emptyTableMsg
            },
            columns: columns,
            columnDefs: columnDefs,
            rowCallback: function (row, data, index) {                
                if (data.length > 1) {
                    if (data[2] == "1") {
                        $(row).addClass('tr-gnp');
                    }
                }
            }
        });
        
        $('#tableOctaMatchParts').DataTable({
            destroy: true,
            data: octaData,
            paging: false,
            ordering: false,
            searching: false,
            info: false,
            language: {
                emptyTable: emptyTableMsg
            },
            columns: columns,
            columnDefs: columnDefs
        });

        $('.match-parts').show();
    },
    getTestData: function () {
        if (window.location.href.indexOf("http://172.27.212.25") == -1) {
            return false;
        }
        var data = [];        
        for (i = 0; i < 100; i++) {
            data.push([
                this.getCheckbox(i), "mpn" + i, "Manufacturer" + i, "desc" + i, "ipcName" + i, "#", "supplier" + i, 1.5, 15, "Yes"]);
        }
        return data;
    },
    setCustomerPart: function () {        
        var mpn = Core.getParameterByName('mpn') || "";
        var manuf = Core.getParameterByName('manuf') || "";
        var desc = Core.getParameterByName('desc') || "";
        var pack = Core.getParameterByName('pack') || "";
        var suppl = Core.getParameterByName('suppl') || "";
        var spn = Core.getParameterByName('spn') || "";
        var libr = Core.getParameterByName('libr') || "";
        var val = Core.getParameterByName('val') || "";        
        var mnt = Core.getParameterByName('mnt') || "";
        var comm = Core.getParameterByName('comm') || "";
        var url = Core.getParameterByName('url') || "";
        if (url != "") {
            url = '<a href="' + url + '" target="_blank" >' + url + '</a>';
        }

        this.customerData = {
            mpn: mpn,
            package: pack,
            description: desc
        }

        var data = [[mpn, manuf, desc, pack, suppl, spn, libr, val, mnt, comm, url]]
        $('#tableCustPart').DataTable({
            data: data,
            paging: false,
            ordering: false,
            searching: false,
            info: false,
            columns: [
               { title: "MPN" },
               { title: "Manufacturer" },
               { title: "Description" },
               { title: "Package" },
               { title: "Supplier" },
               { title: "SPN" },
               { title: "Library" },
               { title: "Value" },
               { title: "Mounting" },
               { title: "Comment" },
               { title: "URL" },
            ]
        });
    },
    searchPart: function (searchData, searchOnMPN, callback) {
        var url = "/shop/assembly/CMBomImport.aspx/SearchPart";
        var postData = {
            searchData: JSON.stringify(searchData),
            searchOnMPN: searchOnMPN
        };

        Core.post(url, postData, function (data) {
            if (data.Status == 0) {                
                Core.showMessage("ftMsg", true, PartLookup.captions.ErrorInLoadingParts, 20);
                return;
            }

            if (callback) {
                callback(data.Parts);
            }
        });
    },    
    sendUpdatedPart: function (data) {
        var url = "/shop/assembly/CMBomImport.aspx/SendChangedPart";
        var postData = {
            data: JSON.stringify(data)
        };

        Core.post(url, postData, function (data) {
            if (data.Status == 0) {
                Core.showMessage("ftMsg", true, PartLookup.captions.ErrorInLoadingParts, 20);
                return;
            }
        });
    },
    getAllParam: function () {
        var vars = {}, hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getCheckbox: function () {
        return ' <input type="checkbox" class="chkPart">';
    }
}

$(function () {
    PartLookup.init();
});