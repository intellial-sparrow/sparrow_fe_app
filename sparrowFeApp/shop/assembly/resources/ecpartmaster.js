﻿var ecpartmaster = {
    templates: {
        prefix: "",
        suffix: " ago",
        seconds: "less than a minute",
        minute: "about a minute",
        minutes: "%d minutes",
        hour: "about an hour",
        hours: "about %d hours",
        day: "a day",
        days: "%d days",
        month: "about a month",
        months: "%d months",
        year: "about a year",
        years: "%d years"
    },
    events: function () {
        $(document).keydown(function (event) {
            if (event.keyCode == 27) { 
                $(".modal").modal("hide");
            }
        });
        $('#btnsearch').click(function () {
            ecpartmaster.searchbar("");
        });
        $('body').click(function () {
            $('#master-list-detail').hide();
        });
        $("#txtsearch").keydown(function (event) {
            if (event.which == 13) {
                event.preventDefault();
                ecpartmaster.searchbar("");
            }
        });
        $("#txtMasterBoardQty").keypress(function (event) {
            return /\d/.test(String.fromCharCode(event.keyCode)); 
        });
        $('#btnsaveList').click(function (e) {
            ecpartmaster.saveList(e);
        });
        $('#createList').click(function () {
            $("#btnCreateListSaveBOM").hide();
            $("#btnsaveList").show();
            $('#list-detail').hide();
            $('#modalcreatelist').modal('show');
        });
        $('.ddlcategorycss').on('change', function (e) {
            $("#ddlallcategorynew").css("color", "");
            $(".ddlcategorycss").css("color", "");
            var value = e.target.value;
            $("#ddlallcategorynew").val(value);
        });
        $('.rohschk').click(function () {
            if (this.checked) {
                $(".rohschk").attr("disabled", true);
                $(this).removeAttr("disabled");
            }
            else {
                $(".rohschk").removeAttr("disabled");
            }
        });
        $('#spmodal').click(function () {
            var txtval = $('#txtsearch').val();
            if (txtval != "") {
                $('#txtmdsearch').val(txtval);
            }

            var ddlvalue = $('#ddlallcategory option:selected').text();
            if (ddlvalue != "" && ddlvalue != "All categories") {
                $("#ddlcategory2 option").attr('selected', true);
                $('#ddlcategory2 option').filter(function () {
                    return ($(this).text() == ddlvalue);
                }).prop('selected', true);
                $(".ddlcategorycss").css("color", "");
            }
            else {
                $(this).css("color", "#555");
            }
            $('#myModal').modal('show');
        });
        $('#btnmdsearch').click(function () {
            ecpartmaster.redirecturlmdparm();
        });
        $(".chksides").click(function () {
            $(".chksides").prop("checked", false);
            $(this).prop("checked", true);
            var sides = $(this).val();
            if (sides=="1") {
                $("#imgassemblysides").attr("src", "/shop/assembly/resources/bottom.svg");
            }
            else if (sides == "2") {
                $("#imgassemblysides").attr("src", "/shop/assembly/resources/both.svg");

            }
            else {
                $("#imgassemblysides").attr("src", "/shop/assembly/resources/top.svg");
            }
        });
    },
    search_auto_comp: function () {
        var options = {
            url: "/shop/assembly/resources/autocomplete_part_numbers.js?v=6",
            getValue: "mpn",
            template: {
                type: "description",
                fields: {
                    description: "class"
                }
            },
            list: {
                match: {
                    enabled: false
                },
                onChooseEvent: function () {
                    var value = $("#txtsearch").getSelectedItemData();
                    ecpartmaster.searchbar(value.mpn)
                }
            },
            theme: "plate-dark",
        };
        try{
            $("#txtsearch").easyAutocomplete(options);
        }
        catch (err) {
            console.log(err);
        }
        
    },
    saveList: function (e) {
        var listname = $("#txtListName").val();
        if (listname == "") {
            $("#txtListName").addClass("list-txt-validate");
            $(".create-list-error").removeClass("hide");
            setTimeout( 
                function(){
                    $("#txtListName").removeClass("list-txt-validate");
                    $(".create-list-error").addClass("hide");
                }, 3000);
            return;
        }
        var url = "/shop/assembly/ecparts.aspx/SaveList";
        var userid = $("#ctl00_hdnmasteruserid").val();
        var boardqty = $("#txtMasterBoardQty").val();
        var sides = 0;
        $('input[name="chksides"]:checked').each(function () {
            sides = this.value;
        }); 
        var postData = {
            cId: userid,
            listName: $("#txtListName").val(), 
            qty: boardqty,
            assemblyside: sides
        }
        Core.post(url, postData, function (result) {
            var data = result;
            if (data.Status == 0) {
                return;
            }
            else {
                window.location.href = "/shop/assembly/bom.aspx?sessionid=" + data.sessionId;
                $("#ctl00_lblTotalList").text(data.Totallist);
                $('#modalcreatelist').modal('hide');
                $("#ctl00_masterdivList").html(data.MasterList);
                ecpartmaster.timeAgo();
                //location.reload(true);
                //var markup = '<tr><td><a lid="' + data.ListId + '" class="part-save"><i class="fa fa-star"></i>' + data.ListName + '</a></td></tr>';
                //$("#tblList tbody").append(markup);
                //$("#txtListName").val("");
                //$("#txtlistDescription").val("");
                //$('#modalcreatelist').modal('hide');
                //location.reload(true);
            }
        });
        e.preventDefault()
        e.stopPropagation();
    },
    searchbar: function (mpn) {
        var category = $('#ddlallcategorynew').children("option:selected").attr('catattr');
        if (mpn != "") {
            $('#txtsearch').val(mpn);
        }
        var txtvalue = $('#txtsearch').val();
        if (typeof category === "undefined") {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?keywords=" + txtvalue;
            }
            else {
                window.location.href = "/shop/assembly/ecparts";
            }
        }
        else {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?category=" + category + "&keywords=" + txtvalue;
            }
            else {
                window.location.href = "/shop/assembly/ecparts/?category=" + category;
            }
        }
    },
    showlist: function (element) {
        //$("#partId").val(element.attributes['id'].value);
        $("#master-list-detail").show('show');
        var t = $(element).offset().top;
        var l = $(element).offset().left;

        //$("#master-list-detail").css({ top: (t - element.offsetTop - 35), left: (l - element.offsetLeft) });
        //$("#master-list-detail").css({ top: "10px", right: "2px" });
        //$("#master-list-detail").css({ top: t + element.offsetTop + 20, right:200});
        event.stopPropagation();
    },
    redirecturlmdparm: function () {
        var category = $('#ddlallcategorynew').children("option:selected").attr('catattr');
        var txtvalue = $('#txtmdsearch').val();
        var extraparm = "";
        $(".chkbox").find("input[type=checkbox]").each(function () {
            if ($(this).is(':checked') == true) {
                exts = 1;
                var key = $(this).val();
                extraparm += "&" + key + "=" + 1;
            }
        });

        $(".chkbox").find("input[type=radio]").each(function () {
            if ($(this).is(':checked') == true) {
                exts = 1;
                var key = $(this).val();
                extraparm += "&" + key + "=" + 1;
            }
        });
        if (typeof category === "undefined") {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?keywords=" + txtvalue + "" + extraparm;
            }
            else {
                extraparm = extraparm.substring(1);
                window.location.href = "/shop/assembly/ecparts/?" + extraparm;
            }
        }
        else {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?category=" + category + "&keywords=" + txtvalue + "" + extraparm;
            }
            else {
                extraparm = extraparm.substring(1);
                window.location.href = "/shop/assembly/ecparts/?category=" + category + "&" + extraparm;
            }
        }
    },
    timeAgo: function (selector) {
        var elements = document.getElementsByClassName('timeago');
        for (var i in elements) {
            var $this = elements[i];
            if (typeof $this === 'object') {
                $this.innerHTML = ecpartmaster.timer($this.getAttribute('datetime'));
            }
        }
        setTimeout(ecpartmaster.timeAgo, 60000);
    },
    timer: function (time) {
        if (!time) return;
        time = time.replace(/\.\d+/, ""); // remove milliseconds
        time = time.replace(/-/, "/").replace(/-/, "/");
        time = time.replace(/T/, " ").replace(/Z/, " UTC");
        time = time.replace(/([\+\-]\d\d)\:?(\d\d)/, " $1$2"); // -04:00 -> -0400
        time = new Date(time * 1000 || time);

        var now = new Date();
        var seconds = (((now.getTime() + now.getTimezoneOffset() * 60000) - time) * .001) >> 0;
        var minutes = seconds / 60;
        var hours = minutes / 60;
        var days = hours / 24;
        var years = days / 365;

        return ecpartmaster.templates.prefix + (
        seconds < 45 && ecpartmaster.template('seconds', seconds) ||
            seconds < 90 && ecpartmaster.template('minute', 1) ||
            minutes < 45 && ecpartmaster.template('minutes', minutes) ||
            minutes < 90 && ecpartmaster.template('hour', 1) ||
            hours < 24 && ecpartmaster.template('hours', hours) ||
            hours < 42 && ecpartmaster.template('day', 1) ||
            days < 30 && ecpartmaster.template('days', days) ||
            days < 45 && ecpartmaster.template('month', 1) ||
            days < 365 && ecpartmaster.template('months', days / 30) ||
            years < 1.5 && ecpartmaster.template('year', 1) ||
            ecpartmaster.template('years', years)) + ecpartmaster.templates.suffix;
    },
    template: function (t, n) {
        return ecpartmaster.templates[t] && ecpartmaster.templates[t].replace(/%d/i, Math.abs(Math.round(n)));
    },
    init: function (event) {
        ecpartmaster.search_auto_comp();
        ecpartmaster.events();
        ecpartmaster.timeAgo();
    }
}
$(document).ready(function () {
    ecpartmaster.init();
});
var Core = {
    getParameterByName: function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    post: function (url, postData, callback, contentType) {
        processData = true;
        data = postData;
        if (contentType || contentType === undefined) {
            contentType = "application/json; charset=utf-8";
            data = JSON.stringify(postData)
        }
        else {
            processData = false;
            contentType = false
        }
        $('.pageloader, .loaderBG').removeClass('hide');
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            contentType: contentType,
            processData: processData,
            success: function (data) {
                $('.pageloader,.loaderBG').addClass('hide');
                $('.btnNext').removeClass('hide');
                if (callback) {
                    if (url.toLowerCase().indexOf(".ashx") > 0) {
                        callback($.parseJSON(data));
                    }
                    else {
                        callback($.parseJSON(data.d));
                    }
                }
            },
            error: function (errorData) {
                $('.pageloader,.loaderBG').addClass('hide');
                console.log(errorData);
                if (callback) {
                    callback({ Status: 0 });
                }
            }
        });
    },
    showMessage: function (id, error, msg, time) {
        $('#' + id).text(msg).removeClass()

        if (error) {
            $('#' + id).html(msg).addClass('error').show();
        } else {
            $('#' + id).html(msg).addClass('success').show();
        }

        if (time) {
            setTimeout(function () {
                $('#' + id).hide()
            }, time * 1000);
        }
    }
}