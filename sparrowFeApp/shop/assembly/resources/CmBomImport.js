﻿var BOM = {
    attached: false,
    bomTable: null,
    bomDuplicateTable: null,
    basketNr: "",
    currentStep: 1,
    delimiter: 'none',
    tempDelimiter: 'none',
    duplicateData: new Object(),
    mapDropdownOptions: '',
    endRow: 0,
    excel: false,
    headerRow: 0,
    mappedColumnName: null,
    nextStep: 0,
    previousStep: 0,
    startRow: 0,
    serverData: null,
    fixdlenthcolumn: '',
    errorMSGInterval: 10,
    selectedSheet: '',
    multipleExcelSheet: false,
    captions: '',
    removeRows: [],
    fileExtension: '',
    previousStepFLColumn: "",
    clearAll: function () {
        BOM.basketNr = '';
        BOM.endRow = 0;
        BOM.headerRow = 0;
        BOM.delimiter = 'none';
        BOM.startRow = 0;
        BOM.excel = false;
        BOM.selectedSheet = '';
        BOM.previousStepFLColumn = '';
        BOM.multipleExcelSheet = false;
        $('#rdoDelimited').prop('checked', true);
        $('input[type=radio][name=delimiter]').prop('checked', false);
        $('.range').val('');
        $('#txtOther').val('');
        $('divtxtNuberOfC').addClass('hide');
        $('.Fixedwidthimage').hide();
        $('#divdelimiters').show();
        //$('#bomTable').removeClass('fixedLenthFont');
        $('.btnBack').removeClass('hide');
        $('.stepMultipleSheet').addClass('hide');
        $('.stepTextBoxData').addClass('hide');
        //$('#textAreaPdf').val('');
    },
    autoMapColumns: function () {
        if (BOM.mappedColumnName == null) {
            return;
        }
        jQuery.each(BOM.mappedColumnName, function (key, val) {
            if (key.indexOf("ignore_") != -1) {
                $("select[col='" + val + "']").val("ignore");
            }
            else if (key.indexOf("unass_") != -1) {
                $("select[col='" + val + "']").val("none");
            }
            else if (key.indexOf("spn_") != -1) {
                $("select[col='" + val + "']").val("spn");
            }
            else {
                $("select[col='" + val + "']").val(key).attr('oldval', key);
            }

            if (key.indexOf("unass_") == -1 && key.indexOf("ignore_") == -1) {
                $("select[col='" + val + "']").parent().addClass('drpMapped');
                $("select").find('option[value="' + key + '"]').addClass('opt-map');
            }
            if (key.toLowerCase() == "supply") {
                var mapLink = '&nbsp;<img src="/shop/images/editCell.png?v=0.1" class="mapColumn ' + key + '_th"/>';
                $('#bomTable thead tr').find('.' + val).append(mapLink);
                var headerIndex = $('#bomTable tbody tr.selectedHeader td:first-child').find('span').data('index');
                BOM.addDistinctMapValue(key.toLowerCase(), val, headerIndex);
                var ele = $('#bomTable thead tr').find('.' + val).find('.mapColumn');
                BOM.openMapBox(ele, null);
            }
        });
    },
    addOption: function (classname, options) {
        var opts = '';
        jQuery.each(options, function (key, obj) {
            opts += '<option value="' + $.trim(obj.toLowerCase()) + '">' + key + '</option>';
        });
        var option =
           '<div class="option ">' +
              '<div class="mapTitle">' + classname + '</div>' +
              '<div class="mapValue">' +
              '<select class="' + classname + '">' +
                  '<option value="">-- none --</option>' + opts
        '</select>' +
        '</div>' +
        '</div>';
        return option;
    },
    addDistinctMapValue: function (column, headerClass, headerIndex) {
        var optionArray = [];
        if (column == "Mounting" || column == "supply") {
            $("td." + headerClass).each(function (index) {
                if (index > headerIndex) {
                    var value = "";
                    if ($(this).parent('tr').hasClass('selectedRow')) {
                        value = $.trim($(this).text());
                    }
                    if (jQuery.inArray(value, optionArray) == -1) {
                        optionArray.push(value);
                    }
                }
            });
            optionArray = $.grep(optionArray, function (n) {
                return (n);
            });
            if (column == "Mounting") {
                $('.arrow_box.Mounting_th .mounting').empty();
                $('.distinctOption').removeAttr('style');
                if (optionArray.length > 3) {
                    $('.distinctOption').css({ 'margin-right': '-6px' });
                }
                var mount = { "TH": "TH", "THD": "THD", "TH+SMD": "TH+SMD", "SMD": "SMD" };
                jQuery.each(optionArray, function (key, obj) {
                    var option = BOM.addOption(obj, mount);
                    $('.arrow_box.Mounting_th .mounting').append(option);
                });
            } if (column == "supply")
            {
                $('.arrow_box.supply_th .supply').empty();
                $('.distinctOption').removeAttr('style');
                if (optionArray.length > 3) {
                    $('.distinctOption').css({ 'margin-right': '-6px' });
                }
                var supply = { "Assembler": "0", "Customer": "1", "Not placed": "2"};
                jQuery.each(optionArray, function (key, obj) {
                    var option = BOM.addOption(obj, supply);
                    $('.arrow_box.supply_th .supply').append(option);
                });
            }
        }
    },
    editableCell: function () {
        if ($(this).children().first().is("input") || $(this).children().first().is("select") || $(this).children().first().is("span")) {
            return;
        }

        var originalContent = $(this).text();
        $(this).addClass("cellEditing");
        $(this).html("<input type='text' value='" + originalContent + "' />");
        $(this).children().first().focus();

        $(this).children().first().keypress(function (e) {
            if (e.which == 13) {
                $(this).parent().removeClass("cellEditing");
                var newContent = $(this).val();
                $(this).parent().text(newContent);
            }
        });

        $(this).children().first().blur(function () {
            var td = $(this).parent();
            td.removeClass("cellEditing");
            var newContent = $(this).val();
            td.text(newContent);
            if (td.hasClass('incorectData')) {
                td.removeClass('incorectData');
                //td.append("<img src='/shop/images/editCell.png' class='img-edit'/>");
            }
        });
    },
    events: function () {
        //$('#bomTable tbody').on('mouseenter', 'tr td.td-index', function (e) {
        //    $(this).find('span').hide();
        //    $(this).find('.imgDelete').show();
        //})
        //$('#bomTable tbody').on('mouseleave', 'tr td.td-index', function (e) {
        //    $(this).find('span').show();
        //    $(this).find('.imgDelete').hide();
        //});
        //$(document).on('click', '#bomTable tbody tr td .imgDelete', function () {
        //    if (confirm(BOM.captions.ConfirmMessage)) {  //"Are you sure you want to delete this?"
        //        var srNo = $(this).closest('tr').children().eq(0).text();
        //        BOM.loadTable(false, false, false, false, false, false, srNo);
        //    }
        //    else {
        //        return false;
        //    }
        //});
        $('#btnDelete').on('click', function () {
            var isCheckBox = false;
            var removeRows = "";
            $('#bomTable tbody tr td.td-index').each(function () {
                var sno = $(this).find('span').text();
                var row = $(this);
                if (row.find('input[type="checkbox"]').is(':checked')) {
                    isCheckBox = true;
                    removeRows = removeRows + sno + ',';
                }
            });

            if (isCheckBox) {
                if (confirm(BOM.captions.ConfirmMessage)) {
                    BOM.loadTable(false, false, false, false, false, false, removeRows.slice(0, -1));
                }
                else {
                    return false;
                }
            }
            else {
                //Core.showMessage("errorMsg", true, 'Please select at least one row.', BOM.errorMSGInterval);
                $('#rowSelectMsg').show();
                $('#rowSelectMsg').delay(10000).fadeOut();
                return false;
            }
        });
        $('#aUploadFile').on('click', function () {
            BOM.clearAll();
            $('#fileUploader').val('');
            $('.file-caption-name').text('No file chosen');
            BOM.currentStep = BOM.steps.uploadData;
            $('#stepActionbar').hide();
            $('.step2').addClass('hide');
            $('.step1').removeClass('hide');
            $('.stepTextBoxData').addClass('hide');
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            $('#textAreaPdf').val('');
            $('#textAreaPdf').hide();
            $('#blockLoading').hide();
        });
        $('.fileUploader').on('change', function (e) {
            $('.btnBack').val(BOM.captions.Back); //"Back"
            $('.btnBack').removeClass('btnbig');
            BOM.uploadFile(e);
        });
        $('#drpWorkShee').on('change', function (e) {
            BOM.headerRow = 0;
            BOM.selectedSheet = $("option:selected", this).text();
            BOM.loadTable(null, false, true, false, false, true);
        });
        $('.btnNext').on('click', function (e) {
            BOM.showNextStep($(this));
            e.preventDefault();
        });
        $('#downloadFile').on('click', function (e) {
            $('#downloadFile').attr("href", "/shop/assembly/uploadHandler.ashx?isDownload=true&entitynr=" + BOM.basketNr + "&bomfiletype=CM");
        });
        $('.btnBack').on('click', function (e) {
            BOM.showPreviousStep($(this));
            e.preventDefault();
        });

        $('#bomTable').on('click', '.mapColumn', function (e) {
            BOM.openMapBox(this, e);
            e.stopPropagation();
        });
        // If an event gets to the body
        $("body").click(function (e) {
            if ($(e.target).hasClass("dropdown") || $(e.target).hasClass("btnNext")) { return; }
            $(".arrow_box").hide();
        });
        $('#btnSubmitPdfDoc').on('click', function (e) {
            $('.stepTextBoxData').removeClass('hide');
            if ($('#textAreaPdf').val() == '') {
                $('.step1').addClass('hide');
                $('.step2').removeClass('hide');
                $('#textAreaPdf').focus();
                return;
            }

            var selectedDelimiter = BOM.delimiter;
            var selectedpreviousStepFLColumn = BOM.previousStepFLColumn;
            BOM.clearAll();
            BOM.delimiter = selectedDelimiter;
            $('input[type=radio][name=delimiter]').each(function () {
                if (BOM.delimiter != 'none') {
                    if ($(this).val() == BOM.delimiter) {
                        $(this).prop("checked", true);
                    }
                }
            });
            if (BOM.delimiter.toLowerCase() == "fixedwidth") {
                BOM.previousStepFLColumn = selectedpreviousStepFLColumn;
                $('#rdoFixedWidth').prop("checked", true);
                $('#divtxtNuberOfC').removeClass('hide');
                BOM.delimiter = 'FixedWidth';
                $('.Fixedwidthimage').show();
                $('#divdelimiters').hide();
            }
            $('.stepTextBoxData').addClass('hide');
            BOM.basketNr = Core.getParameterByName('id');
            BOM.attached = true;
            BOM.loadTable(BOM.fixedLenthColumnMap, true, false, false, true);


            //BOM.currentStep = BOM.steps.chooseDelimiter;
            //BOM.showDelimiterStep();
            //BOM.currentStep = BOM.showDelimiterStep();
            //BOM.showNextStep();
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            //$('#textAreaPdf').val('');
            $('#textAreaPdf').hide();
            $('#blockLoading').hide();
        });
        $('#btnCancelPdfDoc').on('click', function (e) {
            BOM.clearCheckBox();
            if (BOM.fileExtension.toLowerCase() == 'pdf' || BOM.fileExtension.toLowerCase() == 'doc' || BOM.fileExtension.toLowerCase() == 'docx' || BOM.fileExtension.toLowerCase() == 'zip' || BOM.fileExtension.toLowerCase() == 'rar' || BOM.fileExtension.toLowerCase() == 'brd') {
                //BOM.clearAll();
                $('#stepActionbar').hide();
                $('.step2').addClass('hide');
                $('.step1').removeClass('hide');
                BOM.currentStep = BOM.steps.uploadData;
                BOM.multipleExcelSheet = false;
            }
            else {
                $('.divTable').removeClass('hide');
            }
            $('#fileUploader').val('');
            $('.file-caption-name').text('No file chosen');
            $('.stepTextBoxData').addClass('hide');
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            $('#textAreaPdf').val('');
            $('#textAreaPdf').hide();
            $('#blockLoading').hide();
        });
        $('#editBOM').on('click', function () {
            $("#textAreaPdf").height($(window).height() - 230);
            $('#blockLoading').show();
            $(".divTable").addClass('hide');
            $('.stepTextBoxData').addClass('hide');
            $('#textAreaPdf').focus();
            $('#textAreaPdf').show();
            $('#btnSubmitPdfDoc').val(BOM.captions.Save);
            $('#btnCancelPdfDoc').val(BOM.captions.Cancel);
            $('#btnSubmitPdfDoc').show();
            $('#btnCancelPdfDoc').show();
            if (BOM.delimiter == "FixedWidth") {
                BOM.previousStepFLColumn = "";
                $('span.SpanIndex.fwseparaterfirst.fwseparater').each(function (index, element) {
                    var value = $(element).attr('data-charnumber');
                    BOM.previousStepFLColumn = BOM.previousStepFLColumn + value + "/";
                });
                BOM.previousStepFLColumn + "0";
            }
            BOM.loadTable(false, false, false, true);
        });
        // Prevent events from getting pass .popup
        $(".arrow_box").click(function (e) {
            e.stopPropagation();
        });

        $(document).on('click', 'span.SpanIndex,span.SpanCol', function () {
            if ($(this).hasClass('fwseparater')) {
                var att = $(this).attr('data-charnumber');
                $('span[data-charnumber=' + att + ']').each(function () {
                    var value = $(this).text();
                    $(this).text(value.replace('|', ''));
                    $(this).removeClass('fwseparater');
                });
            }
            else {
                var att = $(this).attr('data-charnumber');
                $('span[data-charnumber=' + att + ']').each(function () {
                    var value = $(this).text();
                    $(this).text(value + '|');
                    $(this).addClass('fwseparater');
                });
            }
        });

        $('input[type=radio][name=fileType]').change(function (e) {
            if ($('input[type=radio][name=delimiter]').is(':checked')) {
                BOM.tempDelimiter = $("input[type=radio][name=delimiter]:checked").val();
            }
            var fileType = $(this).val();
            BOM.delimiter = 'none';
            if (fileType == 'FixedWidth') {
                $('input[type=radio][name=delimiter]').each(function () {
                    $(this).prop("checked", false);
                });
                //$('#bomTable').addClass('fixedLenthFont');;
                $('#divtxtNuberOfC').removeClass('hide');
                BOM.delimiter = 'FixedWidth';
                $('.Fixedwidthimage').show();
                $('#divdelimiters').hide();
                //$('#bomTable').addClass('fixedLenthFont');

            }
            else {
                if (BOM.tempDelimiter != 'none') {
                    $('input[type=radio][name=delimiter]').each(function () {
                        if ($(this).val() == BOM.tempDelimiter) {
                            BOM.delimiter = BOM.tempDelimiter;
                            $(this).prop("checked", true);
                        }
                    });
                }
                BOM.previousStepFLColumn = "";
                $('span.SpanIndex.fwseparaterfirst.fwseparater').each(function (index, element) {
                    var value = $(element).attr('data-charnumber');
                    BOM.previousStepFLColumn = BOM.previousStepFLColumn + value + "/";
                });
                BOM.previousStepFLColumn + "0";
                $('divtxtNuberOfC').addClass('hide');
                $('.Fixedwidthimage').hide();
                $('#divdelimiters').show();
                //BOM.loadTable(function () {
                //    BOM.detectCSVSeparater();
                //});
            }
            BOM.loadTable(BOM.fixedLenthColumnMap);
            e.preventDefault();
        });

        //Reload table on delimitor change
        $('input[type=radio][name=delimiter]').change(function (e) {
            BOM.delimiter = $(this).val();
            $('divtxtNuberOfC').addClass('hide');
            if (BOM.delimiter == 'Other') {
                var value = $.trim($('.txtOther').val());
                if (value != "") {
                    BOM.loadTable();
                }
            }
            else {
                $('.txtOther').val('');
                BOM.loadTable();
            }
            e.preventDefault();
        });

        $(".txtOther").blur(function (e) {
            var value = $(this).val();
            if (value.length == 0) {
                BOM.delimiter = 'none';
                BOM.loadTable();
            }
            else if (BOM.delimiter != value) {
                BOM.delimiter = value;
                BOM.loadTable();
            }
            e.preventDefault();
        });

        $(".txtOther").keyup(function (e) {
            var value = $(this).val();
            if (value.length == 0) {
                BOM.delimiter = 'none';
                BOM.loadTable();
            }
            else if (BOM.delimiter != value) {
                BOM.delimiter = value;
                BOM.loadTable();
            }
        });

        $(".txtOther").focus(function () {
            $('#rdoOther').prop('checked', true);
        });

        // Set Header row on out of focus
        //$('.headerRow').on('blur', function (e) {
        //    var row = parseInt($(this).val()) || 0;
        //    if (BOM.headerRow != row) {
        //        BOM.headerRow = parseInt($(this).val()) || 0;
        //        BOM.loadTable();
        //    }
        //    e.preventDefault();
        //});

        $('.headerRow').on('keyup', function (e) {
            if (e.keyCode == 13) {
                BOM.headerRowAction(e);
            }
        });
        $('.headerRow').on('blur', function (e) {
            BOM.headerRowAction(e);
        });
        //$(".startRow").blur(function (e) {
        //    $('.pageloader,.loaderBG').removeClass('hide');
        //    BOM.startRow = parseInt($(this).val()) || 0;
        //    BOM.selectedRows();
        //    e.preventDefault();
        //});

        $(".startRow").keyup(function (e) {            
            if (e.keyCode == 13) {
                BOM.startRowAction(e);
            }
        });
        $(".startRow").blur(function (e) {
            BOM.startRowAction(e);
        });
        //$(".endRow").blur(function (e) {
        //    $('.pageloader,.loaderBG').removeClass('hide');
        //    BOM.endRow = parseInt($(this).val()) || 0;
        //    BOM.selectedRows();
        //    e.preventDefault();
        //});

        $(".endRow").keyup(function (e) {
            $('.pageloader,.loaderBG').removeClass('hide');
            BOM.endRow = parseInt($(this).val()) || 0;
            BOM.startRow = parseInt($(".startRow").val()) || 0;
            BOM.selectedRows();
            BOM.validateInputData();
            e.preventDefault();
        });
        $('#bomTable').on('change', '.dropdown', function (e) {
            var selectedVal = this.value;
            BOM.getMappedColumn();

            if (selectedVal == "ignore") {
                $(this).parent().removeClass('drpMapped');
                return;
            }

            var oldVal = $(this).attr("oldVal");

              var userColumn = $.trim($(this).parent('td').attr('userColumn'));

            $('#bomTable tbody tr:first td select').each(function (index, element) {
                var value = $(this).val();

                $(this).find('option[value="' + oldVal + '"]').removeClass('opt-map');
                if (selectedVal != "none") {
                    $(this).find('option[value="' + selectedVal + '"]').addClass('opt-map');
                }

                if (value == selectedVal && selectedVal != "spn") {
                    $(this).val('');
                    $(this).parent().removeClass('drpMapped');

                    var cs = $.trim($(this).parent('td').attr('class'));
                    $('#bomTable thead tr .' + cs).find('.mapColumn').remove();
                }
            });

            var headerIndex = $('#bomTable tbody tr.selectedHeader td:first-child').find('span').data('index');
            BOM.addDistinctMapValue(selectedVal, userColumn, headerIndex);
            if (selectedVal == 'Mounting') {
                var mapLink = '&nbsp;<img src="/shop/images/editCells.png" class="mapColumn ' + selectedVal + '_th"/>';
                $('#bomTable thead tr').find('.' + userColumn).append(mapLink);
                var ele = $('#bomTable thead tr .' + userColumn).find('.mapColumn');
                BOM.openMapBox(ele, e);
            }
            if (selectedVal == 'supply') {
                var mapLink = '&nbsp;<img src="/shop/images/pencil_red.png" class="mapColumn ' + selectedVal + '_th"/>';
                $('#bomTable thead tr').find('.' + userColumn).append(mapLink);
                var ele = $('#bomTable thead tr .' + userColumn).find('.mapColumn');
                BOM.openMapBox(ele, e);
            }

            $(this).val(selectedVal);
            $(this).attr("oldVal", selectedVal);

            if (selectedVal == "none") {
                $(this).parent().removeClass('drpMapped');
            }
            else {
                $(this).parent().addClass('drpMapped');
            }

            e.stopPropagation();
        });

        //numeric text-box
        $(".range").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    },
    clearCheckBox: function () {
        $('#bomTable tbody tr td.td-index').each(function () {
            var row = $(this);
            if (row.find('input[type="checkbox"]').is(':checked')) {
                row.find('input[type="checkbox"]').removeAttr('checked');
            }
        });
    },
    saveBOM: function () {
        var isValidMap = BOM.getMappedColumn();

        if (!isValidMap) {
            Core.showMessage("errorMsg", true, BOM.captions.RequiredMappingColumns, BOM.errorMSGInterval);  //Select required mapping columns.
            return;
        }
        var searchData = [];
        var xmlData = [];
        var errors = [];

        $('#bomTable tbody').find('tr').each(function (index) {
            var row = $(this);
            row.find('.incorectData').removeClass('incorectData');
            if (row.hasClass('selectedRow')) {

                var data = BOM.getXMLNode(row);

                var error = BOM.validateRow(row, data.xmlNode);
                if (error.row != 0) {
                    errors.push(error);
                }

                xmlData.push(data.xmlNode);
                searchData.push(data.searchNode);
            }
        });

        if (errors.length > 0) {
            var rowNumbers = "";
            for (i = 0; i < errors.length; i++) {
                rowNumbers += errors[i].row + ",";
            }
            rowNumbers = rowNumbers.slice(0, -1);
            var msg = BOM.captions.RowNumber + rowNumbers + BOM.captions.RowNumberContainsInvalidData;  //Row number  +  contains invalid data. To select correct start row and end row click on back button.
            Core.showMessage("errorMsg", true, msg);
            return;
        }

        var url = "/shop/assembly/CMBomImport.aspx/SaveBOM";
        var postData = {
            entityNumber: BOM.basketNr,
            searchData: JSON.stringify(searchData),
            xmlData: JSON.stringify(xmlData),
            mapData: JSON.stringify(BOM.getSystemMappedCol()),
            noAlternatives: BOM.serverData.NoAlternatives
        }

        Core.post(url, postData, function (result) {
            var data = result;
            if (data.Status == 0) {
                Core.showMessage("errorMsg", true, BOM.captions.Error);  //Error occurred.
                console.log(data.Msg)
                return;
            }
            parent.bomLoaded(data.FileName);
            parent.hidePopup();
        });
    },
    getSystemMappedCol: function () {
        var data = [];
        jQuery.each(BOM.mappedColumnName, function (systemCol, userCol) {
            if (systemCol.indexOf("unass_") == -1 && systemCol.indexOf("ignore_") == -1) {

                if (systemCol.indexOf("spn_") != -1) {
                    systemCol = "spn";
                }

                var bomCol = $.grep(BOM.serverData.BOMCols, function (element, index) {
                    return element.name == systemCol;
                });
                data.push({ bomColId: bomCol[0].id, userCol: userCol })
            }
        });

        return data;
    },
    getXMLNode: function (row) {
        var searchNode = {
            index: 0,
            mpn: '',
            spn: '',
            package: '',
            descr: '',
            category: '',
        };

        searchNode.index = parseInt($.trim(row.find('.index').text()));
        var rowData = "";
        // var value = "";
        var xmlNode = {};
        xmlNode['index'] = searchNode.index;
        var suplyrData = {}
        $('.supply.distinctOption').find('.option').each(function (index) {
            var supplyType = $.trim($(this).find('.mapValue select option:selected').val());
            var supplyVal = $.trim($(this).find('.mapTitle').text().toLowerCase());
            suplyrData[supplyVal] = supplyType;
        });
        jQuery.each(BOM.mappedColumnName, function (columnName, mappedValue) {
            if (columnName == 'mpn') {
                searchNode.mpn = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());                
                xmlNode['mpn'] = searchNode.mpn;
            }
            else if (columnName == 'package') {                
                xmlNode['pack'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
                rowData += xmlNode['pack'] + " ";
                //searchNode.package = xmlNode['pack'];                
            }
            else if (columnName == 'description') {                
                xmlNode['desc'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
                rowData += xmlNode['desc'] + " ";
                //searchNode.descr = xmlNode['desc'];
            }
            else if (columnName == 'qty') {
                xmlNode.qty = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
                if (xmlNode.qty == "") {
                    xmlNode.qty = 0;
                }
            }
            else if (columnName == 'manufacturer') {
                xmlNode['manuf'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
            }
            else if (columnName == 'ref_des') {
                xmlNode['refdes'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());

                var refDesList = xmlNode['refdes'].replace(/\d+/g, '').toLowerCase().split(",");

                var category = "";
                refDesList.forEach(function (entry) {
                    if (entry === "r") {
                        category = "resistors";
                    }
                    else if (entry === "c") {
                        category = "capacitors";
                    }
                });
                xmlNode['cat'] = category;
                searchNode.category = category;
            }
            else if (columnName == 'supplier') {
                xmlNode['suppl'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
            }
            else if (columnName == 'library') {
                xmlNode['libr'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
                rowData += xmlNode['libr'] + " ";
            }
            else if (columnName == 'mounting') {
                xmlNode['mnt'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
            }
            else if (columnName == 'value') {
                xmlNode['val'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
                rowData += xmlNode['val'] + " ";
                // value = xmlNode['val'];
            }
            else if (columnName == 'comment') {
                xmlNode['comm'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
                rowData += xmlNode['comm'] + " ";
            }
            else if (columnName == 'url') {
                xmlNode['url'] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
            }
            else if (columnName.indexOf("unass_") != -1) {                
                xmlNode[columnName] = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
                //rowData += xmlNode[columnName] + " ";
            }
            else if (columnName.indexOf("spn_") != -1) {
                var spnValue = $.trim(row.find('.' + BOM.mappedColumnName[columnName] + '').text());
                xmlNode[columnName] = spnValue;

                //Only update spn if previous stored spn value is blank
                if (searchNode.spn == "") {
                    searchNode.spn = spnValue;
                }
            }
            else if (columnName == "supply") {
                var supply = $.trim(row.find('.' + BOM.mappedColumnName['supply'] + '').text());
                 var supply_value = "";
                 if (supply.toLowerCase() in suplyrData) {
                     supply_value = suplyrData[supply.toLowerCase()];
                    }
                 if (supply_value == "") {
                     supply_value='invalid';
                    }
                    xmlNode[columnName] = supply_value;
                
                //rowData += xmlNode[columnName] + " ";
            }
        });

        //Define part qty from refdesc if qty is not mapped
        jQuery.each(BOM.mappedColumnName, function (columnName, mappedValue) {
            if (columnName == 'ref_des') {
                if (xmlNode.qty === undefined || xmlNode.qty == 0 || xmlNode.qty == "") {
                    xmlNode.qty = xmlNode['refdes'].replace(/\d+/g, '').toLowerCase().split(",").length;
                }
            }
        });

        //Sanitize search row data for search
        var rowDataSanatize = BOMUtility.sanatizeKeyword(rowData, false);        

        searchNode.specs = JSON.stringify(rowDataSanatize.specs); 
        searchNode.descr = rowDataSanatize.keyword;
        
        return { xmlNode: xmlNode, searchNode: searchNode };
    },
    getIndexBySNo: function (sno) {
        var index = 0;
        $('#bomTable tbody tr').each(function () {
            var row = $(this).find('td:first-child');

            var rowIndex = parseInt($.trim(row.text())) || 0;
            if (rowIndex == sno) {
                index = parseInt(row.find('span').data('index'));
            }
        });
        return index;
    },
    getDelimiterName: function (delimiter) {

        var name = "none";
        var radioBtnId = "";
        if (delimiter == ",") {
            name = "Comma";
            radioBtnId = "#rdoComma";
        }
        else if (delimiter == "|") {
            name = "Pipe";
            radioBtnId = "#rdoPipe";
        }
        else if (delimiter == ";") {
            name = "Semicolon";
            radioBtnId = "#rdoSemicolon";
        }
        else if (delimiter == "\t") {
            name = "Tab";
            radioBtnId = "#rdoTab";
        }
        else if (delimiter != "") {
            name = delimiter;
        }

        if (radioBtnId != "") {
            $(radioBtnId).prop('checked', true);
        }

        return name;
    },
    detectCSVSeparater: function () {

        var comma = 0
        var pipe = 0
        var Semicolon = 0;
        var tab = 0
        var table = BOM.bomTable;
        var frow = false;
        table.column(1).data().each(function (value, index) {
            if (frow) {
                return false;
            }
            else if ($.trim(value) != null && $.trim(value) != '') {
                comma = value.split(",").length;
                pipe = value.split("|").length;
                Semicolon = value.split(";").length;
                tab = value.split('\t').length;
                frow = true;
            }
        });

        var delimiter;
        if (comma > pipe && comma > Semicolon && comma > tab) {
            $("#rdoComma").prop('checked', true);
            delimiter = "Comma";
        }
        else if (pipe > Semicolon && pipe > tab) {
            $("#rdoPipe").prop('checked', true);
            delimiter = "Pipe";
        }
        else if (Semicolon > tab) {
            $("#rdoSemicolon").prop('checked', true);
            delimiter = "Semicolon";
        }
        else if (tab > 1) {
            $("#rdoTab").prop('checked', true);
            delimiter = "Tab";
        }

        if (delimiter) {
            BOM.delimiter = delimiter;
            this.showDataSelectionStep(true)
            return true;
        }
        return false;
    },
    getMappedColumn: function () {
        var group1 = false;
        var group2 = false;
        BOM.mappedColumnName = {};
        var unassignedIndex = 0;
        var ignoreIndex = 0;
        var spnIndex = 0;

        $('.dropdown').each(function (index, element) {
            var value = $.trim($(this).val());
            if (value == "mpn" || value == "spn" || value == "description" || value == "package" || value == "qty" || value == "ref_des") {
                if (value == "mpn" || value == "spn" || value == "description" || value == "package") {
                    group1 = true;
                }
                if (value == "qty" || value == "ref_des") {
                    group2 = true;
                }
            }

            if (value != '' && value != null) {
                var column = $(this).attr('col');

                if (value == 'none') {
                    unassignedIndex++;
                    BOM.mappedColumnName['unass_' + unassignedIndex] = column;
                }
                else if (value == 'ignore') {
                    ignoreIndex++;
                    BOM.mappedColumnName['ignore_' + ignoreIndex] = column;
                }
                else if (value == 'spn') {
                    spnIndex++;
                    BOM.mappedColumnName['spn_' + spnIndex] = column;
                }
                else {
                    BOM.mappedColumnName[value] = column;
                }
            }
        });

        if (group1 && group2) {
            return true
        }
        return false;
    },
    getDropDown: function (key) {
        return '<select class="form-control dropdown" col="' + key + '">' + this.mapDropdownOptions + '</select>';
    },
    init: function () {
        BOM.events();
        BOM.onLoad();
    },
    loadTable: function (callback, forceColumnMap, checkCustFile, isReadData, isSaveTextArea, isMultipleExcelChange, deleteRowIndex) {
        var postData = new FormData();
        var fileUpload = $(".fileUploader").get(0);

        if (this.currentStep == this.steps.uploadData && BOM.multipleExcelSheet == false) {
            var files = fileUpload.files;
            for (var i = 0; i < files.length; i++) {
                postData.append(files[i].name, files[i]);
            }
        }
        var isHeader = BOM.headerRow > 0 ? 'YES' : 'NO';
        var headerRow;
        if (BOM.headerRow == 0) {
            headerRow = BOM.headerRow == 0 ? 0 : BOM.getIndexBySNo(BOM.headerRow);
        }
        else {
            headerRow = BOM.headerRow;
        }
        postData.append("isHeader", isHeader);
        postData.append("path", "/shop/assembly/uploads/");
        postData.append("hRow", headerRow);
        postData.append("separator", BOM.delimiter);
        postData.append("fixdlenthcolumn", BOM.fixdlenthcolumn);
        postData.append("entitynr", BOM.basketNr);
        postData.append("bomfiletype", "CM");
        postData.append("sheet", BOM.selectedSheet);
        if (checkCustFile) {
            postData.append("checkCustFile", checkCustFile);
        }
        if ($("#textAreaPdf").val().trim() != '') {
            postData.append("clientData", $("#textAreaPdf").val());
            $('#textAreaPdf').val('');
        }
        if (forceColumnMap) {
            postData.append("forceColumnsMap", forceColumnMap);
        }
        if (isReadData) {
            postData.append("isReadData", isReadData);
        }
        if (isMultipleExcelChange) {
            postData.append("isMultipleExcelChange", isMultipleExcelChange);
        }
        if (deleteRowIndex != undefined) {
            postData.append("deleteRowIndex", deleteRowIndex);
        }
        var url = "/shop/assembly/uploadHandler.ashx";

        Core.post(url, postData, function (result) {
            if (BOM.delimiter == 'none') {
                if (result.headers != null || result.headers != undefined) {
                    BOM.getDelimiterName(result.headers[2]);
                }
            }
            var checkCustomer = checkCustFile;
            checkCustFile = false;
            if (result.Status == 0) {
                Core.showMessage("errorMsg", true, BOM.captions.ErrorInFileUpload  + " " + result.Msg, BOM.errorMSGInterval); //Error occurred in file upload.
                $(".divTable").removeClass('hide');
                return;
            }
            if (result.fileExtension != '' || result.fileExtension != undefined) {
                BOM.fileExtension = result.fileExtension;
            }
            //if (result.Status == 1) {
            //    BOM.fileExtension = result.fileExtension;
            //}
            //if (result.Status == 2 && checkCustFile == false) {
            //    if (!confirm(BOM.captions.XMLExist)) { //'you have uploaded a bom'
            //        return;
            //    }
            //}
            if (result.Status == 3 && checkCustomer == undefined) {
                BOM.fileExtension = 'pdf';
                $('#stepActionbar').show();
                $("#divTable").hide();
                BOM.showNextStep();
                $('.stepTextBoxData').removeClass('hide');
                $('#btnSubmitPdfDoc').val(BOM.captions.Save);
                $('#btnCancelPdfDoc').val(BOM.captions.Cancel);
                $('#btnSubmitPdfDoc').show();
                $('#btnCancelPdfDoc').show();
                $('#textAreaPdf').show();
                $('#textAreaPdf').focus();
                $('#blockLoading').show();
                return;
            }
            if (result.Status == 4) {
                if (result.data.length > 0) {
                    BOM.fileExtension = 'xls';
                    $("#divTable").hide();
                    $('#btnSubmitPdfDoc').val(BOM.captions.Save);
                    $('#btnCancelPdfDoc').val(BOM.captions.Cancel);
                    $('#btnSubmitPdfDoc').show();
                    $('#btnCancelPdfDoc').show();
                    $('#textAreaPdf').show();
                    $('#textAreaPdf').val(result.data);
                    $('#textAreaPdf').focus();
                }
                return;
            }
            if (result.Status == 5) {
                $('#fileUploader').click();
                return;
            }

            BOM.showConfirmDialog(result, callback, isSaveTextArea, checkCustomer, deleteRowIndex);
        }, false)
    },

    showConfirmDialog: function (result, callback, isSaveTextArea, checkCustomer, deleteRowIndex) {
        if (result.Status == 2 || (result.Status == 3 && checkCustomer == true)) {
            if (result.fileName) {
                var confirmMsgText = $('#confirmFileMsg').text();
                confirmMsgText = confirmMsgText.replace('##FileName', result.fileName);
                $('#confirmFileMsg').text(confirmMsgText);
            }
            $('.confirmModal').removeClass('hide');
        }
        else {
            BOM.confirmCallback(result, callback, isSaveTextArea, deleteRowIndex);
        }
        $('#btnOverwrite').click(function () {
            $('.confirmModal').addClass('hide');
            if (result.extension.toLowerCase() == ".pdf" || result.extension.toLowerCase() == ".doc" || result.extension.toLowerCase() == ".docx" || result.extension.toLowerCase() == ".zip" || result.extension.toLowerCase() == ".rar" || result.extension.toLowerCase() == ".brd") {
                BOM.fileExtension = 'pdf';
                $('#stepActionbar').show();
                $("#divTable").hide();
                BOM.showNextStep();
                $('.stepTextBoxData').removeClass('hide');
                $('#btnSubmitPdfDoc').val(BOM.captions.Save);
                $('#btnCancelPdfDoc').val(BOM.captions.Cancel);
                $('#btnSubmitPdfDoc').show();
                $('#btnCancelPdfDoc').show();
                $('#textAreaPdf').show();
                $('#textAreaPdf').focus();
                $('#blockLoading').show();
                return;
            }
            BOM.confirmCallback(result, callback, isSaveTextArea);
        });
        $('#btnCancel').on('click', function () {
            $('.confirmModal').addClass('hide');
            $('#fileUploader').click();
            return;
        });
        $('.ec-close').on('click', function () {
            $('.confirmModal').addClass('hide');
            return;
        });

    },
    confirmCallback: function (result, callback, isSaveTextArea, deleteRowIndex) {
        if (result.data.length > 0) {
            BOM.setTable(result.data);

            $('#bomTable tbody tr').each(function () {
                var row = parseInt($.trim($(this).find('td:first-child').text())) || 0;
                if (row == 0) {
                    $(this).addClass('hide');
                    return;
                }
            });

            BOM.selectedRows(false);

            $(".divTable").removeClass('hide');
            $('#uploadFile').val("Reload");
            $('.sno').text('');
        }
        if (result.excelSheets != null && result.excelSheets.length > 0) {
            BOM.excel = true;
        }
        if (result.excelSheets != null && result.excelSheets.length > 1) {
            //BOM.selectedSheet = result.excelSheets[0].name;
            BOM.multipleExcelSheet = true;
            var opts = '';
            $.each(result.excelSheets, function (index, sheet) {
                if (BOM.selectedSheet == sheet.name) {
                    opts += '<option  value="' + sheet.number + '" selected>' + sheet.name + '</option>';
                }
                else {
                    opts += '<option  value="' + sheet.number + '">' + sheet.name + '</option>';
                }
            });
            $('#drpWorkShee').find('option').remove().end().append(opts);
            $('.stepMultipleSheet').removeClass('hide');
            $('#uploadFile').val("Reload");
            $('#stepActionbar').show();
            $('.step1').addClass('hide');
            $('.btnBack').addClass('bom_btnuploaddata');
            $('.btnNext').addClass('bom_btnnext');
            $('.btnBack').val(BOM.captions.UploadNewBOM); //"Upload new BOM"
            $('.btnBack').addClass('btnbig');
            return
        }
        if (deleteRowIndex != undefined) {
            if (BOM.currentStep == BOM.steps.mapColumns) {
                BOM.showMapColumnStep();
            }
        }
        if (callback) {
            callback(result.headers, isSaveTextArea);
        }
    },
    fixedLenthColumnMap: function () {
        var columns = BOM.previousStepFLColumn.split("/");
        $.each(columns, function (index, value) {
            if (value != "0") {
                $('span[data-charnumber=' + value + ']').each(function () {
                    var value = $(this).text();
                    $(this).text(value + '|');
                    $(this).addClass('fwseparater');
                });
            }
        });
    },
    dataSelectionCallback: function (headers) {
        $('#stepActionbar').show('hide');
        if (headers == null) {
            return;
        }
        BOM.mappedColumnName = headers[0];
        BOM.delimiter = BOM.getDelimiterName(headers[2]) || "none";
        BOM.headerRow = headers[1];
        $('#headerRow').val(BOM.headerRow);
        var startRow = BOM.headerRow != 0 ? (BOM.headerRow + 1) : 1;
        $('#startRow').val(startRow);
        BOM.selectedRows();
    },
    steps: {
        uploadData: 1,
        chooseDelimiter: 2,
        dataSelction: 3,
        mapColumns: 4
    },
    onLoad: function () {
        $("#textAreaPdf").height($(window).height() - 268);
        BOM.clearAll();
        this.serverData = $.parseJSON($('#txtData').val());
        BOM.captions = this.serverData.Captions;
        this.setMapDropdownOptions(this.serverData.BOMCols);
        var height = ($(window).height() - $('.header').height()) - 40;
        $('.divTable').css({ 'height': height + 'px' });
        BOM.basketNr = Core.getParameterByName('id');
        BOM.attached = true;
        BOM.loadTable(BOM.loadTableCallback, false, true);
        BOM.applyCssFirefox();
    },
    applyCssFirefox: function () {
        if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('.bom_btnuploaddata, .bom_btnnext, .bom_btntextedit, .bom_btndelete, .bom_adddata, .bom_back, .bom_submit, .bom_cancel, .bom_reload, .bom_save').css({
                'background-position-y': '7px',
            })
        }
    },
    openMapBox: function (ele, event) {
        var classname = $(ele).attr('class');
        classname = $.trim(classname.replace('mapColumn', ''));
        var offset = $(ele).parent('th').offset();
        var width = $(ele).parent('th').width();

        var popupLeft = (parseInt(width) / 2) + offset.left - 98;
        if (popupLeft < 31) {
            popupLeft = 0;
        }
        $('.arrow_box').hide();
        if (classname.indexOf("_th") >= 0) {
            var height = $('.arrow_box.' + classname).height();
            var headerHeight = $('.header ').height();
            var top = headerHeight - height;
            top = top < 0 ? 0 : top;
            var fullWidth = $(window).width();
            var pWidth = fullWidth - popupLeft;
            if (pWidth < 200) {
                popupLeft = fullWidth - 210;
            }
            $('.arrow_box.' + classname).show();
            $('.arrow_box.' + classname).css({ 'left': popupLeft + 'px', 'top': top });
        }
    },
    setMapDropdownOptions: function (data) {
        this.mapDropdownOptions = '<option value="none">-- None --</option>';
        this.mapDropdownOptions += '<option value="ignore">-- Ignore --</option>';

        for (i = 0; i < data.length; i++) {
            this.mapDropdownOptions += '<option value="' + data[i].name + '">' + data[i].display_name + '</option>';
        };
    },
    selectedRows: function (isMap) {
        //BOM.hideDeletedRows();
        if (BOM.headerRow != 0 || BOM.startRow != 0 || BOM.endRow != 0 || isMap) {
            $('#bomTable tbody tr').removeClass('selectedRow');
            $('#bomTable tbody tr').each(function () {

                //var srNo = $(this).children().eq(0).text();
                var row = parseInt($.trim($(this).find('td:first-child').text()));
                $(this).removeClass('unselected');
                //var currentRow = $(this);
                //if (srNo != '') {
                //if (BOM.removeRows.length > 0) {
                //    for (var i = 0; i < BOM.removeRows.length; i++) {
                //        if (srNo == BOM.removeRows[i]) {
                //            currentRow.addClass('tr-remove');
                //            break;
                //        }
                //    }
                //}
                //}
                var startingRow = BOM.startRow;
                var endingRow = BOM.endRow;
                var headRow = BOM.headerRow;

                startingRow = startingRow == 0 ? headRow + 1 : startingRow;
                endingRow = endingRow == 0 ? -1 : endingRow;

                if (headRow == row) {
                    $(this).addClass('selectedHeader');
                }

                // UnSelected row upto header row 
                if (row < startingRow && isMap) {
                    $(this).addClass('unselected');
                }

                if (endingRow != -1 && endingRow < row && isMap) {
                    $(this).addClass('unselected');
                }

                if (endingRow == -1 && startingRow <= row) {
                    // selected all rows after start rows
                    if (!$(this).hasClass('hide')) {
                        $(this).addClass('selectedRow');
                    }
                }

                if (endingRow != -1 && startingRow <= row && endingRow >= row) {
                    // selected rows between start row and end row 
                    if (!$(this).hasClass('hide')) {
                        $(this).addClass('selectedRow');
                    }
                }

                //Unselect row if row is blank
                var blankRow = true;
                $(this).find('td').each(function () {
                    if (!$(this).hasClass('td-index') && $.trim($(this).html()) != "") {
                        blankRow = false;
                    }
                });
                if (blankRow) {
                    $(this).removeClass('selectedRow').addClass('unselected');
                }

                if (BOM.currentStep == BOM.steps.dataSelction) {
                    $(this).removeClass('tr-css');
                }
                else {
                    $(this).addClass('tr-css')
                }
            });
        }
        if (BOM.currentStep != BOM.steps.dataSelction) {
            $('#bomTable tbody tr').removeClass('selectedHeader');
        }
        $('.pageloader,.loaderBG').addClass('hide');
    },
    //hideDeletedRows: function () {
    //    $('#bomTable tbody tr').each(function () {
    //        var srNo = $(this).children().eq(0).text();
    //        var currentRow = $(this);
    //        if (srNo != '') {
    //            if (BOM.removeRows.length > 0) {
    //                for (var i = 0; i < BOM.removeRows.length; i++) {
    //                    if (srNo == BOM.removeRows[i]) {
    //                        currentRow.addClass('tr-remove');
    //                        break;
    //                    }
    //                }
    //            }
    //        }
    //    });
    //},
    setTable: function (data) {
        var columnname = [];
        var colIndex = 0;
        var columns = ["Index"];

        columnname.push({
            data: "Index",
            title: ' Row Nr.',
            defaultContent: "",
            className: "td-index",
            render: function (data, type, row) {
                data = "";
                if (type === 'display') {
                    var visible = row.Isvisible;
                    if (visible != "true") {
                        return "";
                    }
                    colIndex = colIndex + 1;
                    var index = parseInt(row.Index) || 0;
                    if (row.Index > 0) {
                        data = colIndex;
                    }
                    return '<input class="chkDelete" type="checkbox"  style="vertical-align: inherit !important;cursor:pointer;"> <span class="index bold" data-index="' + index + '">' + data;//+ '</span><img class="imgDelete" title="Delete" style="height:13px;cursor:pointer;display:none;" src="resources/dustbin.png?v=0.2"/>';
                }
                return data;
            },
        });

        var row = $('<tr>');
        row.append('<td class="sno"> </td>');
        console.log(data)
        jQuery.each(data[0], function (key, obj) {
            var dtKey = $.trim(key).toLowerCase();

            if (dtKey != "index") {
                var title = key.replace(/_/g, ' ');
                if (dtKey == "isvisible") {
                    columnname.push({ "data": key, "title": title, "className": dtKey, "visible": false });
                }
                else {
                    columnname.push({ "data": key, "title": title, "className": dtKey });
                }
                row.append('<td userColumn=' + dtKey + '>' + BOM.getDropDown(dtKey) + '</td>');
            }
        });

        if ($.fn.DataTable.isDataTable('#bomTable')) {
            BOM.bomTable.destroy();
        }

        $('#bomTable thead, #bomTable tbody').empty();
        BOM.bomTable = $('#bomTable').DataTable({
            paging: false,
            ordering: false,
            searching: true,
            info: false,
            retrieve: true,
            data: data,
            columns: columnname
        });

        BOM.bomTable.row.add(row);
        $('#bomTable tbody').prepend(row);
        BOM.applyFirstTdCss();
    },
    applyFirstTdCss: function () {
        $('#bomTable tbody tr td.td-index').each(function () {
            $(this).css("background", "rgb(243, 243, 243)");
        });
    },
    showNextStep: function (btnNext) {
        BOM.clearCheckBox();
        if (!BOM.attached) {
            Core.showMessage("errorMsg", true, BOM.captions.RequiredFile, BOM.errorMSGInterval);  //Please select a file.
            return;
        }
        if (!BOM.validateInputData()) {
            return;
        }
        $('#stepActionbarVerifyParts, #btnSave, .message').addClass('hide');
        $('.btnBack').val(BOM.captions.Back); //"Back"
        $('.btnBack').removeClass("hide");
        $('.btnBack').removeClass('btnbig');
        $('#errorMsg').hide();
        if (this.currentStep == this.steps.uploadData) {
            if (!BOM.excel) {
                BOM.showDelimiterStep();
                $('.btnBack').addClass('bom_btnuploaddata');
                $('.btnNext').addClass('bom_btnnext');
            }
            else {

                BOM.showDataSelectionStep(false, true);
                $('.btnBack').removeClass('bom_btnuploaddata').addClass('bom_back');
                if (BOM.excel == true && BOM.multipleExcelSheet == false) {
                    $('.btnBack').removeClass('bom_back').addClass('bom_btnuploaddata');
                }
            }
        }
        else if (this.currentStep == this.steps.chooseDelimiter) {
            BOM.fixdlenthcolumn = "";
            if ($('#rdoFixedWidth').is(':checked')) {
                BOM.delimiter = 'FixedWidth';
                $('span.SpanIndex.fwseparaterfirst.fwseparater').each(function (index, element) {
                    var value = $(element).attr('data-charnumber');
                    BOM.fixdlenthcolumn = BOM.fixdlenthcolumn + value + "/";
                });
                BOM.fixdlenthcolumn = BOM.fixdlenthcolumn + "0";
                this.showDataSelectionStep(true, true);
                $('.btnBack').removeClass('bom_btnuploaddata').addClass('bom_back');
            }
            else {
                var delimitor = '';
                if ($('input[type=radio][name=delimiter]').is(':checked')) {
                    delimitor = $("input[type=radio][name=delimiter]:checked").val();
                }

                if (delimitor == "") {
                    $('#stepActionbarVerifyParts, #btnSave, .message').removeClass('hide');
                    $('.btnBack').val(BOM.captions.UploadNewBOM); //UploadNewBOM
                    $('.btnBack').removeClass("hide");
                    $('.btnBack').addClass('btnbig');
                    Core.showMessage("errorMsg", true, BOM.captions.RequiredDelimitor, BOM.errorMSGInterval); //Select delimitor
                    return;
                }
                if (delimitor == 'Other') {
                    var delimitorValue = $('.txtOther').val();
                    if (delimitorValue.length == 0) {
                        Core.showMessage("errorMsg", true, BOM.captions.RequiredDelimitor, BOM.errorMSGInterval); //Select delimitor
                        return;
                    }
                }

                this.showDataSelectionStep(false, true);
                $('.btnBack').removeClass('bom_btnuploaddata').addClass('bom_back');
            }
        }
        else if (this.currentStep == this.steps.dataSelction) {
            this.showMapColumnStep();
            $('.btnNext').removeClass('bom_btnnext').addClass('bom_submit');
            if (BOM.excel == true && BOM.multipleExcelSheet == false) {
                $('.btnBack').removeClass('bom_btnuploaddata').addClass('bom_back');
            }
        }
        else if (this.currentStep == this.steps.mapColumns) {
            this.saveBOM();
        }
    },
    showPreviousStep: function (btnBack) {
        BOM.clearCheckBox();
        if (!BOM.attached) {
            Core.showMessage("errorMsg", true, BOM.captions.RequiredFile, BOM.errorMSGInterval); //Please select a file.
            return;
        }
        $('.btnNext').removeClass('hide');
        $('#btnSave, #stepActionbarVerifyParts, .message').addClass('hide');
        $('.btnNext').val(BOM.captions.Next); //"Next"
        $('#errorMsg').hide();
        if (this.currentStep == this.steps.chooseDelimiter || this.currentStep == this.steps.uploadData) {
            this.showUploadStep();
            BOM.clearAll();
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            $('#textAreaPdf').hide();
            $('#textAreaPdf').val('');
        }
        else if (this.currentStep == this.steps.dataSelction) {
            if (BOM.excel) {
                //when exec is upload with multiple worksheet(for excel sheet section step)
                if ($('.btnBack').val() == BOM.captions.UploadNewBOM || !BOM.multipleExcelSheet) {
                    BOM.showUploadStep();
                    BOM.clearAll();
                    return;
                }
                $('.stepMultipleSheet').removeClass('hide');
                $('#uploadFile').val("Reload");
                $('#stepActionbar').show();
                $('.step1').addClass('hide');
                $('.step3').addClass('hide');
                $('.btnBack').val(BOM.captions.UploadNewBOM);
                $('.btnBack').addClass('btnbig');
                BOM.currentStep = BOM.steps.uploadData;
                $('.btnBack').removeClass('bom_back').addClass('bom_btnuploaddata');
                //this.showUploadStep();
            }
            else {
                this.showDelimiterStep();
                $('.btnBack').removeClass('bom_back').addClass('bom_btnuploaddata');
                $('.btnNext').addClass('bom_btnnext');
            }
            $('#bomTable tbody tr').removeClass('selectedHeader').removeClass('selectedRow');
        }
        else if (this.currentStep == this.steps.mapColumns) {
            this.showDataSelectionStep(true);
            $('#bomTable').off('click', 'tbody td', BOM.editableCell);
            $('.selectedRow').css({ 'cursor': 'none' });
            $('.btnNext').removeClass('bom_submit').addClass('bom_btnnext');
        }
    },
    showUploadStep: function () {
        $('#bomTable').removeClass('fixedLenthFont');
        $('#editBOM').hide();
        this.currentStep = this.steps.uploadData;
        this.nextStep = this.steps.chooseDelimiter;
        $('#fileUploader').val('');
        $('.bom-step').addClass('hide');
        $('.file-caption-name').text('No file chosen');
        $('#stepActionbar').hide();
        $('.btnBack,.stepMultipleSheet').addClass('hide');
        $('.step1').removeClass('hide');
        if ($.fn.DataTable.isDataTable('#bomTable')) {
            BOM.bomTable.destroy();
        }
        $('#bomTable thead, #bomTable tbody').empty();
    },
    showDelimiterStep: function () {
        $('#bomTable').addClass('fixedLenthFont');
        this.currentStep = this.steps.chooseDelimiter;
        this.nextStep = this.steps.dataSelction;
        this.previousStep = this.steps.uploadData;
        $('.btnBack').val(BOM.captions.UploadNewBOM); //"Upload new BOM"
        $('.btnBack').addClass('btnbig');
        $('.bom-step').addClass('hide');
        $('.step2').removeClass('hide');
        $('#editBOM').show();
        if ($('#rdoFixedWidth').is(':checked')) {
            BOM.previousStepFLColumn = BOM.fixdlenthcolumn;
            BOM.fixdlenthcolumn = "";
            BOM.loadTable(function () { BOM.fixedLenthColumnMap() });
        }
    },
    showDataSelectionStep: function (reloadTable, forceColumnMap) {
        $('#bomTable').removeClass('fixedLenthFont');
        $('#editBOM').hide();
        this.currentStep = this.steps.dataSelction;
        this.nextStep = this.steps.mapColumns;
        this.previousStep = this.steps.chooseDelimiter;
        BOM.headerRow = 0; BOM.startRow = 0; BOM.endRow = 0;
        BOM.loadTable(BOM.dataSelectionCallback, true);
        //if (forceColumnMap && BOM.headerRow == 0) {
        //    BOM.loadTable(BOM.dataSelectionCallback, true); //BOM.loadTableCallback instead of null
        //}
        //else if (reloadTable) {  //removed else if and set if 
        //    BOM.loadTable();
        //}
        //else {
        //    BOM.selectedRows(false);
        //}
        if (BOM.excel && !BOM.multipleExcelSheet) {
            $('.btnBack').val(BOM.captions.UploadNewBOM);//"Upload new BOM"
            $('.btnBack').removeClass('bom_back').addClass('bom_btnuploaddata');
        }
        //if (BOM.excel) {
        //    $('.btnBack').val(BOM.captions.UploadNewBOM);//"Upload new BOM"
        //    $('.btnBack').addClass('btnbig');
        //}
        $('.bom-step').addClass('hide');
        $('.step3').removeClass('hide');
    },
    showMapColumnStep: function () {
        $('#bomTable').removeClass('fixedLenthFont');
        $('#editBOM').hide();
        this.currentStep = this.steps.mapColumns;
        this.nextStep = this.steps.verifyParts;
        this.previousStep = this.steps.dataSelction;

        $('.btnNext').val(BOM.captions.Submit);//"Submit"
        $('.btnBack').val(BOM.captions.Back).removeClass('btnbig');
        $('.bom-step').addClass('hide');
        $('.step4').removeClass('hide');

        $('#bomTable tbody tr:first-child').removeClass('hide');

        BOM.selectedRows(true);
        BOM.autoMapColumns();

        $('#bomTable').on('click', 'tbody td', BOM.editableCell);
        $('.selectedRow').css({ 'cursor': 'pointer' });
    },
    uploadFile: function (e) {

        $(".divTable").addClass('hide');
        // BOM.clearAll();
        $('#textAreaPdf').val('');
        var num = Core.getParameterByName('id');
        BOM.basketNr = num;
        if (BOM.basketNr == "" || BOM.basketNr == null) {
            Core.showMessage("errorMsg", true, BOM.captions.RequireOrderOrBasket, BOM.errorMSGInterval); //Order/Basket number is not linked.
            return;
        }

        var filename = [].slice.call(e.target.files).map(function (f) {
            return f.name;
        });
        var fileUpload = $(".fileUploader").get(0);
        var files = fileUpload.files;

        if (files.length == 0) {
            Core.showMessage("errorMsg", true, BOM.captions.RequiredFile, BOM.errorMSGInterval); //Please select a file.
            return;
        }
        if (files[0].size <= 300) {
            Core.showMessage("errorMsg", true, BOM.captions.FileSizeLess300, BOM.errorMSGInterval);
            return;
        }
        if (files[0].size > 15360000) {
            Core.showMessage("errorMsg", true, BOM.captions.FileSizeLong, BOM.errorMSGInterval);
            return;
        }

        $('.file-caption-name').text(filename);
        $('.file-caption').attr("title", filename);
        BOM.attached = true;
        var extension = filename[0].substr((filename[0].lastIndexOf('.') + 1)).toLowerCase();
        BOM.fileExtension = extension;
        if (extension == 'ade' || extension == 'adp' || extension == 'bat' || extension == 'chm' || extension == 'cmd' || extension == 'com' || extension == 'cpl' || extension == 'exe' || extension == 'hta'
        || extension == 'ins' || extension == 'isp' || extension == 'jse' || extension == 'lib' || extension == 'lnk' || extension == 'mde' || extension == 'msc' || extension == 'msp' || extension == 'mst' ||
        extension == 'scr' || extension == 'pif' || extension == 'sct' || extension == 'shb' || extension == 'sys' || extension == 'vb' || extension == 'vbe' || extension == 'vbs' || extension == 'vxd' ||
        extension == 'wsc' || extension == 'wsf' || extension == 'wsh') {
            Core.showMessage("errorMsg", true, BOM.captions.MsgBOMFileSelect, BOM.errorMSGInterval);
            BOM.clearAll();
            BOM.currentStep = BOM.steps.uploadData;
            $('#fileUploader').val('');
            $('.file-caption-name').text('No file chosen');
            $('.stepTextBoxData').addClass('hide');
            $('#btnSubmitPdfDoc').hide();
            $('#btnCancelPdfDoc').hide();
            $('#textAreaPdf').hide();
            return;
        }
        if (extension == 'pdf' || extension == 'doc' || extension == 'docx' || extension == 'zip' || extension == 'rar' || extension == 'brd') {
            //BOM.showNextStep();
            //$("#divTable").hide();
            $('.stepTextBoxData').removeClass('hide');
            $('#btnSubmitPdfDoc').val(BOM.captions.Save);
            $('#btnCancelPdfDoc').val(BOM.captions.Cancel);
            $('#btnSubmitPdfDoc').show();
            $('#btnCancelPdfDoc').show();
            $('#textAreaPdf').show();
            $('#textAreaPdf').focus();
            //return;
        }
        if (extension == "xls" || extension == "xlsx" || extension == "ods") {
            BOM.excel = true;
        }

        BOM.loadTable(BOM.loadTableCallback);
    },
    loadTableCallback: function (headers, saveTextArea) {
        $('#stepActionbar').show('hide');
        if (headers == null) {
            BOM.showNextStep();
        }
        else {
            BOM.mappedColumnName = headers[0];
            BOM.delimiter = BOM.getDelimiterName(headers[2]) || "none";

            if (BOM.mappedColumnName == null && BOM.delimiter == "none") {
                if (saveTextArea) {
                    return;
                }
                BOM.showNextStep();
                return;
            }

            BOM.headerRow = headers[1];
            $('#headerRow').val(BOM.headerRow);

            var startRow = BOM.headerRow != 0 ? (BOM.headerRow + 1) : 1;
            $('#startRow').val(startRow);
            if (saveTextArea) {
                return;
            }
            BOM.showNextStep();
        }
    },
    validateRow: function (row, xmlNode) {
        var error = {
            row: 0,
            column1: '',
            column2: ''
        };

        //var mpn = xmlNode.mpn || '';
        //var spn = xmlNode.spn || '';
        //var description = xmlNode.desc || '';
        //var pack = xmlNode.pack || '';
        var qty = parseInt(xmlNode.qty) || 0;
        var refdes = xmlNode.refdes || '';
        var suply_value = xmlNode.supply || '';
        var errorCells = [];

        //if (mpn == '' && spn == '' && description == '' && pack == '') {
        //    error.row = xmlNode.index;
        //    if (error.column1 == '' && BOM.mappedColumnName['mpn']) {
        //        error.column1 = "mpn";
        //    }
        //    if (error.column1 == '' && BOM.mappedColumnName['spn']) {
        //        error.column1 = "spn";
        //    }
        //    if (error.column1 == '' && BOM.mappedColumnName['package']) {
        //        error.column1 = "package";
        //    }
        //    if (error.column1 == '' && BOM.mappedColumnName['description']) {
        //        error.column1 = "description";
        //    }

        //    var td = row.find('.' + BOM.mappedColumnName[error.column1] + '');
        //    errorCells.push(td);
        //}
        if (suply_value == 'invalid')
        {
            error.row = xmlNode.index;
            var td = row.find('.' + BOM.mappedColumnName['supply'] + '');
            error.column2 = 'supply';
            errorCells.push(td);
        }

      else if (qty == 0 && refdes == '') {
            error.row = xmlNode.index;
            if (error.column2 == '' && BOM.mappedColumnName['qty']) {
                error.column2 = 'qty';
            }
            if (error.column2 == '' && BOM.mappedColumnName['ref_des']) {
                error.column2 = "ref_des";
            }
            var td = row.find('.' + BOM.mappedColumnName[error.column2] + '');
            errorCells.push(td);
        }

        for (i = 0; i < errorCells.length; i++) {
            errorCells[i].addClass('incorectData');
            if (errorCells[i].find('img').length == 0) {
                errorCells[i].append("<img src='/shop/images/editCell.png' class='img-edit' />")
            }
        }

        return error;
    },
    validateInputData: function () {
        if ((BOM.startRow <= BOM.headerRow) && (BOM.headerRow != 0) && (BOM.startRow != 0)) {
            Core.showMessage("errorMsg", true, BOM.captions.StartRowGraterHeaderRow, BOM.errorMSGInterval); //Start row should be grater then header row.
            $('.pageloader,.loaderBG').addClass('hide');
            return false;
        }
        if ((BOM.startRow > BOM.endRow) && (BOM.startRow != 0) && (BOM.endRow != 0)) {
            Core.showMessage("errorMsg", true, BOM.captions.EndRowGraterStartRow, BOM.errorMSGInterval); //End row should be grater then start row.
            return false;
        }
        return true;
    },
    headerRowAction: function (e) {
        var row = parseInt($(".headerRow").val()) || 0;
        if (BOM.headerRow != row) {
            BOM.headerRow = parseInt($(".headerRow").val()) || 0;
            BOM.startRow = parseInt($(".startRow").val()) || 0;
            BOM.loadTable();
        }
        BOM.validateInputData();
        e.preventDefault();
    },
    startRowAction: function (e) {
        $('.pageloader,.loaderBG').removeClass('hide');
        BOM.startRow = parseInt($('.startRow').val()) || 0;
        BOM.selectedRows();
        BOM.validateInputData();
        e.preventDefault();
    }
}

$(document).ready(function () {
    BOM.init();
});
