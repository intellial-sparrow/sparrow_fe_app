﻿var app = new Vue({
    el: '#app',
    sessionId: "",
    data: {
        xmlData: {},
        priceData: {},
        alternativeData:[],
        showLoading: false,
        waitMessage: "Loading...", 
        addedlist: "",
    },
    mounted: function () {
        ES6Promise.polyfill();
        var sessionId = this.getParameterByName("sessionid"); 
        this.events();
        this.getOrderDetail(); 
    },
    methods: {
        events: function () {
            
        }, 
        getUrlParameter: function (sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        },
        getParameterByName: function (key) {
            return decodeURIComponent(window.location.href.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
        },
        getUTCDate: function () {
            var now = new Date();
            var nowUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
            return nowUTC;
        },
    }
});