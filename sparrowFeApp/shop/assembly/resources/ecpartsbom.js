﻿
var ecpartbom = {
    ecpartlist: [],

    bindDataTable: function (data, isSavedBoM) {
        $('#ecbomlist').dataTable({
            'responsive': true,
            'destroy': true,
            'filter': false,
            'bProcessing': true,
            'bServerSide': true,
            'searching': false,
            'pagingType': 'simple_numbers',
            'bAutoWidth': false,
            'bPaginate': false,
            'bInfo': false,
            'language': {
                'zeroRecords': 'No any bill of materials available.'
            },
            'columns': [ 
                 {
                     'sTitle': '', 'sName': 'part_id', 'render': function (data, type, full, meta) {

                         return '<input type="checkbox" name=""  MPN="' + data + '">'

                     }
                 },
                {
                    'sTitle': 'MPN', 'sName': 'mpn', 'min-width': '150px', 'render': function (data, type, full, meta) {
                        var mplValid = "";
                        if (full[25] == "0") {
                            mplValid = "unidentified-parts";
                        }
                        var coldata = "";
                        if (full[3] != "") {
                            coldata = '<p class="extraInfo">' + full[3] + '</p>';
                        }
                        if (full[25] == "2") {
                            coldata = coldata + '<p class="suggested"><i class="fa fa-exclamation-triangle"></i> Suggested Part</p>';
                        }
                        return '<div><span class="mpnNumber ' + mplValid + '">' + data + '</span> ' + coldata + ' <p style="min-width:120px;"><a class="alternatives"><i class="fa fa-search"></i> Find alternatives</a></p></div>';
                    }
                },

                {
                    'sTitle': 'Image', 'sName': 'imgurl', 'render': function (data, type, full, meta) {

                        return "<img src=\"" + data + "\" onError=\"this.src='/shop/assembly/resources/no_image-01.png'; \"  class='imgecpartlist'/>";
                    }
                },

                { 'sTitle': 'Category', 'sName': 'category', 'visible': false },
                {
                    'sTitle': 'Description', 'sName': 'descr', 'render': function (data, type, full, meta) {

                        var roh = "";
                        if (full[6] == "Compliant") {
                            roh = '<p>RoHS : <img class="ecpartrohs" data-toggle="tooltip" data-placement="top" title="RoHS Compliant" src="/shop/assembly/resources/rohs_g.png"  ></img></p>';
                        }
                        var desc = data;
                        if (data == "") {
                            data = full[27];
                        }
                        return '<div><p style="width: 200px;word-wrap: break-word;">' + data + '</p> ' + roh + '<div>';
                    }
                },
                { 'sTitle': 'IPC Name', 'sName': 'ipcname', 'visible': false },
                {
                    'sTitle': 'Availability', 'sName': 'stock', 'width': '100px', 'render': function (data, type, full, meta) {
                        if (full[25] == "0") {

                            return "";
                        }
                        if (data != "0" && data != "") {
                            return '<div><p class="ecpartlbl"> eC-Stock</p><i class="fa fa-check-circle clsstock" aria-hidden="true"></i><div>';
                        }
                        else {
                            return 'On-demand';
                        }
                    }
                },
                {
                    'sTitle': 'RoHS Compliant', 'sName': 'specs.rohs_status', 'visible': false
                },
                {
                    'sTitle': 'Manufacturer', 'sName': 'manufacturer', 'visible': false
                },
                ,
                 {
                     'sTitle': 'Ref des', 'sName': 'refdes', 'sClass': 'refdes editable-col', 'render': function (data, type, full, meta) {

                         return '<div ><span>' + data + ' </span> <i class="fa fa-edit"></i> </div>';
                     }
                 },
                 {
                     'sTitle': 'Type', 'sName': 'mount_type', 'sClass': 'mount_type editable-col', 'render': function (data, type, full, meta) {

                         return '<div style="min-width:50px;" ><span>' + data.toUpperCase() + ' </span> <i class="fa fa-edit"></i> </div>';
                     }
                 },
                  {
                      'sTitle': 'Supply', 'sName': 'supply', 'sClass': 'supply editable-col', 'render': function (data, type, full, meta) {

                          return '<div style="min-width:100px;" ><span>' + data + ' </span> <i class="fa fa-edit"></i> </div>';
                      }
                  },
                {
                    'sTitle': 'Qty per PCB', 'sName': 'qty', 'sClass': 'qty editable-col', 'render': function (data, type, full, meta) {

                        return '<div ><span>' + data + ' </span> <i class="fa fa-edit"></i> </div>';
                    }
                },

                { 'sTitle': 'ft_altium', 'sName': 'ft_altium', 'visible': false },
                { 'sTitle': 'ft_eagle', 'sName': 'ft_eagle', 'visible': false },
                { 'sTitle': 'ft_fpx', 'sName': 'ft_fpx', 'visible': false },
                { 'sTitle': 'ft_kicad', 'sName': 'ft_kicad', 'visible': false },
                { 'sTitle': 'wrl_3d', 'sName': 'wrl_3d', 'visible': false },
                { 'sTitle': 'is_polarized', 'sName': 'is_polarized', 'visible': false },
                { 'sTitle': 'is_poolable', 'sName': 'is_poolable', 'visible': false },
                { 'sTitle': 'is_generic', 'sName': 'is_generic', 'visible': false },
                { 'sTitle': 'category_id', 'sName': 'category_id', 'visible': false },
                { 'sTitle': 'data_Id', 'sName': 'data_Id', 'visible': false },
                { 'sTitle': 'data_Id_0', 'sName': 'data_Id_0', 'visible': false },
                { 'sTitle': 'appr', 'sName': 'appr', 'visible': false },

            {
                'sTitle': 'Media', 'sName': 'datasheet_url', 'width': '150px', 'render': function (data, type, full, meta) {

                    if (full[25] == "0") {
                        return "";
                    }

                    if (full.length > 14) {
                        var strImagdatasheet = "";
                        var strfootprint = "";
                        var str3d_outline = "";
                        var datasheet = "";
                        var footprint = "";
                        var str3d = "";
                        if (full[14] == "") {
                            strImagdatasheet = '/shop/assembly/resources/datasheet_outline.png';
                            datasheet = 'Datasheet not available';
                        }
                        else {
                            strImagdatasheet = '/shop/assembly/resources/datasheet.png';
                            datasheet = 'Datasheet available';
                        }

                        if ((full[15] == "" || full[15] == "false") && (full[16] == "" || full[16] == "false") && (full[16] == "" || full[16] == "false") && (full[17] == "" || full[17] == "false")) {
                            strfootprint = '/shop/assembly/resources/footprint_outline.png';
                            footprint = 'Footprint not available';
                        }
                        else {
                            strfootprint = '/shop/assembly/resources/footprint.png';
                            footprint = 'Footprint available';
                        }
                        if (full[18] == "" || full[18] == "false") {
                            str3d_outline = '/shop/assembly/resources/3d_outline.png';
                            str3d = '3D model not available';
                        }
                        else {
                            str3d_outline = '/shop/assembly/resources/3d.png';
                            str3d = '3D model available';
                        }
                        return '<img class="ecpartDA dataafterlogin" data-toggle="tooltip" data-placement="top" title="' + datasheet + '" src="' + strImagdatasheet + '?v=0.2"></img> &nbsp;&nbsp;' +
                            '<img class="ecpartDA dataafterlogin"   data-toggle="tooltip" data-placement="top" title="' + footprint + '" src="' + strfootprint + '?v=0.2"></img>&nbsp;&nbsp;' +
                            '<img class="ecpartDA dataafterlogin" data-toggle="tooltip" data-placement="top" title="' + str3d + '" src="' + str3d_outline + '?v=0.2"></img> &nbsp;&nbsp;';
                    }

                }


            },
            { 'sTitle': 'desc', 'sName': 'desc', 'visible': false },
            { 'sTitle': 'stock', 'sName': 'stock', 'visible': false }
            ],

            'fnServerData': function (sSource, aoData, fnCallback, oSettings) {

                var draw = $.grep(aoData, function (v, i) { return v.name === 'draw'; })[0].value;
                var columns = $.grep(aoData, function (v, i) { return v.name === 'columns'; })[0].value;
                var order = $.grep(aoData, function (v, i) { return v.name === 'order'; })[0].value;
                var start = $.grep(aoData, function (v, i) { return v.name === 'start'; })[0].value;
                var length = $.grep(aoData, function (v, i) { return v.name === 'length'; })[0].value;
                var search = $.grep(aoData, function (v, i) { return v.name === 'search'; })[0].value;
                var dataSet = [];
                var totaldata = 50;
                if (data != "") {
                    data.Parts.forEach(function (hit) {
                        if (hit.total != null) {
                            return true;
                        }
                        var row = [];
                        columns.forEach(function (col) {
                            if (hit[col.name] == undefined) {
                                row.push("");
                            }
                            else {
                                row.push(hit[col.name]);
                            }
                        });
                        dataSet.push(row);
                    });
                }

                fnCallback({
                    'draw': draw,
                    'recordsTotal': totaldata,
                    'recordsFiltered': totaldata,
                    'data': dataSet
                });
            },
        });
    },

    events: function () {

        $(window).on("load", function () {
            var userid = "0";
            userid = $('#ctl00_mainContent_hduserid').val();
            if (userid != "0") {
                $("#savedbom").show();
            }
            var sessionid = ecpartbom.getUrlParameter('sessionid');
            var filename = ecpartbom.getUrlParameter('filename');
            if (typeof filename !== "undefined") {
                $(".ecbomheader").text(filename + " - Bill of materials");
                $("title").text("Eurocircuits :: " + filename + " - Bill of materials");
                $("#lbldownloadBOM").text(filename + ".CSV");
            }
            if (typeof sessionid !== "undefined") {
                var dataValue = { "sessionid": sessionid };
                $(".pageloader").show()
                //$(".OverLay").show();
                $.ajax({
                    type: "POST",
                    url: "../../EC09WebService/ec.asmx/getbomfilelistdata",
                    data: dataValue,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: 'xml',
                    success: function (data) {
                        $(".bomloadder").hide();
                        $(".pageloader").hide()
                        //$(".OverLay").hide();
                        if (data.childNodes[0].innerHTML != "") {
                            $("#addtoec").hide();
                            //$("#pbacktosearch").text(" Part search");
                            var strjson = "{\"Parts\":" + data.childNodes[0].innerHTML + "}";

                            userid = $('#ctl00_mainContent_hduserid').val();
                            if (userid != "0") {
                                ecpartbom.bindDataTable(JSON.parse(strjson), true);
                            }
                            else {
                                ecpartbom.bindDataTable(JSON.parse(strjson), false);
                            }
                            $(".trashbom").hide();
                        }
                    }
                });
            }
            else {
                if (typeof (Storage) !== "undefined") {
                    if (localStorage.ecparts) {
                        var data = JSON.parse(localStorage.ecparts);
                        ecpartbom.bindDataTable(data, false);
                    }
                    else {
                        ecpartbom.bindDataTable("", false);
                    }
                }
                else {
                    ecpartbom.bindDataTable("", false);
                }
            }
            $(".pageloader").hide()
            //$(".OverLay").hide();
        });

        $('#exporttocsv').click(function () {
            userid = $('#ctl00_mainContent_hduserid').val();
            var sessionid = ecpartbom.getUrlParameter('sessionid');
            if (typeof sessionid !== "undefined" && userid != "0") {
                $('#DownloadBomfiles').modal('show');
                $('#chkBOM').attr('checked', true);
            }
            else {
                $("#lblmsg").html("Media can be downloaded from your saved BoM.");
                $('#msgModal').modal('show');
            }
        });

        $('.chkdownload').click(function () {
            if (this.checked) {
                $(".chkdownload").prop('checked', false);
                $(this).prop('checked', true);
            }
            else {
                $(this).prop('checked', false);
            }
        });


        $('#ecbomlist').on('click', '.editable-col', function (e) {
            var col = this.attributes["class"].value;

            if (col.indexOf("qty") != -1) {
                $("#editdata-tital").text("Edit Qty");
            }
            if (col.indexOf("supply") != -1) {
                $("#editdata-tital").text("Edit supply");
            }
            if (col.indexOf("mount_type") != -1) {
                $("#editdata-tital").text("Edit type");
            }
            if (col.indexOf("refdes") != -1) {
                $("#editdata-tital").text("Edit Ref des");
            }


            $("#editdata").modal('show');

        });
        //$('#btnsearch').click(function () {
        //    ecpartbom.redirecturlparm("");

        //});
        $('#ecbomlist').on('click', '.unidentified-parts', function (e) {


            $("#popupunidentified").modal('show');
        });

        $('#ecbomlist').on('click', '.alternatives', function (e) {

            $("#popupunidentified").modal('show');
        });
        //$("#txtsearch").keydown(function (event) {

        //    if (event.which == 13) {
        //        event.preventDefault();
        //        ecpartbom.redirecturlparm("");
        //    }
        //});

        $('#btndownload').click(function () {

            var $box = $('#chkBOM').attr('checked', true);
            if ($box.is(":checked")) {
                var filename = "BoM_" + ecpartbom.getFormattedTime() + ".csv";
                var filenamenew = ecpartbom.getUrlParameter('filename');
                if (typeof filenamenew !== "undefined") {
                    filename = filenamenew;
                }
                ecpartbom.export_table_to_csv(filename + ".csv");
            }
            $(".pageloader").show();
            var uid = "";
            var footprint = "";
            var isChecked = "false";
            var oTable = $('#ecbomlist').DataTable();
            oTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var rowdata = this.data();

                //Altium
                if ($("#chkAltium").prop("checked") == true) {
                    footprint = "footprint-Altium";
                    isChecked = "true";
                    if (this.data()[9] != "" && this.data()[9] != "false") {
                        uid += this.data()[9] + "|";
                    }
                }

                //Eagle
                if ($("#chkEagle").prop("checked") == true) {
                    footprint = "footprint-Eagle";
                    isChecked = "true";
                    if (this.data()[10] != "" && this.data()[10] != "false") {
                        uid += this.data()[10] + "|";
                    }
                }

                //FPX
                if ($("#chkFPX").prop("checked") == true) {
                    footprint = "footprint-FPX";
                    isChecked = "true";
                    if (this.data()[11] != "" && this.data()[11] != "false") {
                        uid += this.data()[11] + "|";
                    }
                }

                //KiCAD
                if ($("#chkKiCAD").prop("checked") == true) {
                    footprint = "footprint-KiCAD";
                    isChecked = "true";
                    if (this.data()[12] != "" && this.data()[12] != "false") {
                        uid += this.data()[12] + "|";
                    }
                }
            });

            //---------------- Download footprint zip file
            if (uid != "") {
                var dataValue = { "uids": uid, "footprint": footprint };
                $.ajax({
                    type: "post",
                    url: "/shop/assembly/dft.aspx",
                    data: dataValue,
                    contenttype: "application/x-www-form-urlencoded",
                    datatype: 'xml',
                    success: function (data) {
                        $(".pageloader").hide()
                        if (data != "") {
                            window.open("/shop/assembly/dft.aspx?sessionid=" + data + "&footprint=" + footprint);
                        }
                    }
                });
            }
            else if (uid == "" && isChecked == "true") {
                $("#lblBomDownloaderror").removeClass("hide");
                $(".pageloader").hide();
                setTimeout(function () {
                    $("#lblBomDownloaderror").addClass("hide");
                }, 3000);
            }
            else {
                $(".pageloader").hide();
            }

        });

        $('#addtoec').click(function () {
            $('#myModal').modal('show');
        });

        $('#backtosearch').click(function () {
            window.open("/shop/assembly/ecparts", '_blank');
        });

        $('#savedbom').click(function () {
            window.open("/shop/assembly/boms.aspx");
        });

        $('#btnfootprint1').click(function () {
            var uid = $('#btnfootprint1').attr("href");
            $.get(uid, function (data) {

            }, 'json');

        });
        $('#btnfootprint2').click(function () {
            var uid = $('#btnfootprint2').attr("href");
            $.get(uid, function (data) {

            }, 'json');
        });
        $('#btnfootprint3').click(function () {
            var uid = $('#btnfootprint3').attr("href");
            $.get(uid, function (data) {

            }, 'json');
        });
        $('#btnfootprint4').click(function () {
            var uid = $('#btnfootprint4').attr("href");
            $.get(uid, function (data) {

            }, 'json');
        });

        $('#ecbomlist').on('click', 'tbody td:last-child', function (e) {
            var target = $(e.target);
            if (target.length > 0) {
                var removebom = e.target.id;
                if (removebom == "removebom") {

                    var oTable = $('#ecbomlist').DataTable();
                    var row = oTable.row($(this).parents('tr'));

                    var partlist = [];
                    var ecpartlist = [];

                    //---------- Get ecpart list from local storage
                    if (typeof (Storage) !== "undefined") {
                        if (localStorage.ecparts) {
                            var data = JSON.parse(localStorage.ecparts);
                            ecpartlist = data.Parts;
                        }
                    }
                    partlist = oTable.row(this.closest('tr')).data();

                    //-------------------Remove data from ecpart list
                    for (i = ecpartlist.length - 1; i >= 0; --i) {
                        if (ecpartlist[i]["part_id"] === partlist[18]) {
                            ecpartlist.splice(i, 1);
                        }
                    }

                    //-------------------------Save ecpart list in localstorage
                    var strjson = "{\"Parts\":" + JSON.stringify(ecpartlist) + "}"
                    localStorage.setItem('ecparts', strjson);


                    //-------- remove row from html
                    oTable.row(row).remove();
                    $(this).closest("tr").remove();

                }
                else if (e.target.classList.contains("imgfootprint")) {
                    var path = "/shop/assembly/dft.aspx?uid=";
                    var oTable = $('#ecbomlist').DataTable();
                    var row = oTable.row($(this).parents('tr'));
                    //Altium
                    if (row.data()[9] == "" || row.data()[9] == "false") {
                        $('#btnfootprint1').addClass('disabled');
                    }
                    else {
                        $('#btnfootprint1').attr("href", path + row.data()[9]);
                        $('#btnfootprint1').removeClass('disabled');
                    }

                    //Eagle
                    if (row.data()[10] == "" || row.data()[10] == "false") {
                        $('#btnfootprint2').addClass('disabled');
                    }
                    else {
                        $('#btnfootprint2').attr("href", path + row.data()[10]);
                        $('#btnfootprint2').removeClass('disabled');
                    }

                    //FPX
                    if (row.data()[11] == "" || row.data()[11] == "false") {
                        $('#btnfootprint3').addClass('disabled');
                    }
                    else {
                        $('#btnfootprint3').attr("href", path + row.data()[11]);
                        $('#btnfootprint3').removeClass('disabled');
                    }

                    //KiCAD
                    if (row.data()[12] == "" || row.data()[12] == "false") {
                        $('#btnfootprint4').addClass('disabled');
                    }
                    else {
                        $('#btnfootprint4').attr("href", path + row.data()[12]);
                        $('#btnfootprint4').removeClass('disabled');
                    }

                    $('#myModalfootprint').modal('show');

                }
                else if (e.target.classList.contains("dataafterlogin")) {
                    $("#lblmsg").html("Media can be downloaded from your saved BoM.");
                    $('#msgModal').modal('show');
                }
            }
        });

        $("#txtbomname").keypress(function (event) {
            if (event.which == 13) {
                event.preventDefault();
                $('#btnsavebom').trigger("click");
            }
        });

        $('#btnsavebom').click(function () {
            $("#lblname").text("");
            var tbody = $("#ecbomlist tbody");
            if (tbody.children().length == 0) {
                $("#lblname").text("No any BoM to save");
                $("#lblname").removeClass("hidden");
                return;
            }
            var filename = $("#txtbomname").val();
            if (filename == "") {
                $("#lblname").text("Please add bill of materials name");
                $("#lblname").removeClass("hidden");
                return;
            }
            $(".pageloader").show()
            //$(".OverLay").show();

            var csvdata = ecpartbom.ConvertDatatableToCSV(false);
            var userid = "0";
            userid = $('#ctl00_mainContent_hduserid').val();
            var oTable = $('#ecbomlist').DataTable();
            var totalRows = oTable.data().length;
            var dataValue = { "filename": filename, "csvdata": csvdata, "userid": userid, "totalpart": totalRows };

            $.ajax({
                type: "POST",
                url: "../../EC09WebService/ec.asmx/savebomlistfile",
                data: dataValue,
                contentType: "application/x-www-form-urlencoded",
                dataType: 'xml',
                success: function (data) {
                    $(".bomloadder").hide();
                    $(".pageloader").hide()
                    //$(".OverLay").hide();
                    if (data.childNodes[0].innerHTML != "0") {
                        $("#txtbomname").val("");
                        $('#myModal').modal('hide');
                        var userid = "0";
                        userid = $('#ctl00_mainContent_hduserid').val();
                        localStorage.removeItem("ecparts");//Remove Session stoarage after generating csv 
                        //localStorage.removeItem("ecpartstemp");//Remove Session stoarage after generating csv 
                        if (userid == "0") {
                            window.location.href = "/shop/eclogin.aspx?isfrombomlist=true&sessionid=" + data.childNodes[0].innerHTML;//if not logged in then redirect to login page
                        }
                        else {
                            window.location.href = "/shop/assembly/bom.aspx?sessionid=" + data.childNodes[0].innerHTML + "&filename=" + filename;// rediredct to list page
                        }
                    }
                }
            });

            $('#myModal').modal('hide');
        });
    },
    getUrlParameter: function (sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    },
    redirecturlparm: function (mpn) {
        var category = $('#ddlallcategorynew').children("option:selected").attr('catattr');
        if (mpn != "") {
            $('#txtsearch').val(mpn);
        }
        var txtvalue = $('#txtsearch').val();
        if (typeof category === "undefined") {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?keywords=" + txtvalue;
            }
            else {
                window.location.href = "/shop/assembly/ecparts";
            }
        }
        else {
            if (txtvalue.trim() != "") {
                window.location.href = "/shop/assembly/ecparts/?category=" + category + "&keywords=" + txtvalue;
            }
            else {
                window.location.href = "/shop/assembly/ecparts/?category=" + category;
            }
        }
    },
    export_table_to_csv: function (filename) {

        // Download CSV
        var csv = ecpartbom.ConvertDatatableToCSV(true);
        ecpartbom.download_csv(csv, filename);
    },
    ConvertDatatableToCSV: function (isexport) {
        var csv = '';
        var ecpartlist = [];
        var oTable = $('#ecbomlist').DataTable();
        var totalRows = oTable.data().length;
        if (isexport == true) {
            if (totalRows > 0) {
                ecpartlist.push({
                    //imgurl: 'imgurl',
                    mpn: 'MPN',
                    category: 'Category',
                    descr: 'Description',
                    ipcname: 'IPC Name',
                    //stock: 'stock',
                    //rohs_compliant: 'rohs_compliant',
                    manufacturer: 'Manufacturer',
                    //datasheet_url: 'datasheet_url',
                    //eagle_ft: 'eagle_ft',
                    //is_polarized: 'is_polarized',
                    //is_poolable: 'is_poolable',
                    //mount_type: 'mount_type',
                    //is_generic: 'is_generic', 
                    //part_id: 'part_id',
                    //category_id: 'category_id', 
                    //data_Id: 'data_Id',
                    //data_Id_0: 'data_Id_0',
                })
            }
            for (i = 0; i < totalRows; i++) {
                var row = oTable.row(i).data();
                ecpartlist.push({
                    //imgurl: row[0],
                    mpn: row[1],
                    category: row[2],
                    descr: row[3],
                    ipcname: row[4],
                    //stock: row[5],
                    //rohs_compliant: row[6],
                    manufacturer: row[7],
                    //datasheet_url: row[8],
                    //eagle_ft: row[9],
                    //is_polarized: row[10],
                    //is_poolable: row[11],
                    //mount_type: row[12],
                    //is_generic: row[13],
                    //part_id: row[14],
                    //category_id: row[15],
                    //data_Id: row[16],
                    //data_Id_0: row[17],
                })
            }
        }
        else {
            if (totalRows > 0) {
                ecpartlist.push({
                    imgurl: 'imgurl',
                    mpn: 'mpn',
                    category: 'category',
                    descr: 'descr',
                    ipcname: 'ipcname',
                    stock: 'stock',
                    rohs_compliant: 'rohs_compliant',
                    manufacturer: 'manufacturer',
                    datasheet_url: 'datasheet_url',
                    ft_altium: 'ft_altium',
                    ft_eagle: 'ft_eagle',
                    ft_fpx: 'ft_fpx',
                    ft_kicad: 'ft_kicad',
                    wrl_3d: 'wrl_3d',
                    is_polarized: 'is_polarized',
                    is_poolable: 'is_poolable',
                    mount_type: 'mount_type',
                    is_generic: 'is_generic',
                    part_id: 'part_id',
                    category_id: 'category_id',
                    data_Id: 'data_Id',
                    data_Id_0: 'data_Id_0',
                })
            }
            for (i = 0; i < totalRows; i++) {
                var row = oTable.row(i).data();
                ecpartlist.push({
                    imgurl: row[0],
                    mpn: row[1],
                    category: row[2],
                    descr: row[3],
                    ipcname: row[4],
                    stock: row[5],
                    rohs_compliant: row[6],
                    manufacturer: row[7],
                    datasheet_url: row[8],
                    ft_altium: row[9],
                    ft_eagle: row[10],
                    ft_fpx: row[11],
                    ft_kicad: row[12],
                    wrl_3d: row[13],
                    is_polarized: row[14],
                    is_poolable: row[15],
                    mount_type: row[16],
                    is_generic: row[17],
                    part_id: row[18],
                    category_id: row[19],
                    data_Id: row[20],
                    data_Id_0: row[21],
                })
            }
        }

        var jsonObject = JSON.stringify(ecpartlist);
        var csv = ecpartbom.convertToCSV(jsonObject);
        return csv;
    },
    convertToCSV: function (objArray) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += '|'

                line += array[i][index];
            }
            str += line + '\r\n';
        }
        return str;
    },
    getFormattedTime: function () {
        var today = new Date();
        var y = today.getFullYear();
        // JavaScript months are 0-based.
        var m = today.getMonth() + 1;
        var d = today.getDate();
        var h = today.getHours();
        var mi = today.getMinutes();
        var s = today.getSeconds();
        return d + "" + m + "" + y + "" + h + "" + mi + "" + s;
    },
    download_csv: function (csv, filename) {
        var csvFile;
        var downloadLink;
        csvFile = new Blob([csv], { type: "text/csv" });
        downloadLink = document.createElement("a");
        downloadLink.download = filename;
        downloadLink.href = window.URL.createObjectURL(csvFile);
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
        downloadLink.click();
    },

    init: function (event) {
        ecpartbom.events();
    }
}


$(document).ready(function () {
    $("title").text("Eurocircuits :: Part BoM");
    $("#spnlng").hide();
    $("#Span4").hide();
    ecpartbom.init();
});