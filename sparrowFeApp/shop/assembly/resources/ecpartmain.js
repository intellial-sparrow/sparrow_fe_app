﻿Vue.component('partlist', {
    template: "#partlist-template",
    props: ['parts'],
    mounted: function () {
    },
    methods: {

    }
});
var app = new Vue({
    el: '#app',
    sessionId: "",
    data: {
        parts: [],  
        showLoading: false,
        waitMessage: "Loading...",
        addPartList: [],
        addedlist:"",
    },
    mounted: function () {
        ES6Promise.polyfill();
        var sessionId = this.getParameterByName("sessionid");
        var filename = this.getParameterByName('filename');
        if (typeof filename !== "undefined") {
            $("#lbldownloadBOM").text(filename + ".CSV");
        } 
        this.events();
        this.getPartList();
        this.bindBreadcrumb();
    },
    methods: {
        events: function () { 
            $('body').click(function () {  
                $('#list-detail').hide(); 
                if (!$(".part-loading").is(":visible")) { 
                    $('.features').show();
                } 
            }); 
        },
        createListSaveBom:function(){
            $("#btnCreateListSaveBOM").hide();
            $("#divmdlBody").hide();
            $("#divProcessingBOM").show();
            app.saveBOMFile();
        },
        getPartList: function () { 
            var url = $(location).attr('href'),
            parts = url.split("/"),
             categoryname = parts[parts.length - 1];
            if (categoryname != "ecparts") {  
                this.loadurlparm(categoryname);
                this.loaddata("");
            }
            else { 
                this.loaddata("");
            } 
        },
        bindBreadcrumb:function(){
            var bomSavedSessionid = $("#ctl00_mainContent_hdSavedbomSessionid").val(); 
            if (bomSavedSessionid != "") {
                var url = '/shop/pcb/pcbservice.ashx?isBreadcrumb=true&sessionId=' + bomSavedSessionid + '&bomstage=uploadbom';
                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: "text",
                    success: function (data) { 
                        if (data == "bomeditor" || data == "addpcb" || data == "orderpreview") {
                            $("#bomeditor").attr("href", "/shop/assembly/bom.aspx?sessionid=" + bomSavedSessionid);
                        }
                        if (data == "addpcb" || data == "orderpreview") {
                            $("#addpcb").attr("href", "/shop/assembly/ecpartspcb.aspx?bsessionid=" + bomSavedSessionid);
                        }
                        if (data == "orderpreview") {
                            $("#orderpreview").attr("href", "/shop/pcb/board.aspx?sessionid=" + bomSavedSessionid + '&fromecpart=true');
                        }
                    }
                });
            } 
        },
        showListPopup: function (part, e) { 
            app.addPartList.push(part);
            $("#list-detail").show('show');  
            
            var t = $(e.target).offset().top;
            var l = $(e.target).offset().left;
            $("#list-detail").css({ top: (t - $(e.target)[0].offsetTop - 35), left: (l - $(e.target)[0].offsetLeft) });
              
            e.preventDefault()
            e.stopPropagation();
        },
        showAddListPopup: function () {
            $("#btnCreateListSaveBOM").hide();
            $("#btnsaveList").show();
            $('#list-detail').hide();
            $('#modalcreatelist').modal('show');
        },
        continuelist: function () { 
            var mpnlist = $('#txtfile').val().replace(/\n/g, ""); 
            var validated = true; 
            var lines = $('#txtfile').val().split('\n'); 
            for (var i = 0; i < lines.length; i++) { 
                if (lines[i].trim() != '') {
                    var linechar = lines[i].trim().replace(/ /g, '').length;
                    if (linechar < 3) {
                        validated = false;
                        break;
                    }
                }
            }
            if (mpnlist.trim() == '' || validated == false) {
                $('#txtfile').addClass("mpn-error");
                $('.search-input-error').removeClass("hide");
                setTimeout(function () { $('#txtfile').removeClass("mpn-error"); $('.search-input-error').addClass("hide"); }, 3000);
                event.stopPropagation();
                return;
            }

            $("#btnCreateListSaveBOM").show();
            $("#btnsaveList").hide(); 
            $('#modalcreatelist').modal('show');
            
            event.stopPropagation();
        },
        downloadmsg: function () { 
            $("#lblmsg").html("Media can be downloaded from your saved BoM.");
             $("#msgModal").modal('show'); 
        },
        addtolist: function (sessionid, listname) { 
            app.addedlist = listname;
            var searchData = [];
            var searchNode = {}
            var xmlData = [];
            var xmlNode = {};
            for (var i = 0; i < app.parts.length; i++) {
                if (app.parts[i].mpn === app.addPartList[0].mpn) {
                    app.parts[i].addtolist = "1";
                    app.parts[i].index = i;

                    searchNode["index"] = i
                    searchNode["mpn"] = app.parts[i].mpn;
                    searchNode["refdes"] = "";
                    searchNode["qty"] = "";
                    var spec = BOMUtility.sanatizeKeyword(app.parts[i].mpn, 1);
                    searchNode["specs"] = spec.specs;
                    searchData.push(searchNode);

                    xmlNode["index"] = i
                    xmlNode["mpn"] = app.parts[i].mpn;
                    xmlNode["refdes"] = "";
                    xmlNode["qty"] = "";
                    xmlData.push(xmlNode);
                }
            } 
            var postData = {
                sessionId: sessionid,
                searchData: JSON.stringify(searchData),
                xmlData: JSON.stringify(xmlData)
            }
            axios.post('/shop/assembly/ecpartbomimport.aspx/AddtoBoM', postData)
            .then(function (data) {                
                $('#list-detail').hide(); 
                setTimeout(function () {
                        for (var i = 0; i < app.parts.length; i++) {
                            if (app.parts[i].mpn === app.addPartList[0].mpn) {
                                app.parts[i].addtolist = "2"; 
                            }
                        }  
                        setTimeout(function () {
                            $(".part-added").slideUp(500).fadeIn(1); 
                                setTimeout(function () {
                                    for (var i = 0; i < app.parts.length; i++) {
                                        if (app.parts[i].mpn === app.addPartList[0].mpn) { 
                                            app.parts[i].addtolist = "0"; 
                                        }
                                    } 
                                    app.addPartList = [];
                                    app.addedlist = "";
                                }, 400);//-----------hide add to list 
                        }, 3000); //-----------set added to for 3 sec
                    }, 1000); //-----------set added to for 1 sec
            })
            .catch(function (error) {
                console.log(error);
            }); 
        },
        loaddata: function(fromFile) { 
            var searchData = []; 
            var isMulti = "0";
            if (fromFile == "1") {
                var lines = $('#txtfile').val().split('\n');
                for (var i = 0; i < lines.length; i++) {
                    //code here using lines[i] which will give you each line 
                    if (lines[i].trim() != '') {
                        var searchNode = {}
                        var mpnList = lines[i];
                        searchNode["descr"] = mpnList;
                        searchNode["index"] = 0;
                        searchNode["start"] = 0;
                        searchNode["length"] = 50;

                        var spec = BOMUtility.sanatizeKeyword(mpnList, 1);

                        searchNode["specs"] = spec.specs;
                        searchData.push(searchNode);
                    } 
                }
                isMulti = "1";
            }
            else {
                var searchNode = {}
                var txtvalue = $('#txtsearch').val();
                $('#txtmdsearch').val("");
                var ddlval = $('#ddlallcategorynew option:selected').attr("value");

                $("#spnsearchFor").text(txtvalue);

                searchNode["descr"] = txtvalue;
                searchNode["category"] = ddlval;

                if ($("#chkstock").is(':checked') == true) {
                    searchNode["stock"] = true;
                }
                if ($("#chkondemand").is(':checked') == true) {
                    searchNode["ondemand"] = true;
                }
                if ($("#chkdatasheet").is(':checked') == true) {
                    searchNode["datasheet"] = true;
                }
                if ($("#chkfootprints").is(':checked') == true) {
                    searchNode["footprints"] = true;
                }
                if ($("#chk3dmodel").is(':checked') == true) {
                    searchNode["3DModel"] = true;
                }
                if ($("#chkrohscompliant").is(':checked') == true) {
                    searchNode["rohscompliant"] = true;
                }
                if ($("#chknonrohscomliant").is(':checked') == true) {
                    searchNode["nonrohscomliant"] = true;
                }

                searchNode["index"] = 0;
                searchNode["start"] = 0;
                searchNode["length"] = 50;

                var spec = BOMUtility.sanatizeKeyword(txtvalue, 1);

                searchNode["specs"] = spec.specs;
                searchData.push(searchNode);
            }  
            var postsearchData = JSON.stringify(searchData); 
            $.post("/shop/assembly/searchpart.aspx?opencomponentsearch=true &isMulti=" + isMulti, postsearchData, function (response) { 
                if (response != null && response.Parts != null && response.Parts.length > 0) {
                    $("#tbldatapart").show('show');
                    for (var i = 0; i < response.Parts.length; i++) { 
                        response.Parts[i].addtolist = "0";
                        app.parts.push(response.Parts[i]);
                    }
                    if (response.Parts.length > 0) {
                        app.parts = app.sortByKeyDesc(app.parts, "stock");
                    }
                    $('html, body').animate({
                        scrollTop: $('#tbldatapart').offset().top - 100
                    }, 'slow')
                } 
            }, 'json');

        },
        bomSubmited: function (sessionid) {
            //
            // $('#divProcessing').hide();
            //$('#iframeImport').show();
            var userid = $("#ctl00_hdnmasteruserid").val();
            if (userid != "0") {
                document.location.href = '/shop/assembly/bom.aspx?sessionid=' + $('#ctl00_mainContent_hdbomsessionId').val();
            }
            else {
                document.location.href = "/shop/eclogin.aspx?isfrombomlist=true&sessionid=" + $('#ctl00_mainContent_hdbomsessionId').val();//if not logged in then redirect to login page
            }

            //$('#mdlImport').modal('hide');
            $("#iframeImport").attr("src", '');
        },
        saveBOMFile: function () {            
            var searchData = [];
            var xmlData = [];

            var lines = $('#txtfile').val().split('\n');
            for (var i = 0; i < lines.length; i++) {
                if (lines[i].trim() != '') {
                    var searchNode = {
                        index: i,
                        mpn: lines[i],
                        spn: '',
                        package: '',
                        descr: '',
                        category: '',
                    };
                    var xmlNode = {};
                    xmlNode['index'] = i;
                    xmlNode['mpn'] = lines[i];
                    xmlNode['refdes'] = "";
                    xmlNode['qty'] = "1";
                    xmlData.push(xmlNode);
                    searchData.push(searchNode);
                } 
            }
            var qty = $("#txtMasterBoardQty").val();
            var sides = 0;
            $('input[name="chksides"]:checked').each(function () {
                sides = this.value;
            }); 

            var userid = $("#ctl00_hdnmasteruserid").val(); 
            var url = "/shop/assembly/ecpartbomimport.aspx/SaveBOM";
            var postData = {
                listName: $("#txtListName").val(),
                sessionId: $('#ctl00_mainContent_hdbomsessionId').val(),
                searchData: JSON.stringify(searchData),
                xmlData: JSON.stringify(xmlData),
                mapData: "",
                noAlternatives: "1",
                userid: userid,
                boardqty: qty,
                assemblysides: sides
            }

            Core.post(url, postData, function (result) {
                $("#txtListName").val("");
                var data = result;
                if (data.Status == 0) {
                    //Core.showMessage("errorMsg", true, BOMPART.captions.Error);  //Error occurred.
                    console.log(data.Msg)
                    return;
                }
                app.bomSubmited($('#ctl00_mainContent_hdbomsessionId').val());
            });

        },
        loadurlparm: function (categoryname) { 
            categoryname = this.getUrlParameter('category');
            //----------------Set Dropdown color and value 
            if (categoryname != "") {
                $("[catattr=" + categoryname + "]").addClass("bgcolorset");
                var value = $("[catattr=" + categoryname + "]").attr("value")
            } 
            var searchvalue = "";
            var keywords = this.getUrlParameter('keywords');
            if (typeof keywords !== "undefined") {
                $('#txtsearch').val(keywords);
            }

            var search = {};
            var url = $(location).attr('href');
            var searchIndex = url.indexOf("?");
            if (searchIndex != -1) {
                var sPageURL = url.substring(searchIndex + 1);
                var sURLVariables = sPageURL.split('&');
                for (var i = 0; i < sURLVariables.length; i++) {
                    var sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] != "keywords") {
                        search[sParameterName[0]] = sParameterName[1];
                        //---------select checkbox
                        $(".chkbox").find("input[type=checkbox]").each(function () {
                            var key = $(this).val();
                            if (key == sParameterName[0]) {
                                $(this).prop('checked', true);

                                //-----------Check for rohs checkbox enable disable
                                if ($(this).hasClass("rohschk")) {
                                    $(".rohschk").attr("disabled", true);
                                    $(this).removeAttr("disabled");
                                }
                            }
                        });
                    }
                }
            }

            $("#ddlallcategorynew").css("color", "");
            $(".ddlcategorycss").css("color", "");
            $("#ddlallcategorynew").val(value);
            $('#ddlallcategorynew option[value=' + value + ']').attr('selected', 'selected');
            $('.ddlcategorycss option[value=' + value + ']').attr('selected', 'selected');
            $(".ddlcategorycss").val(value);
            $('#ddlallcategorynew option[value=' + value + ']').attr('selected', 'selected');
        },
        getUrlParameter: function (sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        },
        getParameterByName: function (key) {
            return decodeURIComponent(window.location.href.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
        },
        getUTCDate: function () {
            var now = new Date();
            var nowUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
            return nowUTC;
        },
        sortByKeyDesc: function (array, key) {
            return array.sort(function (a, b) {
                var x = a[key]; var y = b[key];
                return ((x > y) ? -1 : ((x < y) ? 1 : 0));
            });
        },
        sortByKeyAsc: function(array, key) {
            return array.sort(function (a, b) {
                var x = a[key]; var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        }
    }
});