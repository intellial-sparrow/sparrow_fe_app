﻿<%@ Page Language="C#" AutoEventWireup="true" Debug="true"  %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections.Specialized" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="sparrowFeApp.shop.assembly" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        string matchPartsData = "";
        using (StreamReader streamReader = new StreamReader(Request.InputStream))
        {
            matchPartsData = streamReader.ReadToEnd();
        }

        string latestPrice = "false";
        if (Request.RawUrl.Contains("latest"))
        {
            latestPrice = "true";
        }

        ComponentPrice componentPrice = new ComponentPrice(latestPrice);
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(matchPartsData);
        XmlNodeList matchNodes = xmlDoc.SelectNodes("Parts/Part");
        if (Session["POWERBASKET"] != null && Session["POWERBASKET"].ToString() == "YES")
        {
              Response.Write(xmlDoc.OuterXml);
        }
        else
        {
            foreach (XmlNode matchNode in matchNodes)
            {
                string mpn = matchNode.Attributes["mpn"].Value;
                int ordQty = Convert.ToInt32(matchNode.Attributes["ordQty"].Value);
                //Do not sent price request for 0 order qty part
                if (ordQty == 0)
                {
                    continue;
                }

                int prodQty = 0;
                if (matchNode.Attributes["prodQty"] != null)
                {
                    int.TryParse(matchNode.Attributes["prodQty"].Value, out prodQty);
                }

                string isGeneric = matchNode.Attributes["gpn"].Value;
                string caetgory = "";
                if (matchNode.Attributes["cat"] != null)
                {
                    caetgory = matchNode.Attributes["cat"].Value;
                }

                componentPrice.AddComponent(mpn, ordQty, prodQty, isGeneric, caetgory);
            }

            string orderNumber = Request.QueryString["id"] ?? "";
            string preferedSupplier = Component.GetRootCompany(orderNumber);
            xmlDoc = componentPrice.UpdatePrice(xmlDoc, "/Parts/Part", preferedSupplier, orderNumber);

            //Remove all nodes from the response whose price is not yet available.
            XmlNodeList removeNodes = xmlDoc.SelectNodes("/Parts/Part[@ordQty!='0' and @isGen!='1' and @price='0']");
            XmlNodeList partNodes = xmlDoc.SelectNodes("Parts");
            foreach (XmlNode node in removeNodes)
            {
                XmlNode parentNode = node.ParentNode;
                parentNode.RemoveChild(node);
            }
            Response.Write(xmlDoc.OuterXml);
        }
    }
</script>