﻿using EC09WebApp.shop.rb;
using Newtonsoft.Json;
using sparrowFeApp.core;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Xml;

namespace sparrowFeApp.shop.assembly
{
    public partial class CPLBomImport : sparrowFeApp.core.BasePage
    {
        string RootPath = Constant.FileServerPath;
        const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
        protected void Page_Load(object sender, EventArgs e)
        {
            ResourceId = 90;
            if (!IsPostBack)
            {
                Dictionary<string, string> captions = SetLanguageData();
                DataTable bomColumns = sparrowFeApp.core.Common.GetBOMColumns(2);
                string data = JsonConvert.SerializeObject(new { BOMCols = bomColumns, Captions = captions });
                txtData.Value = data;
            }
            Session["visSession"] = Request.QueryString["Session_id"] ?? "";
        }
        private Dictionary<string, string> SetLanguageData()
        {
            Dictionary<string, string> captions = new Dictionary<string, string>();
            captions.Add("RequiredMappingColumns", GetLocalResourceObject("RequiredMappingColumns"));
            captions.Add("ErrorInFileUpload", GetLocalResourceObject("ErrorInFileUpload"));
            captions.Add("RequiredFile", GetLocalResourceObject("RequiredFile"));
            captions.Add("RequiredDelimitor", GetLocalResourceObject("RequiredDelimitor"));
            captions.Add("StartRowGraterHeaderRow", GetLocalResourceObject("StartRowGraterHeaderRow"));
            captions.Add("EndRowGraterStartRow", GetLocalResourceObject("EndRowGraterStartRow"));
            captions.Add("RequireOrderOrBasket", GetLocalResourceObject("RequireOrderOrBasket"));
            captions.Add("RowNumber", GetLocalResourceObject("RowNumber"));
            captions.Add("RowNumberContainsInvalidData", GetLocalResourceObject("RowNumberContainsInvalidData"));
            captions.Add("FileUploadError", GetLocalResourceObject("FileUploadError"));
            captions.Add("UploadNewBOM", GetLocalResourceObject("UploadNewBOM"));
            captions.Add("Back", GetLocalResourceObject("Back"));
            captions.Add("Next", GetLocalResourceObject("Next"));
            captions.Add("Submit", GetLocalResourceObject("Submit"));
            captions.Add("Cancel", GetLocalResourceObject("Cancel"));
            captions.Add("FileSizeLong", GetLocalResourceObject("MsgFileSizeLongOrderFail.Text"));
            captions.Add("MsgBOMFileSelect", GetLocalResourceObject("MsgBOMFileSelect.Text"));
            captions.Add("ConfirmMessage", GetLocalResourceObject("ConfirmMessage"));
            captions.Add("Save", GetLocalResourceObject("Save"));
            captions.Add("UploadNewCPL", GetLocalResourceObject("UploadNewCPL"));

            btnOverwrite.Attributes["title"] = GetLocalResourceObject("tooltipReloadCPL");
            btnMerge.Attributes["title"] = GetLocalResourceObject("tooltipAddData");
            btnOverwrite.Value = GetLocalResourceObject("reloadCPL"); ;//GetLocalResourceObject("reloadCPL");
            btnMerge.Value = GetLocalResourceObject("addData");
            btnCancel.InnerText = GetLocalResourceObject("Cancel");
            lblTop.InnerText = GetLocalResourceObject("top");
            lblBottom.InnerText = GetLocalResourceObject("bottom");
            confirmFileMsg.InnerText = GetLocalResourceObject("XMLExist");
            spanSide.InnerText = GetLocalResourceObject("CPLSide");
            btnSideOk.Value = GetLocalResourceObject("Submit");
            btnSideCancel.InnerText = GetLocalResourceObject("Cancel");
            ctl00_lblLoading.InnerText = GetLocalResourceObject("ctl00_lblLoading.Text");
            h3BomWorkSheet.InnerText = GetLocalResourceObject("h3BomWorkSheet.Text");
            divMulWorkSheet.InnerText = GetLocalResourceObject("divMulWorkSheet.Text");
            step1title.InnerText = GetLocalResourceObject("step1title.Text");
            spnBrowse.InnerText = GetLocalResourceObject("spnBrowse.Text");
            divAccepFormat.InnerText = GetLocalResourceObject("divAccepFormat.Text");
            h3ChooseFileType.InnerText = GetLocalResourceObject("h3ChooseFileType.Text");
            lblelimited.InnerText = GetLocalResourceObject("lblelimited.Text");
            lblFixedWidth.InnerHtml = GetLocalResourceObject("lblFixedWidth.Text");
            h3ChooseDel.InnerText = GetLocalResourceObject("h3ChooseDel.Text");
            lblTab.InnerText = GetLocalResourceObject("lblTab.Text");
            lblSemicolon.InnerText = GetLocalResourceObject("lblSemicolon.Text");
            lblPipe.InnerText = GetLocalResourceObject("lblPipe.Text");
            lblComma.InnerText = GetLocalResourceObject("lblComma.Text");
            lblSpace.InnerText = GetLocalResourceObject("lblSpace.Text");
            lblOther.InnerText = GetLocalResourceObject("lblOther.Text");
            lblseeHow.InnerText = GetLocalResourceObject("lblseeHow.Text");
            h3ChooseScope.InnerText = GetLocalResourceObject("h3ChooseScope.Text");
            divDataHeader.InnerText = GetLocalResourceObject("divDataHeader.Text");
            divDataStartFrom.InnerText = GetLocalResourceObject("divDataStartFrom.Text");
            divDataEnds.InnerText = GetLocalResourceObject("divDataEnds.Text");
            divChooseColumns.InnerText = GetLocalResourceObject("divChooseColumns.Text");
            divMappOfTheseCol.InnerText = GetLocalResourceObject("divMappOfTheseCol.Text");
            aDownloadFile.InnerText = GetLocalResourceObject("aDownloadFile.Text");
            h3Review.InnerText = GetLocalResourceObject("h3Review.Text");
            divForMultipleMatches.InnerText = GetLocalResourceObject("divForMultipleMatches.Text");
            divUnitOfMeasureX.InnerText = GetLocalResourceObject("divUnitOfMeasureX.Text");
            divUnitOfMeasureY.InnerText = GetLocalResourceObject("divUnitOfMeasureY.Text");
            divMPN.InnerText = GetLocalResourceObject("divMPN.Text");
            divDefineMount.InnerText = GetLocalResourceObject("divDefineMount.Text");
            divDefineLayers.InnerText = GetLocalResourceObject("divDefineLayers.Text");
            divDefinePeriod.InnerText = GetLocalResourceObject("divDefinePeriod.Text");
            optInch_X.InnerText = GetLocalResourceObject("optInch_X.Text");
            optMM_X.InnerText = GetLocalResourceObject("optMM_X.Text");
            optInch_Y.InnerText = GetLocalResourceObject("optInch_Y.Text");
            optMM_Y.InnerText = GetLocalResourceObject("optMM_Y.Text");
            optDays.InnerText = GetLocalResourceObject("optDays.Text");
            optWeeks.InnerText = GetLocalResourceObject("optWeeks.Text");
            divNoFileChoosen.InnerText = GetLocalResourceObject("divNoFileChoosen.Text");
            divDataHeaderAll.InnerHtml = GetLocalResourceObject("divDataHeaderAll.Text");
            h5EnterDataPdfOrDoc.InnerHtml = GetLocalResourceObject("h5EnterDataPdfOrDocCPL.Text");
            btnSubmitPdfDoc.Value = GetLocalResourceObject("btnSubmitPdfDoc.Text");
            editBOM.Value = GetLocalResourceObject("EditBOM");
            btnDelete.Value = GetLocalResourceObject("deleteRows");
            txtOther.Attributes["placeholder"] = GetLocalResourceObject("placeHolderOther.Text");
            headerRow.Attributes["placeholder"] = GetLocalResourceObject("placeHolderHeaderRow.Text");
            startRow.Attributes["placeholder"] = GetLocalResourceObject("placeHolderStartRow.Text");
            endRow.Attributes["placeholder"] = GetLocalResourceObject("placeHolderEndRow.Text");
            btnLoadNew.Value = GetLocalResourceObject("loadnew");
            return captions;
        }
        private static string getAssemblyPath(string entityNumber)
        {
            //string RootPath = Constant.FileServerPath;
            if (entityNumber.Contains("-A"))
            {
                entityNumber = entityNumber.Split('-')[0].ToString();
            }
            //test
            string folderPath = "";//Common.getFilePath(entityNumber);
            System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);

            if (impersonateOnFileServer != null)
            {
                //folderPath = RootPath + Convert.ToString(ds.Tables[0].Rows[0]["file_path"]);
                folderPath = Common.getFilePath(entityNumber);
                if (!Directory.Exists(folderPath + "\\assembly\\" + entityNumber))
                 {
                    Directory.CreateDirectory(folderPath + "\\assembly\\" + entityNumber);
                 }
                 folderPath = folderPath + "\\assembly\\" + entityNumber;
                 return folderPath;
            }
            
            return null;
        }
        [System.Web.Script.Services.ScriptMethod()]
        [WebMethod(EnableSession = true)]
        public static string SaveBOM(string jsonExport, string rootNode, string fileName, string number, string mapData, string layer, bool isMerge)
        {
            string filePath = "";
            int statusCode = 0;
            string message = "";
            try
            {
                string folderPath = getAssemblyPath(number);
                if (folderPath != null)
                {
                    System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);
                    if (impersonateOnFileServer != null)
                    {

                        folderPath = folderPath + "\\BOM\\";
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        if (Directory.Exists(folderPath + "CPL\\"))
                        {
                            string[] fileList = System.IO.Directory.GetFiles(folderPath + "CPL\\");
                            bool checkUploadedFile = false;
                            foreach (string files in fileList)
                            {
                                if (files.Contains("UPLOADED_"))
                                {
                                    checkUploadedFile = true;
                                }
                            }
                            if (checkUploadedFile)
                            {
                                foreach (string files in fileList)
                                {
                                    if (files.Contains("SUBMITTED_"))
                                    {
                                        File.Delete(files);
                                    }
                                }
                                foreach (string files in fileList)
                                {
                                    if (files.Contains("UPLOADED_"))
                                    {
                                        File.Copy(files, files.Replace("UPLOADED_", "SUBMITTED_"));
                                        File.Delete(files); ;

                                        try
                                        {
                                            long crm_rel_id = 0;
                                            if (sparrowFeApp.core.AppSessions.FEUserId != null)
                                            {
                                                long userid = 0;
                                                long.TryParse(sparrowFeApp.core.AppSessions.FEUserId, out userid);
                                                crm_rel_id = sparrowFeApp.core.Common.GetCrmRelIdFromUserid(userid);
                                            }
                                            string fileName1 = Path.GetFileName(files);
                                            string FEfolderPath = files.Replace(sparrowFeApp.core.Constant.FileServerPath, "").Replace(fileName1, "");
                                            if (FEfolderPath.Trim() != "" && FEfolderPath.Contains(number))
                                            {
                                                string oldfilename = Path.GetFileName(files);
                                                var feApp = new { fun_name = "file_rename", entity_number = number, file_name = oldfilename, rename_name = oldfilename.Replace("UPLOADED_", "SUBMITTED_"), file_path = FEfolderPath, crm_rel_id = crm_rel_id.ToString(), source = "FE", target = "EC" };
                                                string jsonFeApp = Newtonsoft.Json.JsonConvert.SerializeObject(feApp);
                                                sparrowFeApp.core.Common feAppData = new sparrowFeApp.core.Common();
                                                feAppData.FeAppAPI(jsonFeApp);
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }

                        filePath = folderPath + "cpl.xml";

                        CmBomImport.UpdateMapColumns(mapData);

                        DataTable dtBOM = (DataTable)JsonConvert.DeserializeObject(jsonExport, (typeof(DataTable)));
                        XmlDocument doc = new XmlDocument();
                        XmlNode rootXmlNode = null;

                        if (rootXmlNode == null)
                        {
                            rootXmlNode = doc.CreateElement("comps");
                            doc.AppendChild(rootXmlNode);
                        }

                        foreach (DataRow dr in dtBOM.Rows)
                        {
                            XmlNode userNode = doc.CreateElement("comp");
                            foreach (DataColumn dc in dtBOM.Columns)
                            {
                                var value = dr[dc].ToString();
                                XmlAttribute attribute = doc.CreateAttribute(dc.ColumnName.ToLower());
                                if (dc.ColumnName.ToLower() == "refdes")
                                {
                                    attribute.Value = Regex.Replace(value, "[^0-9A-Za-z ,]", ",");
                                }
                                else
                                {
                                    attribute.Value = value;
                                }
                                userNode.Attributes.Append(attribute);
                            }
                            rootXmlNode.AppendChild(userNode);
                        }

                        XmlDeclaration xmldecl = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                        doc.InsertBefore(xmldecl, rootXmlNode);

                        doc.Save(filePath);
                        statusCode = 1;
                        HttpContext.Current.Session["CPLImported"] = "true";
                        CmBomImport.SaveBomHistory(number, "SUBMITCPLFILE");
                    }
                }
            }
            catch (Exception ex)
            {
                statusCode = 0;
                message = ex.ToString();
            }
            var data = new { Status = statusCode, FileName = "cpl.xml", Message = message };
            jsonExport = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            //test
            fileName = Path.GetFileName(filePath);
            //string filePathnw = filePath.Remove(0, 40).Replace(fileName,"");
            long crm_rel_id1 = 0;
            if (sparrowFeApp.core.AppSessions.FEUserId != null)
            {
                long userid = 0;
                long.TryParse(sparrowFeApp.core.AppSessions.FEUserId, out userid);
                crm_rel_id1 = sparrowFeApp.core.Common.GetCrmRelIdFromUserid(userid);
            }
            string filePathnw = filePath.Replace(Constant.FileServerPath, "").Replace(fileName, "");
            if (filePathnw.Trim() != "" && filePathnw.Contains(number))
            {
                var feApp = new { fun_name = "file_sync", entity_number = number, file_name = fileName, file_type = "assembly_editor", file_path = filePathnw, crm_rel_id = crm_rel_id1.ToString(), source = "FE", target = "EC" };
                string jsonFeApp = JsonConvert.SerializeObject(feApp);
                Common feAppData = new Common();
                feAppData.FeAppAPI(jsonFeApp);
            } 
            return jsonExport;
        }
    }
}