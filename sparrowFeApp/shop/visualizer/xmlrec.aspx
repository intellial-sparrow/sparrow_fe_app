﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Runtime.InteropServices" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Threading.Tasks" %>

<script language="c#" runat="server">


    const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
    string configurl = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CofiguratorUrl"]);

    string backupSessionId = "";
    public void Page_Load(object sender, EventArgs e)
    {
        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        if (Request.HttpMethod.ToString().Trim().ToUpper() == "POST")
        {
            try
            {
                var i8responce = new Database.DA();
                string responsecontent = "";
                using (StreamReader sr = new StreamReader(Request.InputStream))
                {
                    responsecontent = sr.ReadToEnd().Trim();
                }
                string ordtype = string.Empty; string customerid = null, sessionid = null, configaction = null;
                long EntityId = 0;
                string basketSessionId = string.Empty;
                string EntityNo = null, StencilBotECRegComp = null, StencilTopECRegComp = null, PanelECRegComp = null;
                string StencilTopQty = string.Empty, StencilBotQty = string.Empty; string BoardDeliveryTerm = string.Empty;
                int count = 1; string BoardDeliveryDate = string.Empty;
                string i8Status = "";
                int datastatus = 0;
                string Track = "";
                int Assembly = 0;
                if (!Request.QueryString["action"].ToUpper().StartsWith("COMPONENTS"))
                {
                    responsecontent = responsecontent.ToString().Replace("'''''''", "");
                    responsecontent = responsecontent.ToString().Replace("''", "'");
                    responsecontent = responsecontent.ToString().Replace("'", "''");
                }
                if (Request.QueryString["session_id"] != null || Request.QueryString["session_id"] != "")
                {
                    backupSessionId = Request.QueryString["session_id"];
                    if (Session["visSession"] == null || Session["visSession"].ToString() != backupSessionId)
                    {
                        Session["BOMImported"] = null;
                        Session["CPLImported"] = null;
                    }
                }
                ordtype = Convert.ToString(Request.QueryString["type"]);
                configaction = Convert.ToString(Request.QueryString["action"]);

                if (Request["status"] != null)
                {
                    int.TryParse(Convert.ToString(Request.QueryString["status"]), out datastatus);
                }


                if (Request.QueryString["action"].ToUpper().StartsWith("NOTAPPROVED") && Request.QueryString["Key"] != null)//
                {
                    Response.Write(configurl + "/shop/PreproductionRemark.aspx?OrderNo=" + Request.QueryString["Key"].ToString());
                    return;
                }
                if (Request.QueryString["action"].ToUpper().StartsWith("APPROVED") && Request.QueryString["key"] != null)// 
                {
                    string orderNo = Request.QueryString["key"].ToString();
                    if (orderNo.Contains("-A"))
                    {
                        string componentPrice = Convert.ToString(Request.QueryString["componentPrice"]) ?? "0";
                        assemblyProductionApprove(orderNo, componentPrice);
                    }
                    else
                    {
                        productionApprove();
                    }


                    EC09WebApp.shop.rb.ReportBuilderUtilities rbObj = new EC09WebApp.shop.rb.ReportBuilderUtilities();
                    rbObj.DeleteRBFile(orderNo);

                    if (orderNo.Contains("-A"))
                    {
                        if (sparrowFeApp.core.AppSessions.EccUserId == string.Empty || sparrowFeApp.core.AppSessions.EccUserId == "0")
                        {
                            Response.Write(configurl + "/shop/orders/eccOrderModified.aspx?RedirectAction=APPROVED");
                        }
                        else
                        {
                            Response.Write(configurl + "/shop/orders/eccOrderModified.aspx?successmsgtitle=APPROVED");
                        }
                    }
                    else
                    {
                        Response.Write(configurl + "/shop/orders/eccOrderModified.aspx?RedirectAction=APPROVED");
                    }
                    return;
                }

                if (Request.QueryString["action"].ToUpper().StartsWith("COMPONENTS") && Request.QueryString["key"] != null)// 
                {
                    string type = Request["type"] ?? "";

                    bool isFromECC = false;
                    if (HttpContext.Current.Session["configurator"] != null)
                    { //TODO first
                        //EC09WebApp.ConfiguratorCl conf = (EC09WebApp.ConfiguratorCl)HttpContext.Current.Session["configurator"];
                        //if (conf.ECCCustUserId != 0)
                        //{
                        //    isFromECC = true;
                        //}
                    }

                    if ((isFromECC == false && (type.ToUpper() == "MODIFYBASKET" || type.ToUpper() == "MODIFYINQ")) || type.ToUpper() == "REPEATORDER")
                    {
                        HttpContext.Current.Session["COMPONENTFILE"] = responsecontent;
                    }
                    else
                    {
                        UploadeComponentsFile(Request.QueryString["key"], responsecontent);
                        InsertHistoryBOMCPL(Request.QueryString["key"]);
                    }
                    if (Request.QueryString["closewindow"] != null && Request.QueryString["closewindow"].ToString().ToLower() == "yes")
                    {
                        Response.Write(configurl + "/shop/orders/eccordermodified.aspx?successmsgtitle=close");
                    }
                    else
                    {
                        Response.Write("OK");
                    }
                    return;
                }

                if (Request.QueryString["action"].ToUpper().StartsWith("PIXPECT") && Request.QueryString["key"] != null)// 
                {
                    string imageName = Request.QueryString["name"] ?? "";
                    string status = Request.QueryString["status"] ?? "";
                    SavePIXpectFile(Request.QueryString["key"], responsecontent, imageName, status);
                    Response.Write("OK");
                }
                if (Request.QueryString["action"].ToUpper().StartsWith("MODIFYSTENCILDETAIL") && Request.QueryString["key"] != null)// 
                {
                    if (Request.QueryString["closewindow"] != null && Request.QueryString["closewindow"].ToString().ToLower() == "yes")
                    {
                        Response.Write(configurl + "/shop/orders/eccordermodified.aspx?successmsgtitle=close");
                    }
                    else
                    {
                        //TODO
                        //string strsql = string.Format("exec spVisualizerfilepath '" + Request.QueryString["key"] + "',''", Keydata);
                        //var objgd = new EC09WebApp.API.gd();
                        //DataSet ds = objgd.GetData(strsql, Keydata);
                        //if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(ds.Tables[0].Rows[0]["FolderPath"]) != "")
                        //{
                        //    string folderPath = Convert.ToString(ds.Tables[0].Rows[0]["FolderPath"]);
                        //    folderPath = folderPath + "\\" + Request.QueryString["key"] + ".xml";
                        //    EC09WebApp.EC09WebService.BPO bpo = new EC09WebApp.EC09WebService.BPO();
                        //    bpo.ConfiguratorXMLUpdate(folderPath, false, responsecontent, 0);

                        //    try
                        //    {
                        //        XmlDocument xdoc = new XmlDocument();
                        //        xdoc.LoadXml(Convert.ToString(responsecontent.Trim()));
                        //        System.Xml.XmlNode nodeBoardProperties3 = xdoc.SelectSingleNode("OrderDetail/BoardProperties3");
                        //        string stencilTopThickness = nodeBoardProperties3.SelectSingleNode("StencilTopThickness").InnerText;
                        //        string stencilBotThickness = nodeBoardProperties3.SelectSingleNode("StencilBotThickness").InnerText;
                        //        string updateSql = string.Format(" exec sp_updateStencilThicknes '{0}','{1}','{2}'", Request.QueryString["key"], stencilTopThickness, stencilBotThickness);
                        //        objgd.PutData(updateSql, Keydata);
                        //    }
                        //    catch
                        //    {
                        //    }



                        //    InsertHistoryBOMCPL(Request.QueryString["key"], "ACTION_MODIFIED");

                        //    Response.Write("OK");
                        //}
                    }
                    return;
                }
                if (Request.QueryString.ToString().ToUpper().Contains("I8STATUS"))
                {
                    i8Status = Convert.ToString(Request.QueryString["i8Status"]);
                }
                sessionid = Guid.NewGuid().ToString();
                string strSessionId = Convert.ToString(sessionid);

                /* for buildup image */
                string buildupimg = "";
                if (Request.QueryString["buildup"] != null && Convert.ToString(Request.QueryString["buildup"]) != "")
                {
                    buildupimg = Convert.ToString(Request.QueryString["buildup"]);
                }

                if (Request.QueryString["action"].ToUpper().StartsWith("BACKTOANALYSIS") && Request.QueryString["Key"] != null)
                {
                    Track = "action=BACKTOANALYSIS";
                }

                if (Request.QueryString["assembly"] != null && Convert.ToString(Request.QueryString["assembly"]) != "")
                {
                    Assembly = Convert.ToInt32(Request.QueryString["assembly"]);
                }
                if (ordtype.ToLower() == "modifybasket" && Request.QueryString["Key"].ToUpper().StartsWith("P4M"))
                {

                    //EC09WebApp.P4Maker.Services.ProjectService service = new EC09WebApp.P4Maker.Services.ProjectService();
                    //long createdBy = Convert.ToInt32(sparrowFeApp.core.AppSessions.EccUserId);
                    //service.ModifyProject(Request.QueryString["key"].ToString(), responsecontent, createdBy);

                    //Response.Write(configurl + "/shop/orders/eccordermodified.aspx?successmsgtitle=close");
                }
                if (ordtype.ToLower() == "modifyorder".ToLower() && Request.QueryString["key"].ToString().ToUpper().Contains("-A"))
                {
                    ModifyAssemblyOrder(Request.QueryString["key"].ToString().ToUpper(), responsecontent);
                    if (Request.QueryString["closewindow"] != null && Request.QueryString["closewindow"].ToString().ToLower() == "yes")
                    {
                        Response.Write(configurl + "/shop/orders/eccordermodified.aspx?successmsgtitle=close&action=" + ordtype.ToLower());
                    }
                    else
                    {
                        Response.Write("OK");
                    }
                    return;
                }
                if ((ordtype.ToLower() == "modifyorder".ToLower() && configaction.ToUpper() == "MODIFYORDER") || (ordtype.ToLower() == "modifyBasket".ToLower() && configaction.ToUpper() == "MODIFYBASKET") || (ordtype.ToLower() == "MODIFYASSEMBLY".ToLower() && configaction.ToUpper() == "MODIFYASSEMBLY"))
                {
                    ModifyAssemblyOrder(Request.QueryString["key"].ToString().ToUpper(), responsecontent);
                    if (Request.QueryString["closewindow"] != null && Request.QueryString["closewindow"].ToString().ToLower() == "yes")
                    {
                        Response.Write(configurl + "/shop/orders/eccordermodified.aspx?successmsgtitle=close&action=" + ordtype.ToLower());
                    }
                    else
                    {
                        Response.Write("OK");
                    }
                    return;
                }
                if (Request.QueryString["closewindow"] != null && Request.QueryString["closewindow"].ToString().ToLower() == "yes")
                {
                    Response.Write(configurl + "/shop/orders/eccordermodified.aspx?successmsgtitle=close");
                }
                if (ordtype.ToLower() == "newBasket".ToLower() || ordtype.ToLower() == "modifyBasket".ToLower() || ordtype.ToLower() == "inquiry".ToLower() || ordtype.ToLower() == "repeatModifyOrder".ToLower() || ordtype.ToLower() == "repeatOrder".ToLower())
                {
                    responsecontent = "<OrderDetailInformation>" + responsecontent + "</OrderDetailInformation>";
                    doc.LoadXml(responsecontent.Trim());

                    System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
                    xDoc.LoadXml(responsecontent.Trim());

                    foreach (System.Xml.XmlNode nodedata in doc.SelectSingleNode("OrderDetailInformation"))
                    {
                        //Coding added for add new node in xml by Heena Dated on 15th Oct 2013
                        System.Xml.XmlNode nodeProductionProperties3 = nodedata.SelectSingleNode("ProductionProperties3");
                        if (nodeProductionProperties3 == null)
                        {
                            System.Xml.XmlNode xmlnewElementProductionProperties3 = doc.CreateNode(System.Xml.XmlNodeType.Element, "ProductionProperties3", null);
                            System.Xml.XmlNode chFullCheck = doc.CreateNode(System.Xml.XmlNodeType.Element, "FullCheck", null);
                            chFullCheck.InnerText = "false";
                            xmlnewElementProductionProperties3.AppendChild(chFullCheck);

                            System.Xml.XmlNode chPlatedSlots = doc.CreateNode(System.Xml.XmlNodeType.Element, "PlatedSlots", null);
                            chPlatedSlots.InnerText = "false";
                            xmlnewElementProductionProperties3.AppendChild(chPlatedSlots);

                            nodedata.AppendChild(xmlnewElementProductionProperties3);
                        }


                        System.Xml.XmlDocument docdata = new System.Xml.XmlDocument();
                        docdata.LoadXml(nodedata.OuterXml.Trim());
                        if (ordtype.ToLower() == "newBasket".ToLower())
                        {
                            if (Request.QueryString["key"] != null && Convert.ToString(Request.QueryString["key"]).ToLower().Contains("STENCIL".ToLower()))
                            {
                                EntityNo = Convert.ToString(Request.QueryString["key"]).Replace("STENCIL", "");
                            }
                            //sessionid = Convert.ToString(Request.QueryString["key"]);
                        }
                        else if (ordtype.ToLower() == "repeatOrder".ToLower())
                        {
                            EntityNo = Convert.ToString(Request.QueryString["key"]);
                            // sessionid = Guid.NewGuid().ToString();
                        }
                        else if (ordtype.ToLower() == "repeatModifyOrder".ToLower())
                        {
                            EntityNo = Convert.ToString(Request.QueryString["key"]);
                            EntityId = GetOrderId();
                            //sessionid = Guid.NewGuid().ToString();
                        }
                        else if (ordtype.ToLower() == "modifyBasket".ToLower())
                        {
                            EntityNo = Convert.ToString(Request.QueryString["key"]);
                            string sqlGetBasketId = string.Format("SELECT Session_Id, Basket_Id from BasketItems where BasketNo = '{0}'", EntityNo);
                            System.Data.DataTable dt = (new sparrowFeApp.ecgdService.gdSoapClient()).GetData(sqlGetBasketId, Keydata).Tables[0];
                            EntityNo = Convert.ToString(dt.Rows[0]["Basket_Id"]);
                            basketSessionId = Convert.ToString(dt.Rows[0]["Session_Id"]);
                            EntityId = Convert.ToInt64(dt.Rows[0]["Basket_Id"]);
                            // sessionid = Guid.NewGuid().ToString();
                        }
                        sessionid = strSessionId + "-" + count;
                        count++;


                        customerid = nodedata.ChildNodes[0].InnerText;
                        int intCustomerId = 0;
                        int.TryParse(customerid, out intCustomerId);

                        System.Xml.XmlNode nodeBoardProperties3 = nodedata.SelectSingleNode("BoardProperties3");
                        if (StencilTopECRegComp == null)
                        {
                            StencilTopECRegComp = nodeBoardProperties3.SelectSingleNode("StencilTopECRegComp").InnerText;
                        }

                        if (StencilBotECRegComp == null)
                        {
                            StencilBotECRegComp = nodeBoardProperties3.SelectSingleNode("StencilBotECRegComp").InnerText;
                        }
                        if (PanelECRegComp == null)
                        {
                            PanelECRegComp = nodeBoardProperties3.SelectSingleNode("PanelECRegComp").InnerText;
                        }
                        System.Xml.XmlNode nodeOrderProperties3 = nodedata.SelectSingleNode("OrderProperties3");
                        if (StencilTopQty == null || StencilTopQty == "")
                        {
                            StencilTopQty = nodeOrderProperties3.SelectSingleNode("StencilTopQty").InnerText;
                        }
                        if (StencilBotQty == null || StencilBotQty == "")
                        {
                            StencilBotQty = nodeOrderProperties3.SelectSingleNode("StencilBotQty").InnerText;
                        }
                        if (BoardDeliveryTerm == null || BoardDeliveryTerm == "")
                        {
                            BoardDeliveryTerm = nodeOrderProperties3.SelectSingleNode("BoardDeliveryTerm").InnerText;
                        }
                        if (BoardDeliveryDate == null || BoardDeliveryDate == "")
                        {
                            BoardDeliveryDate = nodeOrderProperties3.SelectSingleNode("BoardDeliveryDate").InnerText;
                        }
                        if (BoardDeliveryDate == "")
                        {
                            BoardDeliveryDate = DateTime.Now.ToString("dd/MM/yyyy");
                        }
                        string s;
                        s = "exec i8responces_insert '" + Convert.ToString(intCustomerId).Trim() + "','" + Convert.ToString(sessionid) + "', '"
                            + Convert.ToString(ordtype) + "','" + Convert.ToString(nodedata.OuterXml.Trim()) + "','"
                            + Convert.ToString(EntityNo) + "','" + buildupimg.Trim() + "','" + i8Status.Trim() + "','" + datastatus + "','" + Track.Trim() + "'";

                        (new sparrowFeApp.ecgdService.gdSoapClient()).PutData(s, Keydata);
                    }
                }
                else
                {

                    responsecontent = responsecontent.Replace("&shy;", "-");
                    doc.LoadXml(responsecontent);



                    System.Xml.XmlNode node = doc.SelectSingleNode("OrderDetail");
                    customerid = node.ChildNodes[0].InnerText;
                    int intCustomerId = 0;
                    int.TryParse(customerid, out intCustomerId);

                    //responsecontent = "<OrderDetailInformation>" + responsecontent + "</OrderDetailInformation>";
                    //doc.LoadXml(responsecontent.Trim());


                    //System.Xml.XmlDocument docdata = new System.Xml.XmlDocument();
                    //docdata.LoadXml(nodedata.OuterXml.Trim());
                    //Coding added for add new node in xml by Heena Dated on 15th Oct 2013
                    System.Xml.XmlNode nodeProductionProperties3 = node.SelectSingleNode("ProductionProperties3");
                    if (nodeProductionProperties3 == null)
                    {
                        System.Xml.XmlNode xmlnewElementProductionProperties3 = doc.CreateNode(System.Xml.XmlNodeType.Element, "ProductionProperties3", null);

                        System.Xml.XmlNode chFullCheck = doc.CreateNode(System.Xml.XmlNodeType.Element, "FullCheck", null);
                        chFullCheck.InnerText = "false";
                        xmlnewElementProductionProperties3.AppendChild(chFullCheck);

                        System.Xml.XmlNode chPlatedSlots = doc.CreateNode(System.Xml.XmlNodeType.Element, "PlatedSlots", null);
                        chPlatedSlots.InnerText = "false";
                        xmlnewElementProductionProperties3.AppendChild(chPlatedSlots);

                        node.AppendChild(xmlnewElementProductionProperties3);
                    }
                    System.Xml.XmlNode nodeBoardProperties3 = node.SelectSingleNode("BoardProperties3");
                    if (PanelECRegComp == null)
                    {
                        PanelECRegComp = nodeBoardProperties3.SelectSingleNode("PanelECRegComp").InnerText;
                    }
                    System.Xml.XmlNode nodeOrderProperties3 = node.SelectSingleNode("OrderProperties3");
                    if (StencilTopQty == null || StencilTopQty == "")
                    {
                        StencilTopQty = nodeOrderProperties3.SelectSingleNode("StencilTopQty").InnerText;
                    }
                    if (StencilBotQty == null || StencilBotQty == "")
                    {
                        StencilBotQty = nodeOrderProperties3.SelectSingleNode("StencilBotQty").InnerText;
                    }
                    if (BoardDeliveryTerm == null || BoardDeliveryTerm == "")
                    {
                        BoardDeliveryTerm = nodeOrderProperties3.SelectSingleNode("BoardDeliveryTerm").InnerText;
                    }
                    if (BoardDeliveryDate == null || BoardDeliveryDate == "")
                    {
                        BoardDeliveryDate = nodeOrderProperties3.SelectSingleNode("BoardDeliveryDate").InnerText;
                    }
                    if (BoardDeliveryDate == "")
                    {
                        BoardDeliveryDate = DateTime.Now.ToString("dd/MM/yyyy");
                    }

                    if (ordtype.ToLower() == "modifyBasket".ToLower())
                    {
                        EntityNo = Convert.ToString(Request.QueryString["key"]);
                        string sqlGetBasketId = string.Format("SELECT Session_Id, Basket_Id from BasketItems where BasketNo = '{0}'", EntityNo);
                        System.Data.DataTable dt = i8responce.GetDataTable(sqlGetBasketId);
                        EntityNo = Convert.ToString(dt.Rows[0]["Basket_Id"]);
                        basketSessionId = Convert.ToString(dt.Rows[0]["Session_Id"]);
                        EntityId = Convert.ToInt64(dt.Rows[0]["Basket_Id"]);
                        sessionid = Guid.NewGuid().ToString();
                    }
                    else if (ordtype.ToLower() == "modifyOrder".ToLower())
                    {
                        EntityNo = Convert.ToString(Request.QueryString["key"]);
                        EntityId = GetOrderId();
                        //doc.LoadXml(responsecontent.Trim());
                        //System.Xml.XmlNode nodeBoardProperties3 = doc.SelectSingleNode("OrderDetail/BoardProperties3");
                        StencilTopECRegComp = nodeBoardProperties3.SelectSingleNode("StencilBotECRegComp").InnerText;
                        StencilBotECRegComp = nodeBoardProperties3.SelectSingleNode("StencilBotECRegComp").InnerText;

                        sessionid = Guid.NewGuid().ToString();
                    }
                    else if (ordtype.ToLower() == "processPPA".ToLower())
                    {
                        EntityNo = Convert.ToString(Request.QueryString["key"]);
                        EntityId = GetOrderId();
                        //doc.LoadXml(responsecontent.Trim());
                        //System.Xml.XmlNode nodeBoardProperties3 = doc.SelectSingleNode("OrderDetail/BoardProperties3");
                        StencilTopECRegComp = nodeBoardProperties3.SelectSingleNode("StencilBotECRegComp").InnerText;
                        StencilBotECRegComp = nodeBoardProperties3.SelectSingleNode("StencilBotECRegComp").InnerText;

                        sessionid = Guid.NewGuid().ToString();
                    }
                    else if (ordtype.ToLower() == "modifyINQ".ToLower())
                    {
                        EntityNo = Convert.ToString(Request.QueryString["key"]);
                        string sqlGetBasketId = string.Format("SELECT Session_Id, Basket_Id from BasketItems where BasketNo = '{0}'", EntityNo);
                        System.Data.DataTable dt = (new sparrowFeApp.ecgdService.gdSoapClient()).GetData(sqlGetBasketId, Keydata).Tables[0];
                        EntityNo = Convert.ToString(dt.Rows[0]["Basket_Id"]);
                        basketSessionId = Convert.ToString(dt.Rows[0]["Session_Id"]);
                        EntityId = Convert.ToInt64(dt.Rows[0]["Basket_Id"]);
                        sessionid = Guid.NewGuid().ToString();
                        // objException.LogExceptiondata(sessionid, 355, "sessionissuei81");
                    }
                    else if (ordtype.ToLower() == "createOffer".ToLower())
                    {
                        EntityNo = Convert.ToString(Request.QueryString["key"]);
                        string sqlGetBasketId = string.Format("SELECT Basket_Id from BasketItems where BasketNo = '{0}'", EntityNo);
                        System.Data.DataTable dt = i8responce.GetDataTable(sqlGetBasketId);
                        EntityNo = Convert.ToString(dt.Rows[0]["Basket_Id"]);
                        //sessionid = Guid.NewGuid().ToString();
                    }
                    else if (ordtype.ToLower() == "newOffer".ToLower())
                    {
                        //EntityNo = Convert.ToString(Request.QueryString["key"]);
                        sessionid = Convert.ToString(Request.QueryString["key"]);
                    }
                    else if (ordtype.ToLower() == "save".ToLower())
                    {
                        //EntityNo = Convert.ToString(Request.QueryString["key"]);
                        sessionid = Convert.ToString(Request.QueryString["key"]);
                    }
                    string s = "";
                    s = "exec i8responces_insert '" + Convert.ToString(intCustomerId).Trim() + "','" + Convert.ToString(sessionid) + "', '"
                        + Convert.ToString(ordtype) + "','" + Convert.ToString(responsecontent) + "','" + Convert.ToString(EntityNo) + "','"
                        + buildupimg.Trim() + "','" + i8Status.Trim() + "','" + datastatus + "' ,'" + Track.Trim() + "'";
                    (new sparrowFeApp.ecgdService.gdSoapClient()).PutData(s, Keydata);
                }

                if (Request.QueryString["action"].ToUpper().StartsWith("BACKTOANALYSIS") && Request.QueryString["Key"] != null)
                {
                    Response.Write(configurl + "/shop/PreproductionRemark.aspx?InqNo=" + Request.QueryString["Key"].ToString() + "&action=" + Request.QueryString["action"].ToString() + "&sessionid=" + Convert.ToString(sessionid));
                    return;
                }

                if (configaction == "add")
                {
                    if (ordtype.ToLower() == "newBasket".ToLower())
                    {
                        Response.Write(configurl + "/shop/eccuploaddata.aspx?sessionid=" + strSessionId + "&type=newBasket&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm +
                            "&BoardDeliveryDate=" + BoardDeliveryDate + "&dataStatus=" + Convert.ToString(datastatus) + "&Assembly=" + Assembly);

                    }
                    else if (ordtype.ToLower() == "modifyBasket".ToLower())
                    {
                        Response.Write(configurl + "/shop/orders/eccOrderModified.aspx?sessionid=" + strSessionId + "&orderNum=" + EntityNo +
                            "&type=modifyBasket&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp +
                            "&PanelECRegComp=" + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty +
                            "&BoardDeliveryTerm=" + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&i8Status=" + i8Status +
                            "&dataStatus=" + Convert.ToString(datastatus) + "&Assembly=" + Assembly);
                    }
                    else if (ordtype.ToLower() == "modifyOrder".ToLower())
                    {
                        Response.Write(configurl + "/shop/eccUploaddata.aspx?sessionid=" + sessionid + "&orderNum=" + EntityNo +
                            "&type=modifyOrder&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp +
                            "&PanelECRegComp=" + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty +
                            "&BoardDeliveryTerm=" + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&i8Status=" + i8Status +
                            "&dataStatus=" + Convert.ToString(datastatus) + "&Assembly=" + Assembly);
                    }

                    else if (ordtype.ToLower() == "createOffer".ToLower())
                    {
                        if (Request["frompage"] != null)
                        {
                            Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&orderNum=" + EntityNo +
                                "&type=createOffer&frompage=" + Convert.ToString(Request["frompage"]) + "&StencilBotECRegComp=" +
                                StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                                "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" +
                                BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate);
                        }
                        else
                        {
                            Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&orderNum=" + EntityNo +
                                "&type=createOffer&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp +
                                "&PanelECRegComp=" + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty +
                                "&BoardDeliveryTerm=" + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate);
                        }
                    }
                    else if (ordtype.ToLower() == "repeatOrder".ToLower())
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + strSessionId + "&type=repeatOrder&orderNum=" + EntityNo
                            + "&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp +
                            "&PanelECRegComp=" + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty +
                            "&BoardDeliveryTerm=" + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate +
                            "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else if (ordtype.ToLower() == "repeatModifyOrder".ToLower())
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + strSessionId + "&type=repeatOrder&orderNum=" + EntityNo
                            + "&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp +
                            "&PanelECRegComp=" + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty +
                            "&BoardDeliveryTerm=" + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&i8Status=" + i8Status
                            + "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else if (ordtype.ToLower() == "inquiry".ToLower())
                    {
                        Response.Write(configurl + "/shop/eccuploaddata.aspx?sessionid=" + strSessionId + "&type=inquiry&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm
                            + "&BoardDeliveryDate=" + BoardDeliveryDate + "&i8Status=" + i8Status +
                            "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else if (ordtype.ToLower() == "modifyINQ".ToLower())
                    {
                        Response.Write(configurl + "/shop/eccUploaddata.aspx?sessionid=" + sessionid + "&type=modifyINQ&orderNum=" + EntityNo +
                            "&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp="
                            + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm="
                            + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&i8Status=" + i8Status +
                            "&dataStatus=" + Convert.ToString(datastatus) + "&Assembly=" + Assembly);
                    }
                    else if (ordtype.ToLower() == "newOrder".ToLower())
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + strSessionId + "&type=newOrder&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm +
                            "&BoardDeliveryDate=" + BoardDeliveryDate + "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else if (ordtype.ToLower() == "newOffer".ToLower())
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&type=newOffer&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm +
                            "&BoardDeliveryDate=" + BoardDeliveryDate);
                    }
                }
                else if (configaction == "save")
                {
                    if (ordtype.ToLower() == "newbasket")
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&type=save&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" +
                            BoardDeliveryTerm + "&configaction=" + configaction + "&BoardDeliveryDate=" + BoardDeliveryDate + "&Assembly=" + Assembly + "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else if (ordtype.ToLower() == "repeatModifyOrder".ToLower())
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + strSessionId + "&type=save&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm
                            + "&configaction=" + configaction + "&BoardDeliveryDate=" + BoardDeliveryDate + "&PerantOrder=" + EntityNo +
                            "&orderNum=" + EntityNo + "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else if (ordtype.ToLower() == "modifyOrder".ToLower())
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&type=save&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp
                            + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm
                            + "&configaction=" + configaction + "&BoardDeliveryDate=" + BoardDeliveryDate + "&PerantOrder=" + EntityNo
                            + "&orderNum=" + EntityNo + "&dataStatus=" + Convert.ToString(datastatus));
                    }

                    else
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&type=save&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm +
                            "&configaction=" + configaction + "&BoardDeliveryDate=" + BoardDeliveryDate + "&dataStatus=" + Convert.ToString(datastatus));
                    }

                }
                else if (configaction == "inquiry")
                {
                    if (ordtype.ToLower() == "repeatModifyOrder".ToLower())
                    {
                        ordtype = "repeatOrder";
                    }
                    if (ordtype.ToLower() == "newbasket")
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + strSessionId + "&type=inquiry&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm +
                            "&BoardDeliveryDate=" + BoardDeliveryDate + "&InqOrder=" + "&visconfgtype=" + ordtype.ToLower() +
                            "&visordernumber=" + EntityNo + "&Assembly=" + Assembly + "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else if (ordtype.ToLower() == "modifybasket")
                    {
                        Response.Write(configurl + "/shop/orders/eccOrderModified.aspx?sessionid=" + sessionid + "&type=inquiry&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" +
                            BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&InqOrder=" +
                            Convert.ToString(Request.QueryString["key"]) + "&visconfgtype=" + ordtype.ToLower() +
                            "&visordernumber=" + EntityNo + "&i8Status=" + i8Status + "&dataStatus=" + Convert.ToString(datastatus) + "&Assembly=" + Assembly);
                    }
                    else if (ordtype.ToLower() == "modifyinq")
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&type=inquiry&orderNum=" +
                            EntityNo + "&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp +
                            "&PanelECRegComp=" + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" +
                            StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate +
                            "&InqOrder=" + Convert.ToString(Request.QueryString["key"]) + "&visconfgtype=" + ordtype.ToLower() +
                            "&visordernumber=" + EntityNo + "&i8Status=" + i8Status + "&dataStatus=" + Convert.ToString(datastatus) + "&Assembly=" + Assembly);
                    }
                    else if (ordtype.ToLower() == "createoffer")
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&type=inquiry&orderNum=" + EntityNo +
                            "&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp="
                            + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm="
                            + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&InqOrder=" +
                            Convert.ToString(Request.QueryString["key"]) + "&visconfgtype=" + ordtype.ToLower() + "&visordernumber=" + EntityNo);
                    }
                    else if (ordtype.ToLower() == "repeatOrder")
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + strSessionId + "&type=inquiry&StencilBotECRegComp="
                            + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" +
                            PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" +
                            BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&InqOrder=" +
                            Convert.ToString(Request.QueryString["key"]) + "&visconfgtype=" + ordtype.ToLower() + "&visordernumber=" + EntityNo
                             + "&i8Status=" + i8Status + "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&type=inquiry&StencilBotECRegComp=" +
                            StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp + "&PanelECRegComp=" + PanelECRegComp +
                            "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty + "&BoardDeliveryTerm=" + BoardDeliveryTerm +
                            "&BoardDeliveryDate=" + BoardDeliveryDate + "&InqOrder=" + EntityNo + "&visconfgtype=" + ordtype.ToLower() +
                            "&visordernumber=" + EntityNo + "&i8Status=" + i8Status + "&dataStatus=" + Convert.ToString(datastatus) + "&Assembly=" + Assembly);
                    }
                }

                else if (ordtype.ToLower() == "processppa")
                {
                    if (configaction.ToLower() == "releaseppa")
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&orderNum=" + EntityNo +
                            "&type=releasePPA&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp +
                            "&PanelECRegComp=" + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty +
                            "&BoardDeliveryTerm=" + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&i8Status=" + i8Status +
                            "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else if (configaction.ToLower() == "releaseconfirmppa")
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&orderNum=" + EntityNo +
                            "&type=releaseConfirmPPA&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp +
                            "&PanelECRegComp=" + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty +
                            "&BoardDeliveryTerm=" + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&i8Status=" + i8Status +
                            "&dataStatus=" + Convert.ToString(datastatus));
                    }
                    else if (configaction.ToLower() == "ppatosi")
                    {
                        Response.Write(configurl + "/shop/uploaddata.aspx?sessionid=" + sessionid + "&orderNum=" + EntityNo +
                            "&type=PPAtoSI&StencilBotECRegComp=" + StencilBotECRegComp + "&StencilTopECRegComp=" + StencilTopECRegComp +
                            "&PanelECRegComp=" + PanelECRegComp + "&StencilTopQty=" + StencilTopQty + "&StencilBotQty=" + StencilBotQty +
                            "&BoardDeliveryTerm=" + BoardDeliveryTerm + "&BoardDeliveryDate=" + BoardDeliveryDate + "&i8Status=" + i8Status +
                            "&dataStatus=" + Convert.ToString(datastatus));
                    }
                }

            }
            catch (TimeoutException ex1)
            {
                string exp = ex1.ToString();
                exp = "";
                Response.Write(configurl + "/shop/uploaddata.aspx?error=true" + exp);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }
        else if (Request.QueryString["closewindow"] != null && Request.QueryString["closewindow"].ToString().ToLower() == "yes")
        {
            Response.Write(configurl + "/shop/orders/eccordermodified.aspx?successmsgtitle=close");
        }
        else
        {
            //if (Session["UserId"] != null) { userId = (long)EC09WebApp.EC09Sessions.UserId; } TODO
            string a = Convert.ToString(Request.HttpMethod);
            string b = Convert.ToString(Request.Params);
            string c = Convert.ToString(Request.UrlReferrer);
            string d = Convert.ToString(Request.ContentType);
            string f = Convert.ToString(Request.UserAgent);
            Response.Write(configurl + "/shop/uploaddata.aspx?error=true");
        }
    }

    private long GetOrderId()
    {
        string orderNumber = Convert.ToString(Request.QueryString["key"]);
        string sqlGetOrdertId = string.Format("select OrderId from genOrders where OrderNumber = '{0}'", orderNumber);
        System.Data.DataTable dt = (new sparrowFeApp.ecgdService.gdSoapClient()).GetData(sqlGetOrdertId, Keydata).Tables[0];
        return Convert.ToInt64(dt.Rows[0]["OrderId"]);
    }



    public void productionApprove()
    {
        try
        {
            //var objgd = new EC09WebApp.API.gd();
            //string IP = "";

            //try
            //{
            //    IP = EC09.Common.Common1.GetClientIPType();
            //    // IP = BPO.GetClientIP();
            //}
            //catch
            //{
            //}

            //string orderNo = Request.QueryString["key"].ToString();
            //string Sql = string.Format("exec PreProductionApproval '{0}','{1}','{2}'", orderNo, "APPROVED", IP);
            //DataSet validorder = objgd.GetData(Sql, Keydata);

            //if (validorder != null && validorder.Tables.Count > 0 && validorder.Tables[0].Rows.Count > 0)
            //{
            //    bool Status = true;
            //    bool.TryParse(validorder.Tables[0].Rows[0]["Result"].ToString(), out Status);
            //    if (!Status)
            //    {
            //        return;
            //    }
            //}
            //EC09.Business.UserLog userLog = new EC09.Business.UserLog();
            //EC09WebApp.EC09WebService.BPO BPO = new EC09WebApp.EC09WebService.BPO();
            //BPO.PreproductionApproveOrder(orderNo, 0, EC09.Common.Common1.GetClientIPType());
            //try
            //{
            //    string Sqlstr = string.Format("exec GetDeliveryDateForUpdateXml '{0}'", orderNo);
            //    DataSet ds = new DataSet();
            //    ds = objgd.GetData(Sqlstr, Keydata);
            //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            //    {
            //        string DeliveryDate = "";
            //        string FilePath = "";
            //        string UserId = "0";
            //        DeliveryDate = (Convert.ToString(ds.Tables[0].Rows[0]["Deliverydate"]));
            //        FilePath = ds.Tables[0].Rows[0]["filePath"].ToString();
            //        UserId = ds.Tables[0].Rows[0]["UserId"].ToString();
            //        string Xml = "|BoardDeliveryDate$" + DeliveryDate; ;
            //        BPO.ConfiguratorXMLUpdate(FilePath, true, Xml, Convert.ToInt32(UserId));
            //    }
            //}
            //catch { }
        }
        catch
        {
        }
    }

    public void assemblyProductionApprove(string orderNumber, string componentPrice)
    {
        //try
        //{
        //    var objgd = new EC09WebApp.API.gd();
        //    string IP = EC09.Common.Common1.GetClientIPType();
        //    decimal componentPrices = 0;
        //    decimal.TryParse(componentPrice, out componentPrices);
        //    string strsqlStatus = string.Format("exec PreProductionApprovalForAssembly '{0}',{1} ,'{2}'", orderNumber, componentPrices, IP);
        //    DataSet ds = objgd.GetData(strsqlStatus, Keydata);
        //    EC09WebApp.EC09WebService.BPO bpo = new EC09WebApp.EC09WebService.BPO();
        //    if (componentPrices > 0 && ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        string filePath = ds.Tables[0].Rows[0]["filePatht"].ToString();
        //        string Xml = "|ComponentTotalNetPrice$" + componentPrices.ToString("0.00") + "|ComponentCurrentNetPrice$" + componentPrices.ToString("0.00");
        //        bpo.ConfiguratorXMLUpdate(filePath, true, Xml, 0);
        //    }
        //    InsertHistoryBOMCPL(orderNumber, "ACTION_PREPRODUCTION_RELEASED");
        //}
        //catch (Exception ex)
        //{

        //}
    }


    public bool IsImpersonation()
    {
        System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = null;
        impersonateOnFileServer = sparrowFeApp.core.Impersonation.ImpersonateFileServer(sparrowFeApp.core.Constant.FileServerUsername, sparrowFeApp.core.Constant.FileServerPassword, sparrowFeApp.core.Constant.FileServerDomain);
        if (impersonateOnFileServer != null)//todo
        {
            return true;
        }

        return false;//todo

    }

    private string SavePIXpectFile(string entityNo, string fileContent, string name, string status)
    {
        //var objgd = new EC09WebApp.API.gd();
        //string folderPath = "";
        //string path = "";
        //string assemblyNumber = entityNo;
        //if (assemblyNumber.Contains("-A"))
        //{
        //    entityNo = assemblyNumber.Split('-')[0].ToString();
        //}
        //try
        //{

        //    objgd.PutData(string.Format("exec UpdatePixPectStatus '{0}','{1}' ,'{2}'", entityNo, status, name), Keydata);
        //    string strsql = string.Format("exec spVisualizerfilepath '" + entityNo + "',''", Keydata);
        //    DataSet ds = objgd.GetData(strsql, Keydata);
        //    if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(ds.Tables[0].Rows[0]["FolderPath"]) != "")
        //    {
        //        EC09.Data.genFileServer objFileServer = EC09WebApp.EC09WebService.BPO.GetActiveFileServer();
        //        if (objFileServer.IsRequireImpersonation == true)
        //        {
        //            System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = EC09WebApp.EC09WebService.BPO.ImpersonateFileServer(objFileServer.Username, ConfigurationManager.AppSettings["FileServerDomain"].ToString(), objFileServer.Password);
        //            if (impersonateOnFileServer != null)
        //            {
        //                folderPath = Convert.ToString(ds.Tables[0].Rows[0]["FolderPath"]);
        //                folderPath = folderPath + "\\assembly\\" + entityNo.Trim();
        //                if (!Directory.Exists(folderPath))
        //                    Directory.CreateDirectory(folderPath);
        //                path = folderPath + "\\" + name + "_PIXpect.xml";

        //                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        //                doc.LoadXml(fileContent.Trim());
        //                doc.Save(path);
        //                Response.Write("OK");
        //            }
        //        }
        //    }
        //    else
        //    {
        //        genExceptionLog objException = new genExceptionLog();
        //        objException.LogExceptiondata("UploadeComponentsFile-" + entityNo + ",contentLength-" + ",contentType-", 0, entityNo);
        //        Response.Write("FAILED");
        //        return "";
        //    }

        //    try
        //    {
        //        string sql = "exec i8responces_insert '" + Convert.ToString(0).Trim() + "','" + Convert.ToString(Guid.NewGuid().ToString()) + "', '"
        //              + Convert.ToString("ComponentFile") + "','" + Convert.ToString(fileContent).Replace("'", "''") + "','" + Convert.ToString(entityNo) + "','','" + "0" + "','0' ,''";
        //        //objException.LogExceptiondata(s, 355, "sessionissuei82");                    
        //        objgd.PutData(sql, Keydata);
        //    }
        //    catch (Exception ex)
        //    {
        //        genExceptionLog objException = new genExceptionLog();
        //        objException.LogExceptiondata("UploadeComponentsFile-" + entityNo, 0, ex.ToString());
        //    }
        //}
        //catch (Exception ex)
        //{
        //    genExceptionLog objException = new genExceptionLog();
        //    objException.LogExceptiondata("UploadeComponentsFile-" + entityNo, 0, ex.ToString() + fileContent);
        //    Response.Write("FAILED");
        //    return "";
        //}
        return "";
    }

    private string UploadeComponentsFile(string entityNo, string fileContent)
    {
        var objgd = new Database.DA();
        string folderPath = "";
        string path = "";
        string assemblyNumber = entityNo;
        if (assemblyNumber.Contains("-A"))
        {
            entityNo = assemblyNumber.Split('-')[0].ToString();
        }
        try
        {
            if (sparrowFeApp.core.AppSessions.EccUserId == string.Empty || sparrowFeApp.core.AppSessions.EccUserId == "0")
            {
                // objgd.PutData(string.Format("exec UpdateAssemblyData '{0}','{1}'", entityNo, "SENDTOINCOMING"), Keydata);
            }
            sparrowFeApp.visdownload obj = new sparrowFeApp.visdownload();

            folderPath = obj.getVisPath(entityNo, "", "", "");
            if (folderPath != "")
            {
                if (IsImpersonation())
                {
                    folderPath = folderPath + "\\assembly\\" + entityNo.Trim();
                    if (!Directory.Exists(sparrowFeApp.core.Constant.FileServerPath + folderPath))
                        Directory.CreateDirectory(sparrowFeApp.core.Constant.FileServerPath + folderPath);
                    path = sparrowFeApp.core.Constant.FileServerPath + folderPath + "\\" + entityNo + "_components.xml";

                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(fileContent.Trim());
                    doc.Save(path);
                    storeBackupFile(sparrowFeApp.core.Constant.FileServerPath, entityNo, path, fileContent.Trim());
                    string jsonFeApp = "";
                    //string crmuserid = "0";
                    //if (sparrowFeApp.core.AppSessions.EccUserId != null)
                    //{
                    //    crmuserid = sparrowFeApp.core.AppSessions.EccUserId;
                    //}
                    long crm_rel_id = 0;
                    if (sparrowFeApp.core.AppSessions.FEUserId != null)
                    {
                        long userid = 0;
                        long.TryParse(sparrowFeApp.core.AppSessions.FEUserId, out userid);
                        crm_rel_id = sparrowFeApp.core.Common.GetCrmRelIdFromUserid(userid);
                    }
                    if (folderPath.Trim() != "" && folderPath.Contains(entityNo))
                    {
                        //folderPath = "\\" + folderPath + "\\";
                        //var feApp = new { fun_name = "file_sync", entity_number = entityNo, file_name = entityNo + "_components.xml", file_type = "assembly_editor", file_path = "\\"+folderPath+"\\", crm_rel_id = crmuserid, source = "FE", target = "EC" }; 
                        var feApp = new { fun_name = "file_sync", entity_number = entityNo, file_name = entityNo + "_components.xml", file_type = "assembly_editor", file_path = "\\"+folderPath+@"\", crm_rel_id = crm_rel_id.ToString(), source = "FE", target = "EC" };
                        jsonFeApp = Newtonsoft.Json.JsonConvert.SerializeObject(feApp);
                        sparrowFeApp.core.Common feAppData = new sparrowFeApp.core.Common();
                        feAppData.FeAppAPI(jsonFeApp);
                    }

                    UpdAssemblyFileStatus(entityNo);

                    if (Session["BOMImported"] != null && Session["BOMImported"].ToString() == "true")
                    {
                        Session["BOMImported"] = null;
                        RenameBomCPLFile(assemblyNumber, "CM");
                    }
                    if (Session["CPLImported"] != null && Session["CPLImported"].ToString() == "true")
                    {
                        Session["CPLImported"] = null;
                        RenameBomCPLFile(assemblyNumber, "CPL");
                    }
                    Response.Write("OK");
                }
            }
            else
            {
                Response.Write("FAILED");
                return "";
            }

            try
            {
                string sql = "exec i8responces_insert '" + Convert.ToString(0).Trim() + "','" + Convert.ToString(Guid.NewGuid().ToString()) + "', '"
                      + Convert.ToString("ComponentFile") + "','" + Convert.ToString(fileContent).Replace("'", "''") + "','" + Convert.ToString(entityNo) + "','','" + "0" + "','0' ,''";
                objgd.ExecuteQuery(sql);
            }
            catch
            {
            }
        }
        catch
        {
            Response.Write("FAILED");
            return "";
        }
        return "";
    }

    public void storeBackupFile(string serverPath, string entityNo, string oldPath, string fileContent)
    {
        try
        {
            if (serverPath != null && serverPath != "")
            {
                string folderPath = "";
                string sessionId = backupSessionId;

                string strSQL = string.Format("select  folder_path from sales_bomfile_history where entity_num = '{0}' order  by id desc", entityNo);
                //string sql = string.Format("exec get_orders_filepath '{0}'", entityNo); "FE APP"
                DataSet ds = new DataSet();
                var objgd = new Database.DA();
                ds = objgd.GetDataSet(strSQL);
                if(ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    folderPath = serverPath + ds.Tables[0].Rows[0]["folder_path"].ToString();
                }

                string path = sparrowFeApp.core.Common.getFilePath(entityNo);
                if (path != "")
                {
                    if (IsImpersonation())
                    {
                        if (folderPath == "")
                        {
                            folderPath += serverPath + "\\BOM_history" + "\\" + DateTime.UtcNow.Year.ToString() + "\\" + DateTime.UtcNow.Month.ToString() + "\\" + DateTime.UtcNow.Day.ToString() + "\\" + entityNo + "\\";
                        }
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        //string backupFilePath = folderPath + "\\" + sessionId + ".xml";
                        string backupFilePath = folderPath + sessionId + ".xml";
                        string fileName = sessionId + ".xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(fileContent);
                        doc.Save(backupFilePath);

                        var feApp = new { fun_name = "file_sync", entity_number = entityNo, file_name = sessionId + ".xml", file_type = "component_history", file_path = folderPath.Replace(serverPath,"")+@"\", document_num=sessionId, crm_rel_id = sparrowFeApp.core.AppSessions.EccUserId, source = "FE", target = "EC" };
                        string jsonFeApp = Newtonsoft.Json.JsonConvert.SerializeObject(feApp);
                        sparrowFeApp.core.Common feAppData = new sparrowFeApp.core.Common();
                        feAppData.FeAppAPI(jsonFeApp);

                        folderPath = folderPath.Replace(serverPath, "") + "\\";

                        insertHistory(entityNo, folderPath, fileName);

                    }
                }
            }
        }
        catch
        {
            Response.Write("FAILED");
        }
    }

    public void insertHistory(string entityNo, string folderPath, string fileName)
    {
        long createdBy = 0;
        if (sparrowFeApp.core.AppSessions.FEUserId == string.Empty || sparrowFeApp.core.AppSessions.FEUserId == "0")
        {
            createdBy = 0;
        }
        else
        {
            createdBy = Convert.ToInt32(sparrowFeApp.core.AppSessions.FEUserId);
        }
        var objgd = new Database.DA();
        //string sql = string.Format("exec insert_bom_file_history '{0}','{1}',{2},'{3}'", folderPath, entityNo, createdBy, fileName);
        string sql = string.Format("INSERT INTO public.sales_bomfile_history(entity_num, created_date, created_by, folder_path, file_name) VALUES ('{0}', {1}, {2}, '{3}', '{4}')", entityNo, " Now() ", createdBy, folderPath,fileName);
        objgd.ExecuteQuery(sql);
    }

    private void ModifyAssemblyOrder(string orderNumber, string responsecontent)
    {
        var objgd = new Database.DA();
        string FilePath = "";
        DataSet dsOrderPath = objgd.GetDataSet(string.Format("select file_path from sales_file where entity_number = '{0}' and file_type = 'ORD'", orderNumber));
        if (dsOrderPath != null && dsOrderPath.Tables.Count > 0 && dsOrderPath.Tables[0].Rows.Count > 0)
        {
            FilePath = dsOrderPath.Tables[0].Rows[0]["file_path"].ToString();
            if (FilePath != "")
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(responsecontent);

                if (EC09WebApp.shop.rb.ReportBuilderUtilities.IsImpersonation())
                {
                    string RootPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FileServerPath"]);
                    string XmlPath = FilePath + "\\" + orderNumber + ".xml";
                    string FullPath = RootPath + FilePath + "\\" + orderNumber + ".xml";
                    XmlElement root = xdoc.DocumentElement;
                    xdoc.Save(FullPath);

                    string jsonFeApp = "";
                    //string crmuserid = "0";
                    //if (sparrowFeApp.core.AppSessions.EccUserId != null)
                    //{
                    //    crmuserid = sparrowFeApp.core.AppSessions.EccUserId;
                    //}
                    long crm_rel_id = 0;
                    if (sparrowFeApp.core.AppSessions.FEUserId != null)
                    {
                        long userid = 0;
                        long.TryParse(sparrowFeApp.core.AppSessions.FEUserId, out userid);
                        crm_rel_id = sparrowFeApp.core.Common.GetCrmRelIdFromUserid(userid);
                    }
                    if (FilePath.Trim() != "" && FilePath.Contains(orderNumber))
                    {
                        var feApp = new { fun_name = "file_sync", entity_number = orderNumber, file_name = orderNumber + ".xml", file_type = "assembly_editor", file_path =  "\\"+FilePath+@"\", crm_rel_id = crm_rel_id.ToString(), source = "FE", target = "EC" };
                        jsonFeApp = Newtonsoft.Json.JsonConvert.SerializeObject(feApp);
                        sparrowFeApp.core.Common feAppData = new sparrowFeApp.core.Common();
                        feAppData.FeAppAPI(jsonFeApp);
                    }
                }
            }
        }
    }

    private void UpdateAssemblyFileStatus(string path, string entityNo)
    {
        try
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            var objgd = new Database.DA();
            List<object> parts = new List<object>();
            string sql = string.Empty;
            string assemblyDataStatus = "verified";
            XmlNodeList partNodes = xmlDoc.SelectNodes("AssemblyData/part");
            int totalParts = 0;
            int verified = 0;
            int tobeApprove = 0;
            int notIdentified = 0;
            int notVerified = 0;
            int CPLApproved = 0;
            int CPLnotApproved = 0;
            int Supplyflag = 0;
            if (partNodes.Count == 0)
            {
                assemblyDataStatus = "";
            }
            for (int i = 0; i < partNodes.Count; i++)
            {
                XmlNode partNode = partNodes[i];
                XmlNodeList matchNodes = partNode.SelectNodes("match");
                string pack = partNodes[i].Attributes["pack"] != null ? partNodes[i].Attributes["pack"].Value : "";
                string mpn = partNodes[i].Attributes["mpn"] != null ? partNodes[i].Attributes["mpn"].Value : "";
                string spn = partNodes[i].Attributes["spn"] != null ? partNodes[i].Attributes["spn"].Value : "";
                string desc = partNodes[i].Attributes["desc"] != null ? partNodes[i].Attributes["desc"].Value : "";
                string manual = partNodes[i].Attributes["manual"] != null ? partNodes[i].Attributes["manual"].Value : "";
                string supply = partNode.Attributes["supply"] != null ? partNode.Attributes["supply"].Value : "0";
                string ignore = partNode.Attributes["ign"] != null ? partNode.Attributes["ign"].Value : "0";
                if (supply != "1" && ignore == "0" && supply != "2")
                {
                    Supplyflag = 1;
                }

                if ((supply == "0" || supply == "1" || supply == "2") && ignore == "0")
                {
                    totalParts++;
                    if (matchNodes.Count == 0 || matchNodes.Count > 1 && assemblyDataStatus == "verified")
                    {
                        assemblyDataStatus = "unidentified";
                    }
                    XmlNode matchNode = matchNodes[0];
                    if (matchNodes.Count == 1)
                    {
                        string ver = matchNode.Attributes["ver"] != null ? matchNode.Attributes["ver"].Value : "0";
                        if (ver != "1" && assemblyDataStatus == "verified")
                        {
                            assemblyDataStatus = "tocheck";
                        }
                    }
                    string appr = partNode.Attributes["appr"] != null ? partNode.Attributes["appr"].Value : "0";
                    if (appr != "1" && assemblyDataStatus == "verified")
                    {
                        assemblyDataStatus = "tocheck";
                    }
                    for (int j = 0; j < matchNodes.Count; j++)
                    {
                        string sys_mpn = matchNodes[j].Attributes["mpn"] != null ? matchNodes[j].Attributes["mpn"].Value : "";
                        string external = matchNodes[j].Attributes["external"].Value;
                        if (manual == "1")
                        {
                            parts.Add(new { mpn = mpn, spn = spn, desc = desc, pack = pack, sys_mpn = sys_mpn, external = external, ec_order = entityNo });
                        }
                        if (external == "1")
                        {
                            string attrmpn = matchNodes[j].Attributes["mpn"].Value;
                            string attrdesc = matchNodes[j].Attributes["desc"].Value;
                            string datasheet = matchNodes[j].Attributes["url"] != null ? matchNodes[j].Attributes["url"].Value : "";
                            string manuf = matchNodes[j].Attributes["manuf"].Value;
                            sql += string.Format("exec bom_new_parts_insert '{0}','{1}','{2}','{3}','{4}' ", attrmpn, attrdesc, datasheet, manuf, entityNo) + ";";
                        }
                        if (matchNodes.Count == 1)
                        {
                            string verify = matchNode.Attributes["ver"] != null ? matchNode.Attributes["ver"].Value : "0";
                            if (verify == "0")
                            {
                                string attrmpn = matchNodes[j].Attributes["mpn"].Value;
                                string attrdesc = matchNodes[j].Attributes["desc"].Value;
                                string datasheet = matchNodes[j].Attributes["url"] != null ? matchNodes[j].Attributes["url"].Value : "";
                                string manuf = matchNodes[j].Attributes["manuf"].Value;
                                sql += string.Format("exec bom_new_parts_insert '{0}','{1}','{2}','{3}','{4}' ", attrmpn, attrdesc, datasheet, manuf, entityNo) + ";";
                            }
                        }
                    }

                    if (matchNodes.Count == 0 || matchNodes.Count > 1)
                    {
                        notIdentified++;
                    }
                    else if (appr != "1")
                    {
                        tobeApprove++;
                    }
                    else if (matchNodes.Count == 1)
                    {
                        string ver = matchNode.Attributes["ver"] != null ? matchNode.Attributes["ver"].Value : "0";
                        if (ver != "1")
                        {
                            notVerified++;
                        }
                        if (appr == "1" && ver == "1")
                        {
                            verified++;
                        }
                    }

                    XmlNodeList compNodes = partNode.SelectNodes("comp");
                    for (int k = 0; k < compNodes.Count; k++)
                    {
                        string CPLAppr = compNodes[k].Attributes["appr"].Value != null ? compNodes[k].Attributes["appr"].Value : "0";
                        if (CPLAppr == "0")
                        {
                            CPLnotApproved++;
                        }
                        if (CPLAppr == "1")
                        {
                            CPLApproved++;
                        }
                    }
                }
            }
            var objAPI = new Database.DA();
            objAPI.ExecuteQuery(sql);

            string entityType = "order";
            if (entityNo.ToUpper().StartsWith("B"))
            {
                entityType = "basket";
            }

            string updateIscustsupplyParts = "EXEC IscustsupplyParts_update '" + entityNo + "'," + Supplyflag + "";
            objgd.ExecuteQuery(updateIscustsupplyParts);

            var asemlbyPartDetails = new { total_Parts = totalParts, tobe_Approve = tobeApprove, not_Identified = notIdentified, CPLApproved = CPLApproved, CPLnotApproved = CPLnotApproved };
            string jsonAssemblyDetails = JsonConvert.SerializeObject(asemlbyPartDetails);
            string strsqlStatus = string.Format("exec Osp_UpdateAssemblyFileStatus '{0}' ,'{1}','{2}','{3}'", entityNo, entityType, assemblyDataStatus, jsonAssemblyDetails);
            objgd.ExecuteQuery(strsqlStatus);
        }
        catch
        {

        }
    }

    private void RenameBomCPLFile(string entitynr, string fileType)
    {
        try
        {
            string FolderPath = "";
            if (entitynr.Contains("-A"))
            {
                entitynr = entitynr.Split('-')[0].ToString();
            }
            string path = sparrowFeApp.core.Common.getFilePath(entitynr);
            if (path != "")
            {
                if (IsImpersonation())
                {
                    FolderPath = path;

                    if (Directory.Exists(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType))
                    {
                        string[] fileList = System.IO.Directory.GetFiles(FolderPath + "\\assembly\\" + entitynr + "\\BOM\\" + fileType);
                        bool checkUploadedFile = false;
                        foreach (string files in fileList)
                        {
                            if (files.Contains("SUBMITTED_"))
                            {
                                checkUploadedFile = true;
                            }
                        }
                        if (checkUploadedFile)
                        {
                            foreach (string files in fileList)
                            {
                                if (files.Contains("IMPORTED_"))
                                {
                                    File.Delete(files);
                                }
                            }
                            foreach (string files in fileList)
                            {
                                if (files.Contains("SUBMITTED_"))
                                {
                                    File.Copy(files, files.Replace("SUBMITTED_", "IMPORTED_"));
                                    File.Delete(files); ;
                                    try
                                    {
                                        long crm_rel_id = 0;
                                        if (sparrowFeApp.core.AppSessions.FEUserId != null)
                                        {
                                            long userid = 0;
                                            long.TryParse(sparrowFeApp.core.AppSessions.FEUserId, out userid);
                                            crm_rel_id = sparrowFeApp.core.Common.GetCrmRelIdFromUserid(userid);
                                        }
                                        string folderPath = FolderPath.Replace(sparrowFeApp.core.Constant.FileServerPath, "") + "\\assembly\\" + entitynr + "\\BOM\\" + fileType;
                                        if (folderPath.Trim() != "" && folderPath.Contains(entitynr))
                                        {
                                            string oldfilename = Path.GetFileName(files);
                                            var feApp = new { fun_name = "file_rename", entity_number = entitynr, file_name = oldfilename, rename_name = oldfilename.Replace("SUBMITTED_", "IMPORTED_"), file_path = "\\" + folderPath + "\\", crm_rel_id = crm_rel_id.ToString(), source = "FE", target = "EC" };
                                            string jsonFeApp = Newtonsoft.Json.JsonConvert.SerializeObject(feApp);
                                            sparrowFeApp.core.Common feAppData = new sparrowFeApp.core.Common();
                                            feAppData.FeAppAPI(jsonFeApp);
                                        }
                                    }
                                    catch { }
                                }
                            }
                        }
                    }
                }
            }


        }
        catch
        {
        }
    }


    private void UpdAssemblyFileStatus(string entityNo)
    {
        try
        {
            sparrowFeApp.shop.assembly.ComponentStatus compStatus = new sparrowFeApp.shop.assembly.ComponentStatus(entityNo);
            compStatus.LoadComponentState(false,0,true);
            long userid = 0;
            if (sparrowFeApp.core.AppSessions.FEUserId != null)
            {
                long.TryParse(sparrowFeApp.core.AppSessions.FEUserId, out userid);
            }
            List<sparrowFeApp.shop.assembly.ComponentStatus.CustomerPart> custParts = compStatus.GetCustomerSuppliedParts();
            List<sparrowFeApp.shop.assembly.ComponentStatus.Part> extParts = compStatus.GetExternalParts();
            List<sparrowFeApp.shop.assembly.ComponentStatus.Part> notVerParts = compStatus.GetNotVerifedParts();
            List<sparrowFeApp.shop.assembly.ComponentStatus.Part> unIdentifiedPart = compStatus.GetNonIdentifiedPart();
            int GenericParts = compStatus.GenericParts();
            string sql = "";
            string assemblyDataStatus = "verified";

            //int totalParts = 0;
            int tobeApprove = 0;
            int notIdentified = 0;
            //int CPLApproved = 0;
            // int CPLnotApproved = 0;
            bool isSql = false;
            sql += "INSERT INTO public.sales_bomnewpart(order_num, mpn, description, datasheet, manufacturer, is_deleted, created_date, processed_by_id) VALUES";
            for (int extPart = 0; extPart < extParts.Count; extPart++)
            {
                string mpnNumber = extParts[extPart].MPN.ToString();
                string Description = extParts[extPart].Description.ToString();
                string dataSheet = extParts[extPart].Datasheet.ToString();
                string Manufacturer = extParts[extPart].Manufacturer.ToString();
                if (isSql)
                {
                    sql += ",";
                }
                sql += string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}", entityNo, mpnNumber, Description, dataSheet, Manufacturer, false, DateTime.UtcNow, userid + ")");  //test by akshay
                isSql = true;
                //sql += string.Format("exec bom_new_parts_insert '{0}','{1}','{2}','{3}','{4}' ", mpnNumber, Description, dataSheet, Manufacturer, entityNo) + ";";
            }
            if (compStatus.PartsDefined > 0)
            {
                for (int notVerPart = 0; notVerPart < notVerParts.Count; notVerPart++)
                {
                    string mpnNumber = notVerParts[notVerPart].MPN.ToString();
                    string Description = notVerParts[notVerPart].Description.ToString();
                    string dataSheet = notVerParts[notVerPart].Datasheet.ToString();
                    string Manufacturer = notVerParts[notVerPart].Manufacturer.ToString();
                    if (isSql)
                    {
                        sql += ",";
                    }
                    sql += string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}", entityNo, mpnNumber, Description, dataSheet, Manufacturer, false, DateTime.UtcNow, userid + ")");
                    isSql = true;
                    //sql += string.Format("exec bom_new_parts_insert '{0}','{1}','{2}','{3}','{4}' ", mpnNumber, Description, dataSheet, Manufacturer, entityNo) + ";";
                }
            }
            if (compStatus.PartsNotDefined > 0)
            {
                for (int notVerPart = 0; notVerPart < unIdentifiedPart.Count; notVerPart++)
                {
                    string mpnNumber = unIdentifiedPart[notVerPart].MPN.ToString();
                    string Description = unIdentifiedPart[notVerPart].Description.ToString();
                    string dataSheet = unIdentifiedPart[notVerPart].Datasheet.ToString();
                    string Manufacturer = unIdentifiedPart[notVerPart].Manufacturer.ToString();
                    if (isSql)
                    {
                        sql += ",";
                    }
                    sql += string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}", entityNo, mpnNumber, Description, dataSheet, Manufacturer, false, DateTime.UtcNow, userid + ")");
                    isSql = true;
                    //sql += string.Format("exec bom_new_parts_insert '{0}','{1}','{2}','{3}','{4}' ", mpnNumber, Description, dataSheet, Manufacturer, entityNo) + ";";
                }
            }
            notIdentified = 0;
            notIdentified = compStatus.PartsNotDefined;
            assemblyDataStatus = "verified";
            if (notIdentified > 0 && assemblyDataStatus == "verified")
            {
                assemblyDataStatus = "unidentified";
            }
            if (compStatus.PartsDefined > 0)
            {
                if (assemblyDataStatus == "verified") // && notVerParts[0].Verified.ToString() != "1")
                {
                    assemblyDataStatus = "tocheck";
                }
            }
            tobeApprove = compStatus.PartsToBeAppr;
            if (tobeApprove > 0 && assemblyDataStatus == "verified")
            {
                assemblyDataStatus = "tocheck";
            }

            var objAPI = new Database.DA();
            if (isSql)
            {
                sql += "ON CONFLICT ON CONSTRAINT mpn_constraint DO NOTHING";
                objAPI.ExecuteQuery(sql);
            }

            //string entityType = "order";
            //if (entityNo.ToUpper().StartsWith("B"))
            //{
            //    entityType = "basket";
            //}
            //totalParts = compStatus.PartsNotDefined + compStatus.PartsDefined;
            //int Supplyflag = 0;
            //if (custParts.Count == compStatus.PartsDefined)
            //{
            //    Supplyflag = 1;
            //}
            //int generic = 0;
            //if (GenericParts == compStatus.PartsDefined && GenericParts != 0)
            //{
            //    generic = 1;
            //}
            //else if (custParts.Count + GenericParts == compStatus.PartsDefined & GenericParts != 0)
            //{
            //    generic = 1;
            //}
            //string updateIscustsupplyParts = "EXEC IscustsupplyParts_update '" + entityNo + "'," + Supplyflag + "," + generic;
            //objAPI.ExecuteQuery(updateIscustsupplyParts);
            //CPLApproved = compStatus.PlacementAppr;
            //CPLnotApproved = compStatus.PlacementToBeAppr;
            //var asemlbyPartDetails = new { total_Parts = totalParts, tobe_Approve = tobeApprove, not_Identified = notIdentified, CPLApproved = CPLApproved, CPLnotApproved = CPLnotApproved };
            //string jsonAssemblyDetails = JsonConvert.SerializeObject(asemlbyPartDetails);
            //string strsqlStatus = string.Format("exec Osp_UpdateAssemblyFileStatus '{0}' ,'{1}','{2}','{3}'", entityNo, entityType, assemblyDataStatus, jsonAssemblyDetails);
            //objAPI.ExecuteQuery(strsqlStatus);
        }
        catch (Exception)
        {
        }
    }


    private void InsertHistoryBOMCPL(string EntityNo, string actionCode = "")
    {
        try
        {

            long createdBy = 0;
            string IP = "";

            if (sparrowFeApp.core.AppSessions.FEUserId == string.Empty || sparrowFeApp.core.AppSessions.FEUserId == "0")
            {

                createdBy = 0;
                ///if (Session["UserId"] != null) { createdBy = (long)EC09WebApp.EC09Sessions.UserId; }
            }
            else
            {

                createdBy = Convert.ToInt32(sparrowFeApp.core.AppSessions.FEUserId);
            }
            try
            {


                IP = sparrowFeApp.core.Common.GetClientIPType();
            }

            catch
            {
            }
            if (actionCode == "")
            {
                actionCode = "ACTION_BOMCPLMODIFIED";
            }
            long object_id = sparrowFeApp.core.Common.GetEntityId(EntityNo);
            long operator_id = sparrowFeApp.core.Common.GetOperatorId(EntityNo);
            //sparrowFeApp.core.Common.InsertHistory(object_id, DateTime.UtcNow.ToString(), IP, "BOM/CPL updated", 33, actionCode, createdBy.ToString(), backupSessionId,operator_id.ToString());
            sparrowFeApp.core.Common.InsertHistory(object_id, DateTime.UtcNow.ToString(), IP, "BOM/CPL updated", 33, actionCode, createdBy.ToString(), backupSessionId,operator_id.ToString());
        }
        catch
        {
        }
    }

</script>
