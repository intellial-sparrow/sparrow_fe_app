﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sparrowFeApp.shop.visualizer
{
    public partial class servlets : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          string responce=  PriceCalculatorServlet(Request);

            Response.Write(responce);
            
        }
        public string PriceCalculatorServlet(HttpRequest Request)
        {
            string responseFromServer = "";
            if (Request.HttpMethod.ToString().Trim().ToUpper() == "POST")
            {

                var i8responce = new Database.DA();
                string responsecontent = "";
                using (StreamReader sr = new StreamReader(Request.InputStream))
                {
                    responsecontent = sr.ReadToEnd().Trim();
                }
                try
                {
                    string configurl = System.Configuration.ConfigurationManager.AppSettings["servlets"].ToString();
                    string strURL = configurl + "servlets/PriceCalculatorServlet" + string.Format("?") + Request.QueryString.ToString();
                    byte[] bytes = Encoding.ASCII.GetBytes(responsecontent); 
                    System.Net.WebRequest requess = System.Net.WebRequest.Create(strURL);
                    requess.Method = "POST";
                    requess.ContentLength = bytes.Length;
                    Stream os = requess.GetRequestStream();
                    os.Write(bytes, 0, bytes.Length);
                    HttpWebResponse resp = (HttpWebResponse)requess.GetResponse();
                    Stream dataStream = resp.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    responseFromServer = reader.ReadToEnd();
                }
                catch(Exception ex )
                {
                    string exc = ex.ToString();
                }

            }
            return responseFromServer;
        }

    }
}