﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System.Data" %>
<head>
    <title>Communicator</title>
    <script src="../../JS/jquery_1.7.2.min.js?v=3"></script>
    <script type="text/javascript" src="../../visulizer/webclient/webclient.nocache.js?v=<%=DateTime.Now.ToString("ddmmyyyyhhmm") %>"></script>
    <script src="../../JS/Common.js?v=3.5"></script>
    <script src="../assembly/resources/jquery-ui.js"></script>
</head>
<script language="c#" runat="server">

    public void Page_Load(object sender, EventArgs e)
    {
        sparrowFeApp.core.AppSessions.EccUserId = "0";
        sparrowFeApp.core.AppSessions.FEUserId = "0";
        if (Request["crm_id"] != null && Request["crmid"] != "")
        {
            sparrowFeApp.core.AppSessions.EccUserId = Request.QueryString["crmid"];
        }

        if (Request["usrid"] != null && Request["usrid"] != "")
        {
            sparrowFeApp.core.AppSessions.FEUserId = Request.QueryString["usrid"];
        }
        if (Request["type"] != null &&Request["type"]=="newbasket")
        {
            AddNewBasket();
        }
        else {
            LoadVisfromECC();
        }
    }
    void LoadVisfromECC()
    {
        string itemnumber = Convert.ToString(Request.QueryString["itemnumber"]);
        sparrowFeApp.core.ConfiguratorCl conf = new sparrowFeApp.core.ConfiguratorCl();
        var objAPI = new Database.DA();
        string type = Convert.ToString(Request.QueryString["type"]);
        sparrowFeApp.core.AppSessions.EccUserId = Convert.ToInt64(Request.QueryString["crmid"]).ToString();
        conf.PerentBasketId = itemnumber;
        conf.type = type;
        if (type == "PCBPOWERBASKETMODIFY")
        {
            conf.type = "modifyBasket";
            conf.ConfigAction = "PCBPOWERBASKETMODIFY";
        }
        conf.status = "BasketCalculate";
        conf.requestKey = sparrowFeApp.visdownload.EncryptLast(itemnumber);
        sparrowFeApp.ec09Service.ECAPISoapClient ecAPI = new sparrowFeApp.ec09Service.ECAPISoapClient();
        DataTable orderData = ecAPI.GetVisualizerData(itemnumber, conf.custid);
        conf.currency = sparrowFeApp.core.AppSessions.CurrencySymbol;
        conf.ConfiguratorType = "showVisualizer";
        conf.isCustomerView = "true";
        conf.ECCUserId = Convert.ToInt64(sparrowFeApp.core.AppSessions.EccUserId);
        string serviceName = "";
        string serviceid = orderData.Rows[0]["serviceId"].ToString();
        if (serviceid == "110")
        { serviceName = "PCBproto"; }
        else if (serviceid == "105")
        { serviceName = "STANDARDpool"; }
        else if (serviceid == "109")
        { serviceName = "RFpool"; }
        else if (serviceid == "118")
        { serviceName = "IMSpool"; }
        else if (serviceid == "120")
        { serviceName = "StencilService"; }
        else if (serviceid == "127")
        { serviceName = "BINDIpool"; }
        conf.service = serviceName;
        conf.IsFromECC = "true";
        if (Request["frompage"] != null)
        {
            conf.FromPage = Convert.ToString(Request["frompage"]);
        }
        if (Request["ECCCustUserId"] != null)
        {
            conf.ECCCustUserId = Convert.ToInt64(orderData.Rows[0]["userid"].ToString());
        }
        if (conf.custid == "16674")
        {
        }
        if (type.ToUpper() == "PCBPOWERBASKETMODIFY")
        {
            conf.exchangeRate = "1.0";
            conf.currency = "EUR";
            conf.Unit = "mm";
        }
        else
        {
            decimal CurrFactor = 0;
            string unit = "mm";
            string currencySymbol = orderData.Rows[0]["Symbol"].ToString();
            decimal.TryParse(Convert.ToString(orderData.Rows[0]["Factor"]), out CurrFactor);
            Convert.ToString(Math.Round(CurrFactor, 3));
            if (currencySymbol.ToUpper() == "USD")
            {
                unit = "mil";
            }
            conf.exchangeRate = Convert.ToString(Math.Round(CurrFactor, 8));
            conf.Unit = unit;
            conf.currency = currencySymbol;
        }
        conf.custid = orderData.Rows[0]["companyid"].ToString();
        string key = sparrowFeApp.visdownload.EncryptLast(itemnumber);
        string delCountry = orderData.Rows[0]["delCountry"].ToString();
        string invCountry = orderData.Rows[0]["invCountry"].ToString();
        if (type == "PCBPOWERBASKETMODIFY")
        {
            type = "modifyBasket";
        }
        string setAssemblyPartners = " setAssemblyPartners ('EGER,ANY');";
        string vatCountryCode = Convert.ToString(orderData.Rows[0]["vatCountryCode"]);
        string noVAT = Convert.ToString(orderData.Rows[0]["noVAT"]);
        string IsBusinessCustomer = Convert.ToString(orderData.Rows[0]["IsBusinessCustomer"]);
        string handlingCompany = Convert.ToString(orderData.Rows[0]["handlingCompany"]);
        string CV = Convert.ToString(orderData.Rows[0]["CV"]);
        string isBindipollAllow = Convert.ToString(orderData.Rows[0]["IsBindiPoolAllow"]).ToLower();
        string IsAllowAssembly = Convert.ToString(orderData.Rows[0]["IsAllowAssembly"]).ToLower();
        string hasRemarks = Convert.ToString(orderData.Rows[0]["hasremark"]);
        //string ordertype = "OFFER";

        //if (!itemnumber.Trim().ToUpper().StartsWith("B") && !itemnumber.Trim().ToUpper().Contains("INQ"))
        //{
        //    ordertype = "ORDER";
        //} 
        //System.Data.DataSet ds_msg = objAPI.GetDataSet(string.Format("exec osp_CheckGenMessage {0},'{1}'", itemnumber, ordertype));
        //if (ds_msg != null && ds_msg.Tables[0].Rows.Count > 0)
        //{
        //    int checkount = 0;
        //    int.TryParse(Convert.ToString(ds_msg.Tables[0].Rows[0]["CheckCount"]), out checkount);
        //    if (checkount != 0) { hasRemarks = "true"; }
        //}
        Session["configurator"] = null;
        Session["configurator"] = conf;
        string UseConfigurator = "false";
        if (UseConfigurator.ToUpper().Trim() == "TRUE")
        {
            string Str = "function initializeClient() {  setEnableAssemblyOrder(" + IsAllowAssembly + "); " + setAssemblyPartners + " showConfigurator('" + itemnumber + "', 'en','" + key + "','" + delCountry + "', '" + invCountry +
        "', '" + type + "',false,'" + conf.Unit + "','" + conf.currency + "'," + conf.exchangeRate + "," + IsBusinessCustomer +
        ",'" + handlingCompany + "','" + vatCountryCode + "'," + noVAT + ", " + isBindipollAllow + "," + hasRemarks + ",'" + CV + "');}";
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "SetScreen", Str, true);
        }
        else
        {
            string isPCBAEditable = "true";
            if (type.ToLower() == "view")
            {
                isPCBAEditable = "false";
            }

            string Str = "function initializeClient() {  setEnableAssemblyOrder(" + IsAllowAssembly + "); " + setAssemblyPartners + " showVisualizer('" + itemnumber + "', 'en','" + key + "','" + delCountry + "', '" + invCountry +
                "', '" + type + "',false,'" + conf.Unit + "','" + conf.currency + "'," + conf.exchangeRate + "," + IsBusinessCustomer +
                ",'" + handlingCompany + "','" + vatCountryCode + "'," + noVAT + ", " + isBindipollAllow + "," + hasRemarks + ",'" + CV + "',null,null," + isPCBAEditable + ");}";
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "SetScreen", Str, true);

        }
    }

    void AddNewBasket()
    {
        sparrowFeApp.core.ConfiguratorCl conf = new sparrowFeApp.core.ConfiguratorCl();
        conf.delCountry = "nl";
        conf.invCountry = "nl";
        conf.type = "Inquiry";
        conf.currency = "EUR";
        conf.isCustomerView = "false";
        conf.service = "STANDARDpool";
        sparrowFeApp.core.AppSessions.EccUserId = Convert.ToInt64(Request.QueryString["crmid"]).ToString();
        conf.status = "BasketCalculate";
        conf.IsFromECC = "true";
        conf.ConfiguratorType = "showCalculator";
        string sessionid = Guid.NewGuid().ToString();
        conf.sessionid = sessionid;
        conf.FromPage = "searchcustomer";
        Session["configurator"] = null;
        Session["configurator"] = conf;
        string str = "function initializeClient() {  setAssemblyPartners ('EGER,ANY'); setEnableAssemblyOrder(false); showCalculator('New', 'en','1165070','BE', 'BE', 'EUR','"+sessionid+"','Inquiry',false,'mm',1.00000000,true,'BE',false,'',false, false,'');}";
        ScriptManager.RegisterStartupScript(this.Page, GetType(), "SetScreen", str, true);
    }
</script>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
