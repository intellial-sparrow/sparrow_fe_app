﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="eccUploaddata.aspx.cs"
    Inherits="sparrowFeApp.shop.eccUploaddata" ValidateRequest="false" EnableViewState="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/shop/orders/WUCECCUploaddata.ascx" TagName="WUCuploaddata" TagPrefix="ucWUCuploaddata" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../CSS/FontStyle.css" rel="stylesheet" />
    <link rel="shortcut icon" type="image/x-icon" href="../shop/images/favicon.ico" />
    <link href="services.css?v=1.2" rel="stylesheet" type="text/css" />
    <script src="../JS/Common.js?v=3.5"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link href="../ReportBuilder/resources/summernote/jquery-ui.css" rel="stylesheet" />
    <script src="JS/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="assembly/resources/JS/jquery-ui.js" type="text/javascript"></script>
    <link href="JS/datepicker.css" rel="stylesheet" type="text/css" />
    <title id="Title1" runat="server">Upload data</title>
    <style>
        .mainheader
        {
            background: url("../shop/images/header.png") repeat-x scroll center top transparent;
        }
        .maindiv
        {
            border: 1px solid #2a9d00;
        }
        .container-datatop
        {
            padding: 0px;
            width: 100% !important;
        }
        .pagetitle
        {
            margin-bottom: 0px;
        }
        input[type="text"]
        {
            width: 177px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <ucWUCuploaddata:WUCuploaddata ID="WUCuploaddata"  
             EnableViewState="true" runat="server" />
    </div>
        <div runat="server" id="errormsg" visible="false" style="text-align:center; margin-top: 38%;font-size:20px;">
            <asp:Label ID="lblerror" runat="server" Text="Error Occurred" CssClass="error errmsgpostition"></asp:Label>
        </div>
    </form>
</body>
</html>
