﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Configuration;
using System.Xml;
using System.IO;
using System.Net;
using sparrowFeApp.core;
using Newtonsoft.Json;

namespace sparrowFeApp.shop.orders
{
    public partial class eccOrderModified : System.Web.UI.Page
    {

        ecgdService.gdSoapClient objAPI = new ecgdService.gdSoapClient();
        ec09Service.ECAPISoapClient ec09Service = new ec09Service.ECAPISoapClient();
        const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["RedirectAction"] != null && Convert.ToString(Request.QueryString["RedirectAction"]).ToUpper().Trim() == "APPROVED")
            {
                Response.Redirect(core.Constant.FeDomain + "/sales/close_view/");
                return;
            }

            lblMSG.Text = "";
            if (!IsPostBack)
            {
                //&ectype=offerpreview
                if (Request["successmsgtitle"] != null && Request["ectype"] == null)
                {
                    if (Request.QueryString["action"] != null && Convert.ToString(Request.QueryString["action"]).ToUpper().Trim() != "")
                    {
                        string Action = Convert.ToString(Request.QueryString["action"]).ToLower();
                        Response.Redirect(core.Constant.FeDomain + "/sales/close_view/?act=" + Action);
                    }
                    else
                    {
                        Response.Redirect(core.Constant.FeDomain + "/sales/close_view/");
                    } 
                    return;
                }
                else
                {
                    ECCOrderChanges();
                }
            }
        }

        public void ECCOrderChanges()
        {
            string ExceptiomMessage = "";

            try
            {
                HttpBrowserCapabilities browse = Request.Browser;
                ExceptiomMessage = "Start-sessionid=" + Convert.ToString(Request["sessionid"]) + "||";
                string strsql = string.Format("select XMLString from i8response where Sessionid like'%" + Convert.ToString(Request["sessionid"]) + "%'");
                DataSet DS = new DataSet();
                DS = objAPI.GetData(strsql, Keydata);
                if (DS.Tables[0].Rows.Count != 0)
                {
                    ExceptiomMessage += "LoadXML-||";
                    string XMLString = (String)DS.Tables[0].Rows[0][0];

                    string UserAgent = browse.Browser + "-" + browse.Version + "|" + browse.Platform;
                    string IPAdd = "Internal";
                    string Mode = "E";
                    string Type = "";

                    if (Convert.ToString(Request["type"]) == "modifyBasket")
                    {
                        try
                        {
                            divimg.Visible = true;
                            bool Is_File_Attached = false;
                            ConfiguratorCl conf = (ConfiguratorCl)Session["configurator"];
                            if (conf != null && conf.ConfigAction != null && conf.ConfigAction.ToUpper() == "PCBPOWERBASKETMODIFY")
                            {
                                divimg.Visible = false;
                                this.Title = "BasketModified";
                                lblMSG.Text = "Basket modified.";
                                string Sql_bas = "SELECT * from basketitems with(nolock) where Basket_Id=" + Convert.ToString(Request["orderNum"]) + " ";
                                DataSet dasketData = objAPI.GetData(Sql_bas, Keydata);
                                long basketid = Convert.ToInt32(Request["orderNum"]);
                                if (dasketData.Tables[0].Rows.Count != 0)
                                {
                                    long User_Id = 0;
                                    string Company_id = "";
                                    string BasketSessionid = "";
                                    string BasketNo = Convert.ToString(dasketData.Tables[0].Rows[0]["BasketNo"]);
                                    long.TryParse(Convert.ToString(dasketData.Tables[0].Rows[0]["User_Id"]), out User_Id);
                                    BasketSessionid = Convert.ToString(dasketData.Tables[0].Rows[0]["Session_Id"]);
                                    Company_id = Convert.ToString(dasketData.Tables[0].Rows[0]["Company_id"]);
                                    string path = sparrowFeApp.core.Common.getFilePath(BasketNo);
                                    if (path != "")
                                    {
                                        System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = null;
                                        impersonateOnFileServer = sparrowFeApp.core.Impersonation.ImpersonateFileServer(sparrowFeApp.core.Constant.FileServerUsername, sparrowFeApp.core.Constant.FileServerPassword, sparrowFeApp.core.Constant.FileServerDomain);
                                        if (impersonateOnFileServer != null)
                                        {
                                            string FilePath = path + "\\" + BasketNo + ".xml";
                                            XmlDocument doc = new XmlDocument();
                                            doc.LoadXml(XMLString);
                                            doc.Save(FilePath);
                                        }
                                    }

                                    Common.UpdateOrderDetailFromEC(BasketNo);

                                    path = path.Replace(Constant.FileServerPath, "");
                                    if (path != "")
                                    {
                                        var feApp = new { fun_name = "file_sync", entity_number = BasketNo, file_name = BasketNo + ".xml", file_type = "modify_power_basket", file_path ="\\"+ path + "\\", session_id = Convert.ToString(Request["sessionid"]), basket_session_id = BasketSessionid, parent_basket_id = basketid.ToString(), user_id = User_Id.ToString(), crm_rel_id = AppSessions.EccUserId, source = "FE", target = "EC" };
                                        string jsonFeApp = JsonConvert.SerializeObject(feApp);
                                        Common feAppData = new Common();
                                        feAppData.FeAppAPI(jsonFeApp);
                                    }

                                    //EC09DataContext objData = new EC09DataContext();
                                    //genFileServer objGenFileServer = objData.genFileServers.Where(p => p.IsActive == true).Single();
                                    //string filepath = objGenFileServer.RootPath + "tempbasket\\" + BasketSessionid + "\\" + BasketNo + ".xml";
                                    //objbpo.ConfiguratorXMLUpdate(filepath, false, XMLString, User_Id);
                                    //// xmlrec page status query string                                    
                                    //Orders.WUCUploaddata objupl = new Orders.WUCUploaddata();
                                    //string strrtun = objupl.ChangeCustDataAndSIdata(Convert.ToString(Request["sessionid"]), BasketNo, Convert.ToString(basketid));
                                }
                                ScriptManager.RegisterStartupScript(this.Page, GetType(), "", "window.parent.location.href = 'eccordermodified.aspx?successmsgtitle=BasketModified&entityno=" + Convert.ToString(Request["orderNum"]) + "&successmsgdesc=Basket modified.';", true);

                            }
                            else
                            {
                                Type = "modifyBasket";
                                string query = string.Format("exec osp_Update_basketitems {0},'{1}'", Convert.ToInt32(Request["orderNum"]), Convert.ToString(Request["sessionid"]));
                                DataSet dsbasket = objAPI.GetData(query, Keydata);
                                string BasketNo = string.Empty;
                                long basketid = Convert.ToInt32(Request["orderNum"]);
                                if (dsbasket.Tables[0].Rows.Count != 0)
                                {
                                    //insert history for modify file of basket..
                                    long Status_Id = 0;
                                    long User_Id = 0;
                                    string Company_id = "";
                                    string BasketSessionid = "";
                                    string Sql_bas = "SELECT * from basketitems with(nolock) where Basket_Id=" + Convert.ToString(Request["orderNum"]) + " ";
                                    DataSet dsOldBasket = objAPI.GetData(Sql_bas, Keydata);
                                    if (dsOldBasket.Tables[0].Rows.Count != 0)
                                    {
                                        Status_Id = Convert.ToInt32(dsOldBasket.Tables[0].Rows[0]["Status_Id"]);
                                        Is_File_Attached = Convert.ToBoolean(dsOldBasket.Tables[0].Rows[0]["Is_File_Attached"]);
                                        BasketNo = Convert.ToString(dsOldBasket.Tables[0].Rows[0]["BasketNo"]);
                                        long.TryParse(Convert.ToString(dsOldBasket.Tables[0].Rows[0]["User_Id"]), out User_Id);
                                        BasketSessionid = Convert.ToString(dsOldBasket.Tables[0].Rows[0]["Session_Id"]);
                                        Company_id = Convert.ToString(dsOldBasket.Tables[0].Rows[0]["Company_id"]);
                                        long ActionID = ec09Service.GetCodeId("ACTION_BASKETMODIFIED");
                                        long DocumentId = 0;
                                        bool isecAPIBasket = false;
                                        bool.TryParse(Convert.ToString(dsOldBasket.Tables[0].Rows[0]["isecAPI_basket"]), out isecAPIBasket);
                                        if (Is_File_Attached == true)
                                        {
                                            DocumentId = basketid;
                                        }
                                        //---------------------------added by Rushi----------------------------------------------
                                        //string baskethistory_modifiyFile = string.Format("exec oSP_GenerateHistory {0},'{1}',{2},{3},{4},'{5}',{6},'{7}'", basketid, "BASKET", Convert.ToInt32(Request["ECCUserId"]), Status_Id, ActionID, IPAdd, DocumentId, BasketNo);
                                        //objAPI.PutData(baskethistory_modifiyFile, Keydata);
                                        try
                                        {
                                            ec09Service.GenerateHistory(basketid, "BASKET", Convert.ToInt64(Request["ECCUserId"]), Status_Id, ActionID, IPAdd, DocumentId, BasketNo);
                                        }
                                        catch { }
                                        //EC09DataContext objData = new EC09DataContext();
                                        //genFileServer objGenFileServer = objData.genFileServers.Where(p => p.IsActive == true).Single();
                                        //string filepath = objGenFileServer.RootPath + "tempbasket\\" + BasketSessionid + "\\" + BasketNo + ".xml";
                                        //objbpo.ConfiguratorXMLUpdate(filepath, false, XMLString, User_Id);

                                        //// xmlrec page status query string                                    
                                        //Orders.WUCUploaddata objupl = new Orders.WUCUploaddata();
                                        //string strrtun = objupl.ChangeCustDataAndSIdata(Convert.ToString(Request["sessionid"]), BasketNo, Convert.ToString(basketid));
                                        //string[] arry = strrtun.Split('|');
                                        //bool IsSentToI8 = (arry[1] == "1" ? true : false);

                                        //// replace buildupimg in basket folder                                        
                                        //if (!IsSentToI8)
                                        //{
                                        //    EC09WebService.Offer objoffer = new EC09WebService.Offer();
                                        //    string returnmsg = objoffer.ReplaceBuildUpImg(Convert.ToString(basketid), filepath, "basket", Convert.ToString(BasketNo) + "_buildup.png");

                                        //}
                                        long ActionID_File = ec09Service.GetCodeId("ACTION_FILEMODIFY");
                                        try
                                        {
                                            ec09Service.GenerateHistory(basketid, "BASKET", Convert.ToInt64(Request["ECCUserId"]), Status_Id, ActionID, IPAdd, DocumentId, BasketNo);
                                        }
                                        catch { }
                                        divimg.Visible = false;
                                        this.Title = "BasketModified";
                                        lblMSG.Text = "Basket modified.";
                                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "", "window.parent.location.href = 'eccordermodified.aspx?successmsgtitle=BasketModified&entityno=" + Convert.ToString(Request["orderNum"]) + "&successmsgdesc=Basket modified.';", true);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                    else if (Convert.ToString(Request["type"]) == "modifyINQ") //not in use
                    {
                    }
                    else if (Convert.ToString(Request["type"]) == "repeatOrder") // not use when repeat order from ecc
                    {
                    }
                    else if (Convert.ToString(Request["type"]) == "modifyOrder")   // not in use
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                divimg.Visible = false;
                lblMSG.Text = "Error occurred.";
            }

        }

    }
}