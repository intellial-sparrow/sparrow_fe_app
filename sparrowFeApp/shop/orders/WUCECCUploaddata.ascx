﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCECCUploaddata.ascx.cs"
    Inherits="sparrowFeApp.shop.orders.WUCECCUploaddata" %>
<link href="../assembly/resources/FontStyle.css" rel="stylesheet" />
<style>
    .maindivUploaddata
    {
        text-align: left;
        border-radius: 4px 4px 4px 4px;
        width: 100%;
        font-size: 13px;
    }
    input[type="text"] {
    background: none repeat scroll 0 0 #FEFEFE;
    border: 1px solid #BBBBBB;
    border-radius: 3px 3px 3px 3px;
    -moz-border-radius: 3px;
    outline: medium none;
    font-size: 12px;
    padding: 5px;
}
    .rowdiv
    {
        clear: both;
        width: 540px;
        margin: 1px 0px;
        display: inline-block;
    }
    .rowdivChk
    {
        clear: both;
        /*width: 2500px;*/
        margin: 1px 0px;
        display: inline-block;
    }

    .rowlbl
    {
        float: left;
        font-size: 0.9em !important;
        font-weight: bold;
        margin-top: 5px;
        min-width: 150px;
        width: 180px;
        color: #464646;
    }


    select
    {
        width: 142px;
    }

    .headlbl
    {
        height: 50px;
        background-color: Green;
    }

    .divbttonw
    {
        float: left;
        margin: 0px;
    }

    .className
    {
        width: 270px;
        height: 150px;
        position: relative;
        text-align: center;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        border-radius: 6px;
    }

    .hide
    {
        display: none;
    }

    .show
    {
        display: block;
    }


    .textdataleft
    {
        font-size: 11px;
        padding: 0px;
    }

    #WUCuploaddata_ddlDELADDRESS option
    {
        display: block;
        width: 184px;
    }

    .errmsgpostition
    {
        font-size: 13px;
        margin-top: 10px;
        display: block;
    }

    .lbltitleveriant
    {
        display: block;
        margin-left: 10px;
        font-weight: bold;
        font-size: 0.9em;
        color: #464646;
    }

    #divinqvariants
    {
        padding-top: 10px;
    }

    .lblnr
    {
        padding-left: 8px;
    }

    .lblstencilfix
    {
        color: #2A9D00;
        font-size: 16px;
        height: 21px;
        margin-left: 9px;
    }

    .calender
    {
        background: url("../shop/images/calender_24.png") no-repeat scroll 157px 0 transparent !important;
    }

    #WUCuploaddata_divAddr
    {
        font-size: 11px;
    }
    #WUCuploaddata_divInvAddr
    {
        font-size: 11px;
    }

    input[type="text"]
    {
        width: 130px;
        font-size: 0.9em;
    }

    .ECConfiguratorOrderDetailListBox
    {
        border: 1px solid #BBBBBB;
        color: #333333;
        font-size: 0.9em;
        margin-bottom: 4px;
        padding: 2px 3px;
    }

    .clsremark
    {
        font-size: 0.9em;
        background: none repeat scroll 0 0 #FEFEFE;
        border: 1px solid #BBBBBB;
        border-radius: 3px 3px 3px 3px;
        font-size: 12px;
        outline: medium none;
    }

    .darkRecalc
    {
        display: inline;
        text-align: center;
    }

    .clsremark, .clsremark:focus
    {
        padding: 5px;
    }

    .clsuploadgerber
    {
        font-size: 0.9em !important;
        padding-left: 0;
        height: 24px;
    }

        .clsuploadgerber:focus
        {
            border: 1px solid #2a9d00;
            padding-left: 0;
        }

    .btnSearchButton {
         background: url("images/vis_continue.png") repeat-x scroll left top #2B2B2B;
        border: 0 none;
        border-radius: 1px 1px 1px 1px;
        clear: left;
        color: #FFFFFF;
        cursor: pointer;
        font-weight: bold;
        height: 30px;
        margin: 3px;
        padding: 4px 10px 6px 50px;
        text-decoration: none;
        font-size: 1em;
        padding-left: 50px !important;
    }

        .btnSearchButton:hover
        {
            background: url("images/vis_continue.png") repeat-x scroll left top #2B2B2B;
            border-radius: 1px 1px 1px 1px;
            color: #FFFFFF;
            text-decoration: none;
        }

    .priceGrid
    {
        width: 102px;
        float: left;
        padding: 2px 0;
        color: #2A9D00;
    }

    .priceGridre
    {
        width: 122px;
        float: left;
        margin-left: 16px;
        padding: 5px 0;
        color: #2A9D00;
        font-size: 1em;
        font-weight: bold;
    }

    .priceGridrehig
    {
        background-color: #2A9D00;
        border: 1px solid #2A9D00;
        color: #FFFFFF;
        float: left;
        font-size: 1em;
        font-weight: bold;
        margin-left: 16px;
        padding: 2px 0;
        width: 122px;
    }

    .priceGridrehigfinal
    {
        background-color: #4C4D51;
        border: 1px solid #4C4D51;
        color: #FFFFFF;
        float: left;
        font-size: 1em;
        font-weight: bold;
        margin-left: 16px;
        padding: 2px 0;
        width: 122px;
    }


    .darkTitleRecalc
    {
        color: #464646;
        font-size: 0.9em !important;
        font-weight: bold;
        width: 125px;
    }

    .pricinghead
    {
        background-color: green;
        border-color: green;
        width: 404px;
        height: 17px;
        background-color: #2A9D00;
        border: 1px solid #CCCCCC;
        border-radius: 4px 4px 4px 4px;
        color: #FFFFFF;
        font-size: 1.3em;
        font-weight: bold;
        margin-bottom: 1px;
        margin-right: 0;
        margin-top: 0;
        padding: 3px 0.5em;
    }

    .pricingborder
    {
        border: 1px solid #CCCCCC;
        border-radius: 4px 4px 4px 4px;
        width: 422px;
    }

    .eurspan
    {
        padding-left: 3px;
    }

    .calender
    {
        background: url("../../shop/images/calender_24.png") no-repeat scroll 114px 0 transparent !important;
    }
</style>
<script type="text/javascript" language="javascript">

    function setSubmitbtnDisable() {
        //$('#WUCuploaddata_btnsubmit').addClass('show').removeClass('hide');
        $("#<%= btnsubmit.ClientID %>").addClass('hide').removeClass('show');
        $("#<%= imgProcessing.ClientID%>").addClass('show').removeClass('hide');
        //document.getElementById("btnsubmit").disabled = true;
        if ($("#WUCuploaddata_hidType").val() == "modifyorder") {
            return validateInput();
        } else {
            return true;
        }
    }
    function setSubmitbtnEnable() {
        $("#<%= btnsubmit.ClientID %>").addClass('show').removeClass('hide');
        $("#<%= imgProcessing.ClientID%>").addClass('hide').removeClass('show');
        //$('#WUCuploaddata_btnsubmit').addClass('show').removeClass('hide');       
        //document.getElementById("btnsubmit").disabled = false;
    }

    function numericFilter(txb) {
        //        txb.value = txb.value.replace(/[^\0-9]/ig, "");

    }
    function imgloading() {
        $('#divProg').addClass('show').removeClass('hide');
    }
    function imghiding() {
        $('#divProg').addClass('hide').removeClass('show');
    }
    $(function () {
        $("#WUCuploaddata_datepicker").datepicker(
        {
            //            showOn: "both",
            //            buttonImage: "../shop/images/calender_24.png",
            //            buttonImageOnly: true,
            dateFormat: 'dd M yy'
        });
    });

    function DisableDatepicker() {
        $("#<%= datepicker.ClientID %>").datepicker().datepicker('disable');
    }
    function EnableDatepicker() {
        $("#<%= datepicker.ClientID %>").datepicker('enable');
    }


    function changedate() {
        var value = "<%= Deliverydate  %>";
        var selecteddate = $("#WUCuploaddata_datepicker").val();

        if ($.datepicker.parseDate('dd M yy', selecteddate) < $.datepicker.parseDate('dd M yy', value)) {
            $("#WUCuploaddata_datepicker").val(value);
            var errmsg = document.getElementById("WUCuploaddata_hidendelerrmsg").value;
            alert(errmsg);
            //alert("Selected shipment date should be greater than originally calculated shipment date.");
            return false;
        }
        else {
            document.getElementById("WUCuploaddata_hidendeliverydate").value = selecteddate;
            $("#WUCuploaddata_datepicker").val(selecteddate);
            return true;
        }

    }


    $(document).ready(function () {
        $("#WUCuploaddata_Text1").datepicker({
            dateFormat: 'dd M yy',
            minDate: 0,
            beforeShowDay: jQuery.datepicker.noWeekends
        });

        if ($("#WUCuploaddata_hidType").val() == "modifyorder") {
            //$("#divsubmitbutton").style = "margin-left: 69px;";
            $("#divsubmitbutton").css('margin-left', '69px');

        }

        //var disabledDays = ["9-27-2013", "2-24-2010", "2-27-2010", "2-28-2010", "3-3-2010", "3-17-2010", "4-2-2010", "4-3-2010", "4-4-2010", "4-5-2010"];
        function noWeekendsOrHolidays(date) {
            var noWeekend = jQuery.datepicker.noWeekends(date);
            return noWeekend[0] ? nationalDays(date) : noWeekend;
        }

        function nationalDays(date) {
            var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
            //console.log('Checking (raw): ' + m + '-' + d + '-' + y);
            for (i = 0; i < disabledDays.length; i++) {
                if ($.inArray((m + 1) + '-' + d + '-' + y, disabledDays) != -1 || new Date() > date) {
                    //console.log('bad:  ' + (m+1) + '-' + d + '-' + y + ' / ' + disabledDays[i]);
                    return [false];
                }
            }
            //console.log('good:  ' + (m+1) + '-' + d + '-' + y);
            return [true];
        }

        $(this).click(function (e) {

            e = e || window.event;
            var element = e.target || e.srcElement;
            if (element.className != "tooltip") {
                $('.tooltip').hide();
            }

            var posx = e.clientX;
            var posy = e.clientY;

            if (document.getElementById("Div" + element.parentNode.id) != null) {

                var divsize = parseInt(document.getElementById("Div" + element.parentNode.id).style.width.replace("px", ""));
                var total = divsize + posx;
                var finaltotal = ($(window).width() - (total + 30));
                if (finaltotal > 0) {
                    document.getElementById("Div" + element.parentNode.id).style.left = posx + "px";
                }
                else {
                    document.getElementById("Div" + element.parentNode.id).style.left = (posx - (divsize + 15)) + "px";
                }
                $("#Div" + element.parentNode.id).slideToggle();
            }
        });

        $("#WUCuploaddata_ddlDELADDRESS").change(function () {
            var selectedText = $("#WUCuploaddata_ddlDELADDRESS option:selected").text();
            var strArr = selectedText.split(',');
            var strTemp = strArr[0] + "," + strArr[1] + "<br>";

            for (var i = 2; i < strArr.length; i++) {
                strTemp = strTemp + strArr[i] + ",";
            }

            if (strTemp.substr(-1, 1) === ',') {
                strTemp = strTemp.substring(0, strTemp.length - 1)
            }

            $("#WUCuploaddata_divAddr").html(strTemp);
        });

        $("#WUCuploaddata_ddlInvoicesaddress").change(function () {
            var selectedText = $("#WUCuploaddata_ddlInvoicesaddress option:selected").text();
            var strArr = selectedText.split(',');
            var strTemp = strArr[0] + "," + strArr[1] + "<br>";

            for (var i = 2; i < strArr.length; i++) {
                strTemp = strTemp + strArr[i] + ",";
            }

            if (strTemp.substr(-1, 1) === ',') {
                strTemp = strTemp.substring(0, strTemp.length - 1)
            }

            $("#WUCuploaddata_divInvAddr").html(strTemp);
        });
    });


</script>
<%--<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>--%>
<%--<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>--%>
<asp:HiddenField ID="hidendeliverydate" runat="server" />
<asp:HiddenField ID="hidendelerrmsg" runat="server" />
<asp:HiddenField ID="hidenprojectrefreq" runat="server" Value="0" />
<asp:HiddenField ID="hidenarticlereq" runat="server" Value="0" />
<asp:HiddenField ID="hidenpurchasereq" runat="server" Value="0" />
<asp:HiddenField ID="hidType" runat="server" />
<asp:HiddenField ID="hdOriginalUnitPrice" runat="server" />
<asp:HiddenField ID="hdnOriginalASemblyPrice" runat="server" />
<div runat="server" id="controldiv" style="overflow: hidden;">
    <div id="divProg" class="className" style="display: none">
        <div style="margin: 0px auto; text-align: center; width: 100%; height: 100%;" id="divimg"
            runat="server">
            <img src="https://be.eurocircuits.com/shop/images/waiting_grn.gif" alt="" />
        </div>
    </div>
    <div runat="server" id="mainDivControl" class="maindivUploaddata" style="">
        <%-- <div style="height: 30px; color: #fff" class="headlbl" runat="server" id="divHeader">
            <div style="padding-left: 10px; font-size: 13px; display: inline-block; margin-top: 5px;
                font-weight: bold;">
                <asp:Label ID="lbluploaddatadilog" runat="server" Text=""></asp:Label>
            </div>
        </div>--%>
        <div style="width: 555px; margin-top: 10px;">
            <div style="width: 100%; display: inline-block;" class="lblnr">
                <div style="float: left;">
                    <span style="color: Red;" id="spanError"></span>
                    <asp:Label ID="lblpcbnamereq" runat="server" Text="" CssClass="error"></asp:Label>
                    <asp:Label ID="lblqtyrequier" runat="server" Text="" CssClass="error"></asp:Label>
                    <asp:Label ID="MsgZipFileSelect" runat="server" Text="" CssClass="error"></asp:Label>
                    <%-- <asp:Label ID="MsgSelectFileOrderFail" runat="server" Text="" CssClass="error"></asp:Label>
        <asp:Label ID="MsgFileSizeLongOrderFail" runat="server" Text=""  CssClass="error"></asp:Label>
        <asp:Label ID="MsgZipFileSelect" runat="server" Text=""  CssClass="error"></asp:Label>--%>
                </div>
            </div>
            <div class="rowdiv" id="divpcbname" runat="server">
                <div class="rowlbl" style="width: 220px;">
                    <span id="Span1" style="margin: 10px">
                        <asp:Label ID="lblPCBname" runat="server" Text=""></asp:Label>
                        <span class="error">*</span> </span>
                </div>
                <div style="float: left">
                    <asp:TextBox ID="txtPcbName" MaxLength="50" runat="server"></asp:TextBox>
               
                </div>
            </div>
                          <div class="rowdiv" id="DivPPAremarks" style="width: 1500px; margin-top:50px;margin-bottom:80px;   display: none;" runat="server">
                <div class="rowlbl" style="width: 220px;">
                    <span id="Span24" style="margin: 10px">
                        <asp:Label ID="Label4" runat="server" Text="Remark"></asp:Label>
                          <span class="error">*</span> </span>
                   
                </div>
                <div style="float: left">
                      <asp:TextBox ID="txtPPAremarks" runat="server" Width="200px" CssClass="NormalControl clsremark"
                                        Rows="5" TextMode="MultiLine"></asp:TextBox>
                    <div id="div9" runat="server">
                    </div>
                </div>
            </div>
            <div class="rowdiv" id="divUploadRemarkFile" visible="false" runat="server">
                <div style="float: left; width: 220px; min-width: 150px; margin-top: 0;" class="rowlbl">
                    <span id="lblAttachRemark" style="margin: 0px 0px 10px 10px;">
                        <asp:Label ID="Label2" runat="server" Text="">
                      <%--  <span runat="server" id="attrfileupload" visible="" class="error">*</span> --%>
                         Upload remark file
                        </asp:Label><span id="Span25" runat="server" class="error"></span>
                        <div style="display: inline; left: 3px; position: relative; top: 4px;">
                            <a id="RemarkFileDesc" class="infobox">
                                <img border="0" style="border: none; margin-bottom: -5px;" src="../shop/Orders/Style/info.gif" /></a>
                        </div>
                        <div id="DivRemarkFileDesc" style="width: 400px" class="tooltip">
                            <asp:Label ID="Label6" runat="server">Upload remark file only in PDF, Doc, DocX and TXT format.</asp:Label>
                        </div>
                    </span>
                </div>
                <div style="float: left">
                    <asp:FileUpload ID="UploadPPARemark" class="clsuploadgerber" runat="server" />
                </div>
            </div>
            <div class="rowdiv" id="divuploadfile" runat="server">
                <div style="float: left; width: 220px; min-width: 150px; margin-top: 0;" class="rowlbl">
                    <span id="lblAttach" style="margin: 0px 0px 10px 10px;">
                        <asp:Label ID="lblUploaddatafile11" runat="server" Text="Upload customer data">
                      <%--  <span runat="server" id="attrfileupload" visible="" class="error">*</span> --%>
                         
                        </asp:Label><span id="spnuploaddata" runat="server" class="error"> *</span>
                        <div style="display: inline; left: 3px; position: relative; top: 4px;">
                            <a id="FileDesc" class="infobox">
                                <img border="0" style="border: none; margin-bottom: -5px;" src="../shop/Orders/Style/info.gif" /></a>
                        </div>
                        <div id="DivFileDesc" style="width: 400px" class="tooltip">
                            <asp:Label ID="lblInfo_UploadFileDetails" runat="server"></asp:Label>
                        </div>
                    </span>
                </div>
                <div style="float: left">
                    <asp:FileUpload ID="UploadGerber" class="clsuploadgerber" runat="server" />
                </div>
            </div>
   


                
            <div class="rowdiv" id="divpurreferance" runat="server">
                <div class="rowlbl" style="width: 220px;">
                    <span style="margin: 10px">
                        <asp:Label ID="lblPurchasereference" runat="server" Text=""></asp:Label>
                        <span class="error" id="spanpurchaserefreq" runat="server" style="display: none;">*</span>
                    </span>
                </div>
                <div style="float: left">
                    <asp:TextBox ID="txtPurchasereference" MaxLength="50" runat="server"></asp:TextBox>
           
                </div>
            </div>
            <div class="rowdiv" id="divartireferance" runat="server">
                <div class="rowlbl" style="width: 220px;">
                    <span id="Span3" style="margin: 10px">
                        <asp:Label ID="lblArticlereference" runat="server" Text=""></asp:Label>
                        <span class="error" id="spanarticlerefreq" runat="server" style="display: none;">*</span>
                    </span>
                </div>
                <div style="float: left">
                    <asp:TextBox ID="txtArticlereference" MaxLength="50" runat="server"></asp:TextBox>
         
                </div>
            </div>
            <div class="rowdiv" id="divprojreffirst" runat="server">
                <div class="rowlbl" style="width: 220px;">
                    <span id="Span5" style="margin: 10px">
                        <asp:Label ID="lblProjReffirst" runat="server"></asp:Label>
                        <span class="error" id="spanprojrefreq" runat="server" style="display: none;">*</span>
                    </span>
                </div>
                <div style="float: left">
                    <asp:TextBox ID="txtProjReffirst" runat="server" EnableViewState="true"></asp:TextBox>
                </div>
            </div>
            <div class="rowdiv" id="Divdeliveryterms" runat="server" style="display: none;">
                <div class="rowlbl">
                    <span id="Span7" style="margin: 10px">
                        <asp:Label ID="lbldelterms" runat="server"></asp:Label></span>
                </div>
                <div style="float: left">
                    <span class="textdataleft">
                        <input type="text" id="datepicker" runat="server" onchange="return changedate();"
                            class="calender" />
                    </span>
                </div>
            </div>
            <div class="rowdiv" id="divecstencilfix" runat="server" style="margin-top: -3px;">
                <div>
                    <div class="rowlbl" style="width: 220px;">
                        <span id="Span6" style="margin: 10px">
                            <asp:Label ID="lblManualStencilPrice" runat="server"></asp:Label>
                            <a id="ManualStencilPrice" class="infobox" style="padding-top: 0">
                                <img border="0" style="border: none; margin-bottom: -5px;" src="../shop/Orders/Style/info.gif" /></a>
                            <div id="DivManualStencilPrice" style="width: 290px" class="tooltip">
                                <asp:Label ID="lblStencilFixImagePathOrderLean" runat="server"></asp:Label>
                            </div>
                            <asp:HyperLink ID="Hyperlinkyoutube" runat="server" Target="_blank"><img    style="border: none;margin-bottom: -3px;height: 18px;" src="../../style/ecvideo.png" /> </asp:HyperLink>
                        </span>
                    </div>
                    <div style="float: left; margin-left: -4px; margin-top: 5px;">
                        <asp:CheckBox ID="chkStencilFixed" runat="server" />
                        <asp:Label ID="lblStencilFixPrice" runat="server" CssClass="lblstencilfix"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="rowlbl">
                    </div>
                    <div style="float: left; margin-left: 220px; font-size: 11px;">
                        <span id="lblManualStencilPriceText" runat="server"></span>
                    </div>
                </div>
            </div>
            <div class="rowdiv" id="divdeladdress" style="width: 1500px;" runat="server" visible="false">
                <div class="rowlbl" style="width: 220px;">
                    <span id="Span2" style="margin: 10px">
                        <asp:Label ID="lblDelAddress" runat="server"></asp:Label>
                    </span>
                </div>
                <div style="float: left">
                    <asp:DropDownList ID="ddlDELADDRESS" class="ECConfiguratorOrderDetailListBox" runat="server">
                    </asp:DropDownList>
                    <div id="divAddr" runat="server">
                    </div>
                </div>
            </div>
            <%-- for new visualizer  --%>
            <div class="rowdiv" id="divInvoicesAddress" style="width: 1500px; display: none;" runat="server">
                <div class="rowlbl" style="width: 220px;">
                    <span id="Span10" style="margin: 10px">
                        <asp:Label ID="lblInvaddress" runat="server" Text="Invoice address"></asp:Label>
                    </span>
                </div>
                <div style="float: left">
                    <asp:DropDownList ID="ddlInvoicesaddress" class="ECConfiguratorOrderDetailListBox" runat="server">
                    </asp:DropDownList>
                    <div id="divInvAddr" runat="server">
                    </div>
                </div>
            </div>
            <div class="rowdiv" id="divHandlingComp" style="width: 1500px; display: none" runat="server">
                <div class="rowlbl" style="width: 220px;">
                    <span id="Span22" style="margin: 10px">
                        <asp:Label ID="lblHandlingcomp" runat="server" Text="Handling company"></asp:Label>
                    </span>
                </div>
                <div style="float: left;  margin-top: 3px;">
                    <asp:DropDownList ID="ddlHanlingcomp" class="ECConfiguratorOrderDetailListBox" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <%-- added by Maulesh --%>
            <div class="rowdiv" id="divdddatetext" style="width: 1500px; display: none" runat="server">
                <div class="rowlbl" style="width: 220px;">
                    <span id="Span4" style="margin: 10px">
                        <asp:Label ID="lblselectdeliverydate" runat="server" Text="Deliveryate"></asp:Label>
                    </span>
                </div>
                <div style="float: left">
                    <input type="text" id="Text1" style="cursor: pointer; font-size: 11px;"
                        runat="server" onchange="return changedate(); " class="calender" />

                </div>
            </div>
            <div class="rowdiv" id="divReclDelivry" style="width: 1500px; display: none" runat="server">
                <div class="rowlbl" style="width: 220px;">
                    <span id="Span8" style="margin: 10px">
                        <asp:Label ID="lblReclDelivryDate" runat="server" Text="Deliveryate"></asp:Label>
                    </span>
                </div>
                <div style="float: left; margin-left: 6px; margin-top: 3px;">
                    <asp:Label ID="lblrecalcdeliverydatereadonl" ForeColor="Black" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
            <%-- added by ankita --%>
            <div class="rowdivChk" id="divBoardLock" style="width: 1500px; display: none;" runat="server">
                <div class="rowlbl" >
                    <span id="Span9" style="margin: 10px">
                        <asp:Label ID="lblBoardLocked" runat="server" Text="Board lock"></asp:Label>
                    </span>
                </div>
                <div style="float: left; margin-left: 6px; margin-top: 3px;">
                    <asp:CheckBox ID="checkBrdlock" runat="server" />
                </div>
            </div>
            <div class="rowdivChk" id="DivFullcheck" style="width: 1500px; display: none" runat="server">
                <div class="rowlbl" >
                    <span id="Span16" style="margin: 10px">
                        <asp:Label ID="lblFullcheck" runat="server" Text="Full check"></asp:Label>
                    </span>
                </div>
                <div style="float: left; margin-left: 6px; margin-top: 3px;">
                    <asp:CheckBox ID="checkFullcheck" runat="server" />
                </div>
            </div>
            <div class="rowdivChk" id="divPlatedSlots" style="width: 1500px; display: none" runat="server">
                <div class="rowlbl" >
                    <span id="Span18" style="margin: 10px">
                        <asp:Label ID="lblplatedslot" runat="server" Text="Plated slots"></asp:Label>
                    </span>
                </div>
                <div style="float: left; margin-left: 6px; margin-top: 3px;">
                    <asp:CheckBox ID="checkplatedslots" runat="server" />
                </div>
            </div>
            <div class="rowdivChk" id="divPreprductioncheck" style="width: 1500px; display: none" runat="server">
                <div class="rowlbl" >
                    <span id="Span20" style="margin: 10px">
                        <asp:Label ID="lblPreProductcheck" runat="server" Text="Pre-production check"></asp:Label>
                    </span>
                </div>
                <div style="float: left; margin-left: 6px; margin-top: 3px;">
                    <asp:CheckBox ID="checkPreproduct" runat="server" />
                </div>
            </div>
                <div class="rowdivChk" id="divPackWithPaper" style="width: 1500px; display: none" runat="server">
                <div class="rowlbl" >
                    <span id="Span16" style="margin: 10px">
                        <asp:Label ID="lblPackWithPaper" runat="server" Text="Pack with paper"></asp:Label>
                    </span>
                </div>
                <div style="float: left; margin-left: 6px; margin-top: 3px;">
                    <asp:CheckBox ID="ChkPackWithPaper" runat="server" />
                </div>
            </div>

            <script>

                $(document).ready(function () {
                    //if ($('#WUCuploaddata_rbtnMarkup').is(':checked')) {

                    //    $('#WUCuploaddata_markupContainer').css('display', 'block');
                    //    $('#WUCuploaddata_directEditContainer').css('display', 'none');
                    //}
                    //else {
                    //    $('#WUCuploaddata_directEditContainer').css('display', 'block');
                    //    $('#WUCuploaddata_markupContainer').css('display', 'none');
                    //}

                    //$("input[name='WUCuploaddata$pricing']").change(function () {
                    //    if ($("input[name='WUCuploaddata$pricing']:checked").attr("id") == 'WUCuploaddata_rbtnMarkup') {
                    //        $('#WUCuploaddata_markupContainer').css('display', 'block');
                    //        $('#WUCuploaddata_directEditContainer').css('display', 'none');
                    //    }
                    //    else {
                    //        $('#WUCuploaddata_directEditContainer').css('display', 'block');
                    //        $('#WUCuploaddata_markupContainer').css('display', 'none');
                    //    }
                    //});
                
                    //priceCalculation("netprice", "unt");
                    //priceCalculation("vat", "unt");
                    //priceCalculation("gross", "unt");

                    $('#WUCuploaddata_txtPercentage').keyup(function () {
                        priceCalculation("netprice", "per");
                        priceCalculation("vat", "per");
                        priceCalculation("gross", "per");
                    });
                    $('#WUCuploaddata_txtPercentage').focusout(function () {
                        priceCalculation("netprice", "per");
                        priceCalculation("vat", "per");
                        priceCalculation("gross", "per");
                    });
                    $('#WUCuploaddata_txtUnitPrice1').keyup(function () {
                        priceCalculation("netprice", "unt");
                        priceCalculation("vat", "unt");
                        priceCalculation("gross", "unt");
                    });
                    $('#WUCuploaddata_txtEcoTransport1').keyup(function () {
                        priceCalculation("vat");
                        priceCalculation("gross");
                    });
                    $('#WUCuploaddata_txtAssembly').keyup(function () {
                        priceCalculationAssembly("netprice", "unt");
                        priceCalculation("vat", "unt");
                        priceCalculation("gross", "unt");
                    });
                    $('#WUCuploaddata_txtAssemblyPricemarkup').keyup(function () {
                        priceCalculationAssembly("netprice", "per");
                        priceCalculation("vat", "unt");
                        priceCalculation("gross", "unt");
                    });
                    $('#WUCuploaddata_txtComponents').keyup(function () {
                        priceCalculation("netprice", "unt");
                        priceCalculation("vat", "unt");
                        priceCalculation("gross", "unt");
                    });
                });

                //$('#WUCuploaddata_txtPercentage').text('0.000');
                //alert($('#WUCuploaddata_brdNetPrice1').text());

                function priceCalculation(operation, txtbx) {
                    var btdNetPrice = 0;
                    var trasnPortCost = 0;
                    var assemblyprice = 0;
                    var componentPrice = 0;

                    var per = parseFloat($('#WUCuploaddata_txtPercentage').val()) || 0;
                    var beforeUnitprice = parseFloat($('#WUCuploaddata_hdOriginalUnitPrice').val()) || 0;
                    var perUnitPrice = (beforeUnitprice * per) / 100;
                    switch (operation) {
                        case "netprice":
                            var qty = parseInt($('#WUCuploaddata_pcbQty1').text());
                            //console.log("perUnitPrice_" + perUnitPrice.toFixed(3));
                            var unitPrice = (parseFloat($('#WUCuploaddata_hdOriginalUnitPrice').val()) || 0) + perUnitPrice;
                            //console.log("unitPrice_" + unitPrice.toFixed(3));
                            var netPrice = (unitPrice * qty);
                            //console.log("netPrice_" + netPrice.toFixed(3));
                            if (txtbx == "per")
                            {
                                $('#WUCuploaddata_txtUnitPrice1').val(unitPrice.toFixed(3));
                                $('#WUCuploaddata_brdNetPrice1').text(netPrice.toFixed(3));
                                $('#WUCuploaddata_hidBrdNetPrice').val(netPrice.toFixed(3));
                            }
                            else
                            {
                                //((15/20)*100) - 100
                                var brdUnitPrice = parseFloat($('#WUCuploaddata_txtUnitPrice1').val()) || 0;
                                var percentage = ((brdUnitPrice / beforeUnitprice) * 100) - 100;
                                $('#WUCuploaddata_txtPercentage').val(percentage.toFixed(3));
                                //console.log($('#WUCuploaddata_txtPercentage').val());
                                unitPrice = parseFloat($('#WUCuploaddata_txtUnitPrice1').val()) || 0;
                                netPrice = (unitPrice * qty);
                                $('#WUCuploaddata_brdNetPrice1').text(netPrice.toFixed(3));
                                $('#WUCuploaddata_hidBrdNetPrice').val(netPrice.toFixed(3));
                            }
                            //console.log("WUCuploaddata_brdNetPrice1_" + netPrice.toFixed(3));
                            //console.log("WUCuploaddata_hidBrdNetPrice_" + $('#WUCuploaddata_hidBrdNetPrice').val());
                            //if (txtbx == "per") {
                            //    $('#WUCuploaddata_brdNetPrice1').text(netPrice.toFixed(3));
                            //    $('#WUCuploaddata_hidBrdNetPrice').val(netPrice.toFixed(3));
                            //} else {
                            //    unitPrice = parseFloat($('#WUCuploaddata_txtUnitPrice1').val()) || 0;
                            //    netPrice = (unitPrice * qty);
                            //    $('#WUCuploaddata_brdNetPrice1').text(netPrice.toFixed(3));
                            //    $('#WUCuploaddata_hidBrdNetPrice').val(netPrice.toFixed(3));
                            //}
                            //console.log("WUCuploaddata_brdNetPrice1_" +$('#WUCuploaddata_brdNetPrice1').val());
                            //console.log("WUCuploaddata_hidBrdNetPrice_" + $('#WUCuploaddata_hidBrdNetPrice').val());
                            break;
                        case "vat":
                            btdNetPrice = parseFloat($('#WUCuploaddata_brdNetPrice1').text()) || 0;
                            trasnPortCost = parseFloat($('#WUCuploaddata_txtEcoTransport1').val()) || 0;
                            var vat = parseFloat($('#WUCuploaddata_vatPercentage').text()) || 0;
                      
                            assemblyprice = parseFloat($('#WUCuploaddata_txtAssembly').val())||0;
                            componentPrice = parseFloat($('#WUCuploaddata_txtComponents').val())||0;
                           
                            var vatData = ((btdNetPrice + trasnPortCost + assemblyprice + componentPrice) * (vat / 100));
                            $('#WUCuploaddata_vatInfo1').text(vatData.toFixed(3));
                            break;
                        case "gross":
                            btdNetPrice = parseFloat($('#WUCuploaddata_brdNetPrice1').text()) || 0;
                            trasnPortCost = parseFloat($('#WUCuploaddata_txtEcoTransport1').val()) || 0;
                            var vatData = parseFloat($('#WUCuploaddata_vatInfo1').text()) || 0;

                             assemblyprice = parseFloat($('#WUCuploaddata_txtAssembly').val()) || 0;
                             componentPrice = parseFloat($('#WUCuploaddata_txtComponents').val()) || 0;
                           
                            var gross = (btdNetPrice + trasnPortCost + vatData + assemblyprice + componentPrice);
                            $('#WUCuploaddata_grossInfo1').text(gross.toFixed(3));
                            $('#WUCuploaddata_hdgrossInfo1').val(gross.toFixed(3));
                            break;
                    }
                }


                function priceCalculationAssembly(operation, txtbx) {
                    debugger;
                    var per = parseFloat($('#WUCuploaddata_txtAssemblyPricemarkup').val()) || 0;
                    var beforeUnitprice = parseFloat($('#WUCuploaddata_hdnOriginalASemblyPrice').val()) || 0;
                    var perUnitPrice = (beforeUnitprice * per) / 100;
                    switch (operation) {
                        case "netprice":
                            var qty = 1;
                            var unitPrice = (parseFloat($('#WUCuploaddata_hdnOriginalASemblyPrice').val()) || 0) + perUnitPrice;
                            var netPrice = (unitPrice * qty);
                            if (txtbx == "per") {
                                $('#WUCuploaddata_txtAssembly').val(unitPrice.toFixed(3));
                            }
                            else {
                                var brdUnitPrice = parseFloat($('#WUCuploaddata_txtAssembly').val()) || 0;
                                var percentage = ((brdUnitPrice / beforeUnitprice) * 100) - 100;
                                $('#WUCuploaddata_txtAssemblyPricemarkup').val(percentage.toFixed(3));
                            }
                            break;
                    }
                }

                function validateInput() {

                    $('#errMarkUp').hide();
                    // BY VIVEK *** according to mail (Problem with E591568-picture)
                    //var markupVal = parseFloat($('#WUCuploaddata_txtPercentage').val());
                    //if (markupVal < -100) {
                    //    setSubmitbtnEnable();
                    //    $('#spanError').text('Board markup must be greater than -100 otherwise it would result in neg');
                    //    return false;
                    //}
                    var unitPrice1 = parseFloat($('#WUCuploaddata_txtUnitPrice1').val());

                    if (unitPrice1 < 0) {
                        setSubmitbtnEnable();
                        $('#spanError').text('Single PCB price must be greater than 0.');
                        return false;
                    }
                    //if (markupVal > 100) {
                    //    setSubmitbtnEnable();
                    //    $('#spanError').text('Board markup must be less than 100 otherwise it would result in neg');
                    //    return false;
                    //}
                    var unitPrice = parseFloat($('#WUCuploaddata_txtUnitPrice1').val()) || 0;
                    $('#errUnitPrice').hide();
                    if (unitPrice <= 0) {
                        setSubmitbtnEnable()
                        $('#spanError').text('Unit price is not valid please insert valid price');
                        return false;
                    }
                    return true;
                }
            </script>

            <div runat="server" id="divPricing" style="display: none;">
                <%--<div class="rowdiv" id="divRbPricing" style="width: 1500px;" runat="server">

                    <div style="width: auto">
                        <span id="Span9" style="margin: 10px">
                             <asp:RadioButton ID="rbtnDirectEdit" Text="Direct edit" GroupName="pricing" runat="server" Checked="true" />
                            
                        </span>

                        <span id="Span10" style="margin: 10px">
                           <asp:RadioButton ID="rbtnMarkup" Text="Markup" GroupName="pricing" runat="server"
                                 />
                        </span>
                    </div>

                </div>--%>



                <div class="pricinghead">

                    <span style="font-size: 13px !important; margin-left: 165px;">Calculated Price</span>
                    <span style="margin-left: 7px; font-size: 13px !important;">Original Price</span>
                </div>
                <div class="pricingborder">
                    <%--<div class="rowdiv" id="div3" style="width: 1500px;padding-bottom: 5px;" runat="server">

                        <div class="rowlbl" style="width: 180px;">
                            <span id="Span8" style="margin: 10px">
                                
                            </span>
                        </div>
                        <div style="float: left;">
                               <div style="   font-weight:bold;   color: #2F4F4F;    display: block;    float: left;      width: 102px;">  
                                   <asp:Label ID="Label4" CssClass="" runat="server" Text="Current Price">  </asp:Label> 

                               </div> 
                            <div style="   font-weight:bold;   color: #2F4F4F;    display: block;    float: left; margin-left:16px;     width: 122px;"> 
                                <asp:Label ID="Label6" CssClass="" runat="server" Text="Label">   Calculated Price

                                </asp:Label>

                            </div>
                            <asp:HiddenField runat="server" ID="HiddenField1" />
                        </div>
                    </div>--%>
                    <div id="markupContainer" style="display: block;" runat="server">

                        <div class="rowdiv" id="div1" style="width: 100%;" runat="server">

                            <div class="rowlbl" style="width: 174px;">
                                <span id="Span11" style="margin: 10px">
                                    <asp:Label ID="lblPricemarkup" runat="server" Text="Label">PCB Price markup</asp:Label>
                                </span>
                            </div>
                            <div style="float: left;">
                                <div class=" priceGrid">
                                    <span id="Span12" style="">
                                        <asp:TextBox ID="txtPercentage" runat="server" Style="width: 77px;"></asp:TextBox>
                                        <span class="error" id="errMarkUp" style="display: none;">Board markup must be less
                                            than -100 otherwise it would result in negative pricing.</span>
                                    </span>
                                </div>
                                <div class="priceGridre">
                                    <asp:Label ID="lblPercetange" runat="server" Text="Label">   0.0000</asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="directEditContainer" style="" runat="server">
                        <div class="rowdiv" id="div2" style="width: 100%; padding-bottom: 5px;" runat="server">

                            <div class="rowlbl" style="width: 174px;">
                                <span id="Span13" style="margin: 10px;" class="dbpricetxt">
                                    <asp:Label ID="Label1" runat="server" Text="Label">   Quantity</asp:Label>
                                </span>
                            </div>
                            <div style="float: left">

                                <div class="priceGrid">
                                    <asp:Label ID="recalpcbQty1" runat="server" Text="">   </asp:Label>

                                </div>
                                <div class="priceGridre">

                                    <asp:Label ID="pcbQty1" runat="server" Text="">   </asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="rowdiv" id="div4" style="width: 100%;" runat="server">
                            <div class="rowlbl" style="width: 174px; margin-top: 11px;">
                                <span id="Span15" style="margin: 10px">
                                    <asp:Label ID="Label3" runat="server" Text="Label">   Single PCB</asp:Label>
                                </span>
                            </div>
                            <div style="float: left">

                                <div class="priceGrid">
                                    <asp:TextBox ID="txtUnitPrice1" MaxLength="50" runat="server" Style="width: 77px;"></asp:TextBox>
                                </div>
                                <div class="priceGridre">
                                    <span class="eurspan">€</span>
                                    <asp:Label ID="recalUnitPrice" runat="server" CssClass="darkRecalc" Text="Label">   [Recalc Price]</asp:Label>
                                </div>

                                <span class="error" id="errUnitPrice" style="display: none;">Enter valid unit price.</span>
                            </div>
                        </div>


                        <div class="rowdiv" id="div5" style="width: 100%; padding-bottom: 5px;" runat="server">

                            <div class="rowlbl" style="width: 174px;">
                                <span id="Span17" style="margin: 10px">
                                    <asp:Label ID="Label5" runat="server" Text="Label">   Total boards</asp:Label>
                                </span>
                            </div>
                            <div style="float: left">
                                <div class="priceGrid" style="background-color: #2A9D00; color: #fff; border: 1px solid #2A9D00; width: 87px; font-weight: bold;">

                                    <span class="eurspan">€</span>
                                    <asp:Label ID="brdNetPrice1" runat="server" Text="Label">  </asp:Label>
                                </div>

                                <div class="priceGridrehig" style="margin-left: 29px;">
                                    <span class="eurspan">€</span>
                                    <asp:Label ID="rebrdNetPrice1" CssClass="darkRecalc" runat="server" Text="Label">   [Recalc Price]</asp:Label>

                                </div>

                                <asp:HiddenField runat="server" ID="hidBrdNetPrice" />
                            </div>
                        </div>

                     <div id="DidAssemblyPricemarkup" style="display: block;" runat="server">

                        <div class="rowdiv" id="div10" style="width: 100%;" runat="server">

                            <div class="rowlbl" style="width: 174px;">
                                <span id="Span11" style="margin: 10px">
                                    <asp:Label ID="Label12" runat="server" Text="Label"> Assembly Price markup</asp:Label>
                                </span>
                            </div>
                            <div style="float: left;">
                                <div class=" priceGrid">
                                    <span id="Span12" style="">
                                        <asp:TextBox ID="txtAssemblyPricemarkup" runat="server" Style="width: 77px;"></asp:TextBox>
                                        <span class="error" id="errAssemblyMarkUp" style="display: none;">Assembly markup must be less
                                            than -100 otherwise it would result in negative pricing.</span>
                                    </span>
                                </div>
                                <div class="priceGridre">
                                    <asp:Label ID="lblAssemblyPricemarkupold" runat="server" Text="Label">   0.0000</asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>

                        
                        <div class="rowdiv" id="divAssembly" style="width: 100%;display:block" runat="server">
                            <div class="rowlbl" style="width: 174px; margin-top: 11px;">
                                <span id="SpanAssembly" style="margin: 10px">
                                    <asp:Label ID="Label8" runat="server" Text="Label">   Assembly</asp:Label>

                                </span>
                            </div>
                            <div style="float: left">

                                <div class="priceGrid">
                                    <asp:TextBox ID="txtAssembly" MaxLength="50"  Enabled="false"  runat="server" Style="width: 77px;"></asp:TextBox>

                                </div>
                                <div class="priceGridre">
                                    <span class="eurspan">€</span>
                                    <asp:Label ID="lblAssembly" runat="server" CssClass="darkRecalc" Text="Label">   [Recalc Price]</asp:Label>
                                </div>
                            </div>
                        </div>
                         <div class="rowdiv" id="divComponents" style="width: 100%;display:block" runat="server">
                            <div class="rowlbl" style="width: 174px; margin-top: 11px;">
                                <span id="SpanComponent" style="margin: 10px">
                                    <asp:Label ID="Label10" runat="server" Text="Label">   Component</asp:Label>
                                </span>
                            </div>
                            <div style="float: left">

                                <div class="priceGrid">
                                    <asp:TextBox ID="txtComponents" disabled="true"  MaxLength="50" runat="server" Style="width: 77px;"></asp:TextBox>
                                </div>
                                <div class="priceGridre">
                                    <span class="eurspan">€</span>
                                    <asp:Label ID="lblComponent" runat="server" CssClass="darkRecalc" Text="Label">   [Recalc Price]</asp:Label>
                                </div>

                            </div>
                        </div>

                        <div class="rowdiv" id="div6" style="width: 100%;" runat="server">

                            <div class="rowlbl" style="width: 174px; margin-top: 11px;">
                                <span id="Span19" style="margin: 10px">
                                    <asp:Label ID="Label7" runat="server" Text="Label">   Transport cost</asp:Label>
                                </span>
                            </div>
                            <div style="float: left">
                                <div class="priceGrid">
                                    <asp:TextBox ID="txtEcoTransport1" MaxLength="50" runat="server" Style="width: 77px;"></asp:TextBox>

                                </div>
                                <div class="priceGridre">
                                    <span class="eurspan">€</span>
                                    <asp:Label ID="retxtEcoTransport1" CssClass="darkRecalc" runat="server" Text="Label">   [Recalc Price]</asp:Label>

                                </div>

                                <span class="error" id="Span14" style="display: none;">Enter valid unit price.</span>

                            </div>
                        </div>

                        <div class="rowdiv" id="divhor" style="width: 422px; padding-bottom: 1px; background-color: #2A9D00;" runat="server">
                        </div>

                        <div class="rowdiv" id="div7" style="width: 100%; padding-bottom: 5px;" runat="server">
                            <div class="rowlbl" style="width: 174px;">
                                <span id="Span21" style="margin: 10px">
                                    <asp:Label ID="Label9" runat="server" Text="Label">VAT <span id="vatPercentage" runat="server"></span>% </asp:Label>
                                </span>
                            </div>
                            <div style="float: left">
                                <div class="priceGrid" style="margin-top: 3px;">

                                    <span class="eurspan">€</span>
                                    <asp:Label ID="vatInfo1" runat="server" Text="Label">   [Vat]</asp:Label>
                                </div>

                                <div class="priceGridre">
                                    <span class="eurspan">€</span>
                                    <asp:Label ID="revatInfo1" runat="server" CssClass="darkRecalc" Text="Label">   [Recalc Price]</asp:Label>

                                </div>
                            </div>
                        </div>



                        <div class="rowdiv" id="div8" style="width: 100%; padding-bottom: 5px;" runat="server">
                            <br />
                            <div class="rowlbl" style="width: 174px;">
                                <span id="Span23" style="margin: 10px">
                                    <asp:Label ID="Label11" runat="server" Text="Label">   Total gross</asp:Label>
                                </span>
                            </div>
                            <div style="float: left">
                                <div class="priceGrid" style="color: white; background-color: #4C4D51; border: 1px solid #4C4D51; width: 87px; font-weight: bold;">
                                    <span class="eurspan">€</span>
                                    <asp:Label ID="grossInfo1" runat="server" Text="Label">   </asp:Label>

                                </div>

                                <div class="priceGridrehigfinal" style="margin-left: 29px;">
                                    <span class="eurspan">€</span>
                                    <asp:Label ID="regrossInfo1" CssClass="darkRecalc" runat="server" Text="Label">   [Recalc Price]</asp:Label>

                                </div>

                                <asp:HiddenField runat="server" ID="hdgrossInfo1" />
                            </div>
                        </div>



                    </div>

                </div>


            </div>


            <div id="divinqvariants" runat="server" style="padding-top: 0px; border-top: 1px solid #2A9D00;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="LblVariantTitle" CssClass="lbltitleveriant" runat="server"></asp:Label>
                        <div id="diverrortd" runat="server" class="lblnr">
                            <asp:Label ID="error" runat="server" Text="" CssClass="error errmsgpostition"></asp:Label>
                        </div>
                        <table cellpadding="5" cellspacing="5" border="0" width="100%">
                            <tr>
                                <td width="155px" valign="top">
                                    <asp:Label ID="LblOptNoTitle" CssClass="rowlbl" Style="width: auto;" runat="server"></asp:Label>
                                </td>
                                <td width="105px" valign="top">
                                    <asp:Label ID="LblDelTermTitle" CssClass="rowlbl" Style="min-width: 0; width: auto;" runat="server"></asp:Label>
                                </td>
                                <td valign="top" width="70px">
                                    <asp:Label ID="LblQtyTitle" CssClass="rowlbl" Style="min-width: 0; width: auto;" runat="server"></asp:Label>
                                </td>
                                <td valign="top"></td>
                            </tr>
                            <tr>
                                <td valign="middle" class="textdataleft">
                                    <asp:Label ID="LblOptNo01" CssClass="lblvernumb lblnr" runat="server">01</asp:Label>
                                </td>
                                <td valign="middle">
                                    <asp:DropDownList runat="server" ID="DDLDelTerm01" Width="150px" CssClass="NormalControl"
                                        Enabled="False">
                                    </asp:DropDownList>
                                </td>
                                <td valign="middle">
                                    <asp:TextBox ID="TxtQty01" runat="server" Width="54px" MaxLength="8" CssClass="NormalControl"
                                        Enabled="False"></asp:TextBox>
                                </td>
                                <td valign="middle">
                                    <asp:LinkButton ID="LbtnAdd01" runat="server" OnClick="BtnAddVariant_Click">add variant</asp:LinkButton>
                                </td>
                            </tr>
                            <tr id="trVar02" runat="server">
                                <td valign="middle" class="textdataleft">
                                    <asp:Label ID="LblOptNo02" CssClass="lblvernumb lblnr" runat="server">02</asp:Label>
                                </td>
                                <td valign="middle">
                                    <asp:DropDownList runat="server" ID="DDLDelTerm02" Width="150px" CssClass="NormalControl">
                                    </asp:DropDownList>
                                </td>
                                <td valign="middle">
                                    <asp:TextBox ID="TxtQty02" runat="server" Width="54px" MaxLength="8" CssClass="NormalControl"></asp:TextBox>
                                </td>
                                <td valign="middle">
                                    <asp:LinkButton ID="LbtnRemove02" AccessKey="2" runat="server" OnClick="BtnRemoveVariant_Click">remove variant</asp:LinkButton>
                                </td>
                            </tr>
                            <tr id="trVar03" runat="server">
                                <td valign="middle" class="textdataleft">
                                    <asp:Label ID="LblOptNo03" CssClass="lblvernumb lblnr" runat="server">03</asp:Label>
                                </td>
                                <td valign="middle">
                                    <asp:DropDownList runat="server" ID="DDLDelTerm03" Width="150px" CssClass="NormalControl">
                                    </asp:DropDownList>
                                </td>
                                <td valign="middle">
                                    <asp:TextBox ID="TxtQty03" runat="server" Width="54px" MaxLength="8" CssClass="NormalControl"></asp:TextBox>
                                </td>
                                <td valign="middle">
                                    <asp:LinkButton ID="LbtnRemove03" AccessKey="3" runat="server" OnClick="BtnRemoveVariant_Click">remove variant</asp:LinkButton>
                                </td>
                            </tr>
                            <tr id="trVar04" runat="server">
                                <td valign="middle" class="textdataleft">
                                    <asp:Label ID="LblOptNo04" CssClass="lblvernumb lblnr" runat="server">04</asp:Label>
                                </td>
                                <td valign="middle">
                                    <asp:DropDownList runat="server" ID="DDLDelTerm04" Width="150px" MaxLength="8" CssClass="NormalControl">
                                    </asp:DropDownList>
                                </td>
                                <td valign="middle">
                                    <asp:TextBox ID="TxtQty04" runat="server" Width="54px" CssClass="NormalControl"></asp:TextBox>
                                </td>
                                <td valign="middle">
                                    <asp:LinkButton ID="LbtnRemove04" AccessKey="4" runat="server" OnClick="BtnRemoveVariant_Click">remove variant</asp:LinkButton>
                                </td>
                            </tr>
                            <tr id="trVar05" runat="server">
                                <td valign="middle" class="textdataleft">
                                    <asp:Label ID="LblOptNo05" CssClass="lblvernumb lblnr" runat="server">05</asp:Label>
                                </td>
                                <td valign="middle">
                                    <asp:DropDownList runat="server" ID="DDLDelTerm05" Width="150px" MaxLength="8" CssClass="NormalControl">
                                    </asp:DropDownList>
                                </td>
                                <td valign="middle">
                                    <asp:TextBox ID="TxtQty05" runat="server" Width="54px" CssClass="NormalControl"></asp:TextBox>
                                </td>
                                <td valign="middle">
                                    <asp:LinkButton ID="LbtnRemove05" AccessKey="5" runat="server" OnClick="BtnRemoveVariant_Click">remove variant</asp:LinkButton>
                                </td>
                            </tr>
                            <tr id="trVar06" runat="server">
                                <td valign="middle" class="textdataleft">
                                    <asp:Label ID="LblOptNo06" CssClass="lblvernumb lblnr" runat="server">06</asp:Label>
                                </td>
                                <td valign="middle">
                                    <asp:DropDownList runat="server" ID="DDLDelTerm06" Width="150px" MaxLength="8" CssClass="NormalControl">
                                    </asp:DropDownList>
                                </td>
                                <td valign="middle">
                                    <asp:TextBox ID="TxtQty06" runat="server" Width="54px" CssClass="NormalControl"></asp:TextBox>
                                </td>
                                <td valign="middle">
                                    <asp:LinkButton ID="LbtnRemove06" AccessKey="6" runat="server" OnClick="BtnRemoveVariant_Click">remove variant</asp:LinkButton>
                                </td>
                            </tr>
                            <tr id="trVar07" runat="server">
                                <td valign="middle" class="textdataleft">
                                    <asp:Label ID="LblOptNo07" CssClass="lblvernumb lblnr" runat="server">07</asp:Label>
                                </td>
                                <td valign="middle">
                                    <asp:DropDownList runat="server" ID="DDLDelTerm07" Width="150px" MaxLength="8" CssClass="NormalControl">
                                    </asp:DropDownList>
                                </td>
                                <td valign="middle">
                                    <asp:TextBox ID="TxtQty07" runat="server" Width="54px" CssClass="NormalControl"></asp:TextBox>
                                </td>
                                <td valign="middle">
                                    <asp:LinkButton ID="LbtnRemove07" AccessKey="7" runat="server" OnClick="BtnRemoveVariant_Click">remove variant</asp:LinkButton>
                                </td>
                            </tr>
                            <%--<tr>
                    <td valign="top">
                        <asp:Label ID="LblRemarks" CssClass="textdataleft" runat="server"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="TxtRemarks" runat="server" TextMode="MultiLine" Width="325px" CssClass="NormalControl"></asp:TextBox>
                    </td>
                </tr>--%>
                            <asp:HiddenField ID="HdnIOffId" runat="server" />
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" style="margin-left: 13px;">
                            <tr id="trremark" runat="server">
                                <td valign="middle" class="rowlbl" style="width: 166px; padding-left: 0px;">
                                    
                                    <asp:Label ID="lblRemark" runat="server"></asp:Label>
                                    
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRemark" runat="server" Width="219px" CssClass="NormalControl clsremark"
                                        Rows="3" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <%--  <div class="rowdiv" id="divdeliveryterm" runat="server">
                <div class="rowlbl">
                    <span id="Span2" style="margin: 10px">
                        <asp:Label ID="lbldeliveryterm" runat="server" Text=""></asp:Label></span>
                </div>
                <div style="float: left">
                    <asp:DropDownList ID="ddldeliveryterm" runat="server" EnableViewState="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="rowdiv" id="divqty" runat="server">
                <div class="rowlbl">
                    <span id="Span4" style="margin: 10px">
                        <asp:Label ID="lblqyentity" runat="server" Text=""></asp:Label>
                        <span class="error">*</span></span>
                </div>
                <div style="float: left">
                    <asp:TextBox ID="txtquentity" MaxLength="3" runat="server" EnableViewState="true"
                        onkeypress="javascript:numericFilter(this);"></asp:TextBox>
                </div>
            </div>--%>
            <%--   <div class="rowdiv" runat="server" id="DivChecktermscondition">
                <div class="rowlbl">
                    &nbsp
                </div>
                <div style="float: left; margin-left: -3px;">
                    <asp:CheckBox ID="chkTermsofSalesLink" Checked="true" runat="server"></asp:CheckBox>
                    <asp:Label CssClass="textdataleft" for="chkTermsofSalesLink" ID="lblTermsofSalesLink"
                        runat="server"></asp:Label>
                </div>
            </div>--%>
            <div class="rowdiv">
                <div class="rowlbl" id="divsubmitbutton" style="width: 220px;">
                    &nbsp
                </div>
                <div style="float: left">
                    <div class="divbttonw">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="buttons btnSearchButton" OnClick="btnsubit_Click" OnClientClick="return setSubmitbtnDisable();" />
                        <img runat="server" id="imgProcessing" style="margin-top: 8px;" src="https://be.eurocircuits.com/shop/images/newwait.gif" class="hide" />
                        <%--OnClientClick="this.disabled=true;" --%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
</div>
<div runat="server" id="pleasewaitdiv" visible="false">
</div>
<script src="JS/jquery-1.9.1.js" type="text/javascript"></script>
<link href="JS/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="JS/jquery-ui.js" type="text/javascript"></script>
<div runat="server" id="centerErrorMessage" style="margin-left: 27%; margin-top: 38%" visible="false">
    <asp:Label ID="centerErrorMsg" runat="server" Text="Error occured, inquiry can not be placed." CssClass="error errmsgpostition"></asp:Label>
</div>
