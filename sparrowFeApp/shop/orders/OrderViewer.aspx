﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Web.UI" %>


<!DOCTYPE>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../CSS/FontStyle.css" rel="stylesheet" />
    <link href="../../shop/main.css?v=1.10" id="loadMaincss" runat="server" rel="stylesheet" type="text/css" />
    <link href="../../shop/maineps.css?v=1.8" id="loadmainepscss" runat="server" rel="stylesheet"
        type="text/css" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Eurocircuits Web Order Detail Viewer</title>
    <script src="../../JS/jquery_1.7.2.min.js"></script>
    <%--   <script src="../JS/jquery_1_6_1_min.js" language="javascript" type="text/javascript"></script>--%>
    <script type="text/javascript" language="javascript" src="../../visulizer/webclient/webclient.nocache.js?v=<%= DateTime.Now.ToString("ddmmyyyyhhmm")%> "> <%--eurocircuit--%>  </script>
    <%--<link type="text/css" rel="stylesheet" href="WebClient.css?v=<%= getversion() %>" />--%>
    <link href="../../CSS/FontStyle.css" rel="stylesheet" />
    <script src="../JS/jquery-ui.js"></script>
    <script>


        $(function () {
            $("table").attr("border", "0");
        });


    </script>
</head>
<body>

    <form id="from1" runat="server">
        <div>
        </div>
        <style>
            .ECConfiguratorCategoryPanel {
                padding-left: 5px;
            }

            .displaynone {
                display: none;
            }

            .displayblock {
                display: block;
            }

            .paddingtopbot {
                padding: 5px 0 0 0px;
                color: #717171;
                font-size: 13px;
                margin: 10px 0 0 4px;
            }

            .popup_block {
                position: relative;
            }

            .title_text {
                display: none;
            }

            .divbtton {
                margin: 0px;
            }

            .caption {
                font-weight: bold;
            }

            #parent {
                border: medium none !important;
            }

            #maincontent td, #footertab td {
                padding: 0 5px;
            }

            #maincontent tr:first-child {
                display: none;
            }

            #DivOrderPreview {
                padding: 4px 0 10px 14px;
            }



            #footertab {
                background-color: #222222;
            }

            .fldcss {
                border-radius: 4px 4px 4px 4px;
                margin-top: 10px;
                width: 98.5%;
            }

            .lgndcss {
                font-size: 14px;
                font-weight: bold;
            }

            .textdataleft {
                color: #464646;
                font-size: 0.9em;
            }

            .wid_26per {
                width: 27%;
            }

            wid_74per {
                width: 74%;
            }

            .ECConfiguratorCategoryHeaderCommercial {
                background-color: #4C4D51;
                border: 1px solid #CCCCCC;
                border-radius: 4px 4px 4px 4px;
                color: white;
                margin-bottom: 1px;
                margin-right: 0;
                margin-top: 0;
                padding: 0.4em;
                font-size: 1.3em;
                font-weight: bold;
            }

            .ECConfiguratorCategoryContentCommercial {
                border: 1px solid #CCCCCC;
                border-radius: 4px 4px 4px 4px;
                margin-bottom: 1px;
                width: 100%;
            }

            .ECConfiguratorPriceHeader {
                background-color: #2A9D00;
                border: 1px solid #CCCCCC;
                border-radius: 4px 4px 4px 4px;
                color: white;
                margin-bottom: 1px;
                margin-right: 0;
                margin-top: 0;
                padding: 0.5em;
                font-size: 1.3em;
                font-weight: bold;
            }

            #tblPriceContainer .ECConfiguratorCategoryContentCommercial .textdataleft {
                color: #2A9D00 !important;
            }

            #tblPriceContainer .ECConfiguratorCategoryContentCommercial tr td {
                padding: 3px 0;
            }

            .ECConfiguratorCategoryContentCommercial tr {
                line-height: 20px !important;
            }

            table.selectedTab {
                margin-top: -8px;
                padding-top: 5px;
            }

            #clientContainer td:nth-child(2) table:first-child {
                margin-left: 1px;
            }

            .control-box-border {
                border-top: 1px solid #FFFFFF !important;
            }

            #lblBrdNetPrice, #lblGrossAmt {
                width: 93px;
                display: inline-block;
            }

            #tblPriceContainer table tr td:first-child {
                font-weight: normal !important;
            }

            #tblPriceContainer table tr td:nth-child(2), #ctl00_mainContent_txtPercentage {
                font-weight: bold !important;
            }

                #tblPriceContainer table tr td:nth-child(2) span {
                    display: block;
                    padding-left: 3px;
                    text-align: center;
                }

            #tblPriceContainer .textdataleft {
                font-size: 1em;
            }

            .highlightcomment, .highlightcurrency {
                font-size: 1em !important;
            }

            .showcenter {
                width: 100%;
                height: 100%;
                top: 45%;
                text-align: center;
                position: fixed;
            }

            #divProg {
                top: 45%;
                position: fixed;
                left: 40%;
            }

            #clientContainer {
                margin-top: 0px !important;
            }

            .pageloader {
                display: block;
                position: absolute;
                left: 45%;
                z-index: 20;
            }

            .divPrognew {
                background: #e8791c none repeat scroll 0 0;
                border-radius: 1px;
                color: #fff;
                font-size: 12px;
                padding: 5px 7px;
                position: fixed;
                z-index: 100000;
            }
        </style>

        <script src="JS/eccperm.js"></script>

        <script>
            function getQuerystring(name) {
                name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
                var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
                var results = regex.exec(location.search);
                return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
            };

            function HideVisLodingImg() {
                setTimeout(function () {
                    //    console.log($(".gwt-PopupPanel").length);
                    if ($(".gwt-PopupPanel").length > 0) {
                        console.log($(".gwt-PopupPanel").length);
                        $("div").remove(".gwt-PopupPanel");
                        $("div").remove(".gwt-PopupPanelGlass");
                    }
                }, 1000)
            }

            function SetVisualizerdivHeight() {
                $('table').attr("cellspacing", 0);
                $('#clientContainer').attr('style', 'margin-top: 0px !important;');
                var winHeight = $(window).height();
                var clientcontainer = $("#clientContainer").height();
                var nAgt = navigator.userAgent;

                if ($("#clientContainer").length > 0) {
                    if (winHeight > 1250) {
                        if (((nAgt.indexOf("Windows NT 5.1")) == -1)) {
                            // check os is XP                        
                            $('body').attr('style', 'overflow-Y: hidden !important');
                        }
                    }
                }
            }


            jQuery(window).load(function () {
                $('.pageloader').hide();

                var viewMode = getQuerystring("mode");
                if (viewMode == "ecccommercialDetails") {
                    var eccPermission = getCookie("eccPermissions");

                    if (eccPermission) {
                        var eccPermissions = eccPermission.split(",");
                        for (var i = 0; i < eccPermissions.length; i++) {
                            if (eccPermissions[i] == "delAdd") {
                                jQuery("#fieldsetdelivery").hide();
                            }
                            if (eccPermissions[i] == "price") {
                                jQuery("#tblPriceContainer").hide();
                            }
                        }
                    }
                }

            });
            //jQuery(document).ready(function ($) {
            //    $('.pageloader').show();
            //});
        </script>
        <asp:HiddenField ID="hdnorderNumber" runat="server" />
        <asp:HiddenField ID="hdnrequestKey" runat="server" />
        <asp:HiddenField ID="hdncommercial" runat="server" />
        <asp:HiddenField ID="hdnConfigurator" runat="server" />
        <asp:HiddenField ID="hdnLangCode" Value="en" runat="server" />
        <asp:HiddenField ID="hdnlangid" Value="en" runat="server" />
        <asp:HiddenField ID="hdnIsInquiry" Value="0" runat="server" />
        <asp:HiddenField ID="hdnEccUserId" Value="en" runat="server" />
        <div class="pageloader divPrognew">
            <div style="display: inline-block;">
                <img src="/shop/images/Small_Loading.gif" />
            </div>
            <div style="display: inline-block; vertical-align: top;">
                <asp:Label ID="lblLoading" runat="server" Text="Loading..."></asp:Label>
            </div>
        </div>
        <div id="divProg" class="divProg" style="display: none;">
            <img id="imgwait" runat="server" src="~/shop/images/Newwait.gif" />
        </div>
        <div id="ConfiguratorDiv" runat="server" style="display: block;">
            <div id="clientContainer" style="width: 100%; height: auto; min-width: 320px; margin-top: 5px;">
            </div>
            <div runat="server" id="DivOrderPreview" visible="false">
            </div>
        </div>
        <div id="divRegenrateXml" runat="server" visible="false" class="showcenter">
            <asp:Label ID="lblerror" runat="server" Text="Error occurred." Style="color: red; font-weight: bold; font-size: 15px;"></asp:Label>
            <br />
            <asp:LinkButton ID="lnkclickhere" runat="server" Text="Click here" OnClick="lnkclickhere_Click" OnClientClick="$('#divProg').css('display','block');" Visible="false" Font-Underline="true" Style="color: #2A9D00;"></asp:LinkButton>
            <asp:Label ID="lblmsg" runat="server" Text="to regenerate new xml" Visible="false"></asp:Label>
        </div>

        <div id="DivCommercialData" visible="false" style="margin-left: 5px; height: auto; min-width: 320px; margin-top: 5px;"
            runat="server" class="control-box-border">
            <div style="width: 98%; float: left;">
                <div id="legendorder" runat="server" class="ECConfiguratorCategoryHeaderCommercial">
                </div>
                <table class="ECConfiguratorCategoryContentCommercial" style="min-width: 400px;">
                    <tr class="brd-btm" style="line-height: 15px;">
                        <td class="caption" style="width: 150px;">
                            <span class="textdataleft">
                                <asp:Label ID="InqNo" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblInqNo" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px" id="trRepeatorder" runat="server">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="Repeatnr" runat="server" Text=""></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblRepeatnr" runat="server" Text=""></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px" id="trInqOrderNo" runat="server">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="InqOrderNo" runat="server" Text=""></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblInqOrderNo" runat="server" Text=""></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="pcbname" runat="server" Text=""></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblpcbname" runat="server" Text=""></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="purchref" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblpurchesref" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="articleref" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblarticleref" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="projectref" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblprojectref" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px; display: none;">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="lblecstencilcompatible" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="ecstencilcompatible" runat="server" Text="No"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px" id="trboardlock" runat="server">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="Boardlock" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblboardlock" runat="server" Text="No"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px" id="trCV" runat="server">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="lblPricecalctxt" runat="server" Text="Pricecalc version :"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblCV" runat="server" Text=""></asp:Label>
                            </span>
                        </td>
                    </tr>

                    <tr class="brd-btm" style="line-height: 15px">
                        <td style="font-weight: bold; width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="lblStencilFix" runat="server" Text=" "></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblStencilFixValue" runat="server" Text=" "></asp:Label></span>
                            <div style="color: #2A9D00; text-align: right; padding-right: 8px; font-size: 0.9em; margin-top: -20px;">* Stencil fix order will be visible after preparation.</div>

                        </td>
                    </tr>
                </table>
            </div>
            <div id="tblPriceContainer" runat="server" visible="false" style="width: 98%; float: left; margin-left: 3px;">
                <div id="Div2" runat="server" class="ECConfiguratorCategoryHeaderCommercial" style="background-color: #2A9D00;">
                    Pricing
               
                </div>
                <table class="ECConfiguratorCategoryContentCommercial" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr style="display: none;">
                            <td colspan="2" align="right">
                                <asp:Label ID="lblCurrSymbol" class="highlightcomment" Style="margin-right: 3px;" runat="server"></asp:Label></td>
                        </tr>
                        <tr class="brd-btm" style="line-height: 25px">
                            <td style="font-weight: bold; width: 290px;">
                                <span class="textdataleft">
                                    <asp:Label ID="lblPricemarkup" runat="server" Text="Label"> PCB Price markup</asp:Label>
                                </span>
                            </td>
                            <td>
                                <span class="textdataleft">
                                    <asp:Label ID="lblPercentage" runat="server"></asp:Label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <span class="textdataleft">Quantity</span>
                            </td>
                            <td align="left" style="vertical-align: middle;">
                                <span class="textdataleft">
                                    <asp:Label ID="lblPCBQty1" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="vertical-align: middle; font-weight: bold;">
                                <div class="textdataleft">
                                    Single PCB
                               
                                </div>
                            </td>
                            <td align="left" style="vertical-align: middle;">
                                <span class="textdataleft">
                                    <asp:Label ID="lblUntiPrice" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="vertical-align: middle; font-weight: bold;">
                                <div class="textdataleft">
                                    Total boards
                               
                                </div>
                            </td>
                            <td align="left" style="vertical-align: middle; padding-right: 0px;">
                                <span class="textdataleft">
                                    <asp:Label ID="lblBrdNetPrice" class="ECConfiguratorBoldPriceValueReversed" Style="margin-right: 0px; width: 96px; margin-left: 31%;" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr id="AssemblyRow" runat="server">
                            <td align="left" style="vertical-align: middle; font-weight: bold;">
                                <div class="textdataleft">
                                    Assembly
                               
                                </div>
                            </td>
                            <td align="left" style="vertical-align: middle;">
                                <span class="textdataleft">
                                    <asp:Label ID="lblAssemblyPrice" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr id="trAssemblypricemarkup" runat="server" class="brd-btm" style="line-height: 25px">
                            <td style="font-weight: bold; width: 290px;">
                                <span class="textdataleft">
                                    <asp:Label ID="Label1" runat="server" Text="Label"> Assembly Price markup</asp:Label>
                                </span>
                            </td>
                            <td>
                                <span class="textdataleft">
                                    <asp:Label ID="lblAssemblypricemarkup" runat="server"></asp:Label>
                                </span>
                            </td>
                        </tr>
                        <tr id="ComponentsRow" runat="server">
                            <td align="left" style="vertical-align: middle; font-weight: bold;">
                                <div class="textdataleft">
                                    Components
                               
                                </div>
                            </td>
                            <td align="left" style="vertical-align: middle;">
                                <span class="textdataleft">
                                    <asp:Label ID="lblComponentsPrice" runat="server"></asp:Label></span>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" style="vertical-align: middle; font-weight: bold;">
                                <div class="textdataleft">
                                    Transport cost
                               
                                </div>
                            </td>
                            <td align="left" style="vertical-align: middle;">
                                <span class="textdataleft">
                                    <asp:Label ID="lblTransportCost" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="vertical-align: middle; font-weight: bold; border-top: 1px solid #2A9D00;">
                                <div class="textdataleft">
                                    VAT <span id="vatPercentage" runat="server"></span>%
                               
                                </div>
                            </td>
                            <td align="left" style="vertical-align: middle; border-top: 1px solid #2A9D00;">
                                <span class="textdataleft">
                                    <asp:Label ID="lblVatAmount" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="vertical-align: middle; font-weight: bold !important;">
                                <div class="textdataleft" style="color: #464646 !important;">
                                    Total gross 
                               
                                </div>
                            </td>
                            <td align="left" style="vertical-align: middle; padding-right: 0px;">
                                <span class="textdataleft">
                                    <asp:Label ID="lblGrossAmt" CssClass="ECConfiguratorBlackPriceValueReversed" Style="margin-right: 0px; width: 96px; margin-left: 31%;" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="fieldsetdelivery" runat="server" style="width: 98%; clear: both;">
                <div id="legenddelivery" runat="server" class="ECConfiguratorCategoryHeaderCommercial">
                </div>
                <table width="40%" class="ECConfiguratorCategoryContentCommercial" style="min-width: 400px;">
                    <tr class="brd-btm" style="line-height: 15px" id="trInvoicenr" runat="server">
                        <td style="width: 150px;" class="caption">
                            <span class="textdataleft">
                                <asp:Label ID="InvoicNr" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblInvoicNr" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px; display: none;" id="trbasketNo" runat="server">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="basketno" runat="server" Text=""></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblBasketNo" runat="server" Text=""></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px; display: none;">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="PcbQty" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblPcbQty" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px; display: none;">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="PanelQty" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblPanelQty" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px;" id="trshipmentdate" runat="server">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="ShipmentDate" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblShipmentdate" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px; display: none;">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="TransportModes" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblTransportmode" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px" id="trdeliveryaddress" runat="server">
                        <td valign="top" class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="DeliveryAddress" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <div id="lblDeliveryaddress" runat="server" class="textdataleft">
                            </div>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px" id="trDeliverynr" runat="server">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="DeliveryNr" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblDeliverynr" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px;" id="trcurrency" runat="server" visible="false">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="Currency" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblCurrency" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr class="brd-btm" style="line-height: 15px" id="trcurrencyrefdate" runat="server" visible="false">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="CurrencyRefDate" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblCurrencyrefdate" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="fieldsetproductreff" runat="server" style="width: 98%; clear: both;">
                <div id="legendproduct" runat="server" class="ECConfiguratorCategoryHeaderCommercial"></div>

                <table class="ECConfiguratorCategoryContentCommercial" cellpadding="1">
                    <tr class="brd-btm" style="line-height: 15px" id="trIsFC" runat="server" visible="false">
                        <td class="caption" style="width: 170px;">
                            <span class="textdataleft">
                                <asp:Label ID="lblIsFC" runat="server" Text="Label"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label ID="lblFCValue" runat="server" Text="No"></asp:Label>
                            </span>
                        </td>
                    </tr>

                    <tr class="brd-btm" style="line-height: 20px">
                        <td style="font-weight: bold; width: 170px;">
                            <span class="textdataleft">
                                <asp:Label runat="server" ID="lblIsPlatedSlots"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label runat="server" ID="lblIsPlatedSlotsValue"></asp:Label>
                            </span>
                        </td>
                    </tr>

                    <tr class="brd-btm" style="line-height: 20px">
                        <td style="font-weight: bold; width: 170px;">
                            <span class="textdataleft">
                                <asp:Label runat="server" ID="lblAddpreproduction"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label runat="server" ID="lblAddprepro" Text="No"></asp:Label>
                            </span>
                        </td>
                    </tr>

                    <tr class="brd-btm" style="line-height: 20px">
                        <td style="font-weight: bold; width: 170px;">
                            <span class="textdataleft">
                                <asp:Label runat="server" ID="lbltextPackWithPaper"></asp:Label>
                            </span>
                        </td>
                        <td>
                            <span class="textdataleft">
                                <asp:Label runat="server" ID="lblIsPackWithPaper" Text="No"></asp:Label>
                            </span>
                        </td>
                    </tr>

                </table>
                <asp:HiddenField ID="markUpSaveMsg" runat="server" />
            </div>
            <div id="tableinq" runat="server" visible="false" style="min-width: 250px; margin-top: 10px; margin-left: 1px;">
            </div>
        </div>

        <div id="fldDivmsg" style="width: 98%; margin-left: 5px; height: auto; min-width: 320px; margin-top: 5px;" visible="false" runat="server">
            <div id="legendRemark" runat="server" class="ECConfiguratorCategoryHeaderCommercial">
                Pricing
           
            </div>
            <div id="divmsg" runat="server" class="ECConfiguratorCategoryContentCommercial" style="width: auto;">
            </div>
        </div>
    </form>
</body>
<script language="c#" runat="server">

    //create new sp osp_CheckGenMessage    
    const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
    sparrowFeApp.ecgdService.gdSoapClient objAPI = new sparrowFeApp.ecgdService.gdSoapClient();
    string version = string.Empty;
    sparrowFeApp.ecgdService.gdSoapClient objgd = new sparrowFeApp.ecgdService.gdSoapClient();

    System.Data.DataSet ds = new System.Data.DataSet();
    public string lblPriceMessagecheckout = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        string strOrderNumber = "";
        var objBase = new sparrowFeApp.core.BasePage();
        objBase.ResourceId = 39;
        pcbname.Text = objBase.GetLocalResourceObject("pcbname.Text");
        purchref.Text = objBase.GetLocalResourceObject("purchref.Text");
        articleref.Text = objBase.GetLocalResourceObject("articleref.Text");
        projectref.Text = objBase.GetLocalResourceObject("projectref.Text");
        lblecstencilcompatible.Text = objBase.GetLocalResourceObject("lblecstencilcompatible.Text");
        InvoicNr.Text = objBase.GetLocalResourceObject("lblInvoiceNr.Text");
        PcbQty.Text = objBase.GetLocalResourceObject("PcbQty.Text");
        PanelQty.Text = objBase.GetLocalResourceObject("PanelQty.Text");
        ShipmentDate.Text = objBase.GetLocalResourceObject("ShipmentDate.Text");
        TransportModes.Text = objBase.GetLocalResourceObject("TransportModes.Text");
        DeliveryAddress.Text = objBase.GetLocalResourceObject("DeliveryAddress.Text");
        DeliveryNr.Text = objBase.GetLocalResourceObject("DeliveryNr.Text");
        Currency.Text = objBase.GetLocalResourceObject("Currency.Text");
        CurrencyRefDate.Text = objBase.GetLocalResourceObject("CurrencyRefDate.Text");
        //ViewDetailsMenu.Items[0].Text = objBase.GetLocalResourceObject("lblConfigurator.Text");
        //ViewDetailsMenu.Items[1].Text = objBase.GetLocalResourceObject("lblCommercial.Text");
        Repeatnr.Text = objBase.GetLocalResourceObject("lblRepeatnr.Text");
        basketno.Text = objBase.GetLocalResourceObject("BasketNo.Text");
        InqOrderNo.Text = objBase.GetLocalResourceObject("InqNo.Text");
        legendorder.InnerText = objBase.GetLocalResourceObject("lblorderref.Text");
        legenddelivery.InnerText = objBase.GetLocalResourceObject("lbldeliveryref.Text");
        legendRemark.InnerText = objBase.GetLocalResourceObject("lblremarks.Text");
        Boardlock.Text = objBase.GetLocalResourceObject("lblboardlocked.Text");
        lblIsFC.Text = objBase.GetLocalResourceObject("lblIsFC.Text");
        lblStencilFix.Text = objBase.GetLocalResourceObject("lblStencilFix.Text");
        lblerror.Text = objBase.GetLocalResourceObject("lblerror.Text");
        lnkclickhere.Text = objBase.GetLocalResourceObject("lnkclickhere.Text");
        lblmsg.Text = objBase.GetLocalResourceObject("lblmsg.Text");
        lblIsPlatedSlots.Text = objBase.GetLocalResourceObject("lblIsPlatedSlots.Text");
        lbltextPackWithPaper.Text = objBase.GetLocalResourceObject("lbltextPackWithPaper.Text");
        legendproduct.InnerText = objBase.GetLocalResourceObject("lblproductref.Text");
        lblAddpreproduction.Text = objBase.GetLocalResourceObject("lblAddpreproduction.Text");
        lblPricecalctxt.Text = objBase.GetLocalResourceObject("lblPricecalctxt.Text");

        if (!IsPostBack)
        {
            string TokenNumber = Request["tokenid"] ?? "";
            string mode = Request["mode"] ?? "";
            Session["EDAPART"] = "true";

            if (Request["itemnumber"] != null && Request["key"] != null)
            {
                //
                strOrderNumber = Convert.ToString(Request["itemnumber"]);
                hdnorderNumber.Value = Convert.ToString(strOrderNumber).ToUpper();
                hdnrequestKey.Value = Convert.ToString(Request["key"]);
                string type = "";
                if (!hdnorderNumber.Value.StartsWith("B") && !hdnorderNumber.Value.ToUpper().Contains("INQ"))
                { type = "ORDER"; }
                else { type = "OFFER"; }

                hdnLangCode.Value = "en";
                hdnlangid.Value = "493";


                if (Request["loadFrom"] != null && Convert.ToString(Request["loadFrom"]).Trim().ToLower() == "ecc")
                {
                  
                    

                }

                else if (Request["mode"] != null && (Convert.ToString(Request["mode"]).Trim().ToLower().Contains("remarks") || Convert.ToString(Request["mode"]).Trim().ToLower().Contains("commercialdetails")))
                {
                    if (Convert.ToString(Request["mode"]).Trim().ToLower().Contains("commercialdetails"))
                    {
                        DivCommercialData.Visible = true;
                        BindCommercialData(Convert.ToString(Request["mode"]));
                    }
                    else if (Convert.ToString(Request["mode"]).Trim().ToLower().Contains("remarks"))
                    {
                        fldDivmsg.Visible = true;
                        SetRemarksTab(hdnorderNumber.Value, type, Convert.ToString(Request["mode"]));
                    }

                }

            }
        }
    }

    private void BindCommercialData(string Mode)
    {
        string OrderNumber = hdnorderNumber.Value.ToUpper();
        if (!OrderNumber.StartsWith("B"))
        {
            //order
            DataFromOrder(Mode);

        }
        else
        {
            if (!OrderNumber.ToUpper().Contains("INQ"))
            {
                // inq or basket
                if (DataFromInquiry(Mode) == "")
                {
                    DataFromBasket(Mode);
                }
            }
            else
            {
                DataFromInquiryDetail(Mode);
            }
        }
    }

    #region TestOk Function




    public static string GetNormalTextToHtml(string strInput)
    {
        strInput = strInput.Replace("»", "&raquo;");
        strInput = strInput.Replace("&", "&amp;");
        strInput = strInput.Replace("'", "&apos;");
        strInput = strInput.Replace(">", "&gt;");
        strInput = strInput.Replace("<", "&lt;");
        return strInput;

    }

    #endregion


    #region Visualizer




    #endregion







    #region Main Funtion Order, Basket,Inquiry
    private string DataFromOrder(string Mode)
    {
        string OrderNumber = hdnorderNumber.Value;
        System.Data.DataSet dt_order = new System.Data.DataSet();

        string deliveryTerm = "";
        string rootcompId = "";
        string CustcompId = "";
   
        string DeliveryAddressId = "";
        string DelAddContact = "";
        bool Islocked = false;
    
        string pcbQty = "0";
        bool IsFCValue = false;
        bool StencilTop = false;
        bool Stencilbot = false;
        bool IsStencilFix = false;
        bool IsPlatedSlots = false;
        bool IsPackWithPaper = false;
        string OrderId = "";
        if (OrderNumber.ToUpper().Contains("-B") || OrderNumber.ToUpper().Contains("-A"))
        {
            OrderNumber = OrderNumber.Split('-')[0].ToString();
        }

        dt_order = objAPI.GetData("SELECT ( select dbo.fn_globaldateformat(ReferenceDate ,493) as CurrencyRefDate  from admCurrenciesHistory where CurrenciesHistoryID in(so.CurrencyHistoryId)) as CurrencyRefDate,so.InvoiceNo, " +
                                " isnull(so.IsPanel,0) as IsPanel, so.DeliveryTerm,so.RootCompanyId,so.CustCompanyId,isnull(O.cv,'') as CV ,so.DeliveryAddressId,so.PanelQty, so.PCBQty,so.DeliveryNo,so.CurrSymbol,o.Transport_Mode,case when (o.DeliveryDate < o.Postponed_shipment_date ) then dbo.fn_globaldateformat(o.Postponed_shipment_date,493) else dbo.fn_globaldateformat(o.DeliveryDate,493) end as DeliveryDate, " +
                                " so.PCBName,so.PurchaseRef,so.ArticleRef,so.ProjectRef,so.ParentOrderNumber,b.BasketNo,gio.InquiryNo,o.IsFC,o.IsPlatedSlots,isnull(so.PackWithPaper,0) PackWithPaper,so.IsStencilTop,so.IsStencilBottom,b.Is_Stencil_Fixed,o.OrderId  FROM genSearchOrders so with(nolock)  " +
                                " INNER JOIN genOrders o with(nolock)  ON so.OrderNumber=o.OrderNumber  LEFT JOIN BasketItems b WITH(NOLOCK) ON b.Order_Number=so.OrderNumber " +
                                " left join genInqOffers gio with(nolock) ON gio.InqOfferId= o.OfferId where so.OrderNumber='" + OrderNumber.Replace("STENCIL", "") + "'", Keydata);

        string sql_ordpara = string.Format("SELECT top 1 case when isnull(IsManualTool,0)=0 then 'No' else 'Yes' end  as IsManualTool FROM genStencilOrderParameters WHERE OrderId in (select OrderId from genOrders where OrderNumber ='{0}')", OrderNumber);
        sql_ordpara += ";SELECT isnull(is_assembly_data,0) as is_assembly_data,cast(round(UnitPriceRootCo,2) as numeric(36,2)) as UnitPriceRootCo,cast(round(NetOrderPriceRootCo,2) as numeric(36,2)) as NetOrderPriceRootCo,OfferId,DeliveryTermId,cast(round(TransportCostRootCo,2) as numeric(36,2)) as TransportCostRootCo, cast(round(AssemblyTotalNetPrice,2) as numeric(36,2)) AssemblyTotalNetPrice,case when(ComponentTotalNetPrice < 0) then '0' else   cast(round(ComponentTotalNetPrice,2) as numeric(36,2)) end as ComponentTotalNetPrice from genOrders where OrderNumber ='" + OrderNumber.Replace("STENCIL", "") + "'";
        DataSet ds_ordpara = objAPI.GetData(sql_ordpara, Keydata);

        if (dt_order != null)
        {
            if (dt_order.Tables[0].Rows.Count > 0)
            {
                trInvoicenr.Visible = true;
                trDeliverynr.Visible = true;
                trRepeatorder.Visible = true;
                trbasketNo.Visible = true;
                trInqOrderNo.Visible = true;
                trshipmentdate.Visible = true;
                trdeliveryaddress.Visible = true;
                fieldsetdelivery.Visible = true;

                var objBase = new sparrowFeApp.core.BasePage();
                objBase.ResourceId = 39;
                InqNo.Text = objBase.GetLocalResourceObject("OrderNo.Text");
                lblInvoicNr.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["InvoiceNo"]);
                lblpcbname.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["PCBName"]);
                lblpurchesref.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["PurchaseRef"]);
                lblarticleref.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["ArticleRef"]);
                lblprojectref.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["ProjectRef"]);
                lblInqOrderNo.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["InquiryNo"]);
                lblCV.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["CV"]);
                // bool.TryParse(Convert.ToString(dt_order.Tables[0].Rows[0]["Is_Locked"]), out Islocked);
                bool.TryParse(Convert.ToString(dt_order.Tables[0].Rows[0]["IsFC"]), out IsFCValue);
                bool.TryParse(Convert.ToString(dt_order.Tables[0].Rows[0]["IsStencilTop"]), out StencilTop);
                bool.TryParse(Convert.ToString(dt_order.Tables[0].Rows[0]["IsStencilBottom"]), out Stencilbot);
                bool.TryParse(Convert.ToString(dt_order.Tables[0].Rows[0]["Is_Stencil_Fixed"]), out IsStencilFix);
                bool.TryParse(Convert.ToString(dt_order.Tables[0].Rows[0]["IsPlatedSlots"]), out IsPlatedSlots);
                bool.TryParse(Convert.ToString(dt_order.Tables[0].Rows[0]["PackWithPaper"]), out IsPackWithPaper);
                OrderId = Convert.ToString(dt_order.Tables[0].Rows[0]["OrderId"]);
                lblFCValue.Text = "No";
                if (IsFCValue == true)
                {
                    lblFCValue.Text = "Yes";
                }

                lblIsPlatedSlotsValue.Text = "No";
                if (IsPlatedSlots == true)
                {
                    lblIsPlatedSlotsValue.Text = "Yes";
                }
                lblIsPackWithPaper.Text = "No";
                if (IsPackWithPaper == true)
                {
                    lblIsPackWithPaper.Text = "Yes";
                }

                if (ds_ordpara != null && ds_ordpara.Tables[0].Rows.Count != 0)
                {
                    ecstencilcompatible.Text = Convert.ToString(ds_ordpara.Tables[0].Rows[0]["IsManualTool"]);
                }

                if (Convert.ToString(dt_order.Tables[0].Rows[0]["InvoiceNo"]) == "")
                {
                    lblInvoicNr.Text = "-";
                }


                string sql = string.Format("select (select top 1 statusId from GenOrderHistory where OrderId={0} and statusId =(select codeid from admCodes where Code = 'PREPRODUCTION')) as PreproductionApprId,(select isnull(IsPreproduction,0) from genSearchOrders where OrderId={0}) as Ispreproduction", OrderId);
                DataSet dtData = objgd.GetData(sql, Keydata);
                if (dtData.Tables[0].Rows.Count > 0)
                {
                    bool IsPreProduction = false;
                    bool.TryParse(Convert.ToString(dtData.Tables[0].Rows[0]["IsPreproduction"]), out IsPreProduction);
                    long orderStatusId = 0;
                    long.TryParse(Convert.ToString(dtData.Tables[0].Rows[0]["PreproductionApprId"]), out orderStatusId);
                    long PreProductionId = (new sparrowFeApp.ec09Service.ECAPISoapClient()).GetCodeId("PREPRODUCTION");

                    if (IsPreProduction || PreProductionId == orderStatusId)
                    {
                        lblAddprepro.Text = "Yes";
                    }

                    else
                    {
                        lblAddprepro.Text = "No";
                    }
                }
                lblBasketNo.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["BasketNo"]);

                if (Convert.ToBoolean(dt_order.Tables[0].Rows[0]["IsPanel"]) == true)
                {
                    lblPanelQty.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["PanelQty"]);
                    lblPcbQty.Text = "0";
                    pcbQty = Convert.ToString(dt_order.Tables[0].Rows[0]["PanelQty"]);
                }
                else
                {
                    lblPcbQty.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["PCBQty"]);
                    lblPanelQty.Text = "0";
                    pcbQty = Convert.ToString(dt_order.Tables[0].Rows[0]["PCBQty"]);
                }
                lblInqNo.Text = OrderNumber;
                deliveryTerm = Convert.ToString(dt_order.Tables[0].Rows[0]["DeliveryTerm"]).Trim().Replace("Working days", "").Trim();
                rootcompId = Convert.ToString(dt_order.Tables[0].Rows[0]["RootCompanyId"]);
                CustcompId = Convert.ToString(dt_order.Tables[0].Rows[0]["CustCompanyId"]);
                DeliveryAddressId = Convert.ToString(dt_order.Tables[0].Rows[0]["DeliveryAddressId"]);
                OrderId = Convert.ToString(dt_order.Tables[0].Rows[0]["OrderId"]);

                string StencilOrdernumber = "";
                string Sql_stencil = "";
                bool stencilfix = false;
                bool IsCheckOrderStecil = false;
                if (StencilTop == true)
                {
                    StencilOrdernumber = OrderNumber + "-ST";
                    if (OrderNumber.Contains("-"))
                    {
                        StencilOrdernumber = OrderNumber;
                    }
                    Sql_stencil = "SELECT isnull(IsManualTool,0) as IsManualTool from genStencilOrderParameters with(nolock) where OrderId IN (SELECT OrderId from genOrders with(nolock) where OrderNumber='" + StencilOrdernumber + "')";
                    DataSet ds_stencil = objgd.GetData(Sql_stencil, Keydata);
                    if (ds_stencil != null && ds_stencil.Tables[0].Rows.Count > 0)
                    {
                        IsCheckOrderStecil = true;
                        bool.TryParse(Convert.ToString(ds_stencil.Tables[0].Rows[0]["IsManualTool"]), out stencilfix);
                    }
                }
                else if (Stencilbot == true)
                {
                    StencilOrdernumber = OrderNumber + "-SB";
                    if (OrderNumber.Contains("-"))
                    {
                        StencilOrdernumber = OrderNumber;
                    }
                    Sql_stencil = "SELECT isnull(IsManualTool,0) as IsManualTool from genStencilOrderParameters with(nolock) where OrderId IN (SELECT OrderId from genOrders with(nolock) where OrderNumber='" + StencilOrdernumber + "')";
                    DataSet ds_stencil = objgd.GetData(Sql_stencil, Keydata);
                    if (ds_stencil != null && ds_stencil.Tables[0].Rows.Count > 0)
                    {
                        IsCheckOrderStecil = true;
                        bool.TryParse(Convert.ToString(ds_stencil.Tables[0].Rows[0]["IsManualTool"]), out stencilfix);
                    }
                }

                if (IsCheckOrderStecil == false)
                {
                    stencilfix = IsStencilFix;
                }

                lblStencilFixValue.Text = "No";
                if (stencilfix == true)
                {
                    lblStencilFixValue.Text = "Yes";
                }

                //load xml for order
                lblRepeatnr.Text = "-";
       
                string fullpath = "";
           
                System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = null;
                impersonateOnFileServer = sparrowFeApp.core.Impersonation.ImpersonateFileServer(sparrowFeApp.core.Constant.FileServerUsername, sparrowFeApp.core.Constant.FileServerPassword, sparrowFeApp.core.Constant.FileServerDomain);
                if (impersonateOnFileServer != null)
                {
                    string sql_file = sparrowFeApp.core.Common.getFilePath(OrderNumber);
                    {
                        if (sql_file != "")
                        {
                            sql_file = sql_file + "\\" + OrderNumber + ".xml";
                            if (System.IO.File.Exists(fullpath))
                            {
                                try
                                {
                                    XmlDocument xdoc = new XmlDocument();
                                    xdoc.Load(fullpath);

                                    System.Xml.XmlNode nodeCustomerProperties3 = xdoc.SelectSingleNode("OrderDetail/CustomerProperties3");
                                    System.Xml.XmlNode nodeOrderProperties3 = xdoc.SelectSingleNode("OrderDetail/OrderProperties3");
                                    System.Xml.XmlNode nodeBoardProperties3 = xdoc.SelectSingleNode("OrderDetail/BoardProperties3");
                                    if (nodeBoardProperties3 != null && nodeOrderProperties3 != null && nodeCustomerProperties3 != null)
                                    {
                                        string Repeatorder = Convert.ToString(nodeOrderProperties3.SelectSingleNode("RepeatOrderNum").InnerText);
                                        if (Repeatorder != "")
                                        {
                                            lblRepeatnr.Text = Repeatorder;
                                        }

                                        string Str_BoardLock = "";
                                        string str_BoardPID = "";
                                        if (OrderNumber.Trim().ToLower().Contains("-st"))
                                        {
                                            Str_BoardLock = Convert.ToString(nodeOrderProperties3.SelectSingleNode("StencilTopLocked").InnerText);
                                            str_BoardPID = Convert.ToString(nodeOrderProperties3.SelectSingleNode("StencilTopPID").InnerText);
                                        }
                                        else if (OrderNumber.Trim().ToLower().Contains("-sb"))
                                        {
                                            Str_BoardLock = Convert.ToString(nodeOrderProperties3.SelectSingleNode("StencilBotLocked").InnerText);
                                            str_BoardPID = Convert.ToString(nodeOrderProperties3.SelectSingleNode("StencilBotPID").InnerText);
                                        }
                                        else
                                        {
                                            Str_BoardLock = Convert.ToString(nodeOrderProperties3.SelectSingleNode("BoardLocked").InnerText);
                                            str_BoardPID = Convert.ToString(nodeOrderProperties3.SelectSingleNode("BoardPID").InnerText);
                                        }
                                        bool.TryParse(Str_BoardLock, out Islocked);
                                        if (Islocked == true)
                                        {
                                            lblboardlock.Text = "Yes";
                                        }
                                        else
                                        {
                                            lblboardlock.Text = "No";
                                        }

                                        //DataTable dtGetMarkup = objgd.GetData("declare @val decimal(18,5);exec osp_GetDecodeMarkup '" + str_BoardPID + "',@val out;select @val as Result", Keydata).Tables[0];
                                        //if (dtGetMarkup.Rows.Count != 0)
                                        //{
                                        //    string strMarkupvalue = Convert.ToString(dtGetMarkup.Rows[0]["Result"]);
                                        //    decimal.TryParse(strMarkupvalue, out Markupvalue);
                                        //}
                                        //lblpercentageaddedValue.Text = Convert.ToString(Math.Round((Markupvalue * 100), 2)) + "%";

                                    }
                                    else
                                    {
                                        //ConfiguratorDiv.Visible = false;
                                        divRegenrateXml.Visible = true;
                                        if (Convert.ToString(Request["loadFrom"]) == "ECC")
                                        {
                                            lblmsg.Visible = true;
                                            lnkclickhere.Visible = true;
                                        }
                                    }
                                }
                                catch 
                                {
                                    //ConfiguratorDiv.Visible = false;
                                    divRegenrateXml.Visible = true;
                                    if (Convert.ToString(Request["loadFrom"]) == "ECC")
                                    {
                                        lblmsg.Visible = true;
                                        lnkclickhere.Visible = true;
                                    }
                                }
                            }
                        }

                    }
                }






                // shipment date
                lblShipmentdate.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["DeliveryDate"]);

                //delivery address
                string sql_del = " SELECT * FROM genAddresses with(nolock) where AddressId=" + DeliveryAddressId + "";
                System.Data.DataSet objdeladdress = objAPI.GetData(sql_del, Keydata);
                if (objdeladdress != null)
                {
                    DelAddContact = getDelAddDetails(objdeladdress);
                    if (DelAddContact.Trim() != "")
                    {
                        lblDeliveryaddress.InnerHtml = DelAddContact;
                    }
                }

                lblDeliverynr.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["DeliveryNo"]);
                if (Convert.ToString(dt_order.Tables[0].Rows[0]["DeliveryNo"]) == "")
                {
                    lblDeliverynr.Text = "-";
                }
                lblCurrency.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["CurrSymbol"]);
                lblCurrSymbol.Text = objBase.GetLocalResourceObject("lblPriceMessage.Text").ToString().
                    Replace("#CurrencySymbol#", "<!cdata><span class='highlightcurrency'>" + Convert.ToString(dt_order.Tables[0].Rows[0]["CurrSymbol"])
                    + "</span>");

                lblCurrencyrefdate.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["CurrencyRefDate"]);
                lblTransportmode.Text = Convert.ToString(dt_order.Tables[0].Rows[0]["Transport_Mode"]);


                //apply css from rootcompid
                if (rootcompId != "" && rootcompId == "947")
                {
                    loadMaincss.Visible = false;
                    loadmainepscss.Visible = true;
                }
                else
                {
                    loadMaincss.Visible = true;
                    loadmainepscss.Visible = false;
                }

                tblPriceContainer.Visible = false;
                trboardlock.Visible = false;
                trCV.Visible = false;

                if (Mode.Trim().ToLower() == "ecccommercialdetails")
                {
                    trcurrencyrefdate.Visible = true;
                    trcurrency.Visible = true;

                    if (ds_ordpara.Tables[1].Rows.Count > 0)
                    {
                        trIsFC.Visible = true;
                        //trIsStencilFix.Visible = true;
                        tblPriceContainer.Visible = true;
                        trboardlock.Visible = true;
                        trCV.Visible = true;
                        string sqlPer = "SELECT isnull(Percentage,0) as Percentage ,isnull(AssemblyPID ,'AssemblyPID') as AssemblyPID from genOrders  with(nolock)  where OrderNumber ='" + OrderNumber + "'";
                        DataTable dtPer = objAPI.GetData(sqlPer, Keydata).Tables[0];
                        if (dtPer.Rows.Count > 0)
                        {
                            lblPercentage.Text = Convert.ToString(dtPer.Rows[0]["Percentage"]);

                            if (Convert.ToString(dtPer.Rows[0]["AssemblyPID"]).ToUpper() != "AssemblyPID".ToUpper())
                            {
                                DataTable dtGetMarkupforAssembly = objgd.GetData("declare @val decimal(18,5);exec osp_GetDecodeMarkup '" + Convert.ToString(dtPer.Rows[0]["AssemblyPID"]).ToUpper() + "',@val out;select @val as Result", Keydata).Tables[0];
                                lblAssemblypricemarkup.Text = (Convert.ToDecimal(dtGetMarkupforAssembly.Rows[0]["Result"]) * 100).ToString("0.00");

                            }
                        }
                        if (lblPercentage.Text.Trim() == "0.00000")
                        {
                            lblPercentage.Text = "-";
                        }




                        string vat = (new sparrowFeApp.shop.orders.WUCECCUploaddata()).getVATPer(CustcompId, DeliveryAddressId);
                        lblPCBQty1.Text = pcbQty;
                        lblUntiPrice.Text = Convert.ToString(ds_ordpara.Tables[1].Rows[0]["UnitPriceRootco"]);
                        lblBrdNetPrice.Text = Convert.ToString(ds_ordpara.Tables[1].Rows[0]["NetOrderPriceRootCo"]);
                        lblTransportCost.Text = Convert.ToString(ds_ordpara.Tables[1].Rows[0]["TransportCostRootCo"]);
                        if (Convert.ToString(ds_ordpara.Tables[1].Rows[0]["is_assembly_data"]).ToLower() == "true")
                        {
                            lblAssemblyPrice.Text = Convert.ToString(ds_ordpara.Tables[1].Rows[0]["AssemblyTotalNetPrice"]);
                            lblComponentsPrice.Text = Convert.ToString(ds_ordpara.Tables[1].Rows[0]["ComponentTotalNetPrice"]);
                        }
                        else
                        {
                            AssemblyRow.Visible = false;
                            ComponentsRow.Visible = false;
                            trAssemblypricemarkup.Visible = false;
                        }
                        vatPercentage.InnerText = (Convert.ToDecimal(vat) * 100).ToString();

                        priceCalculation("vat");
                        priceCalculation("gross");
                    }
                }

                return "success";
            }

        }
        return "";
    }

    private void priceCalculation(string operation)
    {
        decimal btdNetPrice = 0;
        decimal trasnPortCost = 0;
        decimal assemblypr = 0;
        decimal Compnentpr = 0;
        switch (operation)
        {
            case "vat":
                decimal vat = 0;
                decimal.TryParse(lblBrdNetPrice.Text, out btdNetPrice);
                decimal.TryParse(lblTransportCost.Text, out trasnPortCost);
                decimal.TryParse(vatPercentage.InnerText, out vat);
                string assemblypriceVat = lblAssemblyPrice.Text != null ? lblAssemblyPrice.Text : "";
                string CompnentpriceVat = lblComponentsPrice.Text != null ? lblComponentsPrice.Text : "";
                decimal.TryParse(assemblypriceVat, out assemblypr);
                decimal.TryParse(CompnentpriceVat, out Compnentpr);
                lblVatAmount.Text = Decimal.Round(((btdNetPrice + trasnPortCost + assemblypr + Compnentpr) * (vat / 100)), 2).ToString();
                break;
            case "gross":
                decimal vatData = 0;
                decimal.TryParse(lblBrdNetPrice.Text, out btdNetPrice);
                decimal.TryParse(lblTransportCost.Text, out trasnPortCost);
                decimal.TryParse(lblVatAmount.Text, out vatData);
                string assemblyprice = lblAssemblyPrice.Text != null ? lblAssemblyPrice.Text : "";
                string Compnentprice = lblComponentsPrice.Text != null ? lblComponentsPrice.Text : "";
                decimal.TryParse(assemblyprice, out assemblypr);
                decimal.TryParse(Compnentprice, out Compnentpr);
                lblGrossAmt.Text = Decimal.Round((btdNetPrice + trasnPortCost + vatData + assemblypr + Compnentpr), 2).ToString();
                break;
        }
    }


    private string DataFromInquiry(string Mode)
    {
        string srno = "";
        string qty = "";
        string deliveryterm = "";
        string OrderNumber = hdnorderNumber.Value;
        System.Data.DataSet dt_Inq = new System.Data.DataSet();
        var objAPI = new sparrowFeApp.ecgdService.gdSoapClient();
        string deliveryTerm = "";
        string rootcompId = "";
        string CustcompId = "";
        string DelAddContact = "";
        string StatusId = "";
        string OfferId = "";
        string inqNumber = OrderNumber;

        string unitprice = string.Empty;
        string transportprice = string.Empty;
        string totalprice = string.Empty;
        string vat = string.Empty;
        string gross = string.Empty;
        string strBoardLock = string.Empty;
        string strPID = string.Empty;
        string InqVarient = string.Empty;
        lblPriceMessagecheckout = string.Empty;

        dt_Inq = objAPI.GetData("SELECT isnull(sio.IsPanel,0) as IsPanel,sio.PanelQty, isnull(b.CV,'') as CV, sio.PCBQty,sio.DeliveryTerm,sio.RootCompanyId,sio.CustCompanyId, sio.InqOfferStatusId ,(SELECT Symbol FROM admCurrencies where CurrencyId IN (b.Currency_Id) AND isnull(IsDeleted,0)=0)as  CurrSymbol ,  (select dbo.fn_globaldateformat(ReferenceDate ,493) as CurrencyRefDate  from admCurrenciesHistory where CurrenciesHistoryID IN (b.Currency_History_Id ) ) as CurrencyRefDate ,B.Transport_Mode, dbo.fn_globaldateformat(B.Estimate_Delivery_Date,493) as Estimate_Delivery_Date,sio.PCBName,sio.ProjectRef,sio.ArticleRef,sio.PurchaseRef,b.BasketNo,sio.ParentInqNumber,sio.InqOfferId FROM genSearchInqOffers  sio with(nolock)   LEFT JOIN BasketItems as b ON b.BasketNo=sio.InqNumber where sio.InqNumber='" + inqNumber + "'", Keydata);
        string sql_inqpara = string.Format("SELECT top 1 case when isnull(IsManualTool,0)=0 then 'No' else 'Yes' end as IsManualTool FROM genStencilInquiryParameters WHERE InquiryId in (select InqOfferId from genInqOffers where InquiryNo ='{0}')", inqNumber);
        DataSet ds_inqpara = objAPI.GetData(sql_inqpara, Keydata);
        if (dt_Inq != null)
        {
            if (dt_Inq.Tables[0].Rows.Count > 0)
            {
                trInvoicenr.Visible = false;
                trDeliverynr.Visible = false;
                trRepeatorder.Visible = true;
                trbasketNo.Visible = false;
                trInqOrderNo.Visible = false;
                trshipmentdate.Visible = false;
                trdeliveryaddress.Visible = false;
                tableinq.Visible = true;
                fieldsetdelivery.Visible = false;
                //trIsStencilFix.Visible = true;
                lblStencilFixValue.Text = "No";
                if (ds_inqpara != null && ds_inqpara.Tables[0].Rows.Count != 0)
                {
                    ecstencilcompatible.Text = Convert.ToString(ds_inqpara.Tables[0].Rows[0]["IsManualTool"]);
                    lblStencilFixValue.Text = "No";
                }


                var objBase = new sparrowFeApp.core.BasePage();
                objBase.ResourceId = 39;
                InqNo.Text = objBase.GetLocalResourceObject("InqNo.Text");
                srno = objBase.GetLocalResourceObject("lblsrno.Text");
                qty = objBase.GetLocalResourceObject("lblqty.Text");
                deliveryterm = objBase.GetLocalResourceObject("lbldeliveryterm.Text");
                unitprice = objBase.GetLocalResourceObject("lblunitprice.Text");
                transportprice = objBase.GetLocalResourceObject("lbltransportcost.Text");
                totalprice = objBase.GetLocalResourceObject("lbltotalprice.Text");
                vat = objBase.GetLocalResourceObject("lblvat.Text");
                gross = objBase.GetLocalResourceObject("lblgross.Text");
                strBoardLock = objBase.GetLocalResourceObject("lblboardlocked.Text");
                strPID = objBase.GetLocalResourceObject("lblpercentageadded.Text");
                InqVarient = objBase.GetLocalResourceObject("lblvarientreference.Text");

                string lblRemark = objBase.GetLocalResourceObject("lblremarks.Text");
                string createdon = objBase.GetLocalResourceObject("lblcreatedon.Text");
                string FromUser = objBase.GetLocalResourceObject("lblFromUser.Text");
                string Duedate = objBase.GetLocalResourceObject("lblDuedate.Text");
                string Subject = objBase.GetLocalResourceObject("lblSubject.Text");


                lblInvoicNr.Text = "-";
                lblpcbname.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["PCBName"]);
                lblpurchesref.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["PurchaseRef"]);
                lblarticleref.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["ArticleRef"]);
                lblprojectref.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["ProjectRef"]);
                lblRepeatnr.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["ParentInqNumber"]);
                OfferId = Convert.ToString(dt_Inq.Tables[0].Rows[0]["InqOfferId"]);
                lblCV.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["CV"]);
                if (Convert.ToBoolean(dt_Inq.Tables[0].Rows[0]["IsPanel"]) == true)
                {
                    lblPanelQty.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["PanelQty"]);
                    lblPcbQty.Text = "0";
                }
                else
                {
                    lblPcbQty.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["PCBQty"]);
                    lblPanelQty.Text = "0";
                }
                lblInqNo.Text = inqNumber;
                deliveryTerm = Convert.ToString(dt_Inq.Tables[0].Rows[0]["DeliveryTerm"]).Trim().Replace("Working days", "").Trim();
                rootcompId = Convert.ToString(dt_Inq.Tables[0].Rows[0]["RootCompanyId"]);
                CustcompId = Convert.ToString(dt_Inq.Tables[0].Rows[0]["CustCompanyId"]);
                StatusId = Convert.ToString(dt_Inq.Tables[0].Rows[0]["InqOfferStatusId"]);
                lblBasketNo.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["BasketNo"]);

                lblShipmentdate.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["Estimate_Delivery_Date"]);



                //delivery address    
                string sql_addressrelfordel = "select * from genAddressRelations where EntityId =" + CustcompId + " and EntityTypeId = 1 and AddressTypeId = 88 and isnull(IsDeleted ,0)= 0  and IsPrimaryAddress = 1  ";
                System.Data.DataSet objAddressrelfordel = objAPI.GetData(sql_addressrelfordel, Keydata);
                if (objAddressrelfordel != null)
                {
                    if (objAddressrelfordel.Tables[0].Rows.Count > 0)
                    {
                        string addressId = Convert.ToString(objAddressrelfordel.Tables[0].Rows[0]["AddressId"]);
                        string sql_objdeladdress = "select * from genAddresses Where AddressId =" + addressId + "  and isnull(IsDeleted ,0)= 0  ";
                        System.Data.DataSet objdeladdress = objAPI.GetData(sql_objdeladdress, Keydata);

                        if (objdeladdress != null)
                        {
                            DelAddContact = getDelAddDetails(objdeladdress);
                            if (DelAddContact.Trim() != "")
                            {
                                lblDeliveryaddress.InnerHtml = DelAddContact;
                            }
                        }
                    }
                }

                lblDeliverynr.Text = "-";
                lblTransportmode.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["Transport_Mode"]);
                lblCurrency.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["CurrSymbol"]);
                lblCurrencyrefdate.Text = Convert.ToString(dt_Inq.Tables[0].Rows[0]["CurrencyRefDate"]);


                // getorder xml from fileserver  

                string fullpath = "";

                System.Data.DataSet ds_fileserver = new System.Data.DataSet();
                System.Data.DataSet ds_file = new System.Data.DataSet();
                System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = null;
                impersonateOnFileServer = sparrowFeApp.core.Impersonation.ImpersonateFileServer(sparrowFeApp.core.Constant.FileServerUsername, sparrowFeApp.core.Constant.FileServerPassword, sparrowFeApp.core.Constant.FileServerDomain);
                if (impersonateOnFileServer != null)
                {
                    string sql_file = sparrowFeApp.core.Common.getFilePath(inqNumber);
                    {
                        if (sql_file != "")
                        {
                            sql_file = sql_file + "\\" + OrderNumber + ".xml";
                            if (System.IO.File.Exists(fullpath))
                            {
                                System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
                                xdoc.Load(fullpath);

                                System.Xml.XmlNode nodeCustomerProperties3 = xdoc.SelectSingleNode("OrderDetail/CustomerProperties3");
                                System.Xml.XmlNode nodeOrderProperties3 = xdoc.SelectSingleNode("OrderDetail/OrderProperties3");
                                System.Xml.XmlNode nodeBoardProperties3 = xdoc.SelectSingleNode("OrderDetail/BoardProperties3");
                                System.Xml.XmlNode ProductionProperties3 = xdoc.SelectSingleNode("OrderDetail/ProductionProperties3");
                                string IsBoardlock = "No";
                 
                                string brdlock = Convert.ToString(nodeOrderProperties3.SelectSingleNode("BoardLocked").InnerText);
                                string str_BoardPID = Convert.ToString(nodeOrderProperties3.SelectSingleNode("BoardPID").InnerText);
                                if (brdlock.Trim().ToLower() == "true".Trim().ToLower())
                                {
                                    IsBoardlock = "Yes";
                                }
                                lblboardlock.Text = IsBoardlock;
                                if (ProductionProperties3 != null)
                                {
                                    bool Isplateslots = false;
                                    bool.TryParse(Convert.ToString(ProductionProperties3.SelectSingleNode("PlatedSlots").InnerText), out Isplateslots);
                                    lblIsPlatedSlotsValue.Text = "No";
                                    if (Isplateslots == true)
                                    {
                                        lblIsPlatedSlotsValue.Text = "Yes";
                                    }
                                    try
                                    {
                                        bool IsPackWithPaper = false;
                                        bool.TryParse(Convert.ToString(ProductionProperties3.SelectSingleNode("PackWithPaper").InnerText), out IsPackWithPaper);

                                        lblIsPackWithPaper.Text = "No";
                                        if (IsPackWithPaper == true)
                                        {
                                            lblIsPackWithPaper.Text = "Yes";
                                        }
                                    }
                                    catch { }
                                }

                                //DataTable dtGetMarkup = objgd.GetData("declare @val decimal(18,5);exec osp_GetDecodeMarkup '" + str_BoardPID + "',@val out;select @val as Result", Keydata).Tables[0];
                                //if (dtGetMarkup.Rows.Count != 0)
                                //{
                                //    string strMarkupvalue = Convert.ToString(dtGetMarkup.Rows[0]["Result"]);
                                //    decimal.TryParse(strMarkupvalue, out Markupvalue);
                                //}
                                //lblpercentageaddedValue.Text = Convert.ToString(Math.Round((Markupvalue * 100), 2)) + "%";
                            }
                        }
                    }

                }






                // set inq variants
                string html = "";
                System.Data.DataSet deliverytermday = new System.Data.DataSet();
                string Deliveryterm = "";

                string count = "";
                string CurrSymbol = string.Empty;
                long genofferstatusID = (new sparrowFeApp.ec09Service.ECAPISoapClient()).GetCodeId("GenerateOffer");
                string sql_inqds = string.Format("SELECT DeliveryTermId,PCBQty, " +
                                                "CASE WHEN  isnull(OfferPrice_Curr,0)=0 then CONVERT(decimal(18,2),isnull(Stencilprice_Curr/PCBQty,0) )ELSE CONVERT(decimal(18,2),isnull(OfferPrice_Curr/PCBQty,0) )END as [unit_price]," +
                                                "isnull(TransportTotalCost,0)  as [transport_price]," +
                                                "CASE WHEN  isnull(OfferPrice_Curr,0)=0 THEN  isnull(Stencilprice_Curr,0) else isnull(OfferPrice,0) end as [total_price], " +
                                                "CONVERT(varchar(15),VATPer)+'%' as [vat]," +
                                                "isnull(TransportTotalCost,0)+ CASE WHEN  isnull(OfferPrice_Curr,0)=0 THEN isnull(Stencilprice_Curr,0) else isnull(OfferPrice,0) end +isnull(VATAmount,0) as [gross] ," +
                                                "(select top 1 Symbol from admCurrencies where CurrencyId in (select Currencyid from genInqOffers  where InqOfferId='{0}'))  as symbol ,(case when ISNULL(BoardLock,0)= 0 then 'No' else 'Yes' end) as BoardLock, " +
                                                "isnull(Percentage,0) as Percentage " +
                                                "from genInqOffersVariants  where InqOfferId='{0}'", OfferId);
                DataSet ds_inqvariants = objAPI.GetData(sql_inqds, Keydata);
                if (ds_inqvariants != null)
                {
                    if (ds_inqvariants.Tables[0].Rows.Count > 0)
                    {
                        if (StatusId.Trim() == Convert.ToString(genofferstatusID).Trim())
                        {
                            CurrSymbol = Convert.ToString(ds_inqvariants.Tables[0].Rows[0]["symbol"]);
                            lblPriceMessagecheckout = objBase.GetLocalResourceObject("lblPriceMessage.Text").ToString().Replace("#CurrencySymbol#", "<!cdata><span class='highlightcurrency'>" + CurrSymbol + "</span>");
                            html = html + " <div style='clear: both; width: 100%; text-align: right; padding-top: 5px;margin-bottom: -10px;'>" +
                                            "<lable class='highlightcomment' style='margin-right: 3px;'>" + lblPriceMessagecheckout + "</label></div>";
                            html = html + "<div style='clear: both; margin-top: 10px;'>" +
                                "<div class='ECConfiguratorCategoryHeaderCommercial' >" + InqVarient + "</div>";
                            html = html + "<table width='100%' class='ECConfiguratorCategoryContentCommercial'>" +
                                              " <tr class='brd-btm' style='line-height: 15px;background-color:#E5E5E5;color:#000;'>" +
                                              " <td width='10%' align='center'> " +
                                              " <span class='textdataleft'>" + srno + "</span>" +
                                              " </td>" +
                                              " <td width='10%' align='center'>" +
                                              " <span class='textdataleft'> " + qty + "</span>" +
                                              " </td>" +
                                              " <td width='10%' align='center'> " +
                                              " <span class='textdataleft'> " + deliveryterm + "</span>" +
                                              " </td>" +
                                                " <td width='10%' align='center'> " +
                                              " <span class='textdataleft'> " + unitprice + "</span>" +
                                              " </td>" +
                                                " <td width='10%' align='center'> " +
                                              " <span class='textdataleft'> " + transportprice + "</span>" +
                                              " </td>" +
                                                " <td width='10%' align='center'> " +
                                              " <span class='textdataleft'> " + totalprice + "</span>" +
                                              " </td>" +
                                                " <td width='10%' align='center'> " +
                                              " <span class='textdataleft'> " + vat + "</span>" +
                                              " </td>" +
                                                " <td width='10%' align='center'> " +
                                              " <span class='textdataleft'> " + gross + "</span>" +
                                              " </td>";
                            if (Mode.Trim().ToLower() == "ecccommercialdetails")
                            {
                                html = html +
                                              " <td width='10%' align='center'> " +
                                              " <span class='textdataleft'> " + strBoardLock + "</span>" +
                                              " </td>" + " <td width='10%' align='center'> " +
                                                 " <span class='textdataleft'> " + strPID + "</span>" +
                                                 " </td>";
                            }
                            html = html + " </tr>";

                            for (int i = 0; i < ds_inqvariants.Tables[0].Rows.Count; i++)
                            {
                                count = Convert.ToString(i + 1);
                                html = html + "<tr> ";
                                html = html + "<td align='center'> <span class='textdataleft'>  " + count + " </span> </td>";
                                html = html + "<td align='center'> <span class='textdataleft'>  " + Convert.ToString(ds_inqvariants.Tables[0].Rows[i]["PCBQty"]) + " </span> </td>";
                                Deliveryterm = Convert.ToString(ds_inqvariants.Tables[0].Rows[i]["DeliveryTermId"]);
                                if (Deliveryterm != "")
                                {
                                    deliverytermday = objAPI.GetData("select dbo.fn_getParameterParaoption(219," + hdnlangid.Value + ",(SELECT dbo.fn_getParameterParaoptionInformation(219," + hdnlangid.Value + "," + Deliveryterm.Trim() + "))) as deliveryterm", Keydata);
                                    if (deliverytermday != null)
                                    {
                                        if (deliverytermday.Tables[0].Rows.Count > 0)
                                        {
                                            html = html + "<td align='center'> <span class='textdataleft'> " + Convert.ToString(deliverytermday.Tables[0].Rows[0]["deliveryterm"]) + " </span> </td>";
                                        }
                                    }
                                }
                                html = html + "<td align='center'> <span class='textdataleft'>  " + Convert.ToString(ds_inqvariants.Tables[0].Rows[i]["unit_price"]) + " </span> </td>";
                                html = html + "<td align='center'> <span class='textdataleft'>  " + Convert.ToString(ds_inqvariants.Tables[0].Rows[i]["transport_price"]) + " </span> </td>";
                                html = html + "<td align='center'> <span class='textdataleft'>  " + Convert.ToString(ds_inqvariants.Tables[0].Rows[i]["total_price"]) + " </span> </td>";
                                html = html + "<td align='center'> <span class='textdataleft'>  " + Convert.ToString(ds_inqvariants.Tables[0].Rows[i]["vat"]) + " </span> </td>";
                                html = html + "<td align='center'> <span class='textdataleft'>  " + Convert.ToString(ds_inqvariants.Tables[0].Rows[i]["gross"]) + " </span> </td>";

                                if (Mode.Trim().ToLower() == "ecccommercialdetails")
                                {
                                    html = html + "<td align='center'> <span class='textdataleft'>  " + Convert.ToString(lblboardlock.Text) + " </span> </td>";
                                    html = html + "<td align='center'> <span class='textdataleft'>  " + Math.Round(Convert.ToDecimal(ds_inqvariants.Tables[0].Rows[i]["Percentage"]), 2).ToString() + "%" + " </span> </td>";
                                }
                                html = html + "</tr> ";
                            }

                        }
                        else
                        {
                            html = html + "<div style='clear: both; float: left; margin-top: 5px;width:60%;'>" +
                               "<div class='ECConfiguratorCategoryHeaderCommercial' >" + InqVarient + "</div>";
                            html = html + "<table width='100%' class='ECConfiguratorCategoryContentCommercial'>" +
                                           " <tr class='brd-btm' style='line-height: 15px;background-color:#E5E5E5;color:#000;'>" +
                                           " <td width='16%' align='center'> " +
                                           " <span class='textdataleft'>" + srno + "</span>" +
                                           " </td>" +
                                           " <td width='33%' align='center'>" +
                                           " <span class='textdataleft'> " + qty + "</span>" +
                                           " </td>" +
                                           " <td width='38%' align='center'> " +
                                           " <span class='textdataleft'> " + deliveryterm + "</span>" +
                                           " </td>";
                            if (Mode.Trim().ToLower() == "ecccommercialdetails")
                            {
                                html = html + " <td width='38%' align='center'> " +
                                           " <span class='textdataleft'> " + strPID + "</span>" +
                                           " </td>";
                            }
                            html = html + " </tr>";

                            for (int i = 0; i < ds_inqvariants.Tables[0].Rows.Count; i++)
                            {
                                count = Convert.ToString(i + 1);
                                html = html + "<tr> ";
                                html = html + "<td align='center'> <span class='textdataleft'>  " + count + " </span> </td>";
                                html = html + "<td align='center'> <span class='textdataleft'>  " + Convert.ToString(ds_inqvariants.Tables[0].Rows[i]["PCBQty"]) + " </span> </td>";
                                Deliveryterm = Convert.ToString(ds_inqvariants.Tables[0].Rows[i]["DeliveryTermId"]);
                                if (Deliveryterm != "")
                                {
                                    deliverytermday = objAPI.GetData("select dbo.fn_getParameterParaoption(219," + hdnlangid.Value + ",(SELECT dbo.fn_getParameterParaoptionInformation(219," + hdnlangid.Value + "," + Deliveryterm.Trim() + "))) as deliveryterm", Keydata);
                                    if (deliverytermday != null)
                                    {
                                        if (deliverytermday.Tables[0].Rows.Count > 0)
                                        {
                                            html = html + "<td align='center'> <span class='textdataleft'> " + Convert.ToString(deliverytermday.Tables[0].Rows[0]["deliveryterm"]) + " </span> </td>";
                                        }
                                    }
                                }
                                if (Mode.Trim().ToLower() == "ecccommercialdetails")
                                {
                                    html = html + "<td align='center'> <span class='textdataleft'>  " + Math.Round(Convert.ToDecimal(ds_inqvariants.Tables[0].Rows[i]["Percentage"]), 2).ToString() + "%" + " </span> </td>";
                                }
                                html = html + "</tr> ";
                            }

                        }

                        html = html + "</table></div>";
                        tableinq.InnerHtml = html;
                    }
                }

                trboardlock.Visible = false;
                trCV.Visible = false;
                if (Mode.Trim().ToLower() == "ecccommercialdetails")
                {
                    trCV.Visible = true;
                    trboardlock.Visible = true;
                }

                //apply css from rootcompid
                if (rootcompId != "" && rootcompId == "947")
                {
                    loadMaincss.Visible = false;
                    loadmainepscss.Visible = true;
                }
                else
                {
                    loadMaincss.Visible = true;
                    loadmainepscss.Visible = false;
                }

                if (StatusId == "" || StatusId != "463")  // offer is not created
                {
                    hdnIsInquiry.Value = "1";
                }
                return "success";
            }
        }
        return "";
    }

    private string DataFromBasket(string Mode)
    {
        string OrderNumber = hdnorderNumber.Value;
        System.Data.DataSet dt_Basket = new System.Data.DataSet();
        var objAPI = new sparrowFeApp.ecgdService.gdSoapClient();
        string deliveryTerm = "";
        string rootcompId = "";
        string CustcompId = "";
        string DelAddContact = "";
        string PnlBy = "";
        string Boardqty = "";
        string TransportMode = "";
        string sessionId = "";
        string BasketId = "";
        dt_Basket = objAPI.GetData("select isnull (cv,'')as CV, dbo.fn_globaldateformat(Estimate_Delivery_Date,493) as Estimate_Delivery_Date,Transport_Mode,DeliveryTerm,Company_id, (select top 1 dbo.fn_globaldateformat(ReferenceDate ,493) as CurrencyRefDate  from admCurrenciesHistory where CurrenciesHistoryID in(Currency_History_Id)) as CurrencyRefDate,(select top 1 ac.Symbol from admCurrencies as ac where  ac.CurrencyId in(Currency_Id)) as Symbol,PCB_Name,Purchase_Reference,Article_Reference,Project_Reference ,case when isnull(Is_Stencil_Fixed,0)=0 then 'No' else 'Yes' end as Is_Stencil_Fixed,Session_Id,Basket_Id,BasketNo FROM BasketItems with(nolock) where BasketNo='" + OrderNumber + "'", Keydata);  ///Remove XML_Data Columns 
        if (dt_Basket != null)
        {
            if (dt_Basket.Tables[0].Rows.Count > 0)
            {
                trInvoicenr.Visible = false;
                trDeliverynr.Visible = false;
                trRepeatorder.Visible = false;
                trbasketNo.Visible = false;
                trInqOrderNo.Visible = false;
                trshipmentdate.Visible = false;
                trdeliveryaddress.Visible = false;
                fieldsetdelivery.Visible = false;


                var objBase = new sparrowFeApp.core.BasePage();
                objBase.ResourceId = 39;
                InqNo.Text = objBase.GetLocalResourceObject("BasketNo.Text");
                lblInvoicNr.Text = "-";
                lblInqNo.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["BasketNo"]);
                lblpcbname.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["PCB_Name"]);
                lblpurchesref.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Purchase_Reference"]);
                lblarticleref.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Article_Reference"]);
                lblprojectref.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Project_Reference"]);
                lblCV.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["CV"]);
                sessionId = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Session_Id"]);
                ecstencilcompatible.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Is_Stencil_Fixed"]);
                // chandrapal FX
                //trIsStencilFix.Visible = true;
                lblStencilFixValue.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Is_Stencil_Fixed"]);
                //string xmldata = Convert.ToString(dt_Basket.Tables[0].Rows[0]["XML_Data"]);   ///Remove XML_Data Columns 
                string xmldata = "";
                //EC09WebApp.EC09WebService.BPO objbpo = new EC09WebApp.EC09WebService.BPO();

                //objbpo.GetXMLFromFileServer(dt_Basket.Tables[0].Rows[0]["Session_Id"].ToString(), dt_Basket.Tables[0].Rows[0]["BasketNo"].ToString());
                //if (XMLResult != "" && XMLResult != "NOTFOUND")
                //{
                //    xmldata = XMLResult;
                //}
                System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
                xmldoc.LoadXml(xmldata);
                System.Xml.XmlNode xmlOrderPropertyNode = xmldoc.SelectSingleNode("OrderDetail/OrderProperties3");
                System.Xml.XmlNode xmlBoardPropertyNode = xmldoc.SelectSingleNode("OrderDetail/BoardProperties3");
                System.Xml.XmlNode ProductionProperties3 = xmldoc.SelectSingleNode("OrderDetail/ProductionProperties3");
                if (xmlOrderPropertyNode != null)
                {
                    Boardqty = xmlOrderPropertyNode.SelectSingleNode("BoardQty").InnerText;
                    TransportMode = xmlOrderPropertyNode.SelectSingleNode("BoardTransportMode").InnerText;

                    string IsBoardlock = "No";

                    string brdlock = Convert.ToString(xmlOrderPropertyNode.SelectSingleNode("BoardLocked").InnerText);
                    string str_BoardPID = Convert.ToString(xmlOrderPropertyNode.SelectSingleNode("BoardPID").InnerText);
                    if (brdlock.Trim().ToLower() == "true".Trim().ToLower())
                    {
                        IsBoardlock = "Yes";
                    }

                    lblboardlock.Text = IsBoardlock;
                    if (ProductionProperties3 != null)
                    {
                        bool Isplateslots = false;
                        bool.TryParse(Convert.ToString(ProductionProperties3.SelectSingleNode("PlatedSlots").InnerText), out Isplateslots);
                        lblIsPlatedSlotsValue.Text = "No";
                        if (Isplateslots == true)
                        {
                            lblIsPlatedSlotsValue.Text = "Yes";
                        }
                        try
                        {

                            bool IsPackWithPaper = false;
                            bool.TryParse(Convert.ToString(ProductionProperties3.SelectSingleNode("PackWithPaper").InnerText), out IsPackWithPaper);
                            lblIsPackWithPaper.Text = "No";
                            if (IsPackWithPaper == true)
                            {
                                lblIsPackWithPaper.Text = "Yes";
                            }
                        }
                        catch { }

                    }
                    //DataTable dtGetMarkup = objgd.GetData("declare @val decimal(18,5);exec osp_GetDecodeMarkup '" + str_BoardPID + "',@val out;select @val as Result", Keydata).Tables[0];
                    //if (dtGetMarkup.Rows.Count != 0)
                    //{
                    //    string strMarkupvalue = Convert.ToString(dtGetMarkup.Rows[0]["Result"]);
                    //    decimal.TryParse(strMarkupvalue, out Markupvalue);
                    //}
                    //lblpercentageaddedValue.Text = Convert.ToString(Math.Round((Markupvalue * 100), 2)) + "%";

                }

                if (xmlBoardPropertyNode != null)
                {
                    PnlBy = xmlBoardPropertyNode.SelectSingleNode("PanelType").InnerText;
                    if (PnlBy == null || PnlBy == "0" || PnlBy == "" || PnlBy.ToLower() == "no")
                    {
                        lblPcbQty.Text = Boardqty;
                        lblPanelQty.Text = "0";
                    }
                    else
                    {
                        lblPcbQty.Text = "0";
                        lblPanelQty.Text = Boardqty;
                    }
                }

                deliveryTerm = Convert.ToString(dt_Basket.Tables[0].Rows[0]["DeliveryTerm"]);
                CustcompId = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Company_id"]);
                lblCurrencyrefdate.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["CurrencyRefDate"]);
                lblCurrency.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Symbol"]);

                lblTransportmode.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Transport_Mode"]);
                lblShipmentdate.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Estimate_Delivery_Date"]);
                BasketId = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Basket_Id"]);


                string sql_addressrelfordel = "select * from genAddressRelations with(nolock) where EntityId =" + CustcompId + " and EntityTypeId = 1 and AddressTypeId = 88  and isnull(IsDeleted ,0)= 0  and IsPrimaryAddress = 1 ";
                System.Data.DataSet objAddressrelfordel = objAPI.GetData(sql_addressrelfordel, Keydata);
                if (objAddressrelfordel != null)
                {
                    if (objAddressrelfordel.Tables[0].Rows.Count > 0)
                    {
                        string addressId = Convert.ToString(objAddressrelfordel.Tables[0].Rows[0]["AddressId"]);
                        string sql_objdeladdress = "select * from genAddresses with(nolock) Where AddressId =" + addressId + " and isnull(IsDeleted ,0)= 0 ";
                        System.Data.DataSet objdeladdress = objAPI.GetData(sql_objdeladdress, Keydata);

                        if (objdeladdress != null)
                        {
                            DelAddContact = getDelAddDetails(objdeladdress);
                            if (DelAddContact.Trim() != "")
                            {
                                lblDeliveryaddress.InnerHtml = DelAddContact;
                            }

                        }
                    }
                }
                lblDeliverynr.Text = "-";
                trboardlock.Visible = false;
                trCV.Visible = false;
                if (Mode.Trim().ToLower() == "ecccommercialdetails")
                {
                    trboardlock.Visible = true;
                    trCV.Visible = true;
                }



            }



            //apply css from rootcompid
            if (rootcompId != "" && rootcompId == "947")
            {
                loadMaincss.Visible = false;
                loadmainepscss.Visible = true;
            }
            else
            {
                loadMaincss.Visible = true;
                loadmainepscss.Visible = false;
            }
            return "success";
        }

        return "";
    }

    private string DataFromInquiryDetail(string Mode)
    {
        string OrderNumber = hdnorderNumber.Value;
        System.Data.DataSet dt_Basket = new System.Data.DataSet();
        var objAPI = new sparrowFeApp.ecgdService.gdSoapClient();
        string deliveryTerm = "";
        string rootcompId = "";
        string CustcompId = "";
        string DelAddContact = "";

        string Boardqty = "";
        string TransportMode = "";

        dt_Basket = objAPI.GetData("select  DeliveryDate as Estimate_Delivery_Date,'' as Transport_Mode,DeliveryTerm,CustCompanyId as Company_id,(select top 1 dbo.fn_globaldateformat(ReferenceDate ,493) as CurrencyRefDate  from admCurrenciesHistory where CurrenciesHistoryID in(CurrenciesHistoryID)) as CurrencyRefDate, (select top 1 ac.Symbol from admCurrencies as ac where  ac.CurrencyId in(ac.CurrencyId)) as Symbol,PCBName as PCB_Name,PurchaseRef as Purchase_Reference,ArticleRef as Article_Reference,ProjectRef as Project_Reference,'' as Is_Stencil_Fixed,PCBQty from genSearchInqOffers where InqNumber = '" + OrderNumber + "'", Keydata);
        if (dt_Basket != null)
        {
            if (dt_Basket.Tables[0].Rows.Count > 0)
            {
                trInvoicenr.Visible = false;
                trDeliverynr.Visible = false;
                trRepeatorder.Visible = false;
                trbasketNo.Visible = false;
                trInqOrderNo.Visible = false;
                trshipmentdate.Visible = false;
                trdeliveryaddress.Visible = false;
                fieldsetdelivery.Visible = false;


                var objBase = new sparrowFeApp.core.BasePage();
                objBase.ResourceId = 39;
                InqNo.Text = objBase.GetLocalResourceObject("BasketNo.Text");
                lblInvoicNr.Text = "-";
                lblInqNo.Text = OrderNumber;
                lblpcbname.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["PCB_Name"]);
                lblpurchesref.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Purchase_Reference"]);
                lblarticleref.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Article_Reference"]);
                lblprojectref.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Project_Reference"]);
                ecstencilcompatible.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Is_Stencil_Fixed"]);
                Boardqty = Convert.ToString(dt_Basket.Tables[0].Rows[0]["PCBQty"]);
                TransportMode = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Transport_Mode"]);
                string IsBoardlock = "Yes";
                lblboardlock.Text = IsBoardlock;



                deliveryTerm = Convert.ToString(dt_Basket.Tables[0].Rows[0]["DeliveryTerm"]);
                CustcompId = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Company_id"]);
                lblCurrencyrefdate.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["CurrencyRefDate"]);
                lblCurrency.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Symbol"]);

                lblTransportmode.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Transport_Mode"]);
                lblShipmentdate.Text = Convert.ToString(dt_Basket.Tables[0].Rows[0]["Estimate_Delivery_Date"]);



                string sql_addressrelfordel = "select * from genAddressRelations where EntityId =" + CustcompId + " and EntityTypeId = 1 and AddressTypeId = 88  and isnull(IsDeleted ,0)= 0  and IsPrimaryAddress = 1 ";
                System.Data.DataSet objAddressrelfordel = objAPI.GetData(sql_addressrelfordel, Keydata);
                if (objAddressrelfordel != null)
                {
                    if (objAddressrelfordel.Tables[0].Rows.Count > 0)
                    {
                        string addressId = Convert.ToString(objAddressrelfordel.Tables[0].Rows[0]["AddressId"]);
                        string sql_objdeladdress = "select * from genAddresses Where AddressId =" + addressId + " and isnull(IsDeleted ,0)= 0 ";
                        System.Data.DataSet objdeladdress = objAPI.GetData(sql_objdeladdress, Keydata);

                        if (objdeladdress != null)
                        {
                            DelAddContact = getDelAddDetails(objdeladdress);
                            if (DelAddContact.Trim() != "")
                            {
                                lblDeliveryaddress.InnerHtml = DelAddContact;
                            }

                        }
                    }
                }
                lblDeliverynr.Text = "-";
                trboardlock.Visible = false;
                trCV.Visible = false;

                if (Mode.Trim().ToLower() == "ecccommercialdetails")
                {
                    trboardlock.Visible = true;
                    trCV.Visible = true;
                }


                //apply css from rootcompid
                if (rootcompId != "" && rootcompId == "947")
                {
                    loadMaincss.Visible = false;
                    loadmainepscss.Visible = true;
                }
                else
                {
                    loadMaincss.Visible = true;
                    loadmainepscss.Visible = false;
                }

                // set lang 


                return "success";
            }
        }
        return "";
    }

    public void SetRemarksTab(string OrderNumer, string Type, string ModeType)
    {
        // set remark for basket
        var objBase = new sparrowFeApp.core.BasePage();
        objBase.ResourceId = 39;
        string lblRemark = objBase.GetLocalResourceObject("lblremarks.Text");
        string createdon = objBase.GetLocalResourceObject("lblcreatedon.Text");
        string FromUser = objBase.GetLocalResourceObject("lblFromUser.Text");
        string Duedate = objBase.GetLocalResourceObject("lblDuedate.Text");
        string Subject = objBase.GetLocalResourceObject("lblSubject.Text");
        StringBuilder htmlmsg = new StringBuilder();
        System.Data.DataSet ds_msg = new System.Data.DataSet();
        string liveUrl = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CofiguratorUrl"]);
        string Id = "";
        if (Type.Trim().ToLower() == "order")
        {
            string Sql = "SELECT OrderId from genSearchOrders WITH(NOLOCK) where OrderNumber='" + OrderNumer + "'";
            DataSet ds = objAPI.GetData(Sql, Keydata);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            { Id = Convert.ToString(ds.Tables[0].Rows[0]["OrderId"]); }
        }
        else
        {
            string Sql = "SELECT InqOfferId from genInqOffers WITH(NOLOCK) where InquiryNo='" + OrderNumer + "'";
            DataSet ds = objAPI.GetData(Sql, Keydata);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            { Id = Convert.ToString(ds.Tables[0].Rows[0]["InqOfferId"]); }
            if (Id.Trim() == "")
            {
                Sql = "SELECT Basket_Id from BasketItems with(NOLOCK) where BasketNo='" + OrderNumer + "'";
                ds = objAPI.GetData(Sql, Keydata);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                { Id = Convert.ToString(ds.Tables[0].Rows[0]["Basket_Id"]); }
            }
        }

        if (Id.Trim() != "")
        {
            string strhideElem = "";
            string cookie = null;
            if (Request.Cookies["eccPermissions"] != null)
            {
                cookie = Request.Cookies["eccPermissions"].Value.ToString();
            }
            if (cookie != null)
            {
                strhideElem = cookie
                    ;
            }

            //if (ModeType.Trim().ToLower() == "remarks")
            //{
            ds_msg = objAPI.GetData(string.Format("exec ecc_GetOfferOrderMessages {0},'{1}'", Id, Type), Keydata);
            //}
            //else
            //{ ds_msg = objAPI.GetData(string.Format("exec ecc_GetPreProductionOrderMessages {0},'{1}','{2}'", Id, "Order Not approved.", "VIEWBACKTOANALYSIS"), Keydata); }
            if (ds_msg != null)
            {
                // bool isNewOrdFile = false;
                if (ds_msg.Tables[0].Rows.Count > 0)
                {
                    //for (int j = 0; j < ds_msg.Tables[0].Rows.Count; j++)
                    //{
                    //    if (Convert.ToString(ds_msg.Tables[0].Rows[j]["Subject"]).Trim() == "NEWCUTOMERDATAUPLOADED")
                    //    {
                    //        isNewOrdFile = true;
                    //    }
                    //}
                    //if (isNewOrdFile)
                    //{
                    for (int j = 0; j < ds_msg.Tables[0].Rows.Count; j++)
                    {
                        string strDBFromUser = Convert.ToString(ds_msg.Tables[0].Rows[j]["FromUser"]);
                        if (Convert.ToBoolean(ds_msg.Tables[0].Rows[j]["isCustomer"]) == true)
                        {
                            if (strhideElem.Contains("remarks"))
                            {
                                strDBFromUser = "Customer";
                            }
                        }

                        //if (Convert.ToString(ds_msg.Tables[0].Rows[j]["IsOrderFile"]).Trim().ToLower() != "true")
                        //{
                        htmlmsg.Append(" <table class='datatbl' style='min-width: 500px;  ' width='100%' >" +
                                           " <tr class='brd-btm' style='line-height: 15px'>" +
                                           "<td style='padding:0;' ><table width='100%'><tr> " +
                                           " <td class='caption' style='width: 95px;'>" +
                                           " <span class='textdataleft' style='color: #7B7B7B;font-size: 0.9em;'>" + createdon + "</span>" +
                                           " </td>" +
                                           "<td  style='width: 150px;'>" +
                                           "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;margin-left: -21px;'>" + Convert.ToString(ds_msg.Tables[0].Rows[j]["CreatedOn"]) + "</span>" +
                                           "</td>" +
                                            "<td class='caption'  style='width:85px;'>" +
                                           "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;'>" + Duedate + " </span>" +
                                           "</td>" +
                                           "<td style='width: 80px;'>" +
                                           "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;'>" + Convert.ToString(ds_msg.Tables[0].Rows[j]["DueDate"]) + "</span>" +
                                           "</td>" +
                                           "<td class='caption' style='width:85px;' > " +
                                           "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;'>" + FromUser + "</span>" +
                                           "</td>" +
                                           "<td   >" +
                                           "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;'>" + strDBFromUser + "</span>" +
                                           "</td>" +
                                           "</tr></table></td>" +
                                           "</tr>" +
                                           "<tr>" +
                                            "<td style='padding:0;'><table width='100%'><tr> " +
                                           "<td class='caption' style='width:75px;' >" +
                                           "<span class='textdataleft'>" + Subject + "</span>" +
                                           "</td>" +
                                           "<td>" +
                                           "<span class='textdataleft' style='color: #208E0A;font-size: 1.1em;'>" + Convert.ToString(ds_msg.Tables[0].Rows[j]["Subject"]) + "</span>" +
                                           "</td>" +
                                           "</tr>" +
                                           "</table></td></tr>" +
                                           "<tr>" +
                                           "<td style='padding:0;'><table width='100%'><tr> " +
                                           "<td class='caption' style='' >");

                        if (Convert.ToString(ds_msg.Tables[0].Rows[j]["EntityId"]).Trim().StartsWith("B"))
                        {
                            htmlmsg.Append("<span class='textdataleft'>BasketNum: </span><span class='textdataleft' style='padding-left: 13px;'> " + Convert.ToString(ds_msg.Tables[0].Rows[j]["EntityId"]) + " </span>");

                        }
                        else
                        {
                            htmlmsg.Append("<span class='textdataleft'>OrderNum: </span><span class='textdataleft' style='padding-left: 18px;'>  " + Convert.ToString(ds_msg.Tables[0].Rows[j]["EntityId"]) + " </span>");
                        }
                        //}
                        if (Convert.ToString(ds_msg.Tables[0].Rows[j]["IsOrderFile"]).Trim().ToLower() == "true")
                        {
                            htmlmsg.Append("<span class='textdataleft' style='color: red;font-size: 16px;'>New Customer data uploaded</span>");
                        }
                        htmlmsg.Append("</td>" +
                                        "</tr>" +
                                        "</table></td></tr>" +
                                        "<tr>"
                                        );

                        //else
                        //{
                        //    htmlmsg.Append("<span class='textdataleft'>OrderNum: </span><span class='textdataleft' style='padding-left: 18px;'>  " + Convert.ToString(ds_msg.Tables[0].Rows[j]["EntityId"]) + " </span>" );
                        //    if (Convert.ToString(ds_msg.Tables[0].Rows[j]["Subject"]).Trim() == "NEWCUTOMERDATAUPLOADED")
                        //    {
                        //        htmlmsg.Append("<span class='textdataleft' style='color: red;font-size: 16px;'>New Customer data uploaded</span>");
                        //    }
                        //    htmlmsg.Append("</td>" +
                        //                   "</tr>" +
                        //                   "</table></td></tr>" +
                        //                   "<tr>"
                        //                   ); 
                        //}  

                        if (Convert.ToString(ds_msg.Tables[0].Rows[j]["AttachedFile"]).Trim() != "")
                        {
                            //if (Convert.ToString(ds_msg.Tables[0].Rows[j]["IsOrderFile"]).Trim().ToLower() != "true")
                            //{
                            htmlmsg.Append("<td style='padding:0;'><div style='border: thin dotted rgb(13, 192, 16);padding: 5px;  margin-left: 10px;font-size: 1.4em; color: #208E0A;'> "
                                                + Convert.ToString(ds_msg.Tables[0].Rows[j]["Message"])
                                                + "<br/><br/>" + Convert.ToString(ds_msg.Tables[0].Rows[j]["AttachedFile"])
                                                + "<a  style='margin-left: 5px; color: red;' id=" + j + " href='/api.aspx?PreProductionFile=donloade&&Number="
                                                + OrderNumer + "&&name="
                                                + Convert.ToString(ds_msg.Tables[0].Rows[j]["AttachedFile"]) +
                                                "'> <img style='width: 13px;' src='../images/download-16.png?v=1.1' title='Download' /> Download </a>" +
                                                "<div></td>" +
                                                "</tr>" +
                                                "</table>");
                            // }

                        }
                        else
                        {
                            if (Convert.ToString(ds_msg.Tables[0].Rows[j]["Message"]).ToUpper().Contains("SYSTEM ERROR"))
                            {
                                htmlmsg.Append("<td style='padding:0;'><div style='border: thin dotted rgb(13, 192, 16);padding: 5px;  margin-left: 10px;font-size: 1.4em; color: red;'> "
                                                + Convert.ToString(ds_msg.Tables[0].Rows[j]["Message"])
                                                + "<div></td>"
                                                + "</tr>"
                                                + "</table>");
                            }
                            else
                            {
                                htmlmsg.Append("<td style='padding:0;'><div style='border: thin dotted rgb(13, 192, 16);padding: 5px;  margin-left: 10px;font-size: 1.4em; color: #208E0A;'> " + Convert.ToString(ds_msg.Tables[0].Rows[j]["Message"]) +
                                "<div></td>" +
                                "</tr>" +
                                "</table>");
                            }
                        }
                    }
                    //}
                    //else 
                    //{
                    //    for (int j = 0; j < ds_msg.Tables[0].Rows.Count; j++)
                    //    {
                    //        htmlmsg.Append(" <table class='datatbl' style='min-width: 500px;  ' width='100%' >" +
                    //                             " <tr class='brd-btm' style='line-height: 15px'>" +
                    //                             "<td style='padding:0;' ><table width='100%'><tr> " +
                    //                             " <td class='caption' style='width: 95px;'>" +
                    //                             " <span class='textdataleft' style='color: #7B7B7B;font-size: 0.9em;'>" + createdon + "</span>" +
                    //                             " </td>" +
                    //                             "<td  style='width: 150px;'>" +
                    //                             "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;margin-left: -21px;'>" + Convert.ToString(ds_msg.Tables[0].Rows[j]["CreatedOn"]) + "</span>" +
                    //                             "</td>" +
                    //                              "<td class='caption'  style='width:85px;'>" +
                    //                             "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;'>" + Duedate + " </span>" +
                    //                             "</td>" +
                    //                             "<td style='width: 80px;'>" +
                    //                             "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;'>" + Convert.ToString(ds_msg.Tables[0].Rows[j]["DueDate"]) + "</span>" +
                    //                             "</td>" +
                    //                             "<td class='caption' style='width:85px;' > " +
                    //                             "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;'>" + FromUser + "</span>" +
                    //                             "</td>" +
                    //                             "<td   >" +
                    //                             "<span class='textdataleft'  style='color: #7B7B7B;font-size: 0.9em;'>" + Convert.ToString(ds_msg.Tables[0].Rows[j]["FromUser"]) + "</span>" +
                    //                             "</td>" +
                    //                             "</tr></table></td>" +
                    //                             "</tr>" +
                    //                             "<tr>" +
                    //                              "<td style='padding:0;'><table width='100%'><tr> " +
                    //                             "<td class='caption' style='width:75px;' >" +
                    //                             "<span class='textdataleft'>" + Subject + "</span>" +
                    //                             "</td>" +
                    //                             "<td>" +
                    //                             "<span class='textdataleft' style='color: #208E0A;font-size: 1.1em;'>" + Convert.ToString(ds_msg.Tables[0].Rows[j]["Subject"]) + "</span>" +
                    //                             "</td>" +
                    //                             "</tr>" +
                    //                             "</table></td></tr>" +
                    //                             "<tr>" +
                    //                             "<td style='padding:0;'><table width='100%'><tr> " +
                    //                             "<td class='caption' style='' >");
                    //                             if (Convert.ToString(ds_msg.Tables[0].Rows[j]["EntityId"]).Trim().StartsWith("B"))
                    //                            {
                    //                                htmlmsg.Append("<span class='textdataleft'>BasketNum: </span><span class='textdataleft' style='padding-left: 13px;'> " + Convert.ToString(ds_msg.Tables[0].Rows[j]["EntityId"]) + " </span>" +
                    //                                               //"</td>" +
                    //                                               //"<td>" +

                    //                                               "</td>" +
                    //                                               "</tr>" +
                    //                                               "</table></td></tr>" +
                    //                                               "<tr>"
                    //                                               );   
                    //                            }   
                    //                            else
                    //                            {
                    //                                htmlmsg.Append("<span class='textdataleft'>OrderNum: </span><span class='textdataleft' style='padding-left: 18px;'>  " + Convert.ToString(ds_msg.Tables[0].Rows[j]["EntityId"]) + " </span>" +
                    //                                               //"</td>" +
                    //                                               //"<td>" +

                    //                                               "</td>" +
                    //                                               "</tr>" +
                    //                                               "</table></td></tr>" +
                    //                                               "<tr>"
                    //                                               ); 
                    //                            }  

                    //        if (Convert.ToString(ds_msg.Tables[0].Rows[j]["AttachedFile"]).Trim() != "")
                    //        {
                    //               htmlmsg.Append("<td style='padding:0;'><div style='border: thin dotted rgb(13, 192, 16);padding: 5px;  margin-left: 10px;font-size: 1.4em; color: #208E0A;'> "
                    //                                    + Convert.ToString(ds_msg.Tables[0].Rows[j]["Message"])
                    //                                    + "<br/><br/>" + Convert.ToString(ds_msg.Tables[0].Rows[j]["AttachedFile"])
                    //                                    + "<a  style='margin-left: 5px; color: red;' id=" + j + " href='" + liveUrl + "/shop/visualizer/i8ans.aspx?PreProductionFile=donloade&&Number="
                    //                                    + OrderNumer + " &&name="
                    //                                    + Convert.ToString(ds_msg.Tables[0].Rows[j]["AttachedFile"]) +
                    //                                    "'> <img style='width: 13px;' src='../images/download-16.png?v=1.1' title='Download' /> Download </a>" +
                    //                                    "<div></td>" +
                    //                                    "</tr>" +
                    //                                    "</table>");


                    //        }
                    //        else
                    //        {
                    //            if (Convert.ToString(ds_msg.Tables[0].Rows[j]["Message"]).ToUpper().Contains("SYSTEM ERROR"))
                    //            {
                    //                htmlmsg.Append("<td style='padding:0;'><div style='border: thin dotted rgb(13, 192, 16);padding: 5px;  margin-left: 10px;font-size: 1.4em; color: red;'> " + Convert.ToString(ds_msg.Tables[0].Rows[j]["Message"]) +

                    //                "<div></td>" +
                    //                "</tr>" +
                    //                "</table>");
                    //            }
                    //            else
                    //            {
                    //                htmlmsg.Append("<td style='padding:0;'><div style='border: thin dotted rgb(13, 192, 16);padding: 5px;  margin-left: 10px;font-size: 1.4em; color: #208E0A;'> " + Convert.ToString(ds_msg.Tables[0].Rows[j]["Message"]) +
                    //                "<div></td>" +
                    //                "</tr>" +
                    //                "</table>");
                    //            }
                    //        }
                    //    }
                    //}

                    //if (Type.Trim().ToLower() == "order")
                    //     {
                    //         string sqlfile = "exec GetPreproductionfile '" + OrderNumer + "'";
                    //         DataSet preproductionfile = objAPI.GetData(sqlfile, Keydata);
                    //         if (preproductionfile != null && preproductionfile.Tables[0].Rows.Count > 0)
                    //         {
                    //             for (int j = 0; j < preproductionfile.Tables[0].Rows.Count; j++)
                    //             {
                    //                 htmlmsg.Append(" <table class='datatbl' style='min-width: 500px;  ' width='100%' >" +
                    //                                    " <tr class='brd-btm' style='line-height: 15px'>" +
                    //                                    "<td style='padding:0;' ><table width='100%'><tr> " +
                    //                                    " <td class='caption' style='width: 95px;'>" +
                    //                                      " <span class='textdataleft'>" + createdon + "</span>" +
                    //                                    " </td>" +
                    //                                    "<td  style='width: 150px;'>" +
                    //                                    "<span class='textdataleft'>" + Convert.ToString(preproductionfile.Tables[0].Rows[j]["CreatedDate"])  + "</span>" +
                    //                                    "</td>" +
                    //                                     "<td class='caption'  style='width:85px;'>" +
                    //                                    "<span class='textdataleft'>  </span>" +
                    //                                    "</td>" +
                    //                                    "<td style='width: 80px;'>" +
                    //                                    "<span class='textdataleft'></span>" +
                    //                                    "</td>" +
                    //                                    "<td class='caption' style='width:85px;' > " +
                    //                                    "<span class='textdataleft'>" + FromUser + "</span>" +
                    //                                    "</td>" +
                    //                                    "<td   >" +
                    //                                    "<span class='textdataleft'>" + Convert.ToString(preproductionfile.Tables[0].Rows[j]["FromUser"]) + "</span>" +
                    //                                    "</td>" +
                    //                                    "</tr></table></td>" +
                    //                                    "</tr>" +
                    //                                    "<tr>" +
                    //                                     "<td style='padding:0;'><table width='100%'><tr> " +
                    //                                    "<td class='caption' style='width:75px;' >" +
                    //                                    "<span class='textdataleft'>" + Subject + "</span>" +
                    //                                    "</td>" +
                    //                                    "<td>" +
                    //                                    "<span class='textdataleft'>" + Convert.ToString(preproductionfile.Tables[0].Rows[j]["Subject"]) + "</span>" +
                    //                                    "</td>" +
                    //                                    "</tr>" +
                    //                                    "</table></td></tr><tr>  <table> <tr  style='border: thin dotted rgb(51, 51, 51); padding: 5px;  margin-left: 10px;' >" +

                    //                                       "<td class='caption'  style='width:85px;'>" +
                    //                                    "<span class='textdataleft'> File name </span>" +
                    //                                    "</td>" +
                    //                                    "<td style='  padding-right: 5px; width: auto;'>" +
                    //                                    "<span class='textdataleft'>" + Convert.ToString(preproductionfile.Tables[0].Rows[j]["FileName"]) + "</span>" +
                    //                                    "</td>" +

                    //                                    "<td style='padding:0;'> " + "<a id=" + j + " href='" + liveUrl + "/shop/visualizer/i8ans.aspx?PreProductionFile=donloade&&fileUrl=" + Convert.ToString(preproductionfile.Tables[0].Rows[j]["FilePath"]) + " &&name=" + Convert.ToString(preproductionfile.Tables[0].Rows[j]["FileName"]) + "'> <img style='width: 13px;' src='../images/download-16.png?v=1.1' title='Download' />  </a>" +
                    //                                    "</td> </tr></table>" +
                    //                                    "</tr>" +
                    //                                    "</table>");
                    //             }
                    //         }
                    //         //
                    //     }

                    divmsg.InnerHtml = htmlmsg.ToString();
                }
            }
        }

    }

    public string getDelAddDetails(DataSet objdeladdress)
    {
        try
        {
            string DelAddContact = "";
            if (objdeladdress.Tables[0].Rows.Count > 0)
            {
                bool IsUSACompany = false;
                string countryId = Convert.ToString(objdeladdress.Tables[0].Rows[0]["CountryId"]);
                string sql_country = "select * from admCountries Where CountryId = " + countryId + "  and isnull(IsDeleted ,0)= 0  ";
                System.Data.DataSet objcountry = objAPI.GetData(sql_country, Keydata);
                string Initials = string.Empty;
                string StateName = string.Empty;
                string StateId = Convert.ToString(objdeladdress.Tables[0].Rows[0]["StateId"]);
                string sql_state = "select * from admStates Where StateId = " + StateId + "  and isnull(IsDeleted ,0)= 0  ";
                System.Data.DataSet objstate = objAPI.GetData(sql_state, Keydata);
                if (objstate != null)
                {
                    if (objstate.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToString(objstate.Tables[0].Rows[0]["Name"]) != "")
                        {
                            StateName = Convert.ToString(objstate.Tables[0].Rows[0]["Name"]);
                        }
                    }
                }
                if (objcountry != null && objcountry.Tables[0].Rows.Count > 0)
                {
                    Initials = Convert.ToString(objcountry.Tables[0].Rows[0]["Initial"]).Trim();
                    if (Initials.ToUpper().Trim() == "US")
                    {
                        IsUSACompany = true;
                    }
                }

                //Address name...
                if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["AddressName"]) != "")
                {
                    DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["AddressName"]);
                }
                if (DelAddContact.Trim() != "")
                {
                    DelAddContact = DelAddContact + "<br/>";
                }

                //Contact name...
                if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["ContactName"]) != "")
                {
                    DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["ContactName"]) + "<br/>";
                }

                //for USA( (3)[Street no] (4)[Street name] ) and for other ( (4)[Street name] (3)[Street no] )
                if (IsUSACompany)
                {
                    if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetNo"]) != "")
                    {
                        DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetNo"]);
                        if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetName"]) != "")
                        {
                            DelAddContact = DelAddContact + " " + Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetName"]) + "<br/>";
                        }
                        else
                        {
                            DelAddContact = DelAddContact + "<br/>";
                        }
                    }
                    else
                    {
                        if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetName"]) != "")
                        {
                            DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetName"]) + "<br/>";
                        }
                    }
                }
                else
                {
                    if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetName"]) != "")
                    {
                        DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetName"]);
                        if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetNo"]) != "")
                        {
                            DelAddContact = DelAddContact + " " + Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetNo"]) + "<br/>";
                        }
                        else
                        {
                            DelAddContact = DelAddContact + "<br/>";
                        }
                    }
                    else
                    {
                        if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetNo"]) != "")
                        {
                            DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetNo"]) + "<br/>";
                        }
                    }
                }

                //[Address 1] [Address 2]
                if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetAddress1"]) != "")
                {
                    DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetAddress1"]);
                    if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetAddress2"]) != "")
                    {
                        DelAddContact = DelAddContact + ", " + Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetAddress2"]) + "<br/>";
                    }
                    else
                    {
                        DelAddContact = DelAddContact + "<br/>";
                    }
                }
                else
                {
                    if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetAddress2"]) != "")
                    {
                        DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["StreetAddress2"]) + "<br/>";
                    }
                }

                //for usa(city state country zip) and other (zip city <br> country)
                if (IsUSACompany == true)
                {
                    int count = 0;
                    if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["City"]) != "")
                    {
                        DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["City"]);
                        count++;
                    }
                    if (StateName != "")
                    {
                        DelAddContact = DelAddContact + " " + StateName;
                        count++;
                    }
                    if (Convert.ToString(objcountry.Tables[0].Rows[0]["Name"]) != "")
                    {
                        DelAddContact = DelAddContact + " " + Convert.ToString(objcountry.Tables[0].Rows[0]["Name"]);
                        count++;
                    }
                    if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["PostalCode"]) != "")
                    {
                        DelAddContact = DelAddContact + " " + Convert.ToString(objdeladdress.Tables[0].Rows[0]["PostalCode"]);
                        count++;
                    }
                    if (count > 0)
                    {
                        DelAddContact = DelAddContact + "<br/>";
                    }
                }
                else
                {
                    int count = 0;
                    if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["PostalCode"]) != "")
                    {
                        DelAddContact = DelAddContact + Convert.ToString(objdeladdress.Tables[0].Rows[0]["PostalCode"]);
                        count++;
                    }
                    if (Convert.ToString(objdeladdress.Tables[0].Rows[0]["City"]) != "")
                    {
                        DelAddContact = DelAddContact + " " + Convert.ToString(objdeladdress.Tables[0].Rows[0]["City"]);
                        count++;
                    }
                    if (count > 0)
                    {
                        DelAddContact = DelAddContact + "<br/>";
                    }
                    if (Convert.ToString(objcountry.Tables[0].Rows[0]["Name"]) != "")
                    {
                        DelAddContact = DelAddContact + Convert.ToString(objcountry.Tables[0].Rows[0]["Name"]) + "<br/>";
                    }
                }
                if (DelAddContact.Trim().Contains("<br/><br/>"))
                {
                    DelAddContact = DelAddContact.Replace("<br/><br/>", "<br/>");
                }

                //if in last </br> is available then need to remove that tag...
                string Isstrbr = DelAddContact.Trim().Substring(DelAddContact.Trim().Length - 5);
                if (Isstrbr == "<br/>")
                {
                    DelAddContact = DelAddContact.Remove(DelAddContact.Trim().LastIndexOf("<br/>"));
                }
            }
            return DelAddContact;
        }
        catch 
        {
            return "";
        }
    }
    #endregion

    #region Xml GenerateButton Click Event
    protected void lnkclickhere_Click(object sender, EventArgs e)
    {

    }
    #endregion

</script>

</html>
