﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Security.Principal;
using System.Data;
using System.Xml;
using System.Runtime.InteropServices;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using sparrowFeApp.core;
using Newtonsoft.Json;

namespace sparrowFeApp.shop.orders
{
    public partial class WUCECCUploaddata : System.Web.UI.UserControl
    {

        ecgdService.gdSoapClient objAPI = new ecgdService.gdSoapClient();
        const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
        core.BasePage objbase = new core.BasePage();
        ecgdService.gdSoapClient objgd = new ecgdService.gdSoapClient();
        ec09Service.ECAPISoapClient ec09Service = new ec09Service.ECAPISoapClient();
        #region properties
        public string SessionId
        {
            set { ViewState["SessionId"] = Convert.ToString(value); }
            get { return Convert.ToString(ViewState["SessionId"]); }
        }
        public string LanguageCode
        {
            set
            {
                ViewState["LanguageCode"] = Convert.ToString(value);
            }
            get { return Convert.ToString(ViewState["LanguageCode"]); }
        }
        //public long UserId
        //{
        //    set
        //    {
        //        _UserID = Convert.ToInt32(value);
        //    }
        //    get
        //    {
        //        return _UserID;
        //    }
        //}
        public long UserId
        {
            set { ViewState["UserId"] = Convert.ToString(value); }
            get { return Convert.ToInt64(ViewState["UserId"]); }
        }
        public int LanguageId
        {
            set { ViewState["LanguageId"] = Convert.ToString(value); }
            get { return Convert.ToInt32(ViewState["LanguageId"]); }
        }
        public string PCBName
        {
            set { txtPcbName.Text = Convert.ToString(value); }
            get
            {
                if (_FilteredPcbName == false)
                { return txtPcbName.Text.Trim(); }
                return CheckValidtionforSpec(txtPcbName.Text.Trim());
            }
        }
        public string PurchaseRef
        {
            set { txtPurchasereference.Text = Convert.ToString(value); }
            get { return CheckValidtionforSpec(txtPurchasereference.Text.Trim()); }
        }
        public string ArticalRef
        {
            set { txtArticlereference.Text = Convert.ToString(value); }
            get { return CheckValidtionforSpec(txtArticlereference.Text.Trim()); }
        }
        public string ProjectRef
        {
            set { txtProjReffirst.Text = Convert.ToString(value); }
            get { return CheckValidtionforSpec(txtProjReffirst.Text.Trim()); }
        }
        public long Company_id
        {
            set { ViewState["Company_id"] = Convert.ToString(value); }
            get { return Convert.ToInt64(ViewState["Company_id"]); }
        }
        //public long Company_id
        //{
        //    set { CompID = Convert.ToString(value); }
        //    get { return Convert.ToInt32(CompID); }            
        //}
        //public long eccUserId
        //{
        //    set { _EccUserId = Convert.ToInt32(value); }
        //    get { return _EccUserId; }
        //}
        public long eccUserId
        {
            set { ViewState["eccUserId"] = Convert.ToString(value); }
            get { return Convert.ToInt64(ViewState["eccUserId"]); }
        }
        public bool IsStencilFixed
        {
            set { chkStencilFixed.Checked = Convert.ToBoolean(value); }
            get { return Convert.ToBoolean(chkStencilFixed.Checked); }
        }
        string fileuploadstatus = "";
        public string Status
        {
            set
            {
                ViewState["Status"] = Convert.ToString(value);
            }
            get { return Convert.ToString(ViewState["Status"]); }
        }
        public string Deliverydate
        {
            set { datepicker.Value = Convert.ToString(value); }
            get { return (datepicker.Value).ToString(); }

        }
        public long Deliveryterm
        {
            set { ViewState["Deliveryterm"] = Convert.ToInt32(value); }
            get { return Convert.ToInt32(ViewState["Deliveryterm"]); }

        }
        public string dataStatus
        {
            set { ViewState["dataStatus"] = Convert.ToString(value); }
            get { return Convert.ToString(ViewState["dataStatus"]); }
        }
        public bool IsVisibleLbtnAdd01
        {
            set
            {
                if (Convert.ToBoolean(value) == false)
                {
                    LbtnAdd01.Visible = false;
                }
            }
        }

        public bool IsFileUploaded = false;
        public string Type
        {
            set
            {
                ViewState["Type"] = Convert.ToString(value);
                //if (Convert.ToString(ViewState["Type"]).ToLower() == "repeatOrder".ToLower())
                //{
                //    controldiv.Visible = false;
                //    pleasewaitdiv.Visible = true;
                //}
                //else 
                if (Convert.ToString(ViewState["Type"]).ToLower() == "modifyINQ".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "releasePPA".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "releaseConfirmPPA".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "modifyOrder".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "repeatOrder".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "modifyBasket".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "newBasket".ToLower())
                {
                    divuploadfile.Attributes["style"] = "display:none";
                }
                if (Convert.ToString(ViewState["Type"]).ToLower() == "modifyOrder".ToLower())
                {
                    divdddatetext.Attributes["style"] = "display:inline-table";
                    divPricing.Attributes["style"] = "display:inline-table;";

                    divReclDelivry.Attributes["style"] = "display:inline-table";
                    divBoardLock.Attributes["style"] = "display:inline-table";
                    divInvoicesAddress.Attributes["style"] = "display:inline-table";
                    DivFullcheck.Attributes["style"] = "display:inline-table";
                    divPackWithPaper.Attributes["style"] = "display:inline-table";
                    divPlatedSlots.Attributes["style"] = "display:inline-table";
                    divPreprductioncheck.Attributes["style"] = "display:inline-table";
                    divHandlingComp.Attributes["style"] = "display:inline-table";
                }
                if (Convert.ToString(ViewState["Type"]).ToLower() == "PPAtoSI".ToLower())
                {
                    divuploadfile.Attributes["style"] = "display:inline-table";
                    divpcbname.Attributes["style"] = "display:none";
                    divpurreferance.Attributes["style"] = "display:none";
                    divartireferance.Attributes["style"] = "display:none";
                    divprojreffirst.Attributes["style"] = "display:none";
                    DivPPAremarks.Attributes["style"] = "display:inline-table";
                    divUploadRemarkFile.Visible = true;
                }

            }
            get { return Convert.ToString(ViewState["Type"]); }
        }
        public string EntityNo
        {
            set { ViewState["EntityNo"] = Convert.ToString(value); }
            get { return Convert.ToString(ViewState["EntityNo"]); }
        }
        public string action
        {
            set
            {
                ViewState["action"] = Convert.ToString(value);
                if (Convert.ToString(ViewState["action"]) == "basketitem_editadmindetail")
                {


                }
                if (Convert.ToString(ViewState["action"]) == "uploaddata")
                {
                    if (Type.ToLower() == "modifyINQ".ToLower())
                    {

                        divuploadfile.Attributes["style"] = "display:none";
                    }
                    else
                    {
                        divuploadfile.Attributes["style"] = "display:inline-table";
                    }
                }
                if (Convert.ToString(ViewState["action"]) == "basketitem_uploadfile")
                {


                }

            }
            get { return Convert.ToString(ViewState["action"]); }
        }
        public bool IsfileOnlyUpload
        {
            set
            {
                ViewState["IsfileOnlyUpload"] = Convert.ToString(value);
                if (Convert.ToBoolean(ViewState["IsfileOnlyUpload"]))
                {

                }
            }
            get
            {
                return Convert.ToBoolean(ViewState["IsfileOnlyUpload"]);
            }
        }
        public bool IsfromEdminDetail
        {
            set
            {
                ViewState["IsfromEdminDetail"] = Convert.ToString(value);
                if (Convert.ToBoolean(ViewState["IsfromEdminDetail"]))
                {
                }
            }
            get
            {
                return Convert.ToBoolean(ViewState["IsfromEdminDetail"]);
            }
        }
        public bool IsDialogHead
        {
            set
            {
                //if (value == false)
                //{ divHeader.Visible = false; }
                //else { divHeader.Visible = true; }
            }
        }
        public string AnalysisXML
        {
            set { ViewState["AnalysisXML"] = Convert.ToString(value); }
            get { return Convert.ToString(ViewState["AnalysisXML"]); }
        }
        public string PerentBasketId
        {
            set
            {
                ViewState["PerentBasketId"] = Convert.ToString(value);
            }
            get
            {
                return Convert.ToString(ViewState["PerentBasketId"]);
            }
        }
        public string serviceName
        {
            set
            {
                ViewState["serviceName"] = Convert.ToString(value);
            }
            get
            {
                return Convert.ToString(ViewState["serviceName"]);
            }
        }
        //public string DeliveryTerm
        //{
        //    set { ddldeliveryterm.SelectedValue = value; }
        //    get { return ddldeliveryterm.SelectedValue; }
        //}
        //public string Qty
        //{
        //    set { txtquentity.Text = value; }
        //    get { return txtquentity.Text; }
        //}
        public string IsException
        {
            set { ViewState["IsException"] = value; }
            get { return Convert.ToString(ViewState["IsException"]); }
        }
        public long ExceptionId
        {
            set
            {
                ViewState["ExceptionId"] = value;
                //attrfileupload.Visible = true;
            }
            get { return Convert.ToInt64(ViewState["ExceptionId"]); }
        }
        public string XML
        {
            set { ViewState["XML"] = value; }
            get { return Convert.ToString(ViewState["XML"]); }
        }
        public long statusid;
        public long DeliveryAddressId
        {
            set { ViewState["DeliveryAddressId"] = value; }
            get { return Convert.ToInt64(ViewState["DeliveryAddressId"]); }
        }
        public long InvoiceAddressId
        {
            set { ViewState["InvoiceAddressId"] = value; }
            get { return Convert.ToInt64(ViewState["InvoiceAddressId"]); }
        }
        public long HCompnyID
        {
            set { ViewState["HCompnyID"] = value; }
            get { return Convert.ToInt64(ViewState["HCompnyID"]); }
        }
        public long RCompnyID
        {
            set { ViewState["RCompnyID"] = value; }
            get { return Convert.ToInt64(ViewState["RCompnyID"]); }
        }
        public bool ddlDeliveryAddress
        {
            set { ViewState["ddlDeliveryAddress"] = value; }
            get { return Convert.ToBoolean(ViewState["ddlDeliveryAddress"]); }
        }

        public long OrderDeliveryAddressId
        {
            set { ViewState["OrderDeliveryAddressId"] = value; }
            get
            {
                if (ViewState["OrderDeliveryAddressId"] == null)
                {
                    return 0;
                }
                return Convert.ToInt32(ViewState["OrderDeliveryAddressId"]);
            }
        }

        public long OrderInvoiceAddressId
        {
            set { ViewState["OrderInvoiceAddressId"] = value; }
            get
            {
                if (ViewState["OrderInvoiceAddressId"] == null)
                {
                    return 0;
                }
                return Convert.ToInt32(ViewState["OrderInvoiceAddressId"]);
            }
        }

        public long parentarticalId
        {
            set { ViewState["parentarticalId"] = value; }
            get { return Convert.ToInt64(ViewState["parentarticalId"]); }
        }
        public bool IsStencilOnly
        {
            set { ViewState["IsStencilOnly"] = Convert.ToBoolean(value); }
            get { return Convert.ToBoolean(ViewState["IsStencilOnly"]); }
        }

        private bool _FilteredPcbName = true;
        public bool FilteredPcbName
        {
            set
            {
                _FilteredPcbName = Convert.ToBoolean(value);
                if (_FilteredPcbName == false)
                {

                }
            }
            get
            {
                return _FilteredPcbName;
            }
        }
        public string InqOrderNumber
        {
            set { ViewState["InqOrderNumber"] = Convert.ToString(value); }
            get { return Convert.ToString(ViewState["InqOrderNumber"]); }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (hidendeliverydate.Value != "" && hidendeliverydate.Value != null)
                {
                    datepicker.Value = hidendeliverydate.Value;
                }
            }
            objbase.ResourceId = 66;
            if (Deliveryterm < 10)
            {
                datepicker.Attributes.Add("disabled", "disabled");
            }
            else
            {
                datepicker.Attributes.Add("enabled", "enabled");
                ScriptManager.RegisterStartupScript(this.Page, GetType(), "datepickerenable", "EnableDatepicker();", true);
            }

            if (!IsPostBack)
            {

                chkStencilfixvisibility();

                if (Convert.ToString(ViewState["Type"]).ToLower() == "newOrder".ToLower())
                {
                    spnuploaddata.Visible = true;
                    divUploadRemarkFile.Visible = false; //Added by Milan..........
                    // DivChecktermscondition.Attributes["style"] = "display:inline-block";
                    if (dataStatus != "" && dataStatus != "4" && dataStatus != "5")
                    {
                        divuploadfile.Attributes["style"] = "display:none";
                        spnuploaddata.Visible = false;
                        divUploadRemarkFile.Visible = true;
                    }
                }
                else
                {
                    spnuploaddata.Visible = false;
                    //  DivChecktermscondition.Attributes["style"] = "display:none";
                }

                diverrortd.Attributes["style"] = "display:none";

                SetLanguageData();
                if (ddlDeliveryAddress)
                {
                    divdeladdress.Visible = true;
                    FillDeliveryInvoiceAddress();
                }

                if (Convert.ToString(Type) == "inquiry")
                {
                    divinqvariants.Visible = true;
                    Divdeliveryterms.Visible = false;
                    divUploadRemarkFile.Visible = false;

                    DDLDelTerm01 = FillDeliveryTerm(DDLDelTerm01);
                    DDLDelTerm02 = FillDeliveryTerm(DDLDelTerm02);
                    DDLDelTerm03 = FillDeliveryTerm(DDLDelTerm03);
                    DDLDelTerm04 = FillDeliveryTerm(DDLDelTerm04);
                    DDLDelTerm05 = FillDeliveryTerm(DDLDelTerm05);
                    DDLDelTerm06 = FillDeliveryTerm(DDLDelTerm06);
                    DDLDelTerm07 = FillDeliveryTerm(DDLDelTerm07);
                    trVar02.Visible = false;
                    trVar03.Visible = false;
                    trVar04.Visible = false;
                    trVar05.Visible = false;
                    trVar06.Visible = false;
                    trVar07.Visible = false;

                    string sqlGetXmlString = string.Format("SELECT XMLString from i8response with(nolock) where Sessionid like '%{0}%'", SessionId);
                    DataTable dtGetXmlString = objgd.GetData(sqlGetXmlString, Keydata).Tables[0];
                    string xmlString = "";
                    if (dtGetXmlString != null && dtGetXmlString.Rows.Count > 0)
                    {
                        xmlString = dtGetXmlString.Rows[0]["XMLString"].ToString();
                    }
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xmlString);
                    System.Xml.XmlNode nodeCustomerProperties3 = doc.SelectSingleNode("OrderDetail/CustomerProperties3");
                    System.Xml.XmlNode nodeOrderProperties3 = doc.SelectSingleNode("OrderDetail/OrderProperties3");
                    System.Xml.XmlNode nodeBoardProperties3 = doc.SelectSingleNode("OrderDetail/BoardProperties3");
                    string pcbQty = nodeOrderProperties3.SelectSingleNode("BoardQty").InnerText;
                    string StencilTopQty = "0";
                    string StencilBotQty = "0";
                    if (pcbQty == "0")
                    {
                        StencilTopQty = nodeOrderProperties3.SelectSingleNode("StencilTopQty").InnerText;
                        if (StencilTopQty != "0")
                        {
                            pcbQty = StencilTopQty;
                        }
                        else
                        {
                            StencilBotQty = nodeOrderProperties3.SelectSingleNode("StencilBotQty").InnerText;
                            pcbQty = StencilBotQty;
                        }
                    }
                    string boardDeliveryTerm = nodeOrderProperties3.SelectSingleNode("BoardDeliveryTerm").InnerText;

                    try
                    {
                        string sqlGetdelTermId = string.Format("select ParameterValueId,ParaOption, cast(VisParaOption as int) as VisParaOption  from pcmParameterValueLangs  where ParameterValueId in(select ParameterValueId from pcmParameterValues where ParameterId=219) and LanguageId='493' and VisParaOption  = '{0}'", boardDeliveryTerm);
                        DataTable dtGetdelTermId = objgd.GetData(sqlGetdelTermId, Keydata).Tables[0];
                        if (dtGetdelTermId != null && dtGetdelTermId.Rows.Count > 0)
                        {
                            DDLDelTerm01.SelectedValue = dtGetdelTermId.Rows[0]["ParameterValueId"].ToString();
                            TxtQty01.Text = pcbQty;
                        }
                    }
                    catch (Exception)
                    { }
                }
                else if (Convert.ToString(Type) == "modifyINQ" && action == "uploaddata")
                {
                    try
                    {
                        divinqvariants.Visible = true;
                        divUploadRemarkFile.Visible = false;
                        trVar02.Visible = false; trVar03.Visible = false; trVar04.Visible = false; trVar05.Visible = false; trVar06.Visible = false; trVar07.Visible = false;
                        DDLDelTerm01 = FillDeliveryTerm(DDLDelTerm01);
                        DDLDelTerm02 = FillDeliveryTerm(DDLDelTerm02);
                        DDLDelTerm03 = FillDeliveryTerm(DDLDelTerm03);
                        DDLDelTerm04 = FillDeliveryTerm(DDLDelTerm04);
                        DDLDelTerm05 = FillDeliveryTerm(DDLDelTerm05);
                        DDLDelTerm06 = FillDeliveryTerm(DDLDelTerm06);
                        DDLDelTerm07 = FillDeliveryTerm(DDLDelTerm07);
                        string sqlGetVeriantDetail = string.Format("exec osp_getInqVariantDetail '{0}'", PerentBasketId);
                        DataSet dsGetVeriantDetail = objgd.GetData(sqlGetVeriantDetail, Keydata);
                        if (dsGetVeriantDetail != null && dsGetVeriantDetail.Tables[0].Rows.Count > 0)
                        {
                            int variantCount = Convert.ToInt32(dsGetVeriantDetail.Tables[1].Rows[0][0]);
                            for (int i = 1; i <= variantCount; i++)
                            {
                                switch (i)
                                {
                                    case 1:
                                        //trVar01.Visible = true;
                                        //DDLDelTerm01 = FillDeliveryTerm(DDLDelTerm01);
                                        TxtQty01.Text = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["PCBQty"]);
                                        DDLDelTerm01.SelectedValue = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["DeliveryTermId"]);
                                        LbtnAdd01.AccessKey = "1";
                                        continue;
                                    case 2:
                                        trVar02.Visible = true;
                                        //DDLDelTerm02 = FillDeliveryTerm(DDLDelTerm02);
                                        TxtQty02.Text = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["PCBQty"]);
                                        DDLDelTerm02.SelectedValue = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["DeliveryTermId"]); ;
                                        LbtnAdd01.AccessKey = "2";
                                        continue;
                                    case 3:
                                        trVar03.Visible = true;
                                        //DDLDelTerm03 = FillDeliveryTerm(DDLDelTerm03);
                                        TxtQty03.Text = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["PCBQty"]);
                                        DDLDelTerm03.SelectedValue = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["DeliveryTermId"]); ;
                                        LbtnAdd01.AccessKey = "3";
                                        continue;
                                    case 4:
                                        trVar04.Visible = true;
                                        //DDLDelTerm04 = FillDeliveryTerm(DDLDelTerm04);
                                        TxtQty04.Text = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["PCBQty"]);
                                        DDLDelTerm04.SelectedValue = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["DeliveryTermId"]); ;
                                        LbtnAdd01.AccessKey = "4";
                                        continue;
                                    case 5:
                                        trVar05.Visible = true;
                                        //DDLDelTerm05 = FillDeliveryTerm(DDLDelTerm05);
                                        TxtQty05.Text = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["PCBQty"]);
                                        DDLDelTerm05.SelectedValue = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["DeliveryTermId"]); ;
                                        LbtnAdd01.AccessKey = "5";
                                        continue;
                                    case 6:
                                        trVar06.Visible = true;
                                        //DDLDelTerm06 = FillDeliveryTerm(DDLDelTerm06);
                                        TxtQty06.Text = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["PCBQty"]);
                                        DDLDelTerm06.SelectedValue = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["DeliveryTermId"]); ;
                                        LbtnAdd01.AccessKey = "6";
                                        continue;
                                    case 7:
                                        trVar07.Visible = true;
                                        //DDLDelTerm07 = FillDeliveryTerm(DDLDelTerm07);
                                        TxtQty07.Text = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["PCBQty"]);
                                        DDLDelTerm07.SelectedValue = Convert.ToString(dsGetVeriantDetail.Tables[0].Rows[i - 1]["DeliveryTermId"]); ;
                                        LbtnAdd01.AccessKey = "7";
                                        continue;
                                }
                            }
                        }

                        string sqlGetXmlString = string.Format("SELECT XMLString from i8response with(nolock) where Sessionid like '%{0}%'", SessionId);
                        DataTable dtGetXmlString = objgd.GetData(sqlGetXmlString, Keydata).Tables[0];
                        string xmlString = "";
                        if (dtGetXmlString != null && dtGetXmlString.Rows.Count > 0)
                        {
                            xmlString = dtGetXmlString.Rows[0]["XMLString"].ToString();
                        }

                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(xmlString.Trim());
                        System.Xml.XmlNode nodeCustomerProperties3 = doc.SelectSingleNode("OrderDetail/CustomerProperties3");
                        System.Xml.XmlNode nodeOrderProperties3 = doc.SelectSingleNode("OrderDetail/OrderProperties3");
                        System.Xml.XmlNode nodeBoardProperties3 = doc.SelectSingleNode("OrderDetail/BoardProperties3");
                        System.Xml.XmlNode ProductionProperties3 = doc.SelectSingleNode("OrderDetail/ProductionProperties3");
                        string pcbQty = nodeOrderProperties3.SelectSingleNode("BoardQty").InnerText;
                        string StencilTopQty = "0";
                        string StencilBotQty = "0";
                        if (pcbQty == "0")
                        {
                            StencilTopQty = nodeOrderProperties3.SelectSingleNode("StencilTopQty").InnerText;
                            if (StencilTopQty != "0")
                            {
                                pcbQty = StencilTopQty;
                            }
                            else
                            {
                                StencilBotQty = nodeOrderProperties3.SelectSingleNode("StencilBotQty").InnerText;
                                pcbQty = StencilBotQty;
                            }
                        }
                        string boardDeliveryTerm = nodeOrderProperties3.SelectSingleNode("BoardDeliveryTerm").InnerText;

                        try
                        {
                            string sqlGetdelTermId = string.Format("select ParameterValueId,ParaOption, cast(VisParaOption as int) as VisParaOption  from pcmParameterValueLangs  where ParameterValueId in(select ParameterValueId from pcmParameterValues where ParameterId=219) and LanguageId='493' and VisParaOption  = '{0}'", boardDeliveryTerm);
                            DataTable dtGetdelTermId = objgd.GetData(sqlGetdelTermId, Keydata).Tables[0];
                            if (dtGetdelTermId != null && dtGetdelTermId.Rows.Count > 0)
                            {
                                DDLDelTerm01.SelectedValue = dtGetdelTermId.Rows[0]["ParameterValueId"].ToString();
                                TxtQty01.Text = pcbQty;
                            }
                        }
                        catch (Exception)
                        { }

                        bool IsPlatedSlots = false;
                        if (ProductionProperties3 != null)
                        {
                            string platedslots = Convert.ToString(ProductionProperties3.SelectSingleNode("PlatedSlots").InnerText);
                            divPlatedSlots.Attributes["style"] = "display:inline-table";
                            bool.TryParse(platedslots, out IsPlatedSlots);
                            checkplatedslots.Checked = IsPlatedSlots;
                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    divinqvariants.Visible = false;
                }

                if (Convert.ToString(ViewState["Type"]).ToLower() == "modifyOrder".ToLower())
                {
                    string sqlGetXmlString = string.Format("SELECT XMLString from i8response with(nolock) where Sessionid like '%{0}%'", SessionId);
                    DataTable dtGetXmlString = objgd.GetData(sqlGetXmlString, Keydata).Tables[0];
                    string xmlString = "";
                    if (dtGetXmlString != null && dtGetXmlString.Rows.Count > 0)
                    {
                        xmlString = dtGetXmlString.Rows[0]["XMLString"].ToString();
                    }
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xmlString);
                    System.Xml.XmlNode nodeCustomerProperties3 = doc.SelectSingleNode("OrderDetail/CustomerProperties3");
                    System.Xml.XmlNode nodeOrderProperties3 = doc.SelectSingleNode("OrderDetail/OrderProperties3");
                    System.Xml.XmlNode nodeBoardProperties3 = doc.SelectSingleNode("OrderDetail/BoardProperties3");
                    string RecalcDate = nodeOrderProperties3.SelectSingleNode("BoardDeliveryDate").InnerText;
                    string RecalcDeliveryTerm = nodeOrderProperties3.SelectSingleNode("BoardDeliveryTerm").InnerText;
                    string markup = nodeOrderProperties3.SelectSingleNode("BoardPID").InnerText;
                    string assemblymarkup = nodeOrderProperties3.SelectSingleNode("AssemblyPID").InnerText;

                    string SqlGetDeliveryDate = string.Format("SELECT  dbo.fn_globaldateformat(DeliveryDate,493) as DeliveryDate,IsFC,IsPlatedSlots,IsPreproduction,RootCompanyId,HandlingCompanyId,PackWithPaper,isnull(OrderStatusId ,0) as OrderStatusId from genSearchOrders with(nolock) where OrderNumber = '{0}';exec getDeliveryDate '{0}','{1}'", PerentBasketId, RecalcDeliveryTerm);
                    DataSet dsGetDeliveryDate = objgd.GetData(SqlGetDeliveryDate, Keydata);

                    DataTable dtGetMarkup = objgd.GetData("declare @val decimal(18,5);exec osp_GetDecodeMarkup '" + markup + "',@val out;select @val as Result", Keydata).Tables[0];

                    if (assemblymarkup.ToString().ToUpper() != "AssemblyPID".ToUpper() && !PerentBasketId.Contains("-ST") && !PerentBasketId.Contains("-SB") && !PerentBasketId.Contains("-OK"))
                    {
                        try
                        {
                            DataTable dtGetMarkupforAssembly = objgd.GetData("declare @val decimal(18,5);exec osp_GetDecodeMarkup '" + assemblymarkup + "',@val out;select @val as Result", Keydata).Tables[0];
                            txtAssemblyPricemarkup.Text = (Convert.ToDecimal(dtGetMarkupforAssembly.Rows[0]["Result"]) * 100).ToString();
                            lblAssemblyPricemarkupold.Text = (Convert.ToDecimal(dtGetMarkupforAssembly.Rows[0]["Result"]) * 100).ToString();
                        }
                        catch
                        {
                            txtAssemblyPricemarkup.Text = "0.00";
                            lblAssemblyPricemarkupold.Text = "0.00";
                        }
                    }
                    else
                    {
                        txtAssemblyPricemarkup.Text = "0.00";
                        lblAssemblyPricemarkupold.Text = "0.00";
                    }
                    txtPercentage.Text = (Convert.ToDecimal(dtGetMarkup.Rows[0]["Result"]) * 100).ToString();
                    lblPercetange.Text = (Convert.ToDecimal(dtGetMarkup.Rows[0]["Result"]) * 100).ToString();
                    Text1.Value = Convert.ToString(dsGetDeliveryDate.Tables[0].Rows[0]["DeliveryDate"]);

                    lblrecalcdeliverydatereadonl.Text = Convert.ToString(dsGetDeliveryDate.Tables[1].Rows[0]["deliverydate"]);
                    calcPriceByDeliveryAddress(doc);

                    // new task for new visualizer
                    bool Isbrdlock = false; string brdlock = "";
                    bool IsFC = false, IsPlatedSlots = false, IsPreproduction = false, PackWithPaper = false;
                    long RootcompId = 0, handlingCompId = 0;
                    if (PerentBasketId.Trim().ToLower().Contains("-sb"))
                    {
                        brdlock = Convert.ToString(nodeOrderProperties3.SelectSingleNode("StencilBotLocked").InnerText);
                    }
                    else if (PerentBasketId.Trim().ToLower().Contains("-st"))
                    {
                        brdlock = Convert.ToString(nodeOrderProperties3.SelectSingleNode("StencilTopLocked").InnerText);
                    }
                    else
                    {
                        brdlock = Convert.ToString(nodeOrderProperties3.SelectSingleNode("BoardLocked").InnerText);
                    }
                    bool.TryParse(brdlock, out Isbrdlock);
                    checkBrdlock.Checked = Isbrdlock;
                    long OrderStatusId = 0;
                    bool.TryParse(Convert.ToString(dsGetDeliveryDate.Tables[0].Rows[0]["IsFC"]), out IsFC);
                    bool.TryParse(Convert.ToString(dsGetDeliveryDate.Tables[0].Rows[0]["IsPlatedSlots"]), out IsPlatedSlots);
                    bool.TryParse(Convert.ToString(dsGetDeliveryDate.Tables[0].Rows[0]["IsPreproduction"]), out IsPreproduction);
                    bool.TryParse(Convert.ToString(dsGetDeliveryDate.Tables[0].Rows[0]["PackWithPaper"]), out PackWithPaper);
                    long.TryParse(Convert.ToString(dsGetDeliveryDate.Tables[0].Rows[0]["OrderStatusId"]), out OrderStatusId);
                    checkFullcheck.Checked = IsFC;
                    checkplatedslots.Checked = IsPlatedSlots;

                    //string StatusCode = EC09.Data.admCode.GetCode(OrderStatusId);
                    string StatusCode = "";
                    checkPreproduct.Checked = IsPreproduction;
                    if (StatusCode.ToUpper().Trim() == "PREPRODUCTION")
                    {
                        checkPreproduct.Enabled = false;
                    }
                    ChkPackWithPaper.Checked = PackWithPaper;
                    long.TryParse(Convert.ToString(dsGetDeliveryDate.Tables[0].Rows[0]["RootCompanyId"]), out RootcompId);
                    long.TryParse(Convert.ToString(dsGetDeliveryDate.Tables[0].Rows[0]["HandlingCompanyId"]), out handlingCompId);

                    string Sql = "exec ECC_GethandlingcompanyBaseonRoot " + RootcompId;
                    DataSet ds_handlingcomp = objgd.GetData(Sql, Keydata);
                    ddlHanlingcomp.DataSource = ds_handlingcomp.Tables[0];
                    ddlHanlingcomp.DataTextField = "Name";
                    ddlHanlingcomp.DataValueField = "ID";
                    ddlHanlingcomp.DataBind();
                    ddlHanlingcomp.SelectedValue = Convert.ToString(handlingCompId);
                }

                hidType.Value = Convert.ToString(ViewState["Type"]).ToLower();
                if (Convert.ToString(ViewState["Type"]).ToLower() == "releasePPA".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "releaseConfirmPPA".ToLower())
                {
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnDisable();", true);
                    processPPAOrder();
                }
            }
        }

        private void calcPriceByDeliveryAddress(XmlDocument doc)
        {
            System.Xml.XmlNode nodeCustomerProperties3 = doc.SelectSingleNode("OrderDetail/CustomerProperties3");
            System.Xml.XmlNode nodeOrderProperties3 = doc.SelectSingleNode("OrderDetail/OrderProperties3");
            System.Xml.XmlNode nodeBoardProperties3 = doc.SelectSingleNode("OrderDetail/BoardProperties3");
            decimal recalUnitpriced = 0, rebrdNetPrice1d, retxtEcoTransport1d, regrossInfo1d;
            decimal assemblyPrice = 0, componentPrice = 0;
            string service = nodeOrderProperties3.SelectSingleNode("ServiceName").InnerText;
            string sqlOrderDetail = string.Format("exec getAndUpdateOrderPricing '{0}'", PerentBasketId);
            DataSet ds_order = objgd.GetData(sqlOrderDetail, Keydata);

            //EC09WebService.OTS objots = new EC09WebService.OTS();
            decimal vat = 0;
            string DeliveryAddressId = Convert.ToString(ds_order.Tables[0].Rows[0]["DeliveryAddressId"]);
            if (DeliveryAddressId != "")
            {
                string VATXml = getVATPer();
                if (decimal.TryParse(VATXml, out vat))
                { }
            }
            StringBuilder modifyOrderTrack = new StringBuilder();
            int assemblyOty = Convert.ToInt32(nodeOrderProperties3.SelectSingleNode("AssemblyQty").InnerText);

            if ((assemblyOty == 0))
            {
                divAssembly.Attributes["style"] = "display:none";
                divComponents.Attributes["style"] = "display:none";
                DidAssemblyPricemarkup.Attributes["style"] = "display:none";
            }
            else
            {
                lblAssembly.Text = Convert.ToString(ds_order.Tables[0].Rows[0]["AssemblyTotalNetPrice"]);
                lblComponent.Text = Convert.ToString(ds_order.Tables[0].Rows[0]["ComponentTotalNetPrice"]);
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("AssemblyTotalNetPrice").InnerText, out assemblyPrice);
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("ComponentTotalNetPrice").InnerText, out componentPrice);
                if (componentPrice < 0)
                {
                    componentPrice = 0;
                }
                txtAssembly.Text = Math.Round(assemblyPrice < 0 ? 0 : assemblyPrice, 3).ToString();
                txtComponents.Text = Math.Round(componentPrice < 0 ? 0 : componentPrice, 3).ToString();
                modifyOrderTrack.Append("_" + txtAssembly.Text + "_" + txtComponents.Text);
                hdnOriginalASemblyPrice.Value = Math.Round(assemblyPrice < 0 ? 0 : assemblyPrice, 3).ToString();
                string sqlEccUserDetail = string.Format("exec Ecc_checkisadminUser {0}", eccUserId);
                DataSet ds_UserDetail = objgd.GetData(sqlEccUserDetail, Keydata);
                if (ds_UserDetail != null && ds_UserDetail.Tables.Count > 0 && ds_UserDetail.Tables[0].Rows.Count > 0)
                {
                    bool isAdminUser = false;
                    bool.TryParse(ds_UserDetail.Tables[0].Rows[0]["isAdmin"].ToString(), out isAdminUser);
                    if (isAdminUser)
                    {
                        txtAssembly.Enabled = true;
                        txtAssemblyPricemarkup.Enabled = true;
                    }
                    else
                    {
                        txtAssembly.Enabled = false;
                        txtAssemblyPricemarkup.Enabled = false;
                    }

                }
                else
                {
                    txtAssembly.Enabled = false;
                    txtAssemblyPricemarkup.Enabled = false;
                }
            }

            if (PerentBasketId.ToLower().Contains("-st") || (service.ToLower() == "Stencilservice".ToLower() && recalUnitpriced == 0))
            {
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("StencilTopUnitPrice").InnerText, out recalUnitpriced);
                txtUnitPrice1.Text = Math.Round(recalUnitpriced, 3).ToString();
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("StencilTopTotalNetPrice").InnerText, out rebrdNetPrice1d);
                brdNetPrice1.Text = Math.Round(rebrdNetPrice1d, 3).ToString();
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("StencilTopTransportCost").InnerText, out retxtEcoTransport1d);
                txtEcoTransport1.Text = Math.Round(retxtEcoTransport1d, 3).ToString();
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("StencilTopTotalOrderPrice").InnerText, out regrossInfo1d);
                vatInfo1.Text = (Math.Round((rebrdNetPrice1d + retxtEcoTransport1d) * vat, 3)).ToString();
                grossInfo1.Text = (Math.Round((rebrdNetPrice1d + retxtEcoTransport1d) * vat, 3) + Math.Round(rebrdNetPrice1d, 3) + Math.Round(retxtEcoTransport1d, 3)).ToString();
            }
            else if (PerentBasketId.ToLower().Contains("-sb") || (service.ToLower() == "Stencilservice".ToLower() && recalUnitpriced == 0))
            {
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("StencilBotUnitPrice").InnerText, out recalUnitpriced);
                txtUnitPrice1.Text = Math.Round(recalUnitpriced, 3).ToString();
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("StencilBotTotalNetPrice").InnerText, out rebrdNetPrice1d);
                brdNetPrice1.Text = Math.Round(rebrdNetPrice1d, 3).ToString();
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("StencilBotTransportCost").InnerText, out retxtEcoTransport1d);
                txtEcoTransport1.Text = Math.Round(retxtEcoTransport1d, 3).ToString();
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("StencilBotTotalOrderPrice").InnerText, out regrossInfo1d);

                vatInfo1.Text = (Math.Round((rebrdNetPrice1d + retxtEcoTransport1d) * vat, 3)).ToString();
                grossInfo1.Text = (Math.Round((rebrdNetPrice1d + retxtEcoTransport1d) * vat, 3) + Math.Round(rebrdNetPrice1d, 3) + Math.Round(retxtEcoTransport1d, 3)).ToString();
            }
            else
            {
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("BoardUnitPrice").InnerText, out recalUnitpriced);
                txtUnitPrice1.Text = Math.Round(recalUnitpriced, 3).ToString();
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("BoardTotalNetPrice").InnerText, out rebrdNetPrice1d);
                brdNetPrice1.Text = Math.Round(rebrdNetPrice1d, 3).ToString();
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("BoardTransportCost").InnerText, out retxtEcoTransport1d);
                txtEcoTransport1.Text = Math.Round(retxtEcoTransport1d, 3).ToString();
                decimal.TryParse(nodeOrderProperties3.SelectSingleNode("BoardTotalOrderPrice").InnerText, out regrossInfo1d);
                //decimal.TryParse(nodeOrderProperties3.SelectSingleNode("BoardUnitPrice").InnerText, out recalUnitpriced);
                recalpcbQty1.Text = nodeOrderProperties3.SelectSingleNode("BoardQty").InnerText.ToString();
                vatInfo1.Text = (Math.Round((rebrdNetPrice1d + assemblyPrice + componentPrice + retxtEcoTransport1d) * vat, 3)).ToString();
                grossInfo1.Text = (Math.Round((rebrdNetPrice1d + retxtEcoTransport1d + assemblyPrice + componentPrice) * vat, 3) + Math.Round(rebrdNetPrice1d + assemblyPrice + componentPrice, 3) + Math.Round(retxtEcoTransport1d, 3)).ToString();
            }


            pcbQty1.Text = Convert.ToString(ds_order.Tables[0].Rows[0]["OrderQty"]);
            if (service.ToLower() == "Stencilservice".ToLower())
            {
                recalpcbQty1.Text = "1";
            }
            recalUnitPrice.Text = Convert.ToString(ds_order.Tables[0].Rows[0]["UnitPriceRootco"]);
            decimal perc_new = Convert.ToDecimal(txtPercentage.Text);
            decimal value = 100 + (perc_new);
            if (value == 0)
            {
                value = 1;
            }
            decimal VaryPrice_new = (100 * Convert.ToDecimal(recalUnitPrice.Text)) / value;
            hdOriginalUnitPrice.Value = (Convert.ToDecimal(VaryPrice_new)).ToString();
            vatPercentage.InnerText = (vat * 100).ToString();
            rebrdNetPrice1.Text = Convert.ToString(ds_order.Tables[0].Rows[0]["NetPriceRootCo"]);
            hidBrdNetPrice.Value = brdNetPrice1.Text;
            retxtEcoTransport1.Text = Convert.ToString(ds_order.Tables[0].Rows[0]["TransportCostRootCo"]);
            decimal btdNetPrice_new = 0;
            decimal trasnPortCost_new = 0;
            decimal vatData_new = 0;
            decimal.TryParse(brdNetPrice1.Text, out btdNetPrice_new);
            decimal.TryParse(txtEcoTransport1.Text, out trasnPortCost_new);
            decimal.TryParse(vatInfo1.Text, out vatData_new);

            hdgrossInfo1.Value = Decimal.Round((btdNetPrice_new + trasnPortCost_new + vatData_new), 3).ToString();

            modifyOrderTrack.Append("Loading_" + PerentBasketId + "_" + Text1.Value + "_" + txtPercentage.Text + "_" + txtUnitPrice1.Text + "_" + hidBrdNetPrice.Value + "_" + txtEcoTransport1.Text + "_" + hdgrossInfo1.Value);



            ViewState["modifyOrderTrack"] = modifyOrderTrack.ToString();
            priceCalculation("vat");
            priceCalculation("gross");
        }

        private void priceCalculation(string operation)
        {
            decimal btdNetPrice = 0;
            decimal trasnPortCost = 0;
            decimal assemblypr = 0;
            decimal Compnentpr = 0;
            string assemblypriceVat = lblAssembly.Text != null ? lblAssembly.Text : "";
            string CompnentpriceVat = lblComponent.Text != null ? lblComponent.Text : "";
            decimal.TryParse(assemblypriceVat, out assemblypr);
            decimal.TryParse(CompnentpriceVat, out Compnentpr);
            if (Compnentpr < 0)
            {
                Compnentpr = 0;
            }
            switch (operation)
            {
                case "netprice":
                    decimal qty;
                    decimal unitPrice = 0;
                    decimal.TryParse(pcbQty1.Text, out qty);
                    decimal.TryParse(txtUnitPrice1.Text, out unitPrice);
                    decimal netPrice = (unitPrice * qty);
                    brdNetPrice1.Text = Decimal.Round(netPrice, 3).ToString();
                    hidBrdNetPrice.Value = Decimal.Round(netPrice, 3).ToString();
                    break;
                case "vat":
                    decimal vat = 0;
                    decimal.TryParse(rebrdNetPrice1.Text, out btdNetPrice);
                    decimal.TryParse(retxtEcoTransport1.Text, out trasnPortCost);
                    decimal.TryParse(vatPercentage.InnerText, out vat);
                    revatInfo1.Text = Decimal.Round(((btdNetPrice + Compnentpr + assemblypr + trasnPortCost) * (vat / 100)), 3).ToString();
                    break;
                case "gross":
                    decimal vatData = 0;
                    decimal.TryParse(rebrdNetPrice1.Text, out btdNetPrice);
                    decimal.TryParse(retxtEcoTransport1.Text, out trasnPortCost);
                    decimal.TryParse(revatInfo1.Text, out vatData);
                    regrossInfo1.Text = Decimal.Round((btdNetPrice + assemblypr + Compnentpr + trasnPortCost + vatData), 3).ToString();

                    break;
            }
        }

        private DropDownList FillDeliveryTerm(DropDownList objDDL)
        {
            try
            {
                string handlingcompinital = "";
                var WsPlaceorder = new sparrowFeApp.ec09Service.ECAPISoapClient();
                DataSet dsHand = objAPI.GetData(string.Format("select Initials from genCompanies where CompanyId {0} and isnull(IsDeleted,0)=0", HCompnyID), Keydata);
                if (dsHand != null && dsHand.Tables.Count > 0 && dsHand.Tables[0].Rows.Count > 0)
                {
                    handlingcompinital = Convert.ToString(dsHand.Tables[0].Rows[0]["Initials"]);
                }
                if (handlingcompinital == "ECHANDMMP" || handlingcompinital == "EPSHAND-MMP" || handlingcompinital == "ECHANDMMP-PF")
                {
                    var Deliveryterm = WsPlaceorder.GetDeliveryTermDaysDifferent("DeliveryTerm", 0, 0);
                    objDDL.DataSource = Deliveryterm;
                    objDDL.DataTextField = "ParaOption";
                    objDDL.DataValueField = "ParameterValue";
                    objDDL.DataBind();
                }
                else
                {

                    var Deliveryterm = WsPlaceorder.GetDeliveryTermDays("DeliveryTerm", 0, 0);
                    objDDL.DataSource = Deliveryterm;
                    objDDL.DataTextField = "ParaOption";
                    objDDL.DataValueField = "ParameterValue";
                    objDDL.DataBind();
                }

                if (objDDL.Items.Contains(objDDL.Items.FindByValue("2160")))
                    objDDL.Items.RemoveAt(objDDL.Items.Count - 1);
                return objDDL;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        protected void BtnRemoveVariant_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton objLBtn = (LinkButton)sender;
                int RemoveVariantRow = 0;
                int MaxVariantsRow = 0;
                if (Convert.ToString(objLBtn.AccessKey) != "")
                    int.TryParse(objLBtn.AccessKey, out RemoveVariantRow);

                if (Convert.ToString(LbtnAdd01.AccessKey) != "")
                    int.TryParse(LbtnAdd01.AccessKey, out MaxVariantsRow);

                switch (RemoveVariantRow)
                {
                    case 2:
                        DDLDelTerm02.SelectedValue = DDLDelTerm03.SelectedValue;
                        DDLDelTerm03.SelectedValue = DDLDelTerm04.SelectedValue;
                        DDLDelTerm04.SelectedValue = DDLDelTerm05.SelectedValue;
                        DDLDelTerm05.SelectedValue = DDLDelTerm06.SelectedValue;
                        DDLDelTerm06.SelectedValue = DDLDelTerm07.SelectedValue;

                        TxtQty02.Text = TxtQty03.Text;
                        TxtQty03.Text = TxtQty04.Text;
                        TxtQty04.Text = TxtQty05.Text;
                        TxtQty05.Text = TxtQty06.Text;
                        TxtQty06.Text = TxtQty07.Text;
                        break;
                    case 3:
                        DDLDelTerm03.SelectedValue = DDLDelTerm04.SelectedValue;
                        DDLDelTerm04.SelectedValue = DDLDelTerm05.SelectedValue;
                        DDLDelTerm05.SelectedValue = DDLDelTerm06.SelectedValue;
                        DDLDelTerm06.SelectedValue = DDLDelTerm07.SelectedValue;

                        TxtQty03.Text = TxtQty04.Text;
                        TxtQty04.Text = TxtQty05.Text;
                        TxtQty05.Text = TxtQty06.Text;
                        TxtQty06.Text = TxtQty07.Text;
                        break;
                    case 4:
                        DDLDelTerm04.SelectedValue = DDLDelTerm05.SelectedValue;
                        DDLDelTerm05.SelectedValue = DDLDelTerm06.SelectedValue;
                        DDLDelTerm06.SelectedValue = DDLDelTerm07.SelectedValue;

                        TxtQty04.Text = TxtQty05.Text;
                        TxtQty05.Text = TxtQty06.Text;
                        TxtQty06.Text = TxtQty07.Text;
                        break;
                    case 5:
                        DDLDelTerm05.SelectedValue = DDLDelTerm06.SelectedValue;
                        DDLDelTerm06.SelectedValue = DDLDelTerm07.SelectedValue;

                        TxtQty05.Text = TxtQty06.Text;
                        TxtQty06.Text = TxtQty07.Text;
                        break;
                    case 6:
                        DDLDelTerm05.SelectedValue = DDLDelTerm06.SelectedValue;

                        TxtQty06.Text = TxtQty07.Text;
                        break;
                }
                ShowMaxVariantRow(MaxVariantsRow - 1);
            }
            catch (Exception ex)
            {
            }

        }

        private void ShowMaxVariantRow(int MaxNo)
        {
            try
            {
                switch (MaxNo)
                {
                    case 1:
                        trVar02.Visible = false;
                        trVar03.Visible = false;
                        trVar04.Visible = false;
                        trVar05.Visible = false;
                        trVar06.Visible = false;
                        trVar07.Visible = false;
                        LbtnAdd01.AccessKey = "1";
                        break;
                    case 2:
                        trVar03.Visible = false;
                        trVar04.Visible = false;
                        trVar05.Visible = false;
                        trVar06.Visible = false;
                        trVar07.Visible = false;
                        LbtnAdd01.AccessKey = "2";
                        break;
                    case 3:
                        trVar04.Visible = false;
                        trVar05.Visible = false;
                        trVar06.Visible = false;
                        trVar07.Visible = false;
                        LbtnAdd01.AccessKey = "3";
                        break;
                    case 4:
                        trVar05.Visible = false;
                        trVar06.Visible = false;
                        trVar07.Visible = false;
                        LbtnAdd01.AccessKey = "4";
                        break;
                    case 5:
                        trVar06.Visible = false;
                        trVar07.Visible = false;
                        LbtnAdd01.AccessKey = "5";
                        break;
                    case 6:
                        trVar07.Visible = false;
                        LbtnAdd01.AccessKey = "6";
                        break;
                    case 7:
                        trVar07.Visible = false;
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void BtnAddVariant_Click(object sender, EventArgs e)
        {
            try
            {
                int TotalVariant = 1;
                if (Convert.ToString(LbtnAdd01.AccessKey) != "")
                {
                    int.TryParse(LbtnAdd01.AccessKey, out TotalVariant);
                }
                if ((TotalVariant + 1) < 8)
                {
                    switch (TotalVariant)
                    {
                        case 1:
                            trVar02.Visible = true;
                            //DDLDelTerm02 = FillDeliveryTerm(DDLDelTerm02);
                            TxtQty02.Text = "";
                            DDLDelTerm02.SelectedIndex = 0;
                            LbtnAdd01.AccessKey = "2";
                            break;
                        case 2:
                            trVar03.Visible = true;
                            //DDLDelTerm03 = FillDeliveryTerm(DDLDelTerm03);
                            TxtQty03.Text = "";
                            DDLDelTerm03.SelectedIndex = 0;
                            LbtnAdd01.AccessKey = "3";
                            break;
                        case 3:
                            trVar04.Visible = true;
                            //DDLDelTerm04 = FillDeliveryTerm(DDLDelTerm04);
                            TxtQty04.Text = "";
                            DDLDelTerm04.SelectedIndex = 0;
                            LbtnAdd01.AccessKey = "4";
                            break;
                        case 4:
                            trVar05.Visible = true;
                            //DDLDelTerm05 = FillDeliveryTerm(DDLDelTerm05);
                            TxtQty05.Text = "";
                            DDLDelTerm05.SelectedIndex = 0;
                            LbtnAdd01.AccessKey = "5";
                            break;
                        case 5:
                            trVar06.Visible = true;
                            //DDLDelTerm06 = FillDeliveryTerm(DDLDelTerm06);
                            TxtQty06.Text = "";
                            DDLDelTerm06.SelectedIndex = 0;
                            LbtnAdd01.AccessKey = "6";
                            break;
                        case 6:
                            trVar07.Visible = true;
                            //DDLDelTerm07 = FillDeliveryTerm(DDLDelTerm07);
                            TxtQty07.Text = "";
                            DDLDelTerm07.SelectedIndex = 0;
                            LbtnAdd01.AccessKey = "7";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected bool validate()
        {

            if (txtPcbName.Text == "" || txtPcbName.Text == string.Empty)
            {
                lblpcbnamereq.Text = objbase.GetLocalResourceObject("lblpcbnamereq.Text");
                ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                return false;
            }
            return true;

        }

        private void FillDeliveryInvoiceAddress()
        {
            try
            {

                //var DelAddress = GetDataForFixedParametersInOrderScreen();
                System.Data.DataSet ds = new System.Data.DataSet();
                string sql_getdeliveryaddress = string.Format("exec spPOMGetFixedOrderParamertersDetails {0},{1},{2}", 493, Company_id, 1);
                ds = objgd.GetData(sql_getdeliveryaddress, Keydata);

                ddlDELADDRESS.DataSource = ds.Tables[0];
                ddlDELADDRESS.DataTextField = "StreetNo";
                ddlDELADDRESS.DataValueField = "AddressId";
                ddlDELADDRESS.DataBind();


                ddlInvoicesaddress.DataSource = ds.Tables[2];
                ddlInvoicesaddress.DataTextField = "streetNo";
                ddlInvoicesaddress.DataValueField = "AddressId";
                ddlInvoicesaddress.DataBind();

                if (ddlDELADDRESS.Items.Count > 0)
                {
                    if (OrderDeliveryAddressId != 0)
                    {
                        ddlDELADDRESS.SelectedValue = Convert.ToString(OrderDeliveryAddressId);
                    }
                    else
                    {
                        //txtAddr.Text = ddlDELADDRESS.SelectedItem.Text;
                        string[] strArr = ddlDELADDRESS.SelectedItem.Text.Split(',');
                        string strTemp = strArr[0] + "," + strArr[1] + "<br>";

                        for (int i = 2; i < strArr.Length; i++)
                        {
                            strTemp = strTemp + strArr[i] + ",";
                        }

                        if (strTemp.EndsWith(","))
                        {
                            strTemp = strTemp.Substring(0, strTemp.Length - 1);
                        }

                        divAddr.InnerHtml = strTemp;
                    }
                }
                else
                {
                    divAddr.Visible = false;
                }

                if (ddlInvoicesaddress.Items.Count > 0)
                {
                    if (OrderInvoiceAddressId != 0)
                    {
                        ddlInvoicesaddress.SelectedValue = Convert.ToString(OrderInvoiceAddressId);
                    }
                    else
                    {
                        string[] strArr = ddlInvoicesaddress.SelectedItem.Text.Split(',');
                        string strTemp = strArr[0] + "," + strArr[1] + "<br>";

                        for (int i = 2; i < strArr.Length; i++)
                        {
                            strTemp = strTemp + strArr[i] + ",";
                        }

                        if (strTemp.EndsWith(","))
                        {
                            strTemp = strTemp.Substring(0, strTemp.Length - 1);
                        }

                        divInvAddr.InnerHtml = strTemp;
                    }
                }
                else
                {
                    divInvAddr.Visible = false;
                }


                if (Convert.ToString(ViewState["Type"]).ToLower() == "modifyOrder".ToLower())
                {
                    ////////////Comment Explog entry///////
                    //genExceptionLog objException = new genExceptionLog();
                    //objException.LogExceptiondata("Bug-Deliveryaddress", 0, "OrderNumber-" + EntityNo + "Companyid_Load-" + Convert.ToString(Company_id) + ",OrderDeliveryAddressId-" + Convert.ToString(OrderDeliveryAddressId) + ",address-" + Convert.ToString(divAddr.InnerHtml), false);

                }

            }
            catch (Exception ex)
            {

            }
        }



        protected void btnsubit_Click(object sender, EventArgs e)
        {
            //Session["configurator"] = null;
            try
            {

                //Common check validation for pcb name ...
                if (!validate())
                {
                    //divHeader.Visible = true;
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                    return;
                }

                #region newOrder
                if (Convert.ToString(ViewState["Type"]).ToLower() == "newOrder".ToLower())
                {
                    if (!placenewOrder())
                    {
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                        return;
                    }
                }
                #endregion

                #region inquiry
                else if (Convert.ToString(ViewState["Type"]).ToLower() == "inquiry")
                {
                    if (!placenewInquiry())
                    {
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                        return;
                    }
                }
                #endregion

                #region newOffer
                else if (Convert.ToString(ViewState["Type"]).ToLower() == "newOffer".ToLower()) //not in use
                {
                    //if (!placenewOffer())
                    {
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                        return;
                    }
                }
                #endregion

                #region modifyOrder
                else if (Convert.ToString(ViewState["Type"]).ToLower() == "modifyOrder".ToLower())
                {
                    if (!modifyOrder())
                    {
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                        return;
                    }
                }
                #endregion

                #region modifyINQ
                else if (Convert.ToString(Request["type"]) == "modifyINQ")
                {
                    #region modifyInquiry
                    try
                    {
                        string IP_add = "Internal";
                        divimg.Visible = true;
                        if (!VarifyPlaceInqData())
                        {
                            ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                            return;
                        }

                        string query1 = string.Format("select InquiryStatusId  from genInqOffers with(nolock) where InquiryNo in(select BasketNo from BasketItems with(nolock) where Basket_Id={0}) AND InquiryStatusId =462", PerentBasketId);
                        DataSet dsbasketstatus = objAPI.GetData(query1, Keydata);


                        if (dsbasketstatus.Tables[0].Rows.Count != 0)
                        {
                            error.Text = objbase.GetLocalResourceObject("lblcancelinquiryerror.Text");
                            diverrortd.Attributes["style"] = "display:block";
                            ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                            return;
                        }


                        Session["configurator"] = null;
                        //Coding for modify inquiry from here ...
                        string NewXmlString = "";
                        string sqlGetXML = string.Format("select XMLString from i8response with(nolock) where Sessionid = '{0}'", SessionId);
                        DataTable dtGetXMLString = objgd.GetData(sqlGetXML, Keydata).Tables[0];
                        if (dtGetXMLString != null && dtGetXMLString.Rows.Count > 0)
                        {
                            NewXmlString = Convert.ToString(dtGetXMLString.Rows[0]["XMLString"]);
                        }
                        string query = string.Format("exec osp_Update_basketitems {0},'{1}'", PerentBasketId, SessionId);
                        DataSet dsbasket = objAPI.GetData(query, Keydata);

                        string BasketNo = "";
                        string BasketSessionId = "";
                        string UpdatedBasketID = "";
                        if (dsbasket.Tables[0].Rows.Count != 0)
                        {
                            BasketNo = Convert.ToString(dsbasket.Tables[0].Rows[0]["BasketNo"]);
                            BasketSessionId = Convert.ToString(dsbasket.Tables[0].Rows[0]["Session_Id"]);
                            UpdatedBasketID = Convert.ToString(PerentBasketId);
                            string transportMode = "";

                            //insert history for modify basket...
                            long BasketUserId = 0;
                            long Status_Id = 0;
                            long DocumentId = 0;
                            bool IsWithOutData = false;
                            string sql_basket = string.Format("select * from BasketItems with(nolock) where Basket_Id='" + UpdatedBasketID + "'");
                            DataSet ds_basket = objAPI.GetData(sql_basket, Keydata);
                            if (ds_basket != null && ds_basket.Tables[0].Rows.Count != 0)
                            {
                                Status_Id = Convert.ToInt32(ds_basket.Tables[0].Rows[0]["Status_Id"]);// EC09.Data.admCode.GetCodeId("BasketReadyToCheckout");

                                long ActionID = ec09Service.GetCodeId("ACTION_BASKETMODIFIED");

                                bool Is_File_Attached = Convert.ToBoolean(ds_basket.Tables[0].Rows[0]["Is_File_Attached"]);
                                long.TryParse(Convert.ToString(ds_basket.Tables[0].Rows[0]["User_Id"]), out BasketUserId);
                                string Basket_No = Convert.ToString(ds_basket.Tables[0].Rows[0]["BasketNo"]);
                                string Company_id = Convert.ToString(ds_basket.Tables[0].Rows[0]["Company_id"]);
                                if (Is_File_Attached == true)
                                {
                                    DocumentId = Convert.ToInt32(ds_basket.Tables[0].Rows[0]["Basket_Id"]);
                                }
                                try
                                {
                                    ec09Service.GenerateHistory(Convert.ToInt64(UpdatedBasketID), "BASKET", eccUserId, Status_Id, ActionID, IP_add, DocumentId, Basket_No);
                                }
                                catch { }
                                IsWithOutData = false;
                                if (Is_File_Attached == true)
                                {
                                    IsWithOutData = false;
                                }
                                else
                                {
                                    IsWithOutData = true;
                                }

                                DateTime estimated_deliverydate;
                                if (hidendeliverydate.Value != null && hidendeliverydate.Value != "")
                                {
                                    estimated_deliverydate = Convert.ToDateTime(hidendeliverydate.Value);
                                }
                                else if (datepicker.Value != null && datepicker.Value != "")
                                {
                                    estimated_deliverydate = Convert.ToDateTime(datepicker.Value);
                                }
                                //update stencil fix value runtime..
                                string upd_querybask = string.Format("update BasketItems set Is_Stencil_Fixed='{0}',Estimate_Delivery_Date='{2}' where BasketNo='{1}'", IsStencilFixed, BasketNo, null);
                                objAPI.PutData(upd_querybask, Keydata);

                            }
                            string path = sparrowFeApp.core.Common.getFilePath(BasketNo);
                            if (path != "")
                            {
                                System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = null;
                                impersonateOnFileServer = sparrowFeApp.core.Impersonation.ImpersonateFileServer(sparrowFeApp.core.Constant.FileServerUsername, sparrowFeApp.core.Constant.FileServerPassword, sparrowFeApp.core.Constant.FileServerDomain);
                                if (impersonateOnFileServer != null)
                                {
                                    string FilePath = path + "\\" + BasketNo + ".xml";
                                    XmlDocument doc = new XmlDocument();
                                    doc.LoadXml(NewXmlString);
                                    System.Xml.XmlNode nodeOrderProperties3 = doc.SelectSingleNode("OrderDetail/OrderProperties3");
                                    System.Xml.XmlNode nodeBoardProperties3 = doc.SelectSingleNode("OrderDetail/BoardProperties3");
                                    System.Xml.XmlNode ProductionProperties3 = doc.SelectSingleNode("OrderDetail/ProductionProperties3");
                                    transportMode = nodeOrderProperties3.SelectSingleNode("BoardTransportMode").InnerText;
                                    if (ProductionProperties3 != null)
                                    {
                                        ProductionProperties3.SelectSingleNode("PlatedSlots").InnerText = Convert.ToString(checkplatedslots.Checked).ToLower();
                                    }
                                    nodeBoardProperties3.SelectSingleNode("BoardName").InnerText = PCBName;
                                    nodeOrderProperties3.SelectSingleNode("CustomerPurchaseReference").InnerText = PurchaseRef;
                                    nodeOrderProperties3.SelectSingleNode("CustomerProjectReference").InnerText = ProjectRef;
                                    nodeOrderProperties3.SelectSingleNode("CustomerArticleReference").InnerText = ArticalRef;
                                    NewXmlString = doc.OuterXml.Trim();
                                    doc.Save(FilePath);
                                }
                            }

                            //update inquiry...
                            string strGetInq = string.Format("select * from genInqOffers with(nolock) where InquiryNo in(select BasketNo from BasketItems with(nolock) where Basket_Id=" + PerentBasketId + " )");
                            DataSet dsInq = objAPI.GetData(strGetInq, Keydata);
                            if (dsInq.Tables[0].Rows.Count != 0)
                            {
                                long CustCompanyId = Convert.ToInt32(dsInq.Tables[0].Rows[0]["CompanyId"]);
                                long CustUserId = Convert.ToInt32(dsInq.Tables[0].Rows[0]["CustUserId"]);
                                NewXmlString = NewXmlString.Replace("'", "''");
                                bool IsModifyException = false;
                                long ExceptionId = 0;
                                long InqOfferId = Convert.ToInt32(dsInq.Tables[0].Rows[0]["InqOfferId"]);
                                long DeliveryAddressId = 0;
                                long InvAddressId = 0;
                                long HandlingCompanyID = 0;
                                long LanguageID = 493;
                                HttpBrowserCapabilities browse = Request.Browser;
                                string UserAgent = browse.Browser + "-" + browse.Version + "|" + browse.Platform;


                                //string Mode = "E";
                                string Mode = Convert.ToString(dsInq.Tables[0].Rows[0]["Mode"]);
                                string Sql_CutomerId = "SELECT DeliveryAddressId,InvoiceAddressId,HandlingCompanyID,LanguageID,admCodes.Code as [Code]  FROM genSearchCustomer with(nolock)  inner join admCodes on admCodes.CodeId=genSearchCustomer.LanguageID where genSearchCustomer.CustomerID=" + Convert.ToString(CustCompanyId) + " and ISNULL(genSearchCustomer.IsUserDeleted,0)=0 and ISNULL(genSearchCustomer.IsCompanyDeleted,0)=0 and ISNULL(genSearchCustomer.IsContactDeleted,0)=0 ";
                                DataSet DS_customer = objAPI.GetData(Sql_CutomerId, Keydata);
                                string Code = "en";
                                if (DS_customer != null)
                                {
                                    if (DS_customer.Tables[0].Rows.Count > 0)
                                    {
                                        if (ddlDELADDRESS.SelectedValue == "")
                                        {
                                            DeliveryAddressId = Convert.ToInt32(DS_customer.Tables[0].Rows[0]["DeliveryAddressId"]);
                                        }
                                        else
                                        {
                                            DeliveryAddressId = Convert.ToInt32(ddlDELADDRESS.SelectedValue);
                                        }
                                        InvAddressId = Convert.ToInt32(DS_customer.Tables[0].Rows[0]["InvoiceAddressId"]);
                                        HandlingCompanyID = Convert.ToInt32(DS_customer.Tables[0].Rows[0]["HandlingCompanyID"]);
                                        LanguageID = Convert.ToInt32(DS_customer.Tables[0].Rows[0]["LanguageID"]);
                                        Code = Convert.ToString(DS_customer.Tables[0].Rows[0]["Code"]);
                                    }

                                }
                                bool IsStencilFixed = chkStencilFixed.Checked;
                                string strUpdInq = string.Format("exec osp_InsertInquiryFromVisXml {0},{1},'{2}','{3}','{4}','{5}',{6},{7},{8},'{9}',{10},{11},{12},'{13}','{14}','{15}','{16}'",
                                                                CustCompanyId, CustUserId, NewXmlString.Replace("'", "''"), UserAgent, IP_add, Mode, IsModifyException,
                                                                ExceptionId, InqOfferId, Type, DeliveryAddressId, InvAddressId, IsWithOutData, Code, 0, BasketNo, IsStencilFixed);
                                DataSet dsinq = objAPI.GetData(strUpdInq, Keydata);

                                // replace inquiry xml file 
                                string InqNo = "";
                                if (dsinq != null)
                                {
                                    if (dsinq.Tables[0].Rows.Count > 0)
                                    {
                                        InqNo = Convert.ToString(dsinq.Tables[0].Rows[0][0]);
                                    }
                                }
                                string VariantXml = GetVariantXml();
                                string sqlUpdateVariants = string.Format("exec osp_InsertInquiryVariants '{0}','{1}','{2}','{3}','{4}','{5}','{6}'", InqNo, VariantXml, RCompnyID, HCompnyID, UserId, true, transportMode);
                                objgd.PutData(sqlUpdateVariants, Keydata);
                                DataSet ds = objgd.GetData(string.Format("select * from genInqOffers Where InquiryNo ='{0}'", InqNo), Keydata);
                                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                {
                                    long ECCuserid = 0;
                                    long.TryParse(Convert.ToString(eccUserId), out ECCuserid);
                                    ec09Service.InsertInInquiryHistory(Convert.ToInt64(ds.Tables[0].Rows[0]["InqOfferId"]), ECCuserid, Convert.ToInt64(ds.Tables[0].Rows[0]["InquiryStatusId"]), ec09Service.GetCodeId("ACTION_MODIFIED"), 0, IP_add, 0, 0, 0);
                                }
                                string sqlUpdateInqDetail = string.Format("UPDATE BasketItems SET PCB_Name = '{0}' , Project_Reference='{1}',Article_Reference='{2}',Purchase_Reference = '{3}' WHERE Basket_Id = '{4}'", PCBName, ProjectRef, ArticalRef, PurchaseRef, PerentBasketId, NewXmlString.Replace("'", "''"));  ///Remove XML_Data Columns 
                                objgd.PutData(sqlUpdateInqDetail, Keydata);


                                Common.UpdateOrderDetailFromEC(InqNo);

                                path = path.Replace(Constant.FileServerPath, "");
                                if (path != "")
                                {
                                    var feApp = new { fun_name = "file_sync", entity_number = InqNo, file_name = BasketNo + ".xml", file_type = "modify_inquiry", file_path = "\\" + path + "\\", session_id = SessionId,basket_session_id= Convert.ToString(dsbasket.Tables[0].Rows[0]["Session_Id"]), parent_basket_id = PerentBasketId, crm_rel_id = AppSessions.EccUserId, source = "FE", target = "EC" };
                                    string jsonFeApp = JsonConvert.SerializeObject(feApp);
                                    Common feAppData = new Common();
                                    feAppData.FeAppAPI(jsonFeApp);
                                }

                                //WUCUploaddata objupl = new WUCUploaddata();
                                string strrtun = ChangeCustDataAndSIdata(SessionId, InqNo, PerentBasketId);
                                //string[] arry = strrtun.Split('|');
                                //bool IsSentToI8 = (arry[1] == "1" ? true : false);
                                //if (!IsSentToI8)
                                //{
                                //    string temppath = objGenFileServer.RootPath + "tempbasket\\" + Convert.ToString(dsbasket.Tables[0].Rows[0]["Session_Id"]) + "\\visualizer";
                                //    string returnmsg = objoffer.ReplaceBuildUpImg(Convert.ToString(PerentBasketId), temppath, "basket", Convert.ToString(PerentBasketId) + "_buildup.png");
                                //}
                                divimg.Visible = false;
                                ScriptManager.RegisterStartupScript(this.Page, GetType(), "", "window.parent.location.href = 'orders/eccordermodified.aspx?successmsgtitle=InquiryModified&entityno=" + InqNo + "&successmsgdesc=Inquiry modified.&action=inquirymodify;'", true);

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    #endregion

                }

                #endregion
                #region repeatOrder
                else if (Convert.ToString(ViewState["Type"]).ToLower() == "repeatOrder".ToLower())
                {

                }
                #endregion

                #region newBasket
                else if (Convert.ToString(ViewState["Type"]).ToLower() == "newBasket".ToLower()) // not in use
                {
                    // not in use
                }
                #endregion

                #region modifyBasket
                else if (Convert.ToString(ViewState["Type"]).ToLower() == "modifyBasket".ToLower()) // not in use
                {

                    try
                    {
                    }
                    catch
                    {

                    }
                }
                #endregion

                #region savetobasket
                else if (Convert.ToString(ViewState["Type"]).ToLower() == "save")
                {
                }
                #endregion

                #region PPAtoSI
                else if (Convert.ToString(ViewState["Type"]).ToLower() == "PPAtoSI".ToLower())
                {
                    if (!processPPAOrder())
                    {
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                        return;
                    }
                }
                #endregion
            }
            catch
            {
            }
        }
        public string ChangeCustDataAndSIdata(string SessionId, string Number, string basketid)
        {
            try
            {
                // Not: basketid for basket i8 request and status (basketid) for check  Process PPA order with cut. data or not if cus. data than delete si file else replace with SI_BACKUP 
                // Status=PPACUSTDATA
                int sendtoi8 = 0;
                string status = "";
                string sqlGetXmlString = string.Format("SELECT * from i8response with(nolock) where Sessionid like '%{0}%'", SessionId);
                DataSet dsGetXmlString = objgd.GetData(sqlGetXmlString, Keydata);
                if (dsGetXmlString != null && dsGetXmlString.Tables.Count > 0 && dsGetXmlString.Tables[0].Rows.Count > 0)
                {
                    status = Convert.ToString(dsGetXmlString.Tables[0].Rows[0]["dataStatus"]);
                    string path = sparrowFeApp.core.Common.getFilePath(Number);
                    if (status.Trim() == "2" || status.Trim() == "3" || status.Trim() == "4")
                    {
                        if (path != "" && path.ToUpper().Contains(Number.ToUpper()))
                        {
                            {
                                WindowsImpersonationContext impersonateOnFileServer = null;
                                impersonateOnFileServer = sparrowFeApp.core.Impersonation.ImpersonateFileServer(sparrowFeApp.core.Constant.FileServerUsername, sparrowFeApp.core.Constant.FileServerPassword, sparrowFeApp.core.Constant.FileServerDomain);
                                if (impersonateOnFileServer != null)
                                {
                                    string folderpath = path;
                                    DirectoryInfo dir = new DirectoryInfo(folderpath);
                                    FileInfo[] files = dir.GetFiles();
                                    DirectoryInfo[] dirs = dir.GetDirectories();
                                    string sql = string.Format("select file_path,file_type from sales_file where entity_number = '{0}'", Number);
                                     Database.DA gd = new Database.DA();
                                    DataSet ds = gd.GetDataSet(sql);
                                // delete cust data & si data  dir - ppa back to si deletion
                                  foreach (FileInfo file in files)
                                    {
                                        // for cust data
                                        if (file.Name.ToLower() == Number.ToLower() + ".zip" || file.Name.ToLower() == Number.ToLower() + ".rar" || file.Name.ToLower() == Number.ToLower() + ".brd" || file.Name.ToLower() == Number.ToLower() + ".kicad_pcb")
                                        {
                                            string custdatafolder = folderpath + "\\" + file.Name;
                                            string visfolder = folderpath + "\\Visualizer";
                                            if (status.Trim() == "4" || status.Trim() == "3")
                                            {
                                                if (File.Exists(custdatafolder))
                                                {
                                                    try
                                                    {

                                                        if (Directory.Exists(visfolder))
                                                        {
                                                            string sqli8PC = string.Format("delete from sales_file where entity_number = '{0}' and file_type='I8PC'", Number);
                                                            gd.ExecuteQuery(sqli8PC);
                                                            Directory.Delete(visfolder, true);
                                                        }
                                                    }
                                                    catch
                                                    { //todo: handle this 
                                                    }

                                                }
                                            }

                                            // send cust request to i8
                                            if (File.Exists(custdatafolder))
                                            {
                                                if (status.Trim() == "2" || status.Trim() == "3")
                                                {
                                                    if (Number.Trim().ToUpper().StartsWith("B"))
                                                    {
                                                        sendtoi8 = 1;
                                                        //this part is handel in EC
                                                    }
                                                    else
                                                    {
                                                        sendtoi8 = 1;
                                                        //this part is handel in EC
                                                    }
                                                }
                                                else
                                                {
                                                    if (Number.Trim().ToUpper().StartsWith("B"))
                                                    {
                                                        sendtoi8 = 1;
                                                        //this part is handel in EC
                                                    }
                                                    else
                                                    {
                                                        sendtoi8 = 1;
                                                        //this part is handel in EC
                                                    }
                                                }
                                            }
                                        }
                                        // end code for functionality related to cust data file

                                        // for si data
                                        if (file.Name.ToLower().Contains("si_") || file.Name.ToLower().Contains("cam_"))
                                        {
                                            if (status.Trim() == "4" || status.Trim() == "3")
                                            {
                                                string sidatafolder = folderpath + "\\" + file.Name;
                                                if (File.Exists(sidatafolder))
                                                {

                                                    if (file.Extension.Trim().ToLower() == ".xml")
                                                    {
                                                        IEnumerable<FileInfo> result = from files1 in files where files1.Name.ToLower() == "si_" + Number.ToLower() + ".xml" select files1;
                                                        if (result.Count() > 0)
                                                        {
                                                            File.Delete(sidatafolder);
                                                        }
                                                        else
                                                        {
                                                            ////////////Comment Explog entry///////
                                                            //genExceptionLog objException = new genExceptionLog();
                                                            //objException.LogExceptiondata("SIXMLNOTDELETEDChangeCustDataAndSIdataFileMOve=" + Number, Convert.ToInt64(ViewState["UserId"]), "SIBackupFilepath" + sidatafolder);
                                                        }
                                                        string sqlSI = string.Format("delete from sales_file where entity_number = '{0}' and filename='{1}'", Number, file.Name.ToLower());
                                                        gd.ExecuteQuery(sqlSI);
                                                    }
                                                    else
                                                    {
                                                    
                                                        string sqlSI = string.Format("delete from sales_file where entity_number = '{0}' and filename='{1}'", Number, file.Name.ToLower());
                                                        gd.ExecuteQuery(sqlSI);
                                                        if (basketid.ToUpper() == "PPACUSTDATA")
                                                        {
                                                            // delete SI File  when new customer data is uploaded form PPA Process
                                                            File.Delete(sidatafolder);
                                                        }
                                                        else if ("si_" + Number.ToLower() + "-st.zip" == file.Name.ToLower() || "si_" + Number.ToLower() + "-sb.zip" == file.Name.ToLower())
                                                        {
                                                            File.Delete(sidatafolder);
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {
                                                                File.Move(sidatafolder, folderpath + "\\" + "SI_BACKUP_" + Number + file.Extension);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                            }
                                                            finally { }

                                                            string sqlSIbac = string.Format("select file_path,file_type from sales_file where entity_number = '{0}' and file_name='{1}'", Number, file.Name.ToLower());
                                                            gd.ExecuteQuery(sqlSIbac);
                                                            string fileType = "SI";
                                                            if (Number.Trim().ToLower().StartsWith("b")) { fileType = "SIN"; }

                                                            string sqlInsert = string.Format("insert into sales_file (file_name, entity_number, file_path, file_size, file_type, created_date,) values('{0}','{1}','{2}','{3}','{4}',getutcdate(),)", "SI_BACKUP_" + Number + file.Extension,Number, folderpath.Replace(core.Constant.FileServerPath,""), Convert.ToDecimal(file.Length / 1024), fileType);
                                                        }

                                                        //for delete pi & li files file file

                                                        string PnlFile = string.Format("select file_path,file_type from sales_file where entity_number = '{0}' and file_type in ('LI','PI')", Number, file.Name.ToLower());
                                                       DataTable dt=  gd.GetDataTable(PnlFile);
                                                        string PnlFiledelete = string.Format("delete from sales_file where entity_number = '{0}' and file_type in ('LI','PI')", Number, file.Name.ToLower());
                                                        gd.ExecuteQuery(PnlFiledelete);
                                                        foreach (DataRow gf in dt.Rows)
                                                        {
                                                            try
                                                            {
                                                                if (File.Exists(Constant.FileServerPath+ "\\" + gf["file_path"] + "\\" + gf["File_Name"]))
                                                                {
                                                                    File.Delete(Constant.FileServerPath + "\\" + gf["file_path"] + "\\" + gf["File_Name"]);
                                                                }
                                                            }
                                                            catch
                                                            {
                                                            }
                                                        }
                                                    }

                                                }
                                            }

                                            //send si req to i8
                                            if (status.Trim() == "2")
                                            {
                                                if (file.Name.ToLower() == "si_" + Number.ToLower() + ".zip")
                                                {
                                                    if (File.Exists(folderpath + "\\" + file.Name))
                                                    {
                                                        //obji8request.CreateXmlandSendRequestForSIFile(folderpath + "\\" + file.Name, Number);
                                                    }
                                                }
                                            }

                                        }
                                        // end code for functionality related to si data file

                                    }
                                    // end code for delete data and si file


                                    // delete vis dir
                                    try
                                    {
                                        foreach (DirectoryInfo subdir in dirs)
                                        {
                                            if (subdir.Name.Trim().ToLower() == "visualizer")
                                            {
                                                string visfolder = folderpath + "\\" + subdir.Name;
                                                if (Directory.Exists(visfolder))
                                                {
                                                    string sqli8PC = string.Format("delete  from sales_file where entity_number = '{0}' and file_type='I8PC'", Number);
                                                    gd.ExecuteQuery(sqli8PC);
                                                    Directory.Delete(visfolder, true);
                                                }
                                            }
                                        }
                                    }
                                    catch { }
                                    // end code for delete vis dir 
                                }
                            }


                        }
                        // end condition for data file and si file avaiblity
                    }
                    // end vaild status condition
                }
                return status + "|" + Convert.ToString(sendtoi8);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        #region getVetPercentage
        public string getVATPer(string Company_id, string DeliveryAddressId)
        {
            try
            {
                string CountryCode = ""; long CountryId = 0;
                DataSet dsGetCountry = objAPI.GetData("exec osp_GetDeliveryCountrybyCompany '" + Company_id + "','" + Convert.ToInt64(DeliveryAddressId) + "'", Keydata);
                if (dsGetCountry.Tables[0].Rows.Count == 0)
                {
                    dsGetCountry = objAPI.GetData("exec osp_GetDeliveryCountrybyCompany '" + Company_id + "',0", Keydata);
                    if (dsGetCountry.Tables[0].Rows.Count != 0)
                    {
                        CountryId = Convert.ToInt64(dsGetCountry.Tables[0].Rows[0]["DeliveryCountryId"]);
                    }
                }
                else
                {
                    CountryId = Convert.ToInt64(dsGetCountry.Tables[0].Rows[0]["DeliveryCountryId"]);
                }
                DataTable dtcountrycode = objAPI.GetData("select Initial from admCountries with(nolock) where CountryId=" + CountryId, Keydata).Tables[0];
                if (dtcountrycode.Rows.Count != 0)
                {
                    CountryCode = Convert.ToString(dtcountrycode.Rows[0]["Initial"]);
                }

                string Invoicecountrycode = "";
                DataTable dtcountrycodeinvoice = objAPI.GetData("SELECT (select Initial from admCountries where CountryId=InvoiceCountryId ) as Invoicecountrycode from genSearchCustomer with(nolock) where CustomerID = " + Company_id, Keydata).Tables[0];
                if (dtcountrycode.Rows.Count != 0)
                {
                    Invoicecountrycode = Convert.ToString(dtcountrycodeinvoice.Rows[0]["Invoicecountrycode"]);
                }

                string hasVat = "0";

                string configurl = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CofiguratorUrl"]);
                //hasVat = objorderdata.HasVATapply(Company_id);

                string SqlGetVatDetail = string.Format("spgetEucountruCode '{0}'", Company_id);
                System.Data.DataTable dtGetVatDetail = objAPI.GetData(SqlGetVatDetail, Keydata).Tables[0];
                string vatCountryCode = Convert.ToString(dtGetVatDetail.Rows[0]["vatCountryCode"]);
                string noVAT = Convert.ToString(dtGetVatDetail.Rows[0]["noVAT"]);
                string IsBusinessCustomer = Convert.ToString(dtGetVatDetail.Rows[0]["IsBusinessCustomer"]);
                string handlingCompany = Convert.ToString(dtGetVatDetail.Rows[0]["handlingCompany"]);

                if (noVAT.ToLower() == "false")
                {
                    noVAT = "0";
                }
                else
                {
                    noVAT = "1";
                }

                string strURL = configurl + "/servlets/VATCalculatorServlet?destinationCountry=" + CountryCode.ToUpper() + "&invoiceCountry=" + Invoicecountrycode.ToUpper() + "&handlingCompany=" + handlingCompany + "&vatCountry=" + vatCountryCode + "&noVAT=" + noVAT + "&timestamp=" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
                HttpWebRequest request = WebRequest.Create(strURL) as HttpWebRequest;
                request.UserAgent = "MSIE 6.0";
                request.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(responseFromServer.Trim());

                System.Xml.XmlNode nodePrice = doc.SelectSingleNode("Reply/Price");
                string responsedata = nodePrice.Attributes["vatPercent"].Value;
                decimal value = 0;
                decimal.TryParse(responsedata, out value);
                value = value / 100;
                return Convert.ToString(value);
            }
            catch (Exception ex)
            {
                return "0.00";
            }
        }
        #endregion
        private bool modifyOrder()
        {
            try
            {
                string strerr = "";
                bool IsFC = false; bool IsPlatedSolts = false; bool ISPackWithPaper = false; bool BoardLock = false;
                Session["configurator"] = null;
                string IP_add = "Internal";
                Type = "modifyOrder";
                long operatorid = 0;
                long invoiceaddressId = 0;
                decimal Transport_Price_Marker = 1;
                string strsql = "select * from genOrders with(nolock) where ordernumber='" + PerentBasketId + "';DECLARE @ddate datetime  = '" + Text1.Value + "';select convert (VARCHAR(50), @ddate, 126) as deliverydate";
                strerr += "Order number - " + PerentBasketId + "|";
                DataSet dsOrder = objAPI.GetData(strsql, Keydata);
                string apiRef = "";
                string customerid = "";
                if (dsOrder != null && dsOrder.Tables[0].Rows.Count != 0)
                {
                    long.TryParse(Convert.ToString(dsOrder.Tables[0].Rows[0]["OperatorId"]), out operatorid);
                    string Mode = Convert.ToString(dsOrder.Tables[0].Rows[0]["Mode"]);
                    decimal.TryParse(Convert.ToString(dsOrder.Tables[0].Rows[0]["Transport_Price_Marker"]), out Transport_Price_Marker);
                    apiRef = Convert.ToString(dsOrder.Tables[0].Rows[0]["ecapi_ref"]);
                    decimal TransportCostRootCo = 0;
                    decimal.TryParse(Convert.ToString(dsOrder.Tables[0].Rows[0]["TransportCostRootCo"]), out TransportCostRootCo);
                    customerid = dsOrder.Tables[0].Rows[0]["CompanyId"].ToString();
                    if (Convert.ToString(dsOrder.Tables[0].Rows[0]["InvoiceAddressId"]) != "")
                    {
                        invoiceaddressId = Convert.ToInt32(dsOrder.Tables[0].Rows[0]["InvoiceAddressId"]);
                    }
                    strerr += "1a.invoiceaddressId- " + Convert.ToString(invoiceaddressId) + "|";
                    string sql_langDet = string.Format("select InvoiceAddressId,Code from genSearchCustomer with(nolock) inner join admCodes on admCodes.CodeId=genSearchCustomer.LanguageID where genSearchCustomer.CustomerID =" + Convert.ToInt64(dsOrder.Tables[0].Rows[0]["CompanyId"]) + " and admcodes.IsActive=1 ");
                    DataSet ds_lang = objAPI.GetData(sql_langDet, Keydata);
                    string LangCode = "en";
                    if (ds_lang != null)
                    {
                        if (ds_lang.Tables[0].Rows.Count != 0)
                        {
                            LangCode = Convert.ToString(ds_lang.Tables[0].Rows[0]["Code"]);
                            if (invoiceaddressId == 0)
                            {
                                invoiceaddressId = Convert.ToInt32(ds_lang.Tables[0].Rows[0]["InvoiceAddressId"]);
                            }
                        }
                    }
                    strerr += "1b.invoiceaddressId- " + Convert.ToString(invoiceaddressId) + "|";
                    string BuidUpCode = "";
                    string assemblyQty = "0";
                    string service = "";
                    string sqlGetXMLString = string.Format("SELECT top 1 XMLString,Buildupimg from i8response with(nolock) where Sessionid LIKE '%{0}%'", SessionId), XMLString = string.Empty;
                    DataTable dtGetXMLString = objgd.GetData(sqlGetXMLString, Keydata).Tables[0];
                    if (dtGetXMLString != null && dtGetXMLString.Rows.Count > 0)
                    {
                        BuidUpCode = Convert.ToString(dtGetXMLString.Rows[0]["Buildupimg"]);
                        XMLString = Convert.ToString(dtGetXMLString.Rows[0]["XMLString"]);
                        XmlDocument xdoc = new XmlDocument();
                        xdoc.LoadXml(Convert.ToString(XMLString.Trim()));
                        System.Xml.XmlNode nodeCustomerProperties3 = xdoc.SelectSingleNode("OrderDetail/CustomerProperties3");
                        System.Xml.XmlNode nodeOrderProperties3 = xdoc.SelectSingleNode("OrderDetail/OrderProperties3");
                        System.Xml.XmlNode nodeBoardProperties3 = xdoc.SelectSingleNode("OrderDetail/BoardProperties3");
                        nodeBoardProperties3.SelectSingleNode("BoardName").InnerText = PCBName;
                        nodeOrderProperties3.SelectSingleNode("CustomerPurchaseReference").InnerText = PurchaseRef;
                        nodeOrderProperties3.SelectSingleNode("CustomerProjectReference").InnerText = ProjectRef;
                        nodeOrderProperties3.SelectSingleNode("CustomerArticleReference").InnerText = ArticalRef;
                        service = nodeOrderProperties3.SelectSingleNode("ServiceName").InnerText;
                        StringBuilder modifyOrderTrack = new StringBuilder();
                        if (ViewState["modifyOrderTrack"] != null) modifyOrderTrack.Append(ViewState["modifyOrderTrack"].ToString() + "_");
                        try
                        {
                            string deliveryDate = dsOrder.Tables[1].Rows[0]["deliverydate"].ToString();
                            nodeOrderProperties3.SelectSingleNode("BoardDeliveryDate").InnerText = deliveryDate;
                            nodeOrderProperties3.SelectSingleNode("StencilTopDeliveryDate").InnerText = deliveryDate;
                            nodeOrderProperties3.SelectSingleNode("StencilBotDeliveryDate").InnerText = deliveryDate;
                            modifyOrderTrack.Append("_Saving_" + PerentBasketId + "_" + Text1.Value + "_");
                            modifyOrderTrack.Append(txtPercentage.Text + "_" + txtUnitPrice1.Text + "_" + hidBrdNetPrice.Value + "_" + txtEcoTransport1.Text + "_" + hdgrossInfo1.Value + "_");

                            int assemblyQuantity = Convert.ToInt32(nodeOrderProperties3.SelectSingleNode("AssemblyQty").InnerText);

                            if ((assemblyQuantity != 0))
                            {

                                modifyOrderTrack.Append(txtAssembly.Text != "" ? txtAssembly.Text : "" + "_" + txtComponents.Text != "" ? txtComponents.Text : "");
                                nodeOrderProperties3.SelectSingleNode("AssemblyTotalNetPrice").InnerText = txtAssembly.Text != "" ? txtAssembly.Text : "0";
                            }

                            if (PerentBasketId.ToLower().Contains("-st"))
                            {
                                nodeOrderProperties3.SelectSingleNode("StencilTopUnitPrice").InnerText = txtUnitPrice1.Text;
                                nodeOrderProperties3.SelectSingleNode("StencilTopTotalNetPrice").InnerText = hidBrdNetPrice.Value;
                                nodeOrderProperties3.SelectSingleNode("StencilTopTransportCost").InnerText = txtEcoTransport1.Text;
                                nodeOrderProperties3.SelectSingleNode("StencilTopTotalOrderPrice").InnerText = hdgrossInfo1.Value;
                            }
                            else if (PerentBasketId.ToLower().Contains("-sb"))
                            {
                                nodeOrderProperties3.SelectSingleNode("StencilBotUnitPrice").InnerText = txtUnitPrice1.Text;
                                nodeOrderProperties3.SelectSingleNode("StencilBotTotalNetPrice").InnerText = hidBrdNetPrice.Value;
                                nodeOrderProperties3.SelectSingleNode("StencilBotTransportCost").InnerText = txtEcoTransport1.Text;
                                nodeOrderProperties3.SelectSingleNode("StencilBotTotalOrderPrice").InnerText = hdgrossInfo1.Value;
                            }
                            else
                            {
                                nodeOrderProperties3.SelectSingleNode("BoardUnitPrice").InnerText = txtUnitPrice1.Text;
                                nodeOrderProperties3.SelectSingleNode("BoardTotalNetPrice").InnerText = hidBrdNetPrice.Value;
                                nodeOrderProperties3.SelectSingleNode("BoardTransportCost").InnerText = txtEcoTransport1.Text;
                                nodeOrderProperties3.SelectSingleNode("BoardTotalOrderPrice").InnerText = hdgrossInfo1.Value;
                            }
                            decimal markup;
                            decimal.TryParse(txtPercentage.Text, out markup);
                            markup = markup / 100;
                            DataTable dtGetMarkup = objgd.GetData("declare @val varchar(max);exec osp_GetMarkup " + markup.ToString() + ",@val out;select @val as Result", Keydata).Tables[0];
                            nodeOrderProperties3.SelectSingleNode("BoardPID").InnerText = Convert.ToString(dtGetMarkup.Rows[0]["Result"]);

                            decimal assemblymarkup;
                            decimal.TryParse(txtAssemblyPricemarkup.Text, out assemblymarkup);
                            assemblymarkup = assemblymarkup / 100;

                            DataTable dtGetMarkupAssembly = objgd.GetData("declare @val varchar(max);exec osp_GetMarkup " + assemblymarkup.ToString() + ",@val out;select @val as Result", Keydata).Tables[0];

                            nodeOrderProperties3.SelectSingleNode("AssemblyPID").InnerText = Convert.ToString(dtGetMarkupAssembly.Rows[0]["Result"]);

                            nodeOrderProperties3.SelectSingleNode("StencilTopPID").InnerText = Convert.ToString(dtGetMarkup.Rows[0]["Result"]);
                            nodeOrderProperties3.SelectSingleNode("StencilBotPID").InnerText = Convert.ToString(dtGetMarkup.Rows[0]["Result"]);
                            modifyOrderTrack.Append("_" + dtGetMarkup.Rows[0]["Result"]);
                            assemblyQty = nodeOrderProperties3.SelectSingleNode("AssemblyQty").InnerText;
                        }
                        catch (Exception ex)
                        {

                        }

                        string sqlTrack = string.Format("exec eccinsertmodifyordertrack '{0}','{1}','{2}'", modifyOrderTrack + "_" + eccUserId.ToString(), eccUserId, SessionId);
                        objgd.PutData(sqlTrack, Keydata);
                        //objException.LogExceptiondata("TrackModifyOrder_" + PerentBasketId, (long)eccUserId, modifyOrderTrack.ToString());
                        if (PerentBasketId.ToUpper().Contains("-ST"))
                        {
                            decimal AfterTransportCostRootCo = Math.Round(Convert.ToDecimal(nodeOrderProperties3.SelectSingleNode("StencilTopTransportCost").InnerText), 3);
                            nodeOrderProperties3.SelectSingleNode("StencilTopTransportCost").InnerText = Math.Round(AfterTransportCostRootCo * Transport_Price_Marker, 3).ToString();
                        }
                        else if (PerentBasketId.ToUpper().Contains("-SB"))
                        {
                            decimal AfterTransportCostRootCo = Math.Round(Convert.ToDecimal(nodeOrderProperties3.SelectSingleNode("StencilBotTransportCost").InnerText), 3);
                            nodeOrderProperties3.SelectSingleNode("StencilBotTransportCost").InnerText = Math.Round(AfterTransportCostRootCo * Transport_Price_Marker, 3).ToString();
                        }
                        else
                        {
                            decimal AfterTransportCostRootCo = Math.Round(Convert.ToDecimal(nodeOrderProperties3.SelectSingleNode("BoardTransportCost").InnerText), 3);
                            nodeOrderProperties3.SelectSingleNode("BoardTransportCost").InnerText = Math.Round(AfterTransportCostRootCo * Transport_Price_Marker, 3).ToString();
                        }
                        XMLString = xdoc.OuterXml;
                        XMLString = XMLString.Replace("'", "''");
                    }
                    if (XMLString == null || XMLString == string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "EnableSubmitbutton", "setSubmitbtnEnable();", true);
                        return false;
                    }

                    DateTime estimated_deliverydate = new DateTime();
                    if (hidendeliverydate.Value != null && hidendeliverydate.Value != "")
                    {
                        estimated_deliverydate = Convert.ToDateTime(hidendeliverydate.Value);
                    }
                    else if (datepicker.Value != null && datepicker.Value != "")
                    {
                        estimated_deliverydate = Convert.ToDateTime(datepicker.Value);
                    }
                    bool Is_File_Attached = false;
                    string BasketNo = string.Empty;
                    string sql_bas = string.Format("select * from BasketItems with(nolock) where Order_Number in ('" + Convert.ToString(PerentBasketId) + "')");
                    DataSet dsbas = objAPI.GetData(sql_bas, Keydata);
                    string basketid = "0";
                    if (dsbas != null && dsbas.Tables[0].Rows.Count != 0)
                    {
                        DataTable dtbasket = dsbas.Tables[0];
                        if (dtbasket.Rows.Count != 0)
                        {
                            BasketNo = Convert.ToString(dtbasket.Rows[0]["BasketNo"]);
                            Is_File_Attached = Convert.ToBoolean(dtbasket.Rows[0]["Is_File_Attached"]);
                            basketid = Convert.ToString(dtbasket.Rows[0]["Basket_Id"]);
                            string sql_updbas = string.Format("update BasketItems set Is_Stencil_Fixed='{0}',Estimate_Delivery_Date='{3}',Buildupimg='{4}' where BasketNo='{1}' and Basket_Id ='{2}'", IsStencilFixed, BasketNo, basketid, estimated_deliverydate.ToString("yyyy-MM-dd"), BuidUpCode);
                            objgd.PutData(sql_updbas, Keydata);
                            strerr += "BasketNo- " + Convert.ToString(BasketNo) + "|";
                        }
                    }
                    string TypeSTENCIL = "STENCIL-TOP";
                    if (serviceName.Trim().ToLower() == "stencilservice" && !PerentBasketId.Contains("-S"))
                    {
                        TypeSTENCIL = "STENCIL";
                    }

                    if (PerentBasketId.Contains("-ST") || service.Trim().ToLower() == "stencilservice")
                    {
                        strsql = string.Format("exec osp_InsertStencilOrderFromVisXml '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}'", Company_id, UserId, XMLString.Replace("'", "''"), "ECC", IP_add, Mode, Convert.ToString(dsOrder.Tables[0].Rows[0]["IsStencilOnlyOrder"]), "0", "1", Convert.ToInt64(dsOrder.Tables[0].Rows[0]["OrderId"]), TypeSTENCIL, DeliveryAddressId, invoiceaddressId, LangCode, "0");
                    }
                    else if (PerentBasketId.Contains("-SB"))
                    {
                        strsql = string.Format("exec osp_InsertStencilOrderFromVisXml '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}'", Company_id, UserId, XMLString.Replace("'", "''"), "ECC", IP_add, Mode, Convert.ToString(dsOrder.Tables[0].Rows[0]["IsStencilOnlyOrder"]), "0", "1", Convert.ToInt64(dsOrder.Tables[0].Rows[0]["OrderId"]), "STENCIL-BOT", DeliveryAddressId, invoiceaddressId, LangCode, "0");
                    }
                    else
                    {
                        strsql = string.Format("exec osp_InsertOrderFromVisXml '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}',{13},'{14}',{15}", Convert.ToInt64(dsOrder.Tables[0].Rows[0]["CompanyId"]), Convert.ToInt64(dsOrder.Tables[0].Rows[0]["CustUserId"]), XMLString.Replace("'", "''"), "ECC", IP_add, Mode, 1, 0, Convert.ToInt64(dsOrder.Tables[0].Rows[0]["OrderId"]), Type, Convert.ToInt64(dsOrder.Tables[0].Rows[0]["DeliveryAddressId"]), invoiceaddressId, LangCode, 0, basketid, IsStencilFixed);
                    }
                    DataSet ds_order1 = objAPI.GetData(strsql, Keydata);
                    strerr += "strsql- " + Convert.ToString(strsql) + "|";
                    if (ds_order1 != null)
                    {
                        if (ds_order1.Tables[0].Rows.Count > 0)
                        {
                            string NewOrderNumber = Convert.ToString(ds_order1.Tables[0].Rows[0][0]);
                        }
                    }
                    try
                    {
                        if (PerentBasketId != null && PerentBasketId != "")
                        {
                            if (Convert.ToInt32(dsOrder.Tables[0].Rows[0]["CompanyId"]) == Company_id)
                            {

                                string sqlUpdateDelAdd = string.Format("update genOrders SET DeliveryAddressId = '{0}', DeliveryDate = convert (VARCHAR(50), '{2}' ,121),InvoiceAddressId={3}  WHERE OrderNumber = '{1}'", ddlDELADDRESS.SelectedValue, PerentBasketId, Text1.Value, ddlInvoicesaddress.SelectedValue);

                                strerr += "sqlUpdateDelAdd- " + Convert.ToString(sqlUpdateDelAdd) + "|";
                                strerr += "ddlDELADDRESS- " + Convert.ToString(ddlDELADDRESS.SelectedItem.Text) + "|";
                                strerr += "ddlInvoicesaddress- " + Convert.ToString(ddlInvoicesaddress.SelectedItem.Text) + "|";
                                strerr += "Update of Genorder table time CompanyID- " + Convert.ToString(Company_id) + "|";
                                objgd.PutData(sqlUpdateDelAdd, Keydata);
                                try
                                {
                                    objgd.PutData(string.Format("exec Ecc_UpdatePrepDueDate '{0}'", PerentBasketId), Keydata);
                                    strerr += "Ecc_UpdatePrepDueDate- " + Convert.ToString(PerentBasketId) + "|";
                                }
                                catch
                                {

                                }
                            }
                            //#INPUTMASK

                            ec09Service.ValidateOrderInputMask(PerentBasketId);
                        }
                    }
                    catch (Exception)
                    { }

                    long Status_Id = 0;
                    Status_Id = ec09Service.GetCodeId("BasketReadyToCheckout");
                    long ActionID = ec09Service.GetCodeId("ACTION_BASKETPLACEORDER");
                    long DocumentId = 0;


                    if (Is_File_Attached)
                    {
                        DocumentId = Convert.ToInt32(basketid);
                    }
                    if (Convert.ToString(basketid) != "0")
                    {
                        try
                        {
                            ec09Service.GenerateHistory(Convert.ToInt64(basketid), "BASKET", eccUserId, Status_Id, ActionID, IP_add, DocumentId, BasketNo);
                            //objUserlog.InsertUserlogForGenHistory(UserId.ToString(), "", Company_id.ToString(), "", Convert.ToString(DateTime.UtcNow), Convert.ToString(basketid), Convert.ToString(BasketNo), ActionID.ToString(), "Place order", "", IP_add, eccUserId.ToString(), "", "", "", "ECC");
                        }
                        catch { }
                    }


                    long Actionmodifyorder = ec09Service.GetCodeId("ACTION_MODIFIED");
                    //if (objorder != null)
                    {
                        try
                        {

                            long ECCuserid = 0;
                            long.TryParse(Convert.ToString(eccUserId), out ECCuserid);

                            string sqlId = string.Format("select orderid,ordstatusid,CustUserId,CompanyId from genOrders  with(nolock)  where OrderNumber ='{0}'", PerentBasketId);
                            DataTable dtId = objgd.GetData(sqlId, Keydata).Tables[0];
                            long OrderId = Convert.ToInt64(dtId.Rows[0]["orderid"]);
                            int StatusId = Convert.ToInt32(dtId.Rows[0]["ordstatusid"]);
                            string UserId = dtId.Rows[0]["CustUserId"].ToString();
                            string CompanyId = dtId.Rows[0]["CompanyId"].ToString();
                            ec09Service.GenerateOrderHistory(OrderId, (long)ECCuserid, StatusId, Convert.ToInt32(Actionmodifyorder), IP_add, operatorid, 0, 0, 0, 0, 1, PerentBasketId, "FE");
                        }
                        catch { }
                        //ObjInsHistory.InsertInvoiceNoInOrderHistory(Convert.ToInt64(dsOrder.Tables[0].Rows[0]["OrderId"]), ECCuserid, Convert.ToInt64(objorder.OrdStatusID), Actionmodifyorder, 0, EC09WebService.EC09WebService.GetClientIP(), 0, 0, 0, Convert.ToString(Convert.ToString(objorder.OrderNumber)));
                    }

                    string TranseportCost = null;
                    //genFileServer objGenFileServer = objData.genFileServers.Where(p => p.IsActive == true).Single();
                    //ManageFiles objfilemanagement = new ManageFiles();

                    //var objfile = objData.GenFiles.Where(gf => gf.Number == Convert.ToString(PerentBasketId) && gf.FileName.ToLower() == (Convert.ToString(PerentBasketId).ToLower() + ".xml") && gf.FileTypeCode == "ORD" && (gf.IsDeleted == null || gf.IsDeleted == false) && (gf.IsModified == null || gf.IsModified == false)).Take(1).SingleOrDefault();

                    //if (objGenFileServer.IsRequireImpersonation == true)
                    string jsonfilepath = "";
                    {
                        System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = null;
                        impersonateOnFileServer = sparrowFeApp.core.Impersonation.ImpersonateFileServer(sparrowFeApp.core.Constant.FileServerUsername, sparrowFeApp.core.Constant.FileServerPassword, sparrowFeApp.core.Constant.FileServerDomain);
                        if (impersonateOnFileServer != null)
                        {
                            string path = sparrowFeApp.core.Common.getFilePath(PerentBasketId);
                            jsonfilepath = path;
                            if (path != "")
                            {
                                XmlDocument xdoc = new XmlDocument();
                                xdoc.LoadXml(Convert.ToString(XMLString.Trim()));
                                System.Xml.XmlNode nodeCustomerProperties3 = xdoc.SelectSingleNode("OrderDetail/CustomerProperties3");
                                System.Xml.XmlNode nodeOrderProperties3 = xdoc.SelectSingleNode("OrderDetail/OrderProperties3");
                                System.Xml.XmlNode nodeBoardProperties3 = xdoc.SelectSingleNode("OrderDetail/BoardProperties3");
                                System.Xml.XmlNode nodeProductionProperties3 = xdoc.SelectSingleNode("OrderDetail/ProductionProperties3");
                                nodeBoardProperties3.SelectSingleNode("BoardName").InnerText = PCBName;
                                nodeOrderProperties3.SelectSingleNode("CustomerPurchaseReference").InnerText = PurchaseRef;
                                nodeOrderProperties3.SelectSingleNode("CustomerProjectReference").InnerText = ProjectRef;
                                nodeOrderProperties3.SelectSingleNode("CustomerArticleReference ").InnerText = ArticalRef;
                                TranseportCost = nodeOrderProperties3.SelectSingleNode("BoardTransportCost").InnerText;
                                //xdoc.Save(objGenFileServer.RootPath + objfile.FilePath + "\\" + Convert.ToString(PerentBasketId) + ".xml");

                                DataSet assemblydate = objgd.GetData("exec getAssemblyDeliveryDate '" + PerentBasketId + "'", Keydata);
                                string AssemblyDeliveryDate = "";
                                if (assemblydate != null && assemblydate.Tables.Count > 0 && assemblydate.Tables[0].Rows.Count > 0)
                                {
                                    AssemblyDeliveryDate = assemblydate.Tables[0].Rows[0]["AssemblyDeliveryDate"].ToString();
                                }
                                if (!checkFullcheck.Checked)
                                {
                                    string sql_fc = string.Format("declare @IsFC bit=0 exec osp_CheckFullcheckFlag '{0}',@IsFC out; select @IsFC as IsFC", Convert.ToString(PerentBasketId));
                                    DataSet ds_fc = objAPI.GetData(sql_fc, Keydata);
                                    if (ds_fc != null)
                                    {
                                        DataTable dt_fc = ds_fc.Tables[0];
                                        if (dt_fc.Rows.Count > 0)
                                        {
                                            bool.TryParse(Convert.ToString(dt_fc.Rows[0]["IsFC"]), out IsFC);
                                            if (IsFC == true)
                                            {
                                                checkFullcheck.Checked = true;
                                            }
                                        }
                                    }
                                }
                                IsFC = checkFullcheck.Checked;
                                IsPlatedSolts = checkplatedslots.Checked;
                                ISPackWithPaper = ChkPackWithPaper.Checked;
                                BoardLock = checkBrdlock.Checked;
                                nodeOrderProperties3.SelectSingleNode("AssemblyDeliveryDate").InnerText = AssemblyDeliveryDate;
                                nodeProductionProperties3.SelectSingleNode("FullCheck").InnerText = Convert.ToString(IsFC).ToLower();
                                nodeProductionProperties3.SelectSingleNode("PlatedSlots").InnerText = Convert.ToString(IsPlatedSolts).ToLower();
                                nodeOrderProperties3.SelectSingleNode("BoardLocked").InnerText = Convert.ToString(BoardLock);
                                nodeProductionProperties3.SelectSingleNode("PackWithPaper").InnerText = Convert.ToString(ISPackWithPaper).ToLower();
                                nodeOrderProperties3.SelectSingleNode("StencilTopLocked").InnerText = Convert.ToString(BoardLock);
                                nodeOrderProperties3.SelectSingleNode("StencilBotLocked").InnerText = Convert.ToString(BoardLock);
                                //string Directstr = "AssemblyDeliveryDate$" + AssemblyDeliveryDate + "|IsFC$" + Convert.ToString(IsFC).ToLower() +
                                //    "|PlatedSlots$" + Convert.ToString(IsPlatedSolts).ToLower() +
                                //    "|BoardLocked$" + Convert.ToString(BoardLock) +
                                //    "|StencilTopLocked$" + Convert.ToString(BoardLock) +
                                //    "|StencilBotLocked$" + Convert.ToString(BoardLock) +
                                //"|PackWithPaper$" + Convert.ToString(ISPackWithPaper).ToLower();
                                xdoc.Save(path + "\\" + Convert.ToString(PerentBasketId) + ".xml");
                            }
                        }
                    }

                    string sqlUpdateOrderDetail = string.Format("UPDATE genOrders SET PCBName = '{0}' , OrderRef = '{1}' ,OrderRef3 = '{2}' ,YourRefNo = '{3}',HandlingCompanyId='{6}',Is_Locked='{7}',IsFC='{8}',IsPlatedSlots='{9}',IsPreproduction='{10}',PackWithPaper ='{11}' WHERE OrderNumber = '{4}';update GenFiles SET IsModified = 1 , ModifiedBy = '{5}' where Number = '{4}' AND FileTypeCode = 'ORD_Receipt' AND isnull(ModifiedBy,0)  = 0 AND isnull(IsDeleted,0)  = 0", PCBName, ProjectRef, ArticalRef, PurchaseRef, PerentBasketId, eccUserId, ddlHanlingcomp.SelectedValue, checkBrdlock.Checked, checkFullcheck.Checked, checkplatedslots.Checked, checkPreproduct.Checked, ChkPackWithPaper.Checked);
                    objgd.PutData(sqlUpdateOrderDetail, Keydata);

                    strsql = string.Format("exec osp_getbasketitemforcheckout '{0}'", basketid);
                    DataSet dsPrice = objAPI.GetData(strsql, Keydata);
                    DataTable dt = dsPrice.Tables[1];
                    decimal totalprice = 0;
                    decimal totaltransport = 0;
                    string TotalPrice_Str = Convert.ToString(dt.Rows[0]["Net_Total_Price_sum"]);
                    string TotalTransport_Str = Convert.ToString(dt.Rows[0]["Total_Trasport_Cost"]); if (decimal.TryParse(TotalPrice_Str, out totalprice)) { }
                    if (decimal.TryParse(TotalTransport_Str, out totaltransport))
                    { }
                    string TotalPrice = Convert.ToString(Math.Round(totalprice, 2));
                    string TotalTransport = Convert.ToString(Math.Round(totaltransport, 2));
                    string VATXml = getVATPer();
                    decimal vatper = 0;
                    if (decimal.TryParse(VATXml, out vatper))
                    { }
                    decimal VAT = (totalprice + totaltransport) * vatper;
                    long CustCompany = Company_id; decimal Transeport = Convert.ToDecimal(TranseportCost); decimal vatcost = VAT; decimal vatperr = Convert.ToDecimal(VATXml) * 100;
                    string FstOrderNumber = PerentBasketId.ToUpper();

                    Common.UpdateOrderDetailFromEC(PerentBasketId);

                    jsonfilepath = jsonfilepath.Replace(Constant.FileServerPath, "");
                    if (jsonfilepath != "")
                    { 
                        var feApp = new { fun_name = "file_sync", entity_number = PerentBasketId, file_name = Convert.ToString(PerentBasketId) + ".xml", file_type = "modify_order", file_path = "\\"+jsonfilepath + "\\", session_id = SessionId, parent_basket_id = PerentBasketId, language_id = LanguageId.ToString(), cust_company = CustCompany, vat = VAT.ToString(), vat_per = vatperr.ToString(), assembly_qty = assemblyQty.ToString(), crm_rel_id = AppSessions.EccUserId, source = "FE", target = "EC" };
                        string jsonFeApp = JsonConvert.SerializeObject(feApp);
                        Common feAppData = new Common();
                        feAppData.FeAppAPI(jsonFeApp);
                    } 

                    //FE API
                    //WUCUploaddata objupl = new WUCUploaddata();
                    string strrtun = ChangeCustDataAndSIdata(SessionId, PerentBasketId, "");
                    //string[] arry = strrtun.Split('|');
                    //bool IsSentToI8 = (arry[1] == "1" ? true : false);
                    //if (!IsSentToI8)
                    //{
                    //    string temppath = objGenFileServer.RootPath + objfile.FilePath + "\\visualizer";
                    //    string returnmsg = objoffer.ReplaceBuildUpImg(Convert.ToString(PerentBasketId), temppath, "order", Convert.ToString(PerentBasketId) + "_buildup.png");
                    //}
                    //try
                    //{
                    //    objorderdata.GenerateReceiptsOrderForECC(LanguageId, Convert.ToInt64(CustCompany), FstOrderNumber, PerentBasketId, Transeport, VAT, PCBName, PCBName, ArticalRef, PurchaseRef, true, vatperr);
                    //}
                    //catch (Exception ex)
                    //{
                    //    objException.LogExceptiondata("Order receipt error ", 0, Convert.ToString(PerentBasketId) + " - " + ex.Message);
                    //}
                    //if (!PerentBasketId.Contains("-"))
                    //{
                    //    if (Convert.ToInt16(assemblyQty) > 0)
                    //    {
                    //        objbpo.SyncAssemblyOrderDetail(PerentBasketId + "-A", PerentBasketId, XMLString);
                    //        //objbpo.SplitAssemblyOrder(PerentBasketId, XMLString);
                    //    }
                    //}
                    divimg.Visible = false;
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "", "window.parent.location.href = 'orders/eccordermodified.aspx?successmsgtitle=InquiryModified&entityno=" + PerentBasketId + "&successmsgdesc=order modified.&action=ordermodify';", true);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool processPPAOrder()
        {
            return false;
        }

        private bool placenewOffer()
        {
            return true;

        }
        private bool placenewInquiry()
        {
            return false;
        }

        private string GetVariantXml(int Variants)
        {
            try
            {
                string VariantXml = "<PlaceReqDayQuientyParameters>";
                switch (Variants)
                {
                    case 1:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm01.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty01.Text + "\"></Parameters>";
                        break;
                    case 2:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm02.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty02.Text + "\"></Parameters>";
                        break;
                    case 3:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm03.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty03.Text + "\"></Parameters>";
                        break;
                    case 4:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm04.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty04.Text + "\"></Parameters>";
                        break;
                    case 5:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm05.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty05.Text + "\"></Parameters>";
                        break;
                    case 6:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm06.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty06.Text + "\"></Parameters>";
                        break;
                    case 7:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm07.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty07.Text + "\"></Parameters>";
                        break;
                }
                VariantXml += "</PlaceReqDayQuientyParameters>";
                return VariantXml;
            }
            catch
            {
                return "";
            }
        }
        private bool placenewOrder()
        {
            return false;
        }

        private bool IsHaveDueInvoices(long CompId)
        {
            bool IsStatus = false;
            string strsql = string.Format("exec spGetDueInvoices '{0}'", CompId);
            DataSet ds = objAPI.GetData(strsql, Keydata);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (bool.TryParse(Convert.ToString(ds.Tables[0].Rows[0]["IsOverDue"]), out IsStatus))
                    { }
                    return IsStatus;
                }
            }
            return IsStatus;
        }
        private bool IsOverCredit(long CompId)
        {
            bool IsStatus = false;
            string strsql = string.Format("exec spIsHaveCreditLimit '{0}',{1}", CompId, 0);
            DataSet ds = objAPI.GetData(strsql, Keydata);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (bool.TryParse(Convert.ToString(ds.Tables[0].Rows[0]["IsHaveCreditLimit"]), out IsStatus))
                    { }
                    return IsStatus;
                }
            }
            return IsStatus;
        }




        #region getVetPercentage
        protected string getVATPer()
        {
            try
            {
                string CountryCode = ""; long CountryId = 0;
                DataSet dsGetCountry = objAPI.GetData("exec osp_GetDeliveryCountrybyCompany '" + Company_id + "'," + Convert.ToInt64(DeliveryAddressId), Keydata);
                if (dsGetCountry.Tables[0].Rows.Count == 0)
                {
                    dsGetCountry = objAPI.GetData("exec osp_GetDeliveryCountrybyCompany '" + Company_id + "',0", Keydata);
                    if (dsGetCountry.Tables[0].Rows.Count != 0)
                    {
                        CountryId = Convert.ToInt64(dsGetCountry.Tables[0].Rows[0]["DeliveryCountryId"]);
                    }
                }
                else
                {
                    CountryId = Convert.ToInt64(dsGetCountry.Tables[0].Rows[0]["DeliveryCountryId"]);
                }
                DataTable dtcountrycode = objAPI.GetData("select Initial from admCountries with(nolock) where CountryId=" + CountryId, Keydata).Tables[0];
                if (dtcountrycode.Rows.Count != 0)
                {
                    CountryCode = Convert.ToString(dtcountrycode.Rows[0]["Initial"]);
                }
                string Invoicecountrycode = "";
                DataTable dtcountrycodeinvoice = objAPI.GetData("SELECT (select Initial from admCountries with(nolock) where CountryId=InvoiceCountryId ) as Invoicecountrycode from genSearchCustomer with(nolock) where CustomerID = " + Company_id, Keydata).Tables[0];
                if (dtcountrycode.Rows.Count != 0)
                {
                    Invoicecountrycode = Convert.ToString(dtcountrycodeinvoice.Rows[0]["Invoicecountrycode"]);
                }


                string SqlGetVatDetail = string.Format("spgetEucountruCode '{0}'", Company_id);
                System.Data.DataTable dtGetVatDetail = objAPI.GetData(SqlGetVatDetail, Keydata).Tables[0];
                string vatCountryCode = Convert.ToString(dtGetVatDetail.Rows[0]["vatCountryCode"]);
                string noVAT = Convert.ToString(dtGetVatDetail.Rows[0]["noVAT"]);
                string IsBusinessCustomer = Convert.ToString(dtGetVatDetail.Rows[0]["IsBusinessCustomer"]);
                string handlingCompany = Convert.ToString(dtGetVatDetail.Rows[0]["handlingCompany"]);

                if (noVAT.ToLower() == "false")
                {
                    noVAT = "0";
                }
                else
                {
                    noVAT = "1";
                }

                string configurl = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["servlets"]);
                string strURL = configurl + "/servlets/VATCalculatorServlet?destinationCountry=" + CountryCode.ToUpper() + "&invoiceCountry=" + Invoicecountrycode.ToUpper() + "&handlingCompany=" + handlingCompany + "&vatCountry=" + vatCountryCode + "&noVAT=" + noVAT + "&timestamp=" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
                HttpWebRequest request = WebRequest.Create(strURL) as HttpWebRequest;
                request.UserAgent = "MSIE 6.0";
                request.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(responseFromServer);

                System.Xml.XmlNode nodePrice = doc.SelectSingleNode("Reply/Price");
                string responsedata = nodePrice.Attributes["vatPercent"].Value;
                decimal value = 0;
                decimal.TryParse(responsedata, out value);
                value = value / 100;
                return Convert.ToString(value);
            }
            catch (Exception ex)
            {

                return "0.00";
            }
        }
        #endregion
        private bool VarifyPlaceInqData()
        {
            try
            {
                #region Variant Validation
                int TotalVariants = 1;

                if (LbtnAdd01.AccessKey != "")
                    int.TryParse(LbtnAdd01.AccessKey, out TotalVariants);
                switch (TotalVariants)
                {
                    case 1:
                        if (!IsValidQtyForVariant(TxtQty01))
                        {
                            error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text");
                            diverrortd.Attributes["style"] = "display:block";
                            return false;
                        }
                        break;
                    case 2:
                        if (!IsValidQtyForVariant(TxtQty01)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty02)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!checkForDuplicateVariant(TotalVariants)) { error.Text = objbase.GetLocalResourceObject("Msgduplicatevariant.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        break;
                    case 3:
                        if (!IsValidQtyForVariant(TxtQty01)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty02)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty03)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!checkForDuplicateVariant(TotalVariants)) { error.Text = objbase.GetLocalResourceObject("Msgduplicatevariant.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        break;
                    case 4:
                        if (!IsValidQtyForVariant(TxtQty01)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty02)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty03)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty04)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!checkForDuplicateVariant(TotalVariants)) { error.Text = objbase.GetLocalResourceObject("Msgduplicatevariant.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        break;
                    case 5:
                        if (!IsValidQtyForVariant(TxtQty01)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty02)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty03)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty04)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty05)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!checkForDuplicateVariant(TotalVariants)) { error.Text = objbase.GetLocalResourceObject("Msgduplicatevariant.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        break;
                    case 6:
                        if (!IsValidQtyForVariant(TxtQty01)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty02)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty03)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty04)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty05)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty06)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!checkForDuplicateVariant(TotalVariants)) { error.Text = objbase.GetLocalResourceObject("Msgduplicatevariant.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        break;
                    case 7:
                        if (!IsValidQtyForVariant(TxtQty01)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty02)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty03)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty04)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty05)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty06)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!IsValidQtyForVariant(TxtQty07)) { error.Text = objbase.GetLocalResourceObject("MsgvariantQtyRequire.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        if (!checkForDuplicateVariant(TotalVariants)) { error.Text = objbase.GetLocalResourceObject("Msgduplicatevariant.Text"); diverrortd.Attributes["style"] = "display:block"; return false; }
                        break;
                }
                #endregion
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private bool IsValidQtyForVariant(TextBox txt)
        {
            int Qty = 0;
            int.TryParse(txt.Text, out Qty);
            if (Qty < 1)
            {
                return false;
            }
            return true;
        }
        private void chkStencilfixvisibility()
        {
            if (Request["PanelECRegComp"] != null)
            {
                string strPanelECRegComp = Convert.ToString(Request["PanelECRegComp"]);
                string strStencilTopQty = string.Empty;
                string strStencilBotQty = string.Empty;
                long lStencilTopQty = 0;
                long lStencilBotQty = 0;
                if (strPanelECRegComp.Trim().ToLower() == "true")
                {
                    if (Request["StencilBotQty"] != null && Convert.ToString(Request["StencilBotQty"]).Trim().ToLower() != "")
                    {
                        strStencilBotQty = Convert.ToString(Request["StencilBotQty"]).Trim().ToLower();
                        //divecstencilfix.Attributes["style"] = "display:inline-table";
                    }
                    if (Request["StencilTopQty"] != null && Convert.ToString(Request["StencilTopQty"]).Trim().ToLower() != "")
                    {
                        strStencilTopQty = Convert.ToString(Request["StencilTopQty"]).Trim().ToLower();
                        //divecstencilfix.Attributes["style"] = "display:inline-table";
                    }
                    long.TryParse(strStencilTopQty, out lStencilTopQty);
                    long.TryParse(strStencilBotQty, out lStencilBotQty);
                    if (lStencilTopQty > 0 || lStencilBotQty > 0)
                    {
                        divecstencilfix.Attributes["style"] = "display:inline-table";
                    }
                    else
                    {
                        divecstencilfix.Attributes["style"] = "display:none";
                        chkStencilFixed.Checked = false;
                    }
                }
                else
                {
                    divecstencilfix.Attributes["style"] = "display:none";
                    chkStencilFixed.Checked = false;
                }
            }
            else
            {
                divecstencilfix.Attributes["style"] = "display:none";
                chkStencilFixed.Checked = false;
            }
        }


        #region
        private string isFileUploaded(string OrderNo, string IsException, bool IsfromOrder)
        {

            //try
            //{

            //    if (UploadGerber.HasFile)
            //    {
            //        long FileSize = UploadGerber.PostedFile.ContentLength;
            //        if (FileSize <= 15360000)
            //        {
            //            string filepath = UploadGerber.PostedFile.FileName;
            //            string ext = filepath.Substring(filepath.LastIndexOf('.') + 1);
            //            //var Files = filepath.Split('.');
            //            //string Subfilename = filepath.Substring(0, filepath.LastIndexOf('.'));
            //            string Subfilename = UploadGerber.FileName;
            //            Subfilename = Regex.Replace(Subfilename, @"[^0-9a-zA-Z . , _ -]", "");
            //            if (ext.ToLower() == "zip" || ext.ToLower() == "rar" || ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb") //if (Files[1].ToLower() == "zip")
            //            {
            //                string s;
            //                DataSet ds = null;
            //                int sessionCount = 0;
            //                string singlesessionid = null;
            //                s = String.Format("select Sessionid from i8response where Sessionid like '%{0}%'", SessionId);
            //                ds = objgd.GetData(s, Keydata);
            //                sessionCount = Convert.ToInt32(ds.Tables[0].Rows.Count);
            //                for (int i = 0; i < sessionCount; i++)
            //                {

            //                    if (Convert.ToString(ViewState["Status"]).ToLower() != "BasketAnalysis".ToLower())
            //                    {
            //                        singlesessionid = Convert.ToString(ds.Tables[0].Rows[i]["Sessionid"]);
            //                    }
            //                    else
            //                    {
            //                        singlesessionid = SessionId;
            //                    }
            //                    //string file_ext = ext.ToLower();// Files[1];
            //                    string file_ext = System.IO.Path.GetExtension(UploadGerber.PostedFile.FileName);
            //                    string filename = Subfilename;// Files[0];
            //                    string file = "";
            //                    if (IsException == "true")
            //                    { file = OrderNo + file_ext; }
            //                    else
            //                    {
            //                        file = "temp" + file_ext;
            //                    }
            //                    genFileServer objGenFileServer = objData.genFileServers.Where(p => p.IsActive == true).Single();
            //                    ManageFiles.UserID = (long)UserId;
            //                    ManageFiles objfilemanagement = new ManageFiles();
            //                    string FolderPath = "";
            //                    string inputfilepath = string.Empty;
            //                    string strFilepath = string.Empty;
            //                    //if (ViewState["Type"].ToString().ToLower() != "newbasket" && ViewState["Type"].ToString().ToLower() != "modifyBasket".ToLower() && ViewState["Type"].ToString().ToLower() != "modifyOrder".ToLower() && Convert.ToString(ViewState["Type"]).ToLower() != "repeatOrder".ToLower())
            //                    //    FolderPath = objfilemanagement.ManageDirectoriesForPDF("Order_Files");
            //                    //else
            //                    //    FolderPath = objGenFileServer.RootPath + "tempbasket\\";

            //                    if (IsException == "true")
            //                    {
            //                        if (IsfromOrder == true)
            //                        {
            //                            FolderPath = objfilemanagement.ManageDirectoriesForPDF("Order_Files");
            //                        }
            //                        else
            //                        {
            //                            FolderPath = objfilemanagement.ManageDirectoriesForPDF("INQ_Files");
            //                        }
            //                    }
            //                    else
            //                    {
            //                        FolderPath = objGenFileServer.RootPath + "tempbasket\\";
            //                    }

            //                    try
            //                    {
            //                        if (objGenFileServer.IsRequireImpersonation == true)
            //                        {
            //                            WindowsImpersonationContext impersonateOnFileServer = null;
            //                            impersonateOnFileServer = ImpersonateOnFileServer(objGenFileServer.Username, ConfigurationManager.AppSettings["FileServerDomain"].ToString(), objGenFileServer.Password);
            //                            if (impersonateOnFileServer != null)
            //                            {
            //                                //if (ViewState["Type"].ToString().ToLower() == "newbasket" || ViewState["Type"].ToString().ToLower() == "modifyBasket".ToLower() || ViewState["Type"].ToString().ToLower() == "modifyOrder".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "repeatOrder".ToLower())
            //                                if (IsException != "true")
            //                                {
            //                                    var getOrderCount = new EC09WebApp.API.gd();
            //                                    if (!Directory.Exists(FolderPath + singlesessionid))
            //                                    { Directory.CreateDirectory(FolderPath + singlesessionid); }


            //                                    //UploadGerber.PostedFile.SaveAs(FolderPath + SessionId + "\\" + OrdCount + "\\" + file);
            //                                    //string strFilepath = FolderPath + SessionId + "\\" + OrdCount + "\\" + file;
            //                                    //BRDToZIP(strFilepath, ext);
            //                                    if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                    {
            //                                        UploadGerber.PostedFile.SaveAs(FolderPath + singlesessionid + "\\" + filename);
            //                                        inputfilepath = FolderPath + singlesessionid + "\\" + filename;
            //                                    }
            //                                    else
            //                                    {
            //                                        UploadGerber.PostedFile.SaveAs(FolderPath + singlesessionid + "\\" + file);
            //                                    }
            //                                    strFilepath = FolderPath + singlesessionid + "\\" + file;
            //                                    BRDToZIP(strFilepath, ext, inputfilepath);
            //                                    KicadToZIP(strFilepath, ext, inputfilepath);
            //                                    // delete brd file
            //                                    if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                    {
            //                                        if (File.Exists(FolderPath + singlesessionid + "\\" + filename))
            //                                        {
            //                                            File.Delete(FolderPath + singlesessionid + "\\" + filename);
            //                                        }
            //                                    }
            //                                }
            //                                else
            //                                {
            //                                    if (!Directory.Exists(FolderPath + OrderNo))
            //                                    {
            //                                        Directory.CreateDirectory(FolderPath + OrderNo);
            //                                    }
            //                                    //UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + file);
            //                                    //string strFilepath = FolderPath + OrderNo + "\\" + file;
            //                                    //BRDToZIP(strFilepath, ext);
            //                                    if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                    {
            //                                        UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + filename);
            //                                        inputfilepath = FolderPath + OrderNo + "\\" + filename;
            //                                    }
            //                                    else
            //                                    {
            //                                        UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + file);
            //                                    }
            //                                    strFilepath = FolderPath + OrderNo + "\\" + file;
            //                                    BRDToZIP(strFilepath, ext, inputfilepath);
            //                                    KicadToZIP(strFilepath, ext, inputfilepath);

            //                                    // delete brd file
            //                                    if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                    {
            //                                        if (File.Exists(FolderPath + OrderNo + "\\" + filename))
            //                                        {
            //                                            File.Delete(FolderPath + OrderNo + "\\" + filename);
            //                                        }
            //                                    }
            //                                }

            //                            }
            //                        }
            //                        else
            //                        {
            //                            //if (ViewState["Type"].ToString().ToLower() == "newbasket" || ViewState["Type"].ToString().ToLower() == "modifyBasket".ToLower() || ViewState["Type"].ToString().ToLower() == "modifyOrder".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "repeatOrder".ToLower())
            //                            if (IsException != "true")
            //                            {
            //                                var getOrderCount = new EC09WebApp.API.gd();
            //                                if (!Directory.Exists(FolderPath + singlesessionid))
            //                                {
            //                                    Directory.CreateDirectory(FolderPath + singlesessionid);
            //                                }


            //                                //UploadGerber.PostedFile.SaveAs(FolderPath + SessionId + "\\" + OrdCount + "\\" + file);
            //                                //string strFilepath = FolderPath + SessionId + "\\" + OrdCount + "\\" + file;
            //                                //BRDToZIP(strFilepath, ext);
            //                                if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                {
            //                                    UploadGerber.PostedFile.SaveAs(FolderPath + Session["SessionId"].ToString() + "\\" + filename);
            //                                    inputfilepath = FolderPath + Session["SessionId"].ToString() + "\\" + filename;
            //                                }
            //                                else
            //                                {
            //                                    UploadGerber.PostedFile.SaveAs(FolderPath + Session["SessionId"].ToString() + "\\" + file);
            //                                }
            //                                strFilepath = FolderPath + Session["SessionId"].ToString() + "\\" + file;
            //                                BRDToZIP(strFilepath, ext, inputfilepath);
            //                                KicadToZIP(strFilepath, ext, inputfilepath);
            //                                // delete brd file
            //                                if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                {
            //                                    if (File.Exists(FolderPath + Session["SessionId"].ToString() + "\\" + filename))
            //                                    {
            //                                        File.Delete(FolderPath + Session["SessionId"].ToString() + "\\" + filename);
            //                                    }
            //                                }
            //                            }
            //                            else
            //                            {
            //                                if (!Directory.Exists(FolderPath + OrderNo))
            //                                { Directory.CreateDirectory(FolderPath + OrderNo); }
            //                                //UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + file);
            //                                //string strFilepath = FolderPath + OrderNo + "\\" + file;
            //                                //BRDToZIP(strFilepath, ext);
            //                                if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                {
            //                                    UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + filename);
            //                                    inputfilepath = FolderPath + OrderNo + "\\" + filename;
            //                                }
            //                                else
            //                                {
            //                                    UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + file);
            //                                }
            //                                strFilepath = FolderPath + OrderNo + "\\" + file;
            //                                BRDToZIP(strFilepath, ext, inputfilepath);
            //                                KicadToZIP(strFilepath, ext, inputfilepath);
            //                                // delete brd file
            //                                if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                {
            //                                    if (File.Exists(FolderPath + OrderNo + "\\" + filename))
            //                                    {
            //                                        File.Delete(FolderPath + OrderNo + "\\" + filename);
            //                                    }
            //                                }
            //                            }
            //                        }
            //                        //if (ViewState["Type"].ToString().ToLower() != "newbasket" && ViewState["Type"].ToString().ToLower() != "modifyBasket".ToLower() &&   ViewState["Type"].ToString().ToLower() != "modifyOrder".ToLower() && Convert.ToString(ViewState["Type"]).ToLower() != "repeatOrder".ToLower())
            //                        if (IsException == "true")
            //                        {
            //                            if (IsfromOrder == true)
            //                            {
            //                                EC09DataContext dcEC09 = new EC09DataContext();
            //                                //var genFile = dcEC09.GenFiles.Where(u => u.FileName == OrderNo + file_ext.Replace("brd", "zip") && u.FileTypeCode == "ORD" && (u.IsDeleted == null || u.IsDeleted == false)).ToList();
            //                                var genFile = dcEC09.GenFiles.Where(u => u.FileName == OrderNo + file_ext.Replace("brd", "zip").Replace("kicad_pcb", "zip") && u.FileTypeCode == "ORD" && (u.IsDeleted == null || u.IsDeleted == false)).ToList();
            //                                foreach (GenFile gf in genFile)
            //                                {
            //                                    gf.IsModified = true;
            //                                    gf.ModifiedDate = DateTime.UtcNow;
            //                                    gf.ModifiedBy = UserId;
            //                                    dcEC09.SubmitChanges();
            //                                }

            //                                GenFile objGenFile = new GenFile();
            //                                objGenFile.Number = OrderNo;
            //                                objGenFile.FileTypeCode = "ORD";
            //                                objGenFile.FilePath = FolderPath.Replace(objGenFileServer.RootPath, "") + OrderNo;//filepath;
            //                                //objGenFile.FileName = OrderNo + file_ext.Replace("brd", "zip");
            //                                objGenFile.FileName = OrderNo + file_ext.Replace("brd", "zip").Replace("kicad_pcb", "zip");
            //                                objGenFile.FileSize = FileSize;
            //                                objGenFile.FileServerId = objGenFileServer.FileServerId;
            //                                ManageFiles.UserID = UserId;
            //                                objfilemanagement.InsertUpdateOrderFiles(objGenFile, "", "");

            //                            }
            //                            else
            //                            {

            //                                EC09DataContext dcEC09 = new EC09DataContext();
            //                                //var genFile = dcEC09.GenFiles.Where(u => u.FileName == OrderNo + file_ext.Replace("brd", "zip") && u.FileTypeCode == "INQ" && (u.IsDeleted == null || u.IsDeleted == false)).ToList();
            //                                var genFile = dcEC09.GenFiles.Where(u => u.FileName == OrderNo + file_ext.Replace("brd", "zip").Replace("kicad_pcb", "zip") && u.FileTypeCode == "INQ" && (u.IsDeleted == null || u.IsDeleted == false)).ToList();
            //                                foreach (GenFile gf in genFile)
            //                                {
            //                                    gf.IsModified = true;
            //                                    gf.ModifiedDate = DateTime.UtcNow;
            //                                    gf.ModifiedBy = UserId;
            //                                    dcEC09.SubmitChanges();
            //                                }

            //                                GenFile objGenFile = new GenFile();
            //                                objGenFile.Number = OrderNo;
            //                                objGenFile.FileTypeCode = "INQ";
            //                                objGenFile.FilePath = FolderPath.Replace(objGenFileServer.RootPath, "") + OrderNo;//filepath;
            //                                //objGenFile.FileName = OrderNo + file_ext.Replace("brd", "zip");
            //                                objGenFile.FileName = OrderNo + file_ext.Replace("brd", "zip").Replace("kicad_pcb", "zip");
            //                                objGenFile.FileSize = FileSize;
            //                                objGenFile.FileServerId = objGenFileServer.FileServerId;
            //                                ManageFiles.UserID = UserId;
            //                                objfilemanagement.InsertUpdateOrderFiles(objGenFile, "", "");

            //                            }
            //                        }
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        genExceptionLog objException = new genExceptionLog();
            //                        objException.LogException(ex, 0);

            //                    }
            //                }
            //            }
            //            else
            //            {

            //                lblpcbnamereq.Text = Convert.ToString(objbase.GetLocalResourceObject("MsgZipFileSelect.Text"));
            //                //divHeader.Visible = true;
            //                return "WrongFile";
            //            }
            //        }
            //        else
            //        {

            //            lblpcbnamereq.Text = Convert.ToString(objbase.GetLocalResourceObject("MsgFileSizeLongOrderFail.Text"));
            //            //divHeader.Visible = true;
            //            return "LargeFile";
            //        }
            //        IsFileUploaded = true;
            //        return "Success";
            //    }
            //    else
            //    {
            //        //divHeader.Visible = true;
            //        return "Fail";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    genExceptionLog objException = new genExceptionLog();
            //    objException.LogException(ex, (long)UserId);
            //    return "Fail";
            //}
            return "";
        }
        private string isFileUploaded(string OrderNo, string IsException)
        {
            //try
            //{

            //    if (UploadGerber.HasFile)
            //    {
            //        long FileSize = UploadGerber.PostedFile.ContentLength;
            //        if (FileSize <= 10485760)
            //        {
            //            string filepath = UploadGerber.PostedFile.FileName;
            //            string ext = filepath.Substring(filepath.LastIndexOf('.') + 1);
            //            //var Files = filepath.Split('.');
            //            string Subfilename = filepath.Substring(0, filepath.LastIndexOf('.'));
            //            Subfilename = Regex.Replace(Subfilename, @"[^0-9a-zA-Z . , _ -]", "");
            //            if (ext.ToLower() == "zip" || ext.ToLower() == "rar" || ext.ToLower() == "brd") //if (Files[1].ToLower() == "zip")
            //            {
            //                string s;
            //                DataSet ds = null;
            //                int sessionCount = 0;
            //                string singlesessionid = null;
            //                if (Convert.ToString(ViewState["Status"]).ToLower() != "BasketAnalysis".ToLower() && Convert.ToString(ViewState["Status"]).ToLower() != "BasketAnalysisCalculate".ToLower())
            //                {
            //                    s = String.Format("select Sessionid from i8response where Sessionid like '%{0}%'", SessionId);
            //                    ds = objgd.GetData(s, Keydata);
            //                    sessionCount = Convert.ToInt32(ds.Tables[0].Rows.Count);
            //                }

            //                else
            //                {
            //                    sessionCount = 1;
            //                }

            //                for (int i = 0; i < sessionCount; i++)
            //                {

            //                    if (Convert.ToString(ViewState["Status"]).ToLower() != "BasketAnalysis".ToLower() && Convert.ToString(ViewState["Status"]).ToLower() != "BasketAnalysisCalculate".ToLower())
            //                    {
            //                        singlesessionid = Convert.ToString(ds.Tables[0].Rows[i]["Sessionid"]);
            //                    }

            //                    else
            //                    {
            //                        singlesessionid = SessionId;
            //                    }


            //                    //string file_ext = ext.ToLower();// Files[1];
            //                    string file_ext = System.IO.Path.GetExtension(UploadGerber.PostedFile.FileName);
            //                    string filename = Subfilename;// Files[0];
            //                    string file = "";
            //                    //if (ViewState["Type"].ToString().ToLower() == "newbasket" || ViewState["Type"].ToString().ToLower() == "modifyBasket".ToLower() || ViewState["Type"].ToString().ToLower() == "modifyOrder".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "repeatOrder".ToLower())
            //                    //{
            //                    //    //GenerateSessionId();
            //                    //    file = "temp" + file_ext;
            //                    //}
            //                    //else
            //                    //    file = OrderNo + file_ext;


            //                    if (IsException == "true")
            //                    { file = OrderNo + file_ext; }
            //                    else
            //                    {
            //                        file = "temp" + file_ext;
            //                    }

            //                    genFileServer objGenFileServer = objData.genFileServers.Where(p => p.IsActive == true).Single();
            //                    ManageFiles.UserID = (long)(UserId);
            //                    ManageFiles objfilemanagement = new ManageFiles();
            //                    string FolderPath = "";
            //                    string inputfilepath = string.Empty;
            //                    string strFilepath = string.Empty;
            //                    //if (ViewState["Type"].ToString().ToLower() != "newbasket" && ViewState["Type"].ToString().ToLower() != "modifyBasket".ToLower() && ViewState["Type"].ToString().ToLower() != "modifyOrder".ToLower() && Convert.ToString(ViewState["Type"]).ToLower() != "repeatOrder".ToLower())
            //                    //    FolderPath = objfilemanagement.ManageDirectoriesForPDF("Order_Files");
            //                    //else
            //                    //    FolderPath = objGenFileServer.RootPath + "tempbasket\\";

            //                    if (IsException == "true")
            //                    { FolderPath = objfilemanagement.ManageDirectoriesForPDF("Order_Files"); }
            //                    else
            //                    {
            //                        FolderPath = objGenFileServer.RootPath + "tempbasket\\";
            //                    }

            //                    try
            //                    {
            //                        if (objGenFileServer.IsRequireImpersonation == true)
            //                        {
            //                            WindowsImpersonationContext impersonateOnFileServer = null;
            //                            impersonateOnFileServer = ImpersonateOnFileServer(objGenFileServer.Username, ConfigurationManager.AppSettings["FileServerDomain"].ToString(), objGenFileServer.Password);
            //                            if (impersonateOnFileServer != null)
            //                            {
            //                                //if (ViewState["Type"].ToString().ToLower() == "newbasket" || ViewState["Type"].ToString().ToLower() == "modifyBasket".ToLower() || ViewState["Type"].ToString().ToLower() == "modifyOrder".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "repeatOrder".ToLower())
            //                                if (IsException != "true")
            //                                {
            //                                    var getOrderCount = new EC09WebApp.API.gd();
            //                                    if (!Directory.Exists(FolderPath + singlesessionid))
            //                                    { Directory.CreateDirectory(FolderPath + singlesessionid); }


            //                                    //UploadGerber.PostedFile.SaveAs(FolderPath + SessionId + "\\" + OrdCount + "\\" + file);
            //                                    //string strFilepath = FolderPath + SessionId + "\\" + OrdCount + "\\" + file;
            //                                    //BRDToZIP(strFilepath, ext);
            //                                    if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                    {
            //                                        UploadGerber.PostedFile.SaveAs(FolderPath + singlesessionid + "\\temp" + "." + ext);
            //                                        inputfilepath = FolderPath + singlesessionid + "\\temp" + "." + ext;
            //                                    }
            //                                    else
            //                                    {
            //                                        UploadGerber.PostedFile.SaveAs(FolderPath + singlesessionid + "\\" + file);
            //                                    }
            //                                    strFilepath = FolderPath + singlesessionid + "\\" + file;
            //                                    BRDToZIP(strFilepath, ext, inputfilepath);
            //                                    KicadToZIP(strFilepath, ext, inputfilepath);
            //                                    if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                    {
            //                                        if (File.Exists(strFilepath))
            //                                        {
            //                                            File.Delete(strFilepath);
            //                                        }
            //                                    }
            //                                }
            //                                else
            //                                {
            //                                    if (!Directory.Exists(FolderPath + OrderNo))
            //                                    {
            //                                        Directory.CreateDirectory(FolderPath + OrderNo);
            //                                    }
            //                                    //UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + file);
            //                                    //string strFilepath = FolderPath + OrderNo + "\\" + file;
            //                                    //BRDToZIP(strFilepath, ext);
            //                                    if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                    {
            //                                        UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\temp" + "." + ext);
            //                                        inputfilepath = FolderPath + OrderNo + "\\temp" + "." + ext;
            //                                    }
            //                                    else
            //                                    {
            //                                        UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + file);
            //                                    }
            //                                    strFilepath = FolderPath + OrderNo + "\\" + file;
            //                                    BRDToZIP(strFilepath, ext, inputfilepath);
            //                                    KicadToZIP(strFilepath, ext, inputfilepath);
            //                                    if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                    {
            //                                        if (File.Exists(strFilepath))
            //                                        {
            //                                            File.Delete(strFilepath);
            //                                        }
            //                                    }
            //                                }

            //                            }
            //                        }
            //                        else
            //                        {
            //                            //if (ViewState["Type"].ToString().ToLower() == "newbasket" || ViewState["Type"].ToString().ToLower() == "modifyBasket".ToLower() || ViewState["Type"].ToString().ToLower() == "modifyOrder".ToLower() || Convert.ToString(ViewState["Type"]).ToLower() == "repeatOrder".ToLower())
            //                            if (IsException != "true")
            //                            {
            //                                var getOrderCount = new EC09WebApp.API.gd();
            //                                if (!Directory.Exists(FolderPath + singlesessionid))
            //                                {
            //                                    Directory.CreateDirectory(FolderPath + singlesessionid);
            //                                }


            //                                //UploadGerber.PostedFile.SaveAs(FolderPath + SessionId + "\\" + OrdCount + "\\" + file);
            //                                //string strFilepath = FolderPath + SessionId + "\\" + OrdCount + "\\" + file;
            //                                //BRDToZIP(strFilepath, ext);
            //                                if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                {
            //                                    UploadGerber.PostedFile.SaveAs(FolderPath + Session["SessionId"].ToString() + "\\" + filename + "." + ext);
            //                                    inputfilepath = FolderPath + Session["SessionId"].ToString() + "\\" + filename + "." + ext;
            //                                }
            //                                else
            //                                {
            //                                    UploadGerber.PostedFile.SaveAs(FolderPath + Session["SessionId"].ToString() + "\\" + file);
            //                                }
            //                                strFilepath = FolderPath + Session["SessionId"].ToString() + "\\" + file;
            //                                BRDToZIP(strFilepath, ext, inputfilepath);
            //                                KicadToZIP(strFilepath, ext, inputfilepath);
            //                            }
            //                            else
            //                            {
            //                                if (!Directory.Exists(FolderPath + OrderNo))
            //                                { Directory.CreateDirectory(FolderPath + OrderNo); }
            //                                //UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + file);
            //                                //string strFilepath = FolderPath + OrderNo + "\\" + file;
            //                                //BRDToZIP(strFilepath, ext);
            //                                if (ext.ToLower() == "brd" || ext.ToLower() == "kicad_pcb")
            //                                {
            //                                    UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + filename + "." + ext);
            //                                    inputfilepath = FolderPath + OrderNo + "\\" + filename + "." + ext;
            //                                }
            //                                else
            //                                {
            //                                    UploadGerber.PostedFile.SaveAs(FolderPath + OrderNo + "\\" + file);
            //                                }
            //                                strFilepath = FolderPath + OrderNo + "\\" + file;
            //                                BRDToZIP(strFilepath, ext, inputfilepath);
            //                                KicadToZIP(strFilepath, ext, inputfilepath);
            //                            }
            //                        }
            //                        //if (ViewState["Type"].ToString().ToLower() != "newbasket" && ViewState["Type"].ToString().ToLower() != "modifyBasket".ToLower() &&   ViewState["Type"].ToString().ToLower() != "modifyOrder".ToLower() && Convert.ToString(ViewState["Type"]).ToLower() != "repeatOrder".ToLower())
            //                        if (IsException == "true")
            //                        {
            //                            EC09DataContext dcEC09 = new EC09DataContext();
            //                            //var genFile = dcEC09.GenFiles.Where(u => u.FileName == OrderNo + file_ext.Replace("brd", "zip") && u.FileTypeCode == "ORD" && (u.IsDeleted == null || u.IsDeleted == false)).ToList();
            //                            var genFile = dcEC09.GenFiles.Where(u => u.FileName == OrderNo + file_ext.Replace("brd", "zip").Replace("kicad_pcb", "zip") && u.FileTypeCode == "ORD" && (u.IsDeleted == null || u.IsDeleted == false)).ToList();
            //                            foreach (GenFile gf in genFile)
            //                            {
            //                                gf.IsModified = true;
            //                                gf.ModifiedDate = DateTime.UtcNow;
            //                                gf.ModifiedBy = Convert.ToInt64(UserId);
            //                                dcEC09.SubmitChanges();
            //                            }

            //                            GenFile objGenFile = new GenFile();
            //                            objGenFile.Number = OrderNo;
            //                            objGenFile.FileTypeCode = "ORD";
            //                            objGenFile.FilePath = FolderPath.Replace(objGenFileServer.RootPath, "") + OrderNo;//filepath;
            //                            //objGenFile.FileName = OrderNo + file_ext.Replace("brd", "zip");
            //                            objGenFile.FileName = OrderNo + file_ext.Replace("brd", "zip").Replace("kicad_pcb", "zip");
            //                            objGenFile.FileSize = FileSize;
            //                            objGenFile.FileServerId = objGenFileServer.FileServerId;
            //                            ManageFiles.UserID = Convert.ToInt64(UserId);
            //                            objfilemanagement.InsertUpdateOrderFiles(objGenFile, "", "");
            //                        }
            //                        /// File Article Code
            //                        ////byte[] filebytes = new byte[UploadGerber.PostedFile.ContentLength];
            //                        ////UploadGerber.PostedFile.InputStream.Read(filebytes, 0, UploadGerber.PostedFile.ContentLength);
            //                        ////FileManagementNew fmNew = new FileManagementNew();
            //                        ////fmNew.UploadFileArticle(objGenFile.Number, "ORD", filebytes, objGenFile.FileName, (long)objGenFile.CreatedBy, objGenFileServer, "", 0);



            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        genExceptionLog objException = new genExceptionLog();
            //                        objException.LogException(ex, 0);

            //                    }





            //                }



            //            }
            //            else
            //            {

            //                lblpcbnamereq.Text = Convert.ToString(objbase.GetLocalResourceObject("MsgZipFileSelect.Text"));
            //                //divHeader.Visible = true;
            //                return "WrongFile";
            //            }
            //        }
            //        else
            //        {

            //            lblpcbnamereq.Text = Convert.ToString(objbase.GetLocalResourceObject("MsgFileSizeLongOrderFail.Text"));
            //            //divHeader.Visible = true;
            //            return "LargeFile";

            //        }
            //        IsFileUploaded = true;
            //        return "Success";
            //    }
            //    else
            //    {
            //        //lblErrorMsg.Visible = true;
            //        //lblErrorMsg.Text = Convert.ToString(GetLocalResourceObject("MsgSelectFile.Text"));
            //        return "Fail";
            //    }
            //}
            //catch (Exception ex)
            //{

            //    return "Fail";
            //}
            return "";
        }
        #endregion
        #region "Authentication Methods"
        public static WindowsImpersonationContext ImpersonateOnFileServer(string Username, string Domain, string Password)
        {
            return core.Impersonation.ImpersonateFileServer(Username, Password, "");
        }
        #endregion
        private void BRDToZIP(string strFilepath, string ext, string inputfilepath)
        {
            if (ext.ToLower() == "brd")
            {
                ////EC09.Business.FileManagementNew obj = new FileManagementNew();
                ////obj.CreateSingleZipFile(inputfilepath, strFilepath.ToLower().Replace(".brd", ".zip"), "");
            }
        }
        private void KicadToZIP(string strFilepath, string ext, string inputfilepath)
        {
            if (ext.ToLower() == "kicad_pcb")
            {
                //EC09.Business.FileManagementNew obj = new FileManagementNew();
                //obj.CreateSingleZipFile(inputfilepath, strFilepath.ToLower().Replace(".kicad_pcb", ".zip"), "");
            }
        }
        private void SetLanguageData()
        {
            try
            {
                lblPCBname.Text = objbase.GetLocalResourceObject("lblpcbname.Text");
                //lblPCBname.Text = objbase.GetLocalResourceObject("lblpcbnamereq .Text");
                //lblErr.Text = objbase.GetLocalResourceObject("lblErr.Text");
                //lblUploaddatafile.Text = objbase.GetLocalResourceObject("lblUploaddatafile.Text");
                lblPurchasereference.Text = objbase.GetLocalResourceObject("lblPurchasereference.Text");
                lblArticlereference.Text = objbase.GetLocalResourceObject("lblArticlereference.Text");
                lblProjReffirst.Text = objbase.GetLocalResourceObject("lblProjRef.Text");
                btnsubmit.Text = objbase.GetLocalResourceObject("lblcontinue.Text");
                //lbluploaddatadilog.Text = objbase.GetLocalResourceObject("lbluploaddata.Text");
                //lbldeliveryterm.Text = objbase.GetLocalResourceObject("lbldeliveryterm.Text");
                //lblqyentity.Text = objbase.GetLocalResourceObject("lblqyentity.Text");

                LblOptNoTitle.Text = objbase.GetLocalResourceObject("LblOptNoTitle.Text");
                LblDelTermTitle.Text = objbase.GetLocalResourceObject("LblDelTermTitle.Text");
                LblQtyTitle.Text = objbase.GetLocalResourceObject("LblQtyTitle.Text");
                LbtnAdd01.Text = objbase.GetLocalResourceObject("LbtnAdd01.Text");
                LbtnRemove02.Text = objbase.GetLocalResourceObject("LbtnRemove02.Text");
                LbtnRemove03.Text = objbase.GetLocalResourceObject("LbtnRemove02.Text");
                LbtnRemove04.Text = objbase.GetLocalResourceObject("LbtnRemove02.Text");
                LbtnRemove05.Text = objbase.GetLocalResourceObject("LbtnRemove02.Text");
                LbtnRemove06.Text = objbase.GetLocalResourceObject("LbtnRemove02.Text");
                LbtnRemove07.Text = objbase.GetLocalResourceObject("LbtnRemove02.Text");
                lblDelAddress.Text = objbase.GetLocalResourceObject("lblDelAddress.Text");
                lblRemark.Text = objbase.GetLocalResourceObject("lblRemark.Text");
                LblVariantTitle.Text = objbase.GetLocalResourceObject("LblVariantTitle.Text");

                hidendelerrmsg.Value = objbase.GetLocalResourceObject("lbldeliveryerrmsg.Text");

                lbldelterms.Text = objbase.GetLocalResourceObject("lblestimatedate.Text");
                lblManualStencilPriceText.InnerText = objbase.GetLocalResourceObject("lblManualStencilPriceText.Text");
                lblManualStencilPrice.Text = objbase.GetLocalResourceObject("lblManualStencilPrice.Text");
                lblStencilFixImagePathOrderLean.Text = objbase.GetLocalResourceObject("lblStencilFixImagePathOrderLean.Text");
                Hyperlinkyoutube.ToolTip = objbase.GetLocalResourceObject("lblecvideoURLToolTip.Text");
                Hyperlinkyoutube.NavigateUrl = objbase.GetLocalResourceObject("lblecvideoURL.Text");
                lblInfo_UploadFileDetails.Text = objbase.GetLocalResourceObject("lblInfo_UploadFileDetails.Text");
                lblselectdeliverydate.Text = objbase.GetLocalResourceObject("lblselectdeliverydate.Text");
                lblReclDelivryDate.Text = objbase.GetLocalResourceObject("lblReclDelivryDate.Text");
                lblBoardLocked.Text = objbase.GetLocalResourceObject("lblBoardLocked.Text");
                lblInvaddress.Text = objbase.GetLocalResourceObject("lblInvaddress.Text");
                lblFullcheck.Text = objbase.GetLocalResourceObject("lblFullcheck.Text");
                lblplatedslot.Text = objbase.GetLocalResourceObject("lblplatedslot.Text");
                lblPreProductcheck.Text = objbase.GetLocalResourceObject("lblPreProductcheck.Text");
                lblHandlingcomp.Text = objbase.GetLocalResourceObject("lblHandlingcomp.Text");
                //lblTermsofSalesLink.Text = objbase.GetLocalResourceObject("lblTermsofSalesLink.Text");

                //DataTable dtcurrency = objgd.GetData("select * from admCurrencies as ac where ac.CurrencyId in(select gc.CurrencyId from genCompanies as gc where gc.CompanyId in(" + Convert.ToInt64(ViewState["Company_id"]) + ") )", Keydata).Tables[0];
                DataTable dtcurrency = objgd.GetData("select * from admCurrencies as ac where ac.CurrencyId in (select gc.CurrencyId from genCompanies as gc with(nolock) where gc.CompanyId in(" + Convert.ToInt64(Company_id) + ") )", Keydata).Tables[0];
                decimal currencyfactor = 1;
                string CurrencySymbol = "EUR";
                if (dtcurrency.Rows.Count != 0)
                {
                    currencyfactor = Math.Round(Convert.ToDecimal(dtcurrency.Rows[0]["Factor"]), 2);
                    CurrencySymbol = Convert.ToString(dtcurrency.Rows[0]["Symbol"]);
                }
                //lblStencilFixPrice.Text = string.Format("{0:#,0.00}", Convert.ToDecimal(((Math.Round(Convert.ToDecimal(objbase.GetLocalResourceObject("lblStencilFixPrice.Text").ToString()) * currencyfactor, 2)).ToString())) + " " + CurrencySymbol;
            }
            catch (Exception ex)
            {

            }
        }
        protected string CheckValidtionforSpec(string strToSearch)
        {
            if (strToSearch != null)
            {
                strToSearch = strToSearch.Replace("<", " ");
                strToSearch = strToSearch.Replace(">", " ");
                strToSearch = strToSearch.Replace("|", " ");
                strToSearch = strToSearch.Replace("&", " ");
                strToSearch = strToSearch.Replace("\"", "'");
                strToSearch = strToSearch.Replace("--", "-");
            }
            return strToSearch;

        }
        private string GetVariantXml()
        {
            string VariantXml = "<PlaceReqDayQuientyParameters>";
            try
            {

                int TotalVariants = 1;
                if (LbtnAdd01.AccessKey != "")
                    int.TryParse(LbtnAdd01.AccessKey, out TotalVariants);
                switch (TotalVariants)
                {
                    case 1:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm01.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty01.Text + "\"></Parameters>";
                        break;
                    case 2:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm01.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty01.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm02.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty02.Text + "\"></Parameters>";
                        break;
                    case 3:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm01.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty01.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm02.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty02.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm03.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty03.Text + "\"></Parameters>";
                        break;
                    case 4:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm01.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty01.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm02.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty02.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm03.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty03.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm04.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty04.Text + "\"></Parameters>";
                        break;
                    case 5:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm01.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty01.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm02.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty02.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm03.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty03.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm04.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty04.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm05.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty05.Text + "\"></Parameters>";
                        break;
                    case 6:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm01.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty01.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm02.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty02.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm03.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty03.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm04.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty04.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm05.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty05.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm06.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty06.Text + "\"></Parameters>";
                        break;
                    case 7:
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm01.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty01.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm02.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty02.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm03.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty03.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm04.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty04.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm05.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty05.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm06.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty06.Text + "\"></Parameters>";
                        VariantXml += "<Parameters ReqDaysValue=\"" + DDLDelTerm07.SelectedValue + "\" ReqQuientyValue=\"" + TxtQty07.Text + "\"></Parameters>";
                        break;
                }
                VariantXml += "</PlaceReqDayQuientyParameters>";
                return VariantXml;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        private bool checkForDuplicateVariant(int TotalVariants)
        {
            #region validateforduplicatevariant
            switch (TotalVariants)
            {
                case 2:
                    if (TxtQty01.Text == TxtQty02.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm02.SelectedIndex) return false;
                    }
                    break;
                case 3:
                    if (TxtQty01.Text == TxtQty02.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm02.SelectedIndex) return false;
                    }
                    if (TxtQty02.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm03.SelectedIndex) return false;
                    }
                    if (TxtQty01.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm03.SelectedIndex) return false;
                    }
                    break;
                case 4:
                    if (TxtQty01.Text == TxtQty02.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm02.SelectedIndex) return false;
                    }
                    if (TxtQty02.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm03.SelectedIndex) return false;
                    }
                    if (TxtQty01.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm03.SelectedIndex) return false;
                    }
                    if (TxtQty01.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm04.SelectedIndex) return false;
                    }
                    if (TxtQty02.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm04.SelectedIndex) return false;

                    }
                    if (TxtQty03.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm04.SelectedIndex) return false;
                    }
                    break;
                case 5:
                    if (TxtQty01.Text == TxtQty02.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm02.SelectedIndex)
                        {
                            return false;
                        }
                    }
                    if (TxtQty02.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm03.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm03.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm04.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty02.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm04.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty03.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm04.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty02.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty03.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty04.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm04.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    break;

                case 6:
                    if (TxtQty01.Text == TxtQty02.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm02.SelectedIndex)
                        {
                            return false;
                        }
                    }
                    if (TxtQty02.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm03.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm03.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm04.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty02.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm04.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty03.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm04.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty02.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty03.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty04.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm04.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }

                    if (TxtQty01.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty02.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty03.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty04.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm04.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty05.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm05.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    break;


                case 7:
                    if (TxtQty01.Text == TxtQty02.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm02.SelectedIndex)
                        {
                            return false;
                        }
                    }
                    if (TxtQty02.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm03.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty03.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm03.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm04.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty02.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm04.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty03.Text == TxtQty04.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm04.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty02.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty03.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty04.Text == TxtQty05.Text)
                    {
                        if (DDLDelTerm04.SelectedIndex == DDLDelTerm05.SelectedIndex)
                        {
                            return false;
                        }

                    }

                    if (TxtQty01.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty02.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty03.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty04.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm04.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty05.Text == TxtQty06.Text)
                    {
                        if (DDLDelTerm05.SelectedIndex == DDLDelTerm06.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty01.Text == TxtQty07.Text)
                    {
                        if (DDLDelTerm01.SelectedIndex == DDLDelTerm07.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty02.Text == TxtQty07.Text)
                    {
                        if (DDLDelTerm02.SelectedIndex == DDLDelTerm07.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty03.Text == TxtQty07.Text)
                    {
                        if (DDLDelTerm03.SelectedIndex == DDLDelTerm07.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty04.Text == TxtQty07.Text)
                    {
                        if (DDLDelTerm04.SelectedIndex == DDLDelTerm07.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty05.Text == TxtQty07.Text)
                    {
                        if (DDLDelTerm05.SelectedIndex == DDLDelTerm07.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    if (TxtQty06.Text == TxtQty07.Text)
                    {
                        if (DDLDelTerm06.SelectedIndex == DDLDelTerm07.SelectedIndex)
                        {
                            return false;
                        }

                    }
                    break;
            }


            return true;
            #endregion
        }

        protected void datepicker_TextChanged(object sender, EventArgs e)
        {
            string a = Deliverydate;
        }

        private string Convertdateformate(string deldate)
        {
            string[] del_date = deldate.Split('-');

            return Convert.ToString(del_date[2] + "-" + del_date[1] + "-" + del_date[0]);
        }
        private void bindCompanywiseRef(long Company_id)
        {
            try
            {

                string sql_ref = string.Format("select isnull(IsActiveOrderRef2,0) as IsActiveOrderRef2,isnull(IsActiveOrderRef3,0) as IsActiveOrderRef3,isnull(IsActiveYourOrderRef,0) as IsActiveYourOrderRef,isnull(IsOrderRef2Required,0) as IsOrderRef2Required,isnull(IsOrderRef3Required,0) as IsOrderRef3Required,isnull(IsYourOrderRefRequired,0) as IsYourOrderRefRequired,isnull(IsActiveYourInqRef,0) as IsActiveYourInqRef,isnull(IsActiveYourInqRef2,0) as IsActiveYourInqRef2,isnull(IsActiveYourInqRef3,0) as IsActiveYourInqRef3,isnull(IsInqRef2Required,0) as IsInqRef2Required,isnull(IsInqRef3Required,0) as IsInqRef3Required,isnull(IsInqRefRequired,0) as IsInqRefRequired  from genCompanies with(nolock) where CompanyId='{0}'", Company_id);
                DataSet ds_ref = objgd.GetData(sql_ref, Keydata);
                DataTable dt_ref = new DataTable();
                if (ds_ref != null && ds_ref.Tables[0].Rows.Count != 0)
                {
                    dt_ref = ds_ref.Tables[0];

                    //for required or not ref textbox...
                    bool IsOrderRef2Required = Convert.ToBoolean(dt_ref.Rows[0]["IsOrderRef2Required"]);
                    bool IsOrderRef3Required = Convert.ToBoolean(dt_ref.Rows[0]["IsOrderRef3Required"]);
                    bool IsYourOrderRefRequired = Convert.ToBoolean(dt_ref.Rows[0]["IsYourOrderRefRequired"]);



                    //for show or hide ref textbox..
                    bool IsActiveOrderRef2;
                    if (Boolean.TryParse(dt_ref.Rows[0]["IsActiveOrderRef2"].ToString().Trim(), out IsActiveOrderRef2))
                    {
                        if (IsActiveOrderRef2) // IsActiveOrderRef2 = Project reference
                        {
                            divprojreffirst.Attributes["style"] = "display:inline-block";
                            if (IsOrderRef2Required)
                            {
                                spanprojrefreq.Attributes["style"] = "display:inline-block";
                                hidenprojectrefreq.Value = "1";
                            }
                            else
                            {
                                spanprojrefreq.Attributes["style"] = "display:none";
                            }
                        }
                        else
                        {
                            divprojreffirst.Attributes["style"] = "display:none";
                        }
                    }
                    bool IsActiveOrderRef3;
                    if (Boolean.TryParse(dt_ref.Rows[0]["IsActiveOrderRef3"].ToString().Trim(), out IsActiveOrderRef3))
                    {
                        if (IsActiveOrderRef3) // IsActiveOrderRef3 = Article reference
                        {
                            divartireferance.Attributes["style"] = "display:inline-block";
                            if (IsOrderRef3Required)
                            {
                                spanarticlerefreq.Attributes["style"] = "display:inline-block";
                                hidenarticlereq.Value = "1";
                            }
                            else
                            {
                                spanarticlerefreq.Attributes["style"] = "display:none";
                            }
                        }
                        else
                        {
                            divartireferance.Attributes["style"] = "display:none";
                        }
                    }

                    bool IsActiveYourOrderRef;
                    if (Boolean.TryParse(dt_ref.Rows[0]["IsActiveYourOrderRef"].ToString().Trim(), out IsActiveYourOrderRef))
                    {
                        if (IsActiveYourOrderRef) //IsActiveYourOrderRef = Purchase reference
                        {
                            divpurreferance.Attributes["style"] = "display:inline-block";
                            if (IsYourOrderRefRequired)
                            {
                                spanpurchaserefreq.Attributes["style"] = "display:inline-block";
                                hidenpurchasereq.Value = "1";
                            }
                            else
                            {
                                spanpurchaserefreq.Attributes["style"] = "display:none";
                            }
                        }
                        else
                        {
                            divpurreferance.Attributes["style"] = "display:none";
                        }
                    }


                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}