﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="eccOrderModified.aspx.cs"
    Inherits="sparrowFeApp.shop.orders.eccOrderModified" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../../CSS/FontStyle.css" rel="stylesheet" />
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title></title>
   
	<style>
		/*
	These CSS rules affect the styling of the demo pages. The centering techniques
	are demonstrated in the individual in the individual 1.html, 2.html and 3.html files.
*/


*{
	margin:0;
	padding:0;
}

body{
	/* Setting default text color, background and a font stack */
	color:#555555;
	font-size:0.825em;
	background: #fcfcfc;
}

#exampleBody{
	background-color:#F5F5F5;
}

.example{
	background-color:#F0F0F0;
	border:1px solid #FFFFFF;
	margin:40px auto;
	padding:10px;
	width:700px;
	
	-moz-box-shadow:0 0 2px gray;
	-webkit-box-shadow:0 0 2px gray;
	box-shadow:0 0 2px gray;
}

.demo{
	background-color:#F5F5F5;
	border:2px solid white;
	float:left;
	
	-moz-box-shadow:0 0 1px gray;
	-webkit-box-shadow:0 0 1px gray;
	box-shadow:0 0 1px gray;
}

.demo iframe{
	width:360px;
	height:300px;
	border:none;
}

.code{
	background-color:#E6E6E6;
	border:1px solid #DDDDDD;
	float:right;
	padding:10px;
	width:300px;
	
	-moz-box-shadow:0 0 1px white;
	-webkit-box-shadow:0 0 1px white;
	box-shadow:0 0 1px white;
}

pre{
	text-shadow:0 1px 1px white;
}

.className{
	
	width:270px;
	height:150px;
	position:relative;
	text-align: center;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
}


.className p{
	font-size:22px;
	margin:45px 10px 10px;
	color:white;
	text-align:center;
	position:absolute;
}

/* The styles below are only necessary for the styling of the demo page: */

h1{
	background:#F4F4F4;
	border-bottom:1px solid #EEEEEE;
	font-size:20px;
	font-weight:normal;
	margin-bottom:15px;
	padding:15px;
	text-align:center;
}

h2 {
	font-size:12px;
	font-weight:normal;
	padding-right:40px;
	position:relative;
	right:0;
	text-align:right;
	text-transform:uppercase;
	top:-48px;
}

a, a:visited {
	color:#0196e3;
	text-decoration:none;
	outline:none;
}

a:hover{
	text-decoration:underline;
}

.clear{
	clear:both;
}

p.tutInfo{
	/* The tutorial info on the bottom of the page */
	padding:10px 0;
	text-align:center;
	position:fixed;
	bottom:0px;
	background-color:#011D2E;
	border-top:1px solid #011d2e;

	width:100%;
}

	</style>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script type="text/javascript">
      $(document).ready(function () {

          $(window).resize(function () {

              $('.className').css({
                  position: 'absolute',
                  left: ($(window).width() - $('.className').outerWidth()) / 2,
                  top: ($(window).height() - $('.className').outerHeight()) / 2
              });

          });
          // To initially run the function:
          $(window).resize();

      });
</script>
</head>
<body>
    <form id="form1" runat="server">
   
   <div id="divProg" class="className" >
        <div style="margin: 0px auto;text-align:center;width:100%;height:100%;" id="divimg" runat="server" >
            <img src="http://be.eurocircuits.com/shop/images/waiting_grn.gif"  />
        </div>
        <div style="margin:0px auto;text-align:center;width:100%;height:100%;font-size:17px;" id="divtext" runat="server">
            <asp:Label ID="lblMSG" runat="server">Processing...</asp:Label>
        </div>
    </div>
    </form>
</body>
</html>
