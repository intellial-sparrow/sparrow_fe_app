﻿using System;
using System.Web;
using System.Xml;
using System.Data;
using System.IO;
using System.Text;
using System.Net;
using System.Drawing;
using System.Security.Principal;
using System.Drawing.Drawing2D;
using System.Web.Caching;
using EC09WebApp.shop.rb;
using sparrowFeApp.core;

namespace sparrowFeApp
{
    //delegate dynamic ComponentPirceDelegate(ComponentPrice componentPrice);

    public partial class visdownload : System.Web.UI.Page
    {
        const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
        Database.DA objgd = new Database.DA();
     
        //EC09WebApp.EC09WebService.OTS objOts = new EC09WebApp.EC09WebService.OTS();
        //EC09WebApp.EC09WebService.Offer objOffer = new EC09WebApp.EC09WebService.Offer();
        protected void Page_Load(object sender, EventArgs e)
        {
            string number = "";
            string file = Request.QueryString["file"] ?? "";
            if (Request.QueryString["ID"] == null && Request.QueryString["key"] == null)
            {
                return;
            }
                number = DecryptLast(Convert.ToString(Request["Key"]));
                if ((number.Contains("-STA") || number.Contains("-SBA")) && !file.Contains("orderdetails"))
                {
                    if (Request.QueryString["ID"].ToString().ToUpper() == number.Split('-')[0].ToUpper())
                    {
                        number = number.Split('-')[0];
                    }
                }
            
            if (number.ToUpper().Contains("-P") && number.Length > 5)
            {
                number = number.Substring(0, number.Length - 3);
            }


            if ((number.ToUpper().Contains("-ST") || number.ToUpper().Contains("-SB")) && number.Length > 5 && Request.QueryString["file"].ToLower().Contains("orderdetails") == false && number.Contains("-STA") == false &&  number.Contains("-SBA") == false)
            {
                DataSet Storder = new DataSet();
                string Ordernr = number.Substring(0, number.Length - 3); ;
                string getNumber = string.Format("exec CheckParentOrder '{0}'", Ordernr);
                Storder = objgd.GetDataSet(getNumber);
                if (Storder != null && Storder.Tables.Count > 0 && Storder.Tables[0].Rows.Count > 0)
                {
                    number = Ordernr;
                }
            }


            string prefix = "";
            if (Request.QueryString["prefix"] != null && Convert.ToString(Request.QueryString["prefix"]) != string.Empty)
            {
                prefix = Convert.ToString(Request.QueryString["prefix"]);
                if (prefix.ToLower() == "pi" || prefix.ToLower() == "px")
                {
                    number = Convert.ToString(Request.QueryString["ID"]);
                    prefix = "";
                }
            }


            if (getPackages())
            {
                return;
            }

            if (Request.QueryString["prefix"] != null && Convert.ToString(Request.QueryString["prefix"]) != string.Empty)
            {
                prefix = Convert.ToString(Request.QueryString["prefix"]);
                if (prefix.ToLower() == "sch" || prefix.ToLower() == "xml")
                {
                    downloadSchFile();
                    return;
                }
            }

            if (downloadExceptionfile())
            {
                return;
            }

            if (downloadOrderFile())
            {
                return;
            }

            if (number.Contains("AB"))
            {
                DataSet dsor = new DataSet();
                string getNumber = string.Format("exec getOrdernumberBYJOB '{0}'", number);
                string Ordernr = "";
                dsor = objgd.GetDataSet(getNumber);
                if (dsor != null && dsor.Tables.Count > 0 && dsor.Tables[0].Rows.Count > 0)
                {
                    Ordernr = dsor.Tables[0].Rows[0]["EntityNumber"].ToString();
                    number = Ordernr;
                }
            }

            prefix = Request.QueryString["prefix"] ?? "";
            string filename = Request.QueryString["filename"] ?? "";
            if (file.Contains("orderdetails") && prefix == "" && !number.ToUpper().StartsWith("P4M"))
            {
                isForceXml(number, "");
            }
            string path = getVisPath(number, prefix, file, filename);

            if (path == "")//if file path is not fount
            {
                Response.Write("file not found");
                return;
            }
            if (prefix == "px" || prefix == "pi")/// for pixture file 
            {
                downloadPixture(path, prefix);
                return;
            }
            if(prefix.ToLower() == "pixpect")
              {
                path = path  + Request.QueryString["name"] + "_PIXpect.xml";
                writefile(path);
                return;
             }

            if (prefix == "comp") /// Component file 
            {
                downloadComponentFile(path);
                return;
            }

            if (file.ToLower().Contains("orderdetails"))
            {
                if (Request.QueryString["VId"] != null && Request.QueryString["VId"] != "")
                {
                    string Vid = Request.QueryString["VId"].ToString();
                    if (path.ToLower().Contains(".xml"))
                    {
                        UpdateXml(Vid, path);
                    }
                }
            }
            if (path.Contains(".png"))
            {
                writeImageFile(path);
            }
            else
            {
                writefile(path); // order file ,sifile ,cpl,bom,si and order image and drc ,PIXpect  file 
            }
        }
        private bool getPackages()
        {
            if (Request.QueryString["file"] != null && Request.QueryString["file"].ToString().ToLower() == "packages")
            {
                if (Request.HttpMethod.ToString().Trim().ToUpper() == "POST")
                {
                    try
                    {
                        string sparrowAPI =Constant.SparrowAPI;
                        string postData = "";
                        using (StreamReader inputStream = new StreamReader(Request.InputStream))
                        {
                            postData = inputStream.ReadToEnd().Trim();
                        }

                        HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(sparrowAPI + "get_footprint/");
                        myHttpWebRequest.UseDefaultCredentials = true;
                        myHttpWebRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        myHttpWebRequest.Method = "POST";

                        byte[] data = Encoding.ASCII.GetBytes(postData);
                        myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
                        myHttpWebRequest.ContentLength = data.Length;

                        using (Stream requestStream = myHttpWebRequest.GetRequestStream())
                        {
                            requestStream.Write(data, 0, data.Length);
                        }

                        using (HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse())
                        {
                            string pageContent = "";
                            using (Stream responseStream = myHttpWebResponse.GetResponseStream())
                            {
                                using (StreamReader sr = new StreamReader(responseStream, Encoding.Default))
                                {
                                    pageContent = sr.ReadToEnd();
                                }
                            }

                            byte[] responseData = Encoding.ASCII.GetBytes(pageContent);
                            Response.BinaryWrite(responseData);
                        }
                    }
                    catch (Exception ex)
                    {

                        string e = ex.ToString();
                    }
                    return true;
                }
            }

            return false;
        }

        private bool downloadSchFile()
        {
            if (Request.QueryString["Key"] != null && Request.QueryString["id"].ToString() == DecryptLast(Convert.ToString(Request.QueryString["Key"])))
            {
                System.Data.DataSet dsorder;

                string id = DecryptLast(Convert.ToString(Request.QueryString["Key"]));
                string sql = string.Format("exec oSP_GetSchfile '{0}','{1}','{2}'", Convert.ToString(Request.QueryString["id"]), Convert.ToString(Request.QueryString["file"]), Convert.ToString(Request.QueryString["prefix"]));
                dsorder = objgd.GetDataSet(sql);

                if (dsorder.Tables[0].Rows.Count > 0)
                {
                    WindowsImpersonationContext impersonateOnFileServer = null;
                    impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);

                    if (impersonateOnFileServer != null)
                    {
                        string filePath = Convert.ToString(dsorder.Tables[0].Rows[0]["FilePath"]);
                        string fileName = Convert.ToString(dsorder.Tables[0].Rows[0]["filename"]);

                        string fullPath = Path.Combine(filePath, fileName);

                        System.IO.FileInfo FileName = new System.IO.FileInfo(fullPath);

                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.FileStream fs = null;
                            // using (System.IO.FileStream fs = System.IO.File.OpenRead(fullPath))
                            try
                            {
                                fs = System.IO.File.OpenRead(fullPath);
                                int length = (int)fs.Length;
                                byte[] buffer;

                                using (System.IO.BinaryReader br = new System.IO.BinaryReader(fs))
                                {
                                    buffer = br.ReadBytes(length);
                                }
                                Response.Clear();
                                Response.ContentType = "application/download";
                                Response.AddHeader("content-disposition", "attachment; filename=" + FileName.Name.ToString() + "");
                                Response.BinaryWrite(buffer);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            finally
                            {
                                if (fs != null)
                                {
                                    fs.Close();
                                    fs.Dispose();
                                }
                            }
                        }
                        else
                        {
                            Response.Write("file not found");
                            return true;
                        }
                    }
                    return true;
                }
                else
                {
                    Response.Write("file not found");
                    return true;
                }
            }
            return false;
        }

        public static string DecryptLast(string Data)
        {
            byte[] dEC_data = Convert.FromBase64String(Data);
            string dEC_Str = System.Text.ASCIIEncoding.ASCII.GetString(dEC_data);
            return dEC_Str;
        }
        public static string EncryptLast(string Data)
        {
            System.Security.Cryptography.SHA1Managed shaM = new System.Security.Cryptography.SHA1Managed();
            Convert.ToBase64String(shaM.ComputeHash(System.Text.Encoding.ASCII.GetBytes(Data)));
            byte[] eNC_data = System.Text.ASCIIEncoding.ASCII.GetBytes(Data);
            string eNC_str = Convert.ToBase64String(eNC_data);
            return eNC_str;
        }

        public string getVisPath(string number, string prefix, string file, string fileName)
        {
      
            string assemblyJob = "";
            assemblyJob = number;
            if (number.Contains("-A") || number.Contains("-B"))
            {
                assemblyJob = number.Split('-')[0].ToString();
                number = assemblyJob;
            }

            string siPrefix = prefix;
            if (prefix.ToLower().Contains("si"))
            {
                siPrefix = "si_";
            }
            string filePath = "";
            if (HttpContext.Current.Cache["FilePath" + number] != null)
            {
                filePath = HttpContext.Current.Cache["FilePath" + number].ToString();
            }
            else
            {
                string getNumber = string.Format(" select file_path,file_type from sales_file where entity_number = '{0}' and file_type = '{1}'", number, "ORD");
                if (number.StartsWith("B"))
                {
                    getNumber = string.Format(" select file_path,file_type from sales_file where entity_number = '{0}' and file_type = '{1}'", number, "INQ");
                } //test
                Database.DA da = new Database.DA();
                DataSet dsorder = da.GetDataSet(getNumber);
                if (dsorder != null && dsorder.Tables.Count > 0 && dsorder.Tables[0].Rows.Count > 0)
                {
                    filePath = dsorder.Tables[0].Rows[0]["file_path"].ToString();
                    if (filePath != "")
                    {
                        string DocumentKey = "FilePath" + number;
                        CacheDependency newcd = new CacheDependency(System.Web.HttpContext.Current.Server.MapPath("CHVisfile.html"));
                        HttpContext.Current.Cache.Add(DocumentKey, filePath, newcd, DateTime.UtcNow.AddDays(3), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                    }
                    else
                    {
                        return "";
                    }
                }
            }


            if (file == "holidays")
            {
                filePath = "\\holiday\\holidays.xml";
            }
            else if (prefix == "pi")
            {
                string filename = fileName;
                filePath = filePath + "\\pixture\\" + filename;
            }
            else if (prefix == "px")
            {
                filePath = filePath + "\\pixture\\";
            }
            else if (file == "P4MIMAGES")
            {
                filePath = filePath + "\\visualizer\\P4M_IMG\\" + fileName;
            }
            else if (prefix == "comp")
            {
                filePath = filePath + "\\assembly\\" + assemblyJob + "\\" + assemblyJob + "_components.xml";
            }
            else if (prefix.ToLower() == "pixpect")
            {
                filePath = filePath + "\\assembly\\" + assemblyJob + "\\";
            }
            else if (prefix == "cpl")
            {
                filePath = filePath + "\\assembly\\" + assemblyJob + "\\BOM\\cpl.xml";
            }
            else if (prefix == "bom")
            {
                filePath = filePath + "\\assembly\\" + assemblyJob + "\\BOM\\bom.xml";
            }
            else if (file.Contains("orderdetails"))
            {
                filePath = filePath + "\\" + prefix + number + ".xml";
            }
            else if (file.ToLower().Contains("drc"))
            {
                filePath = filePath + "\\visualizer\\" + prefix + number + "_vis_drc.xml";
            }

            else if (file.ToLower().Contains("images"))
            {
                filePath = filePath + "\\visualizer\\" + prefix + number + "_vis_images.xml";
            }

            else if (file.ToLower().Contains("bottomplating"))
            {
                filePath = filePath + "\\visualizer\\" + siPrefix + number + "_bottom_plating.png";
            }

            else if (file.ToLower().Contains("topplating"))
            {
                filePath = filePath + "\\visualizer\\" + siPrefix + number + "_top_plating.png";
            }

            else if (file.ToLower().Contains("bottom"))
            {
                filePath = filePath + "\\visualizer\\" + siPrefix + number + "_bottom.png";
            }
            else if (file.ToLower().Contains("top"))
            {
                filePath = filePath + "\\visualizer\\" + siPrefix + number + "_top.png";
            }

            else if (file.ToLower().Contains("mechanicalplan"))
            {
                filePath = filePath + "\\visualizer\\" + siPrefix + number + "__mechanicalplan.png";
            }

            else if (file.ToLower().Contains("buildup"))
            {
                filePath = filePath + "\\visualizer\\" + siPrefix + number + "_buildup.png";
            }
            return filePath;
        }

        private bool downloadExceptionfile()
        {
            if (Request.QueryString["file"] != null && Request.QueryString["file"].ToUpper().Contains("EXCEPTIONPDF") && Request.QueryString["Key"] != null && Request.QueryString["id"].ToString() == DecryptLast(Convert.ToString(Request.QueryString["Key"])))
            {
                System.Data.DataSet dsorder;
                string test = EncryptLast(Convert.ToString(Request.QueryString["id"]));
                string id = DecryptLast(Convert.ToString(Request.QueryString["Key"]));
                dsorder = objgd.GetDataSet("exec oSP_GetExeptionfile '" + Convert.ToString(Request.QueryString["id"]) + "'");

                if (dsorder.Tables[0].Rows.Count > 0)
                {
                    System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);
                    if (impersonateOnFileServer != null)
                    {
                        string filePath = Convert.ToString(dsorder.Tables[0].Rows[0]["FilePath"]);
                        string fileName = Convert.ToString(dsorder.Tables[0].Rows[0]["FileName"]);
                        string rootPath = Convert.ToString(dsorder.Tables[1].Rows[0]["RootPath"]);
                        string fullPath = Path.Combine(rootPath, filePath);
                        fullPath = Path.Combine(fullPath, fileName);
                        if (System.IO.File.Exists(fullPath))
                        {
                            if ((Request.QueryString["prefix"] != null && Request.QueryString["prefix"].ToUpper().Contains("PDF")) || fileName.ToUpper().EndsWith(".PDF"))
                            {
                                writefile(Path.Combine(filePath, fileName));
                            }
                            else
                            {
                                Response.Clear();
                                Response.ContentType = "text/html; charset=UTF-8";
                                Response.Write(string.Format("<HTML><HEAD><META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=windows-1252\"><TITLE></TITLE></HEAD><BODY><div style=\"font-family:arial;font-size:16px;font-weight:bold;padding:20px;\"><object data=\"/visdownload.aspx?id={0}&prefix=pdf&file=exceptionpdf&key={1}&timestamp=0.021011025364818025\" type=\"application/pdf\" width=\"100%\" height=\"100%\">  <p>Click <a href=\"/visdownload.aspx?id={0}&prefix=pdf&file=exceptionpdf&key={1}&timestamp=0.021011025364818025\">here</a> to open/download the repair details document</p></object></div></object></BODY></HTML>", Request.QueryString["id"].ToString(), Request.QueryString["key"].ToString()));
                            }
                        }
                        else
                        {
                            Response.Write("file not found");
                            return true;
                        }
                    }
                    return true;
                }
                else
                {
                    Response.Write("file not found");
                    return true;
                }
            }
            return false;
        }

        private bool downloadOrderFile()
        {
            if (Request.QueryString["file"] != null && Request.QueryString["file"].ToLower().Contains("orderfile") && Request.QueryString["Key"] != null && Request.QueryString["id"].ToString() == DecryptLast(Convert.ToString(Request.QueryString["Key"])))
            {
                System.Data.DataSet dsorder;
                string test = EncryptLast(Convert.ToString(Request.QueryString["id"]));
                string id = DecryptLast(Convert.ToString(Request.QueryString["Key"]));
                dsorder = objgd.GetDataSet("select * from GenFiles with(nolock) where Number='" + id + "' and ( FileTypeCode='ORD' or FileTypeCode='INQ') and ISNULL(IsDeleted,0)=0 and ISNULL(IsModified,0)=0 AND (FileName like '%.zip%' or FileName like '%.rar%' or FileName like '%.brd%');select * from genFileServers where ISNULL(IsActive,0)=1");

                if (dsorder.Tables[0].Rows.Count == 0)
                {
                    dsorder = objgd.GetDataSet("SELECT Session_Id as FilePath ,BasketNo as FileName FROM BasketItems  where BasketNo ='" + id + "' AND isnull(Is_Deleted ,0)=0 AND Basket_Type<>'inquiry';select * from genFileServers where ISNULL(IsActive,0)=1");
                }
                if (dsorder.Tables[0].Rows.Count > 0)
                {
                    System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);
                    if (impersonateOnFileServer != null)
                    {
                        string filePath = Convert.ToString(dsorder.Tables[0].Rows[0]["FilePath"]);
                        string fileName = Convert.ToString(dsorder.Tables[0].Rows[0]["FileName"]);
                        string rootPath = Convert.ToString(dsorder.Tables[1].Rows[0]["RootPath"]);
                        string fullPath = Path.Combine(rootPath, filePath);
                        fullPath = Path.Combine(fullPath, fileName);
                        if (id.StartsWith("B"))
                        {
                            DirectoryInfo objDir;
                            string dirpath = rootPath + "tempbasket\\" + filePath;
                            objDir = new DirectoryInfo(dirpath);
                            if (objDir.Exists)
                            {
                                foreach (FileInfo files in objDir.GetFiles("*.zip"))
                                {
                                    string FType;
                                    FType = files.Extension;
                                    string strFilename = files.Name;
                                    fullPath = rootPath + "tempbasket\\" + filePath + "\\" + strFilename;
                                }
                                foreach (FileInfo files in objDir.GetFiles("*.rar"))
                                {
                                    string FType;
                                    FType = files.Extension;
                                    string strFilename = files.Name;
                                    fullPath = rootPath + "tempbasket\\" + filePath + "\\" + strFilename;
                                }
                                foreach (FileInfo files in objDir.GetFiles("*.brd"))
                                {
                                    string FType;
                                    FType = files.Extension;
                                    string strFilename = files.Name;
                                    fullPath = rootPath + "tempbasket\\" + filePath + "\\" + strFilename;
                                }
                            }
                        }

                        System.IO.FileInfo FileName = new System.IO.FileInfo(fullPath);

                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.FileStream fs = null;
                            try
                            {
                                fs = System.IO.File.OpenRead(fullPath);
                                int length = (int)fs.Length;
                                byte[] buffer;

                                using (System.IO.BinaryReader br = new System.IO.BinaryReader(fs))
                                {
                                    buffer = br.ReadBytes(length);
                                }
                                Response.Clear();
                                Response.ContentType = "application/download";
                                Response.AddHeader("content-disposition", "attachment; filename=" + FileName.Name.ToString() + "");
                                Response.BinaryWrite(buffer);
                            }
                            catch (Exception ex)
                            {

                                throw ex;
                            }
                            finally
                            {
                                if (fs != null)
                                {
                                    fs.Close();
                                    fs.Dispose();
                                }
                            }
                        }
                        else
                        {
                            Response.Write("file not found");
                            return true;
                        }
                    }
                    return true;
                }
                else
                {
                    Response.Write("file not found");
                    return true;
                }
            }
            return false;
        }

        private bool downloadPixture(string path, string prefix)
        {
            System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);
            if (impersonateOnFileServer != null)
            {

                if (prefix == "pi")
                {
                    if (Request.QueryString["filename"] != null)
                    {
                        string filename = Convert.ToString(Request.QueryString["filename"]);
                        writeImageFile(path);
                    }
                    else
                    {
                        Response.Write("FAILED");
                        return false;
                    }
                }
                else if (prefix == "px")
                {
                    path = Constant.FileServerPath + path;
                    if (path != string.Empty)
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        string strxml = string.Empty;
                        DirectoryInfo objDir = new DirectoryInfo(path );
                        if (objDir.Exists && Directory.GetFiles(path).Length != 0)
                        {
                            strxml += "<images>";
                            foreach (FileInfo files in objDir.GetFiles())
                            {
                                if (files.Name.ToLower() == "thumbs.db")
                                {
                                    continue;
                                }
                                strxml += "<image fileName='" + files.Name + "'/>";
                            }
                            strxml += "</images>";
                            xmlDoc.LoadXml(strxml);
                            if (xmlDoc.FirstChild.NodeType != System.Xml.XmlNodeType.XmlDeclaration)
                            {
                                XmlDeclaration xmldecl;
                                xmldecl = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                                xmldecl.Encoding = "UTF-8";
                                // Add the new node to the document.
                                XmlElement root = xmlDoc.DocumentElement;
                                xmlDoc.InsertBefore(xmldecl, root);
                            }

                            Response.Write(strxml);
                            Response.ContentType = "text/xml";
                        }
                        else
                        {
                            Response.Write("FAILED");
                            return false;
                        }
                    }
                    else
                    {
                        Response.Write("FAILED");
                        return false;
                    }
                }

            }
            return false;
        }

        private bool downloadComponentFile(string path)
        {
            try
            {
                string loc = path;
                loc = Constant.FileServerPath + loc;
                using (WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain)) 
                {
                    if (impersonateOnFileServer != null)
                    {
                        if (File.Exists(loc))
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(loc);
                            //xmlDoc = shop.assembly.Component.UpdateXMLPartAttr(xmlDoc);
                            //xmlDoc.Save(loc);
                        }
                        else
                        {
                            Response.Write("FAILED");
                            return false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Response.Write("FAILED");
                //genExceptionLog objException = new genExceptionLog();
                //objException.LogException(ex, 0);

                return false;
            }
            return writefile(path);
        }


        private bool writefile(string path)
        {
            path = Constant.FileServerPath + path;
            System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);
            if (impersonateOnFileServer != null)
            {
                if (System.IO.File.Exists(path))
                {
                    System.IO.FileStream fs = null;
                    fs = System.IO.File.OpenRead(path);
                    int length = (int)fs.Length;
                    byte[] buffer;

                    using (System.IO.BinaryReader br = new System.IO.BinaryReader(fs))
                    {
                        buffer = br.ReadBytes(length);
                    }
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/" + System.IO.Path.GetExtension(path).Substring(1);
                    Response.BinaryWrite(buffer);
                }
                else
                {
                    Response.Write(path);
                    Response.StatusCode = 404;
                }
                return true;
            }
            return true;
        }
        private void UpdateXml(string VariantId, string path)
        {
            try
            {
                //EC09.Data.genFileServer objFileServer = EC09WebApp.EC09WebService.BPO.GetActiveFileServer();
                path = Constant.FileServerPath + path;
                string sqlGetRootPath = string.Format("ECC_getvariantDetail " + VariantId);
                DataSet VariantIdDetail = objgd.GetDataSet(sqlGetRootPath);
                if (VariantIdDetail != null && VariantIdDetail.Tables.Count > 0 && VariantIdDetail.Tables[0].Rows.Count > 0)
                {
                    string strProperty = "BoardUnitPrice$" + Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardUnitPrice"]) +
                     "|BoardDeliveryTerm$" + Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardDeliveryTerm"]) +
                     "|BoardDeliveryDate$" + Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardDeliveryDate"]) +
                     "|BoardTotalNetPrice$" + Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardTotalNetPrice"]) +
                     "|BoardWeight$" + Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardWeight"]) +
                     "|BoardQty$" + Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardQty"]) +
                     "|BoardLocked$" + Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardLocked"]).ToLower() +
                     "|BoardTotalOrderPrice$" + Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardTotalNetPrice"]).ToLower();
                    if (Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardPID"]).ToLower().Trim() != "")
                    {
                        strProperty = strProperty + "|BoardPID$" + Convert.ToString(VariantIdDetail.Tables[0].Rows[0]["BoardPID"]).ToUpper();
                    }
                    //EC09WebApp.EC09WebService.BPO bpo = new EC09WebApp.EC09WebService.BPO(); //pending
                    //bpo.ConfiguratorXMLUpdate(path, true, strProperty.Trim(), 0);
                    //objgd.PutData("update genInqOffersVariants set isUpdateXml=1 where InqVaraintsId=" + VariantId, Keydata);
                }

            }
            catch
            {
            }
        }

        public void isForceXml(string number, string path) //Pending
        {
            DataSet ds = new DataSet();
            try
            {
                if (!number.ToUpper().StartsWith("B"))
                {
                    System.Data.DataSet dsorder = objgd.GetDataSet("select orderid from genorders where ordernumber='" + number.Replace("STENCIL", "") + "'  and ISNULL(IsForceXml,0)=1");
                    if (dsorder.Tables[0].Rows.Count != 0)
                    {
                        long userid = 0;
                        //if (EC09WebApp.EC09Sessions.UserId != null)
                        //{
                        //    UserId = Convert.ToInt64(EC09WebApp.EC09Sessions.UserId);
                        //}
                        //HttpContext.Current.Session["UserId"]
                        if (HttpContext.Current.Session["UserId"] != null)
                        {
                            userid = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
                        }
                        //objOts.GenerateConfiguratorOrderXMl(Convert.ToString(number.Replace("STENCIL", "")), userid);
                        //objgd.PutData("update genOrders set IsForceXml=0 where OrderNumber='" + number + "'", Keydata); //pending
                    }
                }
                else
                {
                    if (number.ToLower().Contains("STENCIL") == false && number.ToUpper().StartsWith("B"))
                    {
                        System.Data.DataSet dsinquiry = objgd.GetDataSet("select * from geninqoffers where inquiryno='" + number + "'  and ISNULL(IsForceXml,0)=1");
                        if (dsinquiry.Tables[0].Rows.Count != 0)
                        {
                            System.Data.DataSet dsinquiryvariant = objgd.GetDataSet("select * from genInqOffersVariants where InqOfferId=" + Convert.ToString(dsinquiry.Tables[0].Rows[0]["InqOfferId"]));
                            if (dsinquiryvariant.Tables[0].Rows.Count != 0)
                            {
                                System.Data.DataSet dsBasket = objgd.GetDataSet("select * from BasketItems where BasketNo='" + number + "'");
                                if (dsBasket.Tables[0].Rows.Count != 0)
                                {
                                    long UserId = 0;
                                    //if (EC09WebApp.EC09Sessions.UserId != null)
                                    //{
                                    //    UserId = Convert.ToInt64(EC09WebApp.EC09Sessions.UserId);
                                    //}
                                    if (HttpContext.Current.Session["UserId"] != null)
                                    {
                                        UserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
                                    }
                                    //objOffer.GenerateConfiguratorInqXMl(number, Convert.ToString(dsBasket.Tables[0].Rows[0]["Basket_Id"]), Convert.ToString(dsBasket.Tables[0].Rows[0]["Session_Id"]), Convert.ToInt64(dsinquiryvariant.Tables[0].Rows[0]["InqVaraintsId"]));
                                    //objgd.PutData("update genInqOffers set IsForceXml=0 where InquiryNo='" + number + "'", Keydata); //pending
                                } 
                                else
                                {
                                    long UserId = 0;
                                    //if (EC09WebApp.EC09Sessions.UserId != null)
                                    //{
                                    //    UserId = Convert.ToInt64(EC09WebApp.EC09Sessions.UserId);
                                    //}
                                    if (HttpContext.Current.Session["UserId"] != null)
                                    {
                                        UserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
                                    }
                                    //objOffer.GenerateConfiguratorInquiryOlyXML(number, Convert.ToInt64(dsinquiryvariant.Tables[0].Rows[0]["InqVaraintsId"]));
                                    //objgd.PutData("update genInqOffers set IsForceXml=0 where InquiryNo='" + number + "'", Keydata);
                                }
                            }
                        }
                        else
                        {
                            if (number.ToUpper().Contains("B"))
                                dsinquiry = objgd.GetDataSet("select * from geninqoffers where inquiryno ='" + number + "' and   ISNULL(IsForceXml,0)=1");
                            else
                                dsinquiry = objgd.GetDataSet("select * from geninqoffers where inquiryno in (select BasketNo from BasketItems where convert(varchar(max),Basket_Id)='" + number + "') and   ISNULL(IsForceXml,0)=1");
                            if (dsinquiry.Tables[0].Rows.Count != 0)
                            {
                                System.Data.DataSet dsinquiryvariant = objgd.GetDataSet("select * from genInqOffersVariants where InqOfferId=" + Convert.ToString(dsinquiry.Tables[0].Rows[0]["InqOfferId"]));
                                if (dsinquiryvariant.Tables[0].Rows.Count != 0)
                                {
                                    System.Data.DataSet dsBasket = objgd.GetDataSet("select * from BasketItems where BasketNo='" + Convert.ToString(dsinquiry.Tables[0].Rows[0]["InquiryNo"]) + "'");
                                    if (dsBasket.Tables[0].Rows.Count != 0)
                                    {
                                        long UserId = 0;
                                        //if (EC09WebApp.EC09Sessions.UserId != null)
                                        //{
                                        //    UserId = Convert.ToInt64(EC09WebApp.EC09Sessions.UserId);
                                        //}
                                        if (HttpContext.Current.Session["UserId"] != null)
                                        {
                                            UserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
                                        }
                                        //objOffer.GenerateConfiguratorInqXMl(Convert.ToString(dsinquiry.Tables[0].Rows[0]["InquiryNo"]), Convert.ToString(dsBasket.Tables[0].Rows[0]["Basket_Id"]), Convert.ToString(dsBasket.Tables[0].Rows[0]["Session_Id"]), Convert.ToInt64(dsinquiryvariant.Tables[0].Rows[0]["InqVaraintsId"]));
                                        //objgd.PutData("update genInqOffers set IsForceXml=0 where InquiryNo='" + Convert.ToString(dsinquiry.Tables[0].Rows[0]["InquiryNo"]) + "'", Keydata);
                                    }
                                    else
                                    {
                                        long UserId = 0;
                                        //if (EC09WebApp.EC09Sessions.UserId != null)
                                        //{
                                        //    UserId = Convert.ToInt64(EC09WebApp.EC09Sessions.UserId);
                                        //}
                                        if (HttpContext.Current.Session["UserId"] != null)
                                        {
                                            UserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
                                        }
                                        //objOffer.GenerateConfiguratorInquiryOlyXML(number, Convert.ToInt64(dsinquiryvariant.Tables[0].Rows[0]["InqVaraintsId"]));
                                        //objgd.PutData("update genInqOffers set IsForceXml=0 where InquiryNo='" + number + "'", Keydata);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            //End Froce xml update
        }


        public void writeImageFile(string path)
        {
            System.Security.Principal.WindowsImpersonationContext impersonateOnFileServer = ReportBuilderUtilities.ImpersonateFileServer(Constant.FileServerUsername, Constant.FileServerPassword, Constant.FileServerDomain);
            if (impersonateOnFileServer != null)
            {
                path = Constant.FileServerPath + path;
                if (System.IO.File.Exists(path))
                {
                    System.Drawing.Image image = null;
                    System.IO.MemoryStream imageStream = null;
                    try
                    {
                        image = System.Drawing.Image.FromFile(path);
                        imageStream = new System.IO.MemoryStream();
                        if (Request.QueryString["Thumbimage"] != null && Request.QueryString["Thumbimage"].ToLower().Contains("yes"))
                        {
                            float OriginalHeight = image.Height;
                            float OriginalWidth = image.Width;
                            int ThumbnailWidth;
                            int ThumbnailHeight;
                            int ThumbnailMax = 2000;
                            if (OriginalHeight > OriginalWidth)
                            {
                                ThumbnailHeight = ThumbnailMax;
                                ThumbnailWidth = (int)((OriginalWidth / OriginalHeight) * (float)ThumbnailMax);
                            }
                            else
                            {
                                ThumbnailWidth = ThumbnailMax;
                                ThumbnailHeight = (int)((OriginalHeight / OriginalWidth) * (float)ThumbnailMax);
                            }
                            if (OriginalHeight < ThumbnailHeight)
                            {
                                ThumbnailWidth = (int)OriginalWidth;
                                ThumbnailHeight = (int)OriginalHeight;
                            }
                            Bitmap ThumbnailBitmap = new Bitmap(ThumbnailWidth, ThumbnailHeight);
                            Graphics ResizedImage = Graphics.FromImage(ThumbnailBitmap);
                            ResizedImage.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            ResizedImage.CompositingQuality = CompositingQuality.HighQuality;
                            ResizedImage.SmoothingMode = SmoothingMode.HighQuality;
                            ResizedImage.DrawImage(image, 0, 0, ThumbnailWidth, ThumbnailHeight);
                            ThumbnailBitmap.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                        else
                        {
                            image.Save(imageStream, System.Drawing.Imaging.ImageFormat.Png);
                        }
                        byte[] imageContent = new Byte[imageStream.Length];
                        imageStream.Position = 0;
                        imageStream.Read(imageContent, 0, (int)imageStream.Length);
                        Response.ContentType = "image/png";
                        Response.BinaryWrite(imageContent);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        if (image != null)
                        {
                            image.Dispose();
                        }
                        if (imageStream != null)
                        {
                            imageStream.Close();
                        }
                    }
                }
                else
                {
                    Response.Write("file not found");
                    return;
                }
            }
        }
    }
}
