﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="visualizer.aspx.cs" Inherits="sparrowFeApp.visulizer.visualizer" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="Microsoft.WindowsAzure" %>
<%@ Import Namespace="Microsoft.WindowsAzure.StorageClient" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%--<%@ Register Src="~/shop/WUCComments.ascx" TagPrefix="ucWUCComments" TagName="WUCComments" %>--%>

<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Communicator</title>
    <%--<script src="../../JS/jquery_1.7.2.min.js?v=3"></script>--%>
    <script src="../../JS/jquery_1.7.2.min.js?v=3"></script>  
    <script  type="text/javascript"  src="../../visulizer/webclient/webclient.nocache.js?v=<%=DateTime.Now.ToString("ddmmyyyyhhmm") %>""></script>
    <%--<script src="../Plugin/jquery.watermark.min.js?v=<%=DateTime.Now.ToString("ddmmyyyyhhmm") %>" type="text/javascript"></script>--%>
    <link href="../../shop/main.css" rel="stylesheet" type="text/css" />
    <script src="../JS/Common.js?v=3.5"></script>
    <script src="../JS/jquery-ui.js" type="text/javascript"></script>
    <style type="text/css">
        .divPrognew {
            background: none repeat scroll 0 0 #e8791c;
            border-radius: 1px;
            color: #fff;
            font-size: 12px;
            padding: 5px 7px;
            position: fixed;
            z-index: 100000;
        }

        .pageloader {
            display: none;
            /*top: 49%;*/
            position: absolute;
            left: 45%;
            z-index: 20;
        }

        .loader-block {
            display: none;
            height: 100%;
            position: absolute;
            width: 100%;
            background: black;
            z-index: 10;
            opacity: 0.1;
        }
    </style>
</head>
<body>
    <div class="pageloader divPrognew" style="display: none;">
        <div style="display: inline-block;">
            <img src="/shop/images/Small_Loading.gif" />
        </div>
        <div style="display: inline-block; vertical-align: top;">
            <span id="lblLoading">Loading...</span>
        </div>
    </div>
    <form id="form1" runat="server">
        <div>
            <%--<ucWUCComments:WUCComments ID="WUCComments" EnableViewState="true" runat="server" />--%>
        </div>
        <script>
            function CallVis(OnNmber, EncrptCode, Visualizertype,action) {
                var Langcode = '<%= hdnLangcode.Value %>';
                var DelAddPrefix = '<%= hidenDelAddCountryId.Value %>';
                var InvAddPreFix = '<%= hidenInvAddCountryId.Value %>';
                var vatCountryCode = '<%= hdnvatCountryCode.Value %>';
                var noVAT = '<%= hdnnoVAT.Value %>';
                var IsBusinessCustomer = '<%= hdnIsBusinessCustomer.Value %>';
                var handlingCompany = '<%= hdnhandlingCompany.Value %>';
                var Visualizertype = Visualizertype;
                var isBindipullAllow = '<%= hdnIsBindipullAllow.Value %>';
                var factor = '<%= hdnCurrencyFactor.Value %>';
                var symbol = '<%= hdnCurrencySymbol.Value %>';
                var Unit = '<%= hdnCurrencyUnit.Value %>';
                var CV = '<%= hdnCV.Value %>';
                var hasremark = '<%= hdnhasremark.Value %>';
                var ppaexception = '<%= hdnppaexception.Value %>';
                var isPCBAEditable = true;
                if (Visualizertype.toLowerCase() == 'view') {
                    isPCBAEditable = false;
                }
                if (action == "assembly") {
                    resetClient();
                    setEnableAssemblyOrder(true);
                    showAssemblyEditor(OnNmber, Langcode, EncrptCode, DelAddPrefix.toUpperCase(), InvAddPreFix.toUpperCase(), Visualizertype, false,
                        Unit, symbol, factor, IsBusinessCustomer, handlingCompany, vatCountryCode, noVAT, isBindipullAllow, hasremark, null, null, CV, null, null);
                }
                
                else { 
                    resetClient();
                    setEnableAssemblyOrder(true);
                    if (!OnNmber.startsWith("B")) { 
                        if (ppaexception == "true") {
                            setHasPPAExceptionDocument(true);
                        }
                        else {
                            setHasPPAExceptionDocument(false);
                        } 
                    }
                    showVisualizer(OnNmber, "en", EncrptCode, DelAddPrefix.toUpperCase(), InvAddPreFix.toUpperCase(), Visualizertype, false,
                        Unit, symbol, factor, IsBusinessCustomer, handlingCompany, vatCountryCode, noVAT, isBindipullAllow, hasremark, CV, null, null, false);
                }
                
            }
            function showAssembly_Editor(OnNmber, EncrptCode, Visualizertype,action) {
                if (typeof setPopupVisualizer != 'undefined') {
                    setPopupVisualizer(false);
                    CallVis(OnNmber, EncrptCode, Visualizertype,action);
                }
                else {
                    setTimeout(function () {
                        showAssembly_Editor(OnNmber, EncrptCode, Visualizertype,action);
                    }, 100);
                }
            }

            function showPIXpect(id, language, key, itemName, imageName, region) {

                if (typeof setPopupVisualizer != 'undefined') {
                    setPopupVisualizer(false);
                    showPIXpect(id, language, key, itemName, imageName, region);
                }
                else {
                    setTimeout(function () {
                        showPIXpect(id, language, key, itemName, imageName, region);
                    }, 100);
                }

            }
            jQuery(window).load(function () {
                $('.loader-block').hide();
                $('.pageloader').hide();
            });
            jQuery(document).ready(function ($) {
                $('.loader-block').hide();
                $('.pageloader').show();
            });
        </script>
        <asp:HiddenField ID="hdnorderNumber" runat="server" />
        <asp:HiddenField ID="hdnrequestKey" runat="server" />
        <asp:HiddenField ID="hdncommercial" runat="server" />
        <asp:HiddenField ID="hdnConfigurator" runat="server" />
        <asp:HiddenField ID="hdnLangcode" Value="en" runat="server" />
        <asp:HiddenField ID="hdnlangid" Value="en" runat="server" />
        <asp:HiddenField ID="hdnIsInquiry" Value="0" runat="server" />
        <asp:HiddenField ID="hdnEccUserId" Value="en" runat="server" />
        <asp:HiddenField ID="hidenDelAddCountryId" runat="server" />
        <asp:HiddenField ID="hidenInvAddCountryId" runat="server" />
        <asp:HiddenField ID="hdnvatCountryCode" runat="server" />
        <asp:HiddenField ID="hdnnoVAT" runat="server" />
        <asp:HiddenField ID="hdnIsBusinessCustomer" runat="server" />
        <asp:HiddenField ID="hdnhandlingCompany" runat="server" />
        <asp:HiddenField ID="hidenhasVat" runat="server" />
        <asp:HiddenField ID="hdnIsBindipullAllow" runat="server" />
        <asp:HiddenField ID="hdnCurrencyFactor" runat="server" />
        <asp:HiddenField ID="hdnCurrencySymbol" runat="server" />
        <asp:HiddenField ID="hdnCurrencyUnit" runat="server" />
        <asp:HiddenField ID="ctl00_hdnUserid" runat="server" />
        <asp:HiddenField ID="ctl00_hdnUserFullName" runat="server" />
        <asp:HiddenField ID="hdnCV" runat="server" />
        <asp:HiddenField ID="hdnhasremark" Value="false" runat="server" />
        <asp:HiddenField ID="hdnppaexception" Value="false" runat="server" />
    </form>
</body>
<script runat="server"> 
    const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
    protected new void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //string ip = Request.UserHostAddress; 
            //string userAgent = Request.UserAgent;
            //long ticks = DateTime.UtcNow.Ticks;
            //string token = sparrowFeApp.core.Security.GenerateToken("username","password",ip,userAgent,ticks);

            bool isTokenValid = sparrowFeApp.core.Security.IsTokenValid(Request["tokenid"]);

            if (!isTokenValid)
            {
                Response.Write("Token is Expired !!!");
                return;
            }
            //EC09WebApp.API.gd objAPI = new EC09WebApp.API.gd();
            if (!IsPostBack)
            {
                if (Request["number"] != null && Request["number"] != "")
                {
                    sparrowFeApp.core.AppSessions.EccUserId = "0";
                    sparrowFeApp.core.AppSessions.FEUserId = "0";
                    if (Request["crm_id"] != null && Request["crmid"] != "")
                    {
                        sparrowFeApp.core.AppSessions.EccUserId = Request.QueryString["crmid"];
                    }

                    if (Request["usrid"] != null && Request["usrid"] != "")
                    {
                        sparrowFeApp.core.AppSessions.FEUserId = Request.QueryString["usrid"];
                    }

                    Session["EDAPART"] = "TRUE";
                    if (Request["PIXpec"] != null && Request["PIXpec"] == "yes")
                    {
                        string image = Request["image"].ToString();
                        string Str = " showPIXpect('" + Request["number"] + "','en','" + sparrowFeApp.visdownload.EncryptLast(Request["number"]) + "','" + Request["number"] + "', '" + image + "','s3.eu-central-1.amazonaws.com/euroc-inspect');";
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "resrser", Str, true);

                        return;
                    }
                    else
                    {
                        string type = "basket";

                        string OrderNumber = Request["number"].ToString();
                        if (!OrderNumber.ToUpper().StartsWith("B"))
                        {
                            type = "order";
                        }
                        string viewType = "modifyOrder";
                        if (type == "basket")
                        {
                            viewType = "modifyassembly";
                        }

                        if (Request["viewType"] != null && Request["viewType"] != "")
                        {
                            viewType = Request.QueryString["viewType"];
                        }

                        string action = "";
                        if(Request["action"]!=null && Request["action"] != "")
                        {
                            action = Request["action"].ToString();
                        }
                        if (action.Equals("visualizer") || action.Equals("assembly"))
                        {
                            viewType = "view";
                        }
                        else if (action.Equals("modifyassembly"))
                        {
                            viewType = "modifyassembly";
                            action = "assembly";
                        
                        }

                        string itemnumber = Request["number"].ToString();
                        var objAPI = new Database.DA();
                        sparrowFeApp.ec09Service.ECAPISoapClient ecAPI = new sparrowFeApp.ec09Service.ECAPISoapClient();
                        System.Data.DataTable orderData = ecAPI.GetVisualizerData(itemnumber,"0");
                        string DelAddPrefix = "";
                        string InvAddPreFix = "";
                        string factor = orderData.Rows[0]["Factor"].ToString();
                        string symbol = orderData.Rows[0]["Symbol"].ToString();
                        string Unit ="mm";
                        if (symbol.ToUpper() == "USD")
                        {
                            Unit = "mil";
                        }

                        DelAddPrefix = orderData.Rows[0]["delCountry"].ToString(); //"US";
                        InvAddPreFix = orderData.Rows[0]["InvCountry"].ToString(); // "BE";
                        ctl00_hdnUserid.Value = orderData.Rows[0]["UserName"].ToString();
                        ctl00_hdnUserFullName.Value =orderData.Rows[0]["CustomerName"].ToString(); //"MitulOpenComp";
                        hdnLangcode.Value = "en";
                        hidenDelAddCountryId.Value = DelAddPrefix;
                        hidenInvAddCountryId.Value = InvAddPreFix;
                        string vatCountryCode = Convert.ToString(orderData.Rows[0]["vatCountryCode"]);
                        string noVAT = Convert.ToString(orderData.Rows[0]["noVAT"]);
                        string IsBusinessCustomer =  Convert.ToString(orderData.Rows[0]["IsBusinessCustomer"]);
                        string handlingCompany = Convert.ToString(orderData.Rows[0]["handlingCompany"]);
                        string isBindipollAllow = Convert.ToString(orderData.Rows[0]["IsBindiPoolAllow"]).ToLower();
                        string CV =  Convert.ToString(orderData.Rows[0]["CV"]);
                        string IsAllowAssembly = Convert.ToString(orderData.Rows[0]["IsAllowAssembly"]).ToLower();
                        string hasremark =  Convert.ToString(orderData.Rows[0]["hasremark"]);
                        string ppaexception = Convert.ToString(orderData.Rows[0]["ppaexception"]).ToLower();
                        Session["POWERBASKET"] = "YES";
                        hdnvatCountryCode.Value = vatCountryCode;
                        hdnnoVAT.Value = noVAT;
                        hdnIsBusinessCustomer.Value = IsBusinessCustomer;
                        hdnhandlingCompany.Value = handlingCompany;
                        hdnIsBindipullAllow.Value = isBindipollAllow;
                        hdnCurrencyFactor.Value = factor;
                        hdnCurrencySymbol.Value = symbol;
                        hdnCurrencyUnit.Value = Unit;
                        hdnCV.Value = CV;
                        hdnhasremark.Value = hasremark;
                        hdnppaexception.Value = ppaexception;



                        string Str = " {showAssembly_Editor('" + OrderNumber + "','" + sparrowFeApp.visdownload.EncryptLast(OrderNumber) + "', '" + viewType + "','" + action +"');}";
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "resrser", Str, true);

                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
</script>
<script>
    function showAssemblyFeedback() { 
        $('#divComment').css("display", "none"); 
        //window.location = "https://be.eurocircuits.com/shop/orders/eccordermodified.aspx?successmsgtitle=saveandclose&entityno=Assembly&successmsgdesc=Assembly.";
        window.location.href = "/shop/orders/eccOrderModified.aspx?successmsgtitle=APPROVED&action=modifyassembly";
    }
</script>
</html>

