﻿<%@ Page Language="C#" Debug="true" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Collections.Generic" %>

<script runat="server">
      
    /// <summary>
    /// Max records that are allowed to affected by query. 
    /// If affected data are more then set limit then it can be only done by super user.
    /// </summary>
    private const int maxAffectedRows = 10;

    private const string superPWD = "ispl123;";

    private const string password = "EC@2025";

    protected void Page_Load(object sender, EventArgs e)
    {

        txtPassword.Focus();
        if (txtPassword.Value == password)
        {
            pwdContainer.Visible = false;
            container.Visible = true;
            txtQuery.Focus();
        }
    }

    private bool ValidateQuery(string query, out bool allowExec, out string command, out string error, out int affectedRows)
    {
        string superUserMsg = "Contact to super user in order to excute query.";
        string joinError = "Join is used in query. Not able to find affected rows. " + superUserMsg;
        command = "";
        error = "";
        allowExec = false;
        affectedRows = 0;

        if (query == "")
        {
            error = "You are genius. How can I excute blank query !!!";
            return false;
        }

        if (txtSaPwd.Value.Trim() != "" && txtSaPwd.Value != superPWD)
        {
            error = "Super user password is wrong.";
            return false;
        }

        //bool isACStatement = false;
        //if (query.Contains("alter ") || query.Contains("create "))
        //{
        //  isACStatement = true;
        //}

        //string[] qs = null;
        //if (isACStatement)
        //{
        //  qs = query.Split(new string[2] { "alter ", "create " }, StringSplitOptions.RemoveEmptyEntries);
        //}
        //else
        //{
        //  if (query.Contains("update "))
        //  {
        //    command = "update";
        //  }
        //  else if (query.Contains("delete "))
        //  {
        //    command = "delete";
        //  }
        //  else if (query.Contains("select "))
        //  {
        //    command = "select";
        //  }
        //  else if (query.Contains("drop "))
        //  {
        //    command = "drop";
        //  }

        //  qs = query.Split(new string[3] { "delete ", "drop  ", "update " }, StringSplitOptions.RemoveEmptyEntries);
        //}

        //if (qs.Length > 1)
        //{
        //  error = "Only multiple select statements are allowed to excute.";
        //  return false;
        //}

        //if (isACStatement)
        //{
        //  return true;
        //}

        ////DROP validation
        //if (command == "drop")
        //{
        //  error = superUserMsg;
        //  allowExec = true;
        //  return false;
        //}

        if (!query.Contains("where"))
        {
            error = "WHERE condition is required.";
            return false;
        }

        //if (query.Contains("--"))
        //{
        //    error = "Comment is not allowed.";
        //    return false;
        //}

        //if (command == "select")
        //{
        //    return true;
        //}

        //query = query.Replace(System.Environment.NewLine, " ");

        //string select = "", table = "", where = "";
        //string[] up1;

        ////UPDATE / DELETE validation

        //if (command == "update")
        //{
        //    //TODO: Find affected rows    
        //    if (query.Contains("from ") && query.Contains("join "))
        //    {
        //        /*up1 = query.Split(new string[1] { " from " }, StringSplitOptions.RemoveEmptyEntries);
        //        select = "select count(*)";
        //        for(int i = 1; i < up1.Length; i++)
        //        {
        //          select += " from " + up1[i];
        //        }*/

        //        error = joinError;
        //        allowExec = true;
        //        return false;
        //    }
        //    else
        //    {
        //        up1 = query.Split(new string[1] { " set " }, StringSplitOptions.RemoveEmptyEntries);

        //        table = up1[0].Split(new string[1] { "update " }, StringSplitOptions.None)[1].Trim();
        //        where = up1[1].Split(new string[1] { " where " }, StringSplitOptions.None)[1].Trim();
        //        select = "select count(*) from " + table + " where " + where;
        //    }
        //}
        //else if (command == "delete")
        //{
        //    //TODO: Find affected rows
        //    if (query.Contains(" from ") && query.Contains("join "))
        //    {
        //        error = joinError;
        //        allowExec = true;
        //        return false;
        //    }
        //    else
        //    {
        //        up1 = query.Split(new string[1] { " from " }, StringSplitOptions.RemoveEmptyEntries);
        //        string[] up2 = up1[1].Split(new string[1] { " where " }, StringSplitOptions.None);
        //        table = up2[0].Trim();
        //        where = up2[1].Trim();
        //        select = "select count(*) from " + table + " where " + where;
        //    }
        //}

        //affectedRows = Convert.ToInt32(GetDataSet(select).Tables[0].Rows[0][0]);

        //if (affectedRows == 0)
        //{
        //    error = "Oops!! ZERO row is affected from query. Query cannot be executed.";
        //    return false;
        //}

        //if (affectedRows > maxAffectedRows)
        //{
        //    allowExec = true;
        //    error = "Oops!! Affected rows are " + affectedRows + "." + superUserMsg;
        //    return false;
        //}

        return true;
    }

    protected void btnExecute_ServerClick(object sender, EventArgs e)
    {
        string query = txtQuery.Value.Trim();
        string command;
        string error;
        int affectedRows = 0;
        bool allowExec = false;

        try
        {
            bool isValid = ValidateQuery(query, out allowExec, out command, out error, out affectedRows);
            ViewState["Command"] = command;
            ViewState["AffectedRows"] = affectedRows;

            if (isValid)
            {
                spnMsg.Attributes.Add("class", "success");
                btnExe_ServerClick(null, null);
            }
            else
            {
                spnMsg.InnerHtml = "<h3>" + error + "</h3>";
                spnMsg.Attributes.Add("class", "error");


                return;
            }


        }
        catch (Exception ex)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<h3>Oops!! Something went wrong. Take a cup of coffee and correct the mistake.</h3>");
            builder.Append("Message: " + ex.Message);
            builder.Append("<br/>InnerException: " + ex.InnerException);
            builder.Append("<br/>Source: " + ex.Source);
            builder.Append("<br/>StackTrace: " + ex.StackTrace);
            builder.Append("<br/>TargetSite: " + ex.TargetSite);
            builder.Append("<br/>HelpLink: " + ex.HelpLink);

            spnMsg.InnerHtml = builder.ToString();
            spnMsg.Attributes.Add("class", "error");
        }
    }

    protected void btnExe_ServerClick(object sender, EventArgs e)
    {
        txtSaPwd.Value = "";

        DateTime startTime = DateTime.UtcNow;

        DataSet data = GetDataSet(txtQuery.Value);
        if (data.Tables.Count > 0)
        {
            for (int i = 0; i < data.Tables.Count; i++)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl createDiv =
               new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                createDiv.Attributes["class"] = "GHeadMain";
                System.Web.UI.HtmlControls.HtmlGenericControl GHead =
                new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                GHead.ID = "GHead" + i;
                GHead.Attributes["class"] = "GHead";
                createDiv.Controls.Add(GHead);
                GridView grid = new GridView();
                grid.HeaderStyle.CssClass = "GridHeader";
                grid.ID = "Grd"+i.ToString();
                grid.RowCreated += delegate(object dsender, GridViewRowEventArgs ge)
                {
                    if (ge.Row.RowType == DataControlRowType.Footer)
                    {
                        ge.Row.Cells[0].Text = "Total rows:" + data.Tables[i].Rows.Count;

                        int cols = ge.Row.Cells.Count;
                        for (int ic = cols - 1; ic >= 1; ic += -1)
                        {
                            ge.Row.Cells.RemoveAt(ic);
                        }
                        ge.Row.Cells[0].ColumnSpan = cols;
                    }
                };
                grid.ShowFooter = true;
                grid.FooterStyle.CssClass = "footer";
                grid.DataSource = data.Tables[i];
                grid.DataBind();
                createDiv.Controls.Add(grid);
                divData.Controls.Add(createDiv);
                createDiv.ID = i.ToString();
            }
        }

        double seconds = (DateTime.UtcNow - startTime).TotalSeconds;
        string time = seconds + " Seconds";
        if (seconds > 60 && seconds < 3600)
        {
            time = (seconds / 60) + " Minutes";
        }

        string infoText = ViewState["Command"].ToString() == "select" ? "Rows: " : "Affected rows: ";
        spnMsg.InnerHtml = "<h3>Query executed successfully. Execution time:" + time + "</h3>";
        if (ViewState["Command"].ToString() == "update" || ViewState["Command"].ToString() == "delete")
        {
            spnMsg.InnerHtml += "Affected rows:" + ViewState["AffectedRows"].ToString();
        }
        spnMsg.Attributes.Add("class", "success");
    }

    private DataSet GetDataSet(string strQuery)
    {
        try
        {
            //using (SqlConnection sqlConnnection = new SqlConnection(connectionString))
            //{
            //    sqlConnnection.Open();
            //    using (SqlCommand cmd = new SqlCommand(strQuery, sqlConnnection))
            //    {
            //        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            //        {
            //            DataSet ds = new DataSet();
            //            adapter.Fill(ds);
            //            return ds;
            //        }
            //    }
            //} 
            DataSet ds = new DataSet();
            Database.DA da = new Database.DA();
            ds = da.GetDataSet(strQuery);
            return ds;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
    }
</script>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script src="shop/JS/ScrollableGridPlugin.js"></script>
  <title></title>
  <style>
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: url('/fonts/open-sans-v13-latin-regular.eot'); /* IE9 Compat Modes */
  src: local('Open Sans'), local('OpenSans'),
       url('/fonts/open-sans-v13-latin-regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('/fonts/open-sans-v13-latin-regular.woff2') format('woff2'), /* Super Modern Browsers */
       url('/fonts/open-sans-v13-latin-regular.woff') format('woff'), /* Modern Browsers */
       url('/fonts/open-sans-v13-latin-regular.ttf') format('truetype'), /* Safari, Android, iOS */
       url('/fonts/open-sans-v13-latin-regular.svg#OpenSans') format('svg'); /* Legacy iOS */
}
/* open-sans-italic - latin */
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 400;
  src: url('/fonts/open-sans-v13-latin-italic.eot'); /* IE9 Compat Modes */
  src: local(' Italic'), local('OpenSans-Italic'),
       url('/fonts/open-sans-v13-latin-italic.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('/fonts/open-sans-v13-latin-italic.woff2') format('woff2'), /* Super Modern Browsers */
       url('/fonts/open-sans-v13-latin-italic.woff') format('woff'), /* Modern Browsers */
       url('/fonts/open-sans-v13-latin-italic.ttf') format('truetype'), /* Safari, Android, iOS */
       url('/fonts/open-sans-v13-latin-italic.svg#OpenSans') format('svg'); /* Legacy iOS */
}
/* open-sans-600 - latin */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: url('/fonts/open-sans-v13-latin-600.eot'); /* IE9 Compat Modes */
  src: local('Open Sans Semibold'), local('OpenSans-Semibold'),
       url('/fonts/open-sans-v13-latin-600.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('/fonts/open-sans-v13-latin-600.woff2') format('woff2'), /* Super Modern Browsers */
       url('/fonts/open-sans-v13-latin-600.woff') format('woff'), /* Modern Browsers */
       url('/fonts/open-sans-v13-latin-600.ttf') format('truetype'), /* Safari, Android, iOS */
       url('/fonts/open-sans-v13-latin-600.svg#OpenSans') format('svg'); /* Legacy iOS */
}
/* open-sans-600italic - latin */
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 600;
  src: url('/fonts/open-sans-v13-latin-600italic.eot'); /* IE9 Compat Modes */
  src: local('Open Sans Semibold Italic'), local('OpenSans-SemiboldItalic'),
       url('/fonts/open-sans-v13-latin-600italic.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('/fonts/open-sans-v13-latin-600italic.woff2') format('woff2'), /* Super Modern Browsers */
       url('/fonts/open-sans-v13-latin-600italic.woff') format('woff'), /* Modern Browsers */
       url('/fonts/open-sans-v13-latin-600italic.ttf') format('truetype'), /* Safari, Android, iOS */
       url('/fonts/open-sans-v13-latin-600italic.svg#OpenSans') format('svg'); /* Legacy iOS */
}
    body *{
      font-family: "Open Sans", Trebuchet MS;
      font-size: 12px;
    }

    #btnLogin{
      display:none;
    }
    .error {
      color: red;
    }

    .success {
      color: green;
    }

    table {
      /*display: block;*/
      overflow-x: auto;
      width: 100%;
      max-height: 500px;
      color: #333;
      border-collapse: collapse;
      border-spacing: 0;
      /*margin-top: 5px;*/
    }
      .GHeadMain {
          width:100%;
         overflow-x:scroll;
      }
    .GridHeader {
      font-weight: bold;
      background: #DFDFDF;
top:expression(this.offsetParent.scrollTop);  
z-index: 10;  
    }

    td {
      border: 1px solid #ccc;
      transition: all 0.3s;
    }
td:hover {
background: #fffea1 !important;
}

    tr:hover {
      background: #fffea1 !important;
    }

    tr:nth-child(even) td {
      background: #F1F1F1;
    }

    tr:nth-child(odd) td {
      background: #FEFEFE;
    }

    .footer {      
      margin-top:5px;
      display:inline-block;
    }
    h3 {
      line-height:0;
    }
  ::-webkit-scrollbar-button {
  background-color: #fff;
  background-repeat: no-repeat;
  cursor: pointer;
}

::-webkit-scrollbar-track {
  background-color: #fff;
}

::-webkit-scrollbar-corner {
  background-color: #fff;
}

::-webkit-scrollbar {
  height: 12px;
  width: 12px;
}

::-webkit-scrollbar-thumb {
  border-radius: 9px;
  border: solid 3px #fff;
  background-color: #c8c8c8;
}

::-webkit-scrollbar-thumb:vertical:hover {
  background-color: #666;
}
::-webkit-scrollbar-thumb:horizontal:hover {
  background-color: #666;
}
  </style>

  <script>
    document.onkeydown = function (e) {
      if (e.keyCode === 116) {
        document.getElementById('btnExecute').click();
        return false;
      }
    };
    $(document).ready(function () {
        $(".GHeadMain").each(function () {

            var Id = this.id;
              $('#Grd'+Id).Scrollable({
                  ScrollHeight: 300
    });
        });
    });


  </script>
</head>
<body>
  <form id="form1" runat="server">
      
    <div id="pwdContainer" runat="server" visible="true" style="text-align:center">
      <input type="password" id="txtPassword" runat="server" placeholder="Enter password"/>
    </div>

    <div id="container" runat="server" visible="false">
      <div>
        <textarea runat="server" id="txtQuery" style="width: 100%;" rows="13" placeholder="Only multiple select statements are allowed to excute."></textarea>
      </div>
      <div>
        <input type="button" runat="server" value="Run or F5" id="btnExecute" onserverclick="btnExecute_ServerClick"  title="Shortcut F5" />
        &nbsp;&nbsp;
      <input type="password" runat="server" id="txtSaPwd" placeholder="Super user password" visible="false" />
      </div>
      <div style="margin-top: 10px;">
        <span runat="server" id="spnMsg"></span>
      </div>
      
      <div runat="server" id="divData"></div>
            </div>
      <input type="button" id="btnExe" runat="server" onserverclick="btnExe_ServerClick" visible="false" />
    
  </form>
</body>
</html>
