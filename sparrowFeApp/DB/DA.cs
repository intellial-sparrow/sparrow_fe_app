﻿using Npgsql;
using System;
using System.Configuration;
using System.Data;


namespace Database
{
  public class DA
  {
    private string connectionString;
    public DA()
    {
      connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;        
    }

    public int ExecuteQuery(string query)
    {
      try
      {
        using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
        {
          connection.Open();
          using (NpgsqlCommand command = new NpgsqlCommand(query, connection))
          {
            return command.ExecuteNonQuery();
          }
        }
      }
      catch (NpgsqlException ex)
      {
        throw ex;
      }
    }

    public int ExecuteQuery(string storeProcedure, NpgsqlParameter[] parameters)
    {
      try
      {
        using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
        {
          connection.Open();
          using (NpgsqlCommand command = new NpgsqlCommand(storeProcedure, connection))
          {
            command.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < parameters.Length; i++)
            {
              command.Parameters.Add(parameters[i]);
            }
            return command.ExecuteNonQuery();
          }
        }
      }
      catch (NpgsqlException ex)
      {
        throw ex;
      }
    }

    public long ExecuteQueryReturnIdentity(string storeProcedure, NpgsqlParameter[] parameters, string outParameter)
    {
      long id = -1;
      try
      {
        using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
        {
          connection.Open();
          using (NpgsqlCommand cmd = new NpgsqlCommand(storeProcedure, connection))
          {
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < parameters.Length; i++)
            {
              cmd.Parameters.Add(parameters[i]);
            }
            cmd.Parameters[outParameter].Direction = ParameterDirection.Output;
            int row = cmd.ExecuteNonQuery();
            id = Convert.ToInt64(cmd.Parameters[outParameter].Value);
            return id;
          }
        }
      }
      catch (NpgsqlException ex)
      {
        throw ex;
      }
    }

    public object ExecuteQueryReturnIdentityObject(string storeProcedure, NpgsqlParameter[] parameters, string outParameter)
    {
      try
      {
        using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
        {
          connection.Open();
          using (NpgsqlCommand cmd = new NpgsqlCommand(storeProcedure, connection))
          {
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < parameters.Length; i++)
            {
              cmd.Parameters.Add(parameters[i]);
            }
            cmd.Parameters[outParameter].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            return cmd.Parameters[outParameter].Value.ToString();
          }
        }
      }
      catch (NpgsqlException ex)
      {
        throw ex;
      }
    }

    public object ExecuteScaler(string query, NpgsqlParameter[] Params)
    {
      try
      {
        using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
        {
          connection.Open();
          using (NpgsqlCommand cmd = new NpgsqlCommand(query, connection))
          {
            return cmd.ExecuteScalar();
          }
        }
      }
      catch (NpgsqlException ex)
      {
        throw ex;
      }
    }

    public DataSet FillDataSet(NpgsqlCommand command, DataSet ds, DataTable cur = null, int? timeout = null)
    {
      using (NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command))
      {
        if (timeout.HasValue)
        {
          adapter.SelectCommand.CommandTimeout = timeout.Value;
        }

        DataSet dataSet = new DataSet();
        if (cur == null)
        {
          adapter.Fill(dataSet);
        }
        else
        {
          dataSet.Tables.Add(cur.Copy());
        }

        foreach (DataTable dataTable in dataSet.Tables)
        {
          if (dataTable.Columns.Count == 1 && dataTable.Rows.Count > 0 && dataTable.Rows[0][0].ToString().Contains("unnamed portal"))
          {
            foreach (DataRow row in dataTable.Rows)
            {
              command.CommandText = "fetch all in \"" + row[0].ToString() + "\"";
              command.CommandType = CommandType.Text;
              using (NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(command))
              {
                if (timeout.HasValue)
                {
                  dataAdapter.SelectCommand.CommandTimeout = timeout.Value;
                }

                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);

                FillDataSet(command, ds, dt);
              }
            }
          }
          else
          {
            DataTable dt = dataTable.Copy();
            dt.TableName = ds.Tables.Count == 0 ? "Table" : "Table" + ds.Tables.Count;
            ds.Tables.Add(dt);
          }
        }
      }
      return ds;
    }

    public DataTable GetDataTable(string strQuery)
    {
      return GetDataSet(strQuery).Tables[0];
    }

    public DataTable GetDataTable(string StoreProcedureName, NpgsqlParameter[] Params)
    {
      return GetDataSet(StoreProcedureName, Params).Tables[0];
    }

    public DataSet GetDataSet(string query)
    {
      DataSet ds = new DataSet();
      try
      {
        using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
        {
          connection.Open();
          using (NpgsqlTransaction tran = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
          {
            using (NpgsqlCommand command = new NpgsqlCommand(query, connection))
            {
              ds = FillDataSet(command, ds);
            }
            tran.Commit();
          }
        }
      }
      catch (NpgsqlException ex)
      {
        throw ex;
      }
      return ds;
    }

    public DataSet GetDataSet(string storeProcedure, NpgsqlParameter[] parameters, int? timeout = null)
    {
      DataSet ds = new DataSet();
      try
      {
        using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
        {
          connection.Open();
          using (NpgsqlTransaction tran = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
          {
            using (NpgsqlCommand command = new NpgsqlCommand(storeProcedure, connection))
            {
              command.CommandType = CommandType.StoredProcedure;

              for (int i = 0; i < parameters.Length; i++)
              {
                command.Parameters.Add(parameters[i]);
              }
              ds = FillDataSet(command, ds, null, timeout);
            }
            tran.Commit();
          }
        }
      }
      catch (NpgsqlException ex)
      {
        throw ex;
      }
      return ds;
    }
  }
}
