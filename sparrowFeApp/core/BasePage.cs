﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Xml;

namespace sparrowFeApp.core
{
    public partial class BasePage : System.Web.UI.Page
    {
        private int handlingCompanyId = 42708;

        private int languageId = 493;

        public int ResourceId { get; set; }

        public new string GetLocalResourceObject(string resourceKey)
        {
            string value = "";
            XmlNode newXmlNode = GetCaptionXmlDocument().SelectSingleNode("resources/data[@name='" + resourceKey + "']");
            if (newXmlNode != null)
            {
                value = newXmlNode.SelectSingleNode("value").InnerText;
            }

            return value;
        }

        private XmlDocument GetCaptionXmlDocument()
        {
            string cacheKey = "Resource_" + ResourceId + "_" + handlingCompanyId + "_" + languageId;

            XmlDocument xmlDocument = HttpContext.Current.Cache[cacheKey] as XmlDocument;

            if (xmlDocument == null)
            {
                ec09Service.ECAPISoapClient ec09Service = new ec09Service.ECAPISoapClient();
                try
                {
                    string xmlResource = ec09Service.GetXMLResource(ResourceId, handlingCompanyId, languageId);
                    if (xmlResource == "")
                    {
                        throw new Exception("XML resource is not available");
                    }

                    xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(xmlResource);

                    HttpRuntime.Cache.Add(cacheKey, xmlDocument, new CacheDependency(HostingEnvironment.MapPath("~/shop/assembly/resources/cache_dependency.txt")), DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Normal, null);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ec09Service.Close();
                }
            }

            return xmlDocument;
        }
    }
}