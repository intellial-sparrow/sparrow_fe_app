﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace sparrowFeApp.core
{
    public static class Constant
    {
        public static string FileServerDomain = ConfigurationManager.AppSettings["FileServerDomain"];
        public static string BomLimit = ConfigurationManager.AppSettings["bom_limit"].ToString();
        public static string BomCellLimit = ConfigurationManager.AppSettings["bom_Cell_limit"].ToString();
        public static string FileServerUsername = ConfigurationManager.AppSettings["FileServerUsername"].ToString();
        public static string FileServerPassword = ConfigurationManager.AppSettings["FileServerPassword"].ToString();
        public static string FileServerPath = ConfigurationManager.AppSettings["FileServerPath"].ToString();
        public static string DwLogonProvider = ConfigurationManager.AppSettings["dwLogonProvider"].ToString();
        public static string DwLogonType = ConfigurationManager.AppSettings["dwLogonType"].ToString(); 
        public static string SparrowAPI = ConfigurationManager.AppSettings["SparrowAPI"].ToString(); 
        public static string ECurl = ConfigurationManager.AppSettings["ECurl"].ToString();
        public static string CofiguratorUrl = ConfigurationManager.AppSettings["CofiguratorUrl"].ToString();
        public static string sparrowFeAppAPI = ConfigurationManager.AppSettings["sparrowFeAppAPI"].ToString();
        public static string Username = ConfigurationManager.AppSettings["Username"].ToString();
        public static string Password = ConfigurationManager.AppSettings["Password"].ToString();
        public static string FeDomain = ConfigurationManager.AppSettings["FEDOMAIN"].ToString();
        public static string FeAppApiKey = ConfigurationManager.AppSettings["FeAppApiKey"].ToString();


        public enum CacheKey
        {
            Customers,
            OTK,
            Widget,
            BOMColumns,
            MappedColumns
        }
    }
}