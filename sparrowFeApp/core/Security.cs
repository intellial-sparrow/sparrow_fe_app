﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace sparrowFeApp.core
{
    public class Security
    {
        private const string _alg = "HmacSHA256";
        private const string _salt = "rz8LuOtFBXphj9WQfvFh";
        private const int _expirationMinutes = 10;
        static string Username, Password;

        //public static string GenerateToken(string username, string password, string ip, string userAgent, long ticks)
        //{
        //    Username = username; Password = password;
        //    return visdownload.EncryptLast(ticks.ToString());
        //}
        //public static string GetHashedPassword(string password)
        //{
        //    string key = string.Join(":", new string[] { password, _salt });
        //    using (HMAC hmac = HMACSHA256.Create(_alg))
        //    {
        //        // Hash the key.
        //        hmac.Key = Encoding.UTF8.GetBytes(_salt);
        //        hmac.ComputeHash(Encoding.UTF8.GetBytes(key));
        //        return Convert.ToBase64String(hmac.Hash);
        //    }
        //}
    
        public static bool IsTokenValid(string token)
        {
            bool result = false;
            try
            {
                string key = visdownload.DecryptLast(token);
                // Split the parts.
                long ticks = long.Parse(key);
                DateTime timeStamp = new DateTime(ticks);
                bool expired = Math.Abs((DateTime.UtcNow - timeStamp).TotalMinutes) > _expirationMinutes;
                if (!expired)
                {
                    result= true;
                }
                else
                {
                    result = false;
                }
            }
            catch
            {
                result= false;
            }
            return result;
        }
        //public static String GetTimestamp(DateTime value)
        //{
        //    return value.ToString("yyyyMMddHHmmssffff");
        //}
    }
}