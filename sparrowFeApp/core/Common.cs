﻿using EC09WebApp.shop.rb;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using sparrowFeApp.shop.rb;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Xml;

namespace sparrowFeApp.core
{
    public class Common
    {
        static ecgdService.gdSoapClient objgd = new ecgdService.gdSoapClient();
        const string Keydata = "7C44CE88F375BA74702D6F262CE8DC3AC204A6618378ED16";
        public static DataTable GetBOMColumns(int bomType)
        {
            ec09Service.ECAPISoapClient ec09Service = new ec09Service.ECAPISoapClient();
            DataTable bomColumns = HttpContext.Current.Cache[Constant.CacheKey.BOMColumns.ToString() + bomType] as DataTable;
            if (bomColumns == null)
            {
                try
                {
                    bomColumns = ec09Service.GetBOMColumns(bomType);
                    HttpRuntime.Cache.Add(Constant.CacheKey.BOMColumns.ToString() + bomType, bomColumns, new CacheDependency(HostingEnvironment.MapPath("~/shop/assembly/resources/cache_dependency.txt")), DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Normal, null);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ec09Service.Close();
                }
            }
            return bomColumns;
        }
        public static DataTable GetMappedColumnsByWeight(int bomType)
        {
            ec09Service.ECAPISoapClient ec09Service = new ec09Service.ECAPISoapClient();
            DataTable mappedColumns = HttpContext.Current.Cache[Constant.CacheKey.MappedColumns.ToString()] as DataTable;
            if (mappedColumns == null)
            {
                try
                {
                    mappedColumns = ec09Service.GetMappedColumnsByWeight(bomType);
                    HttpRuntime.Cache.Add(Constant.CacheKey.MappedColumns.ToString(), mappedColumns, new CacheDependency(HostingEnvironment.MapPath("~/shop/assembly/resources/cache_dependency.txt")), DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Normal, null);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ec09Service.Close();
                }
            }
            return mappedColumns;
        }

        public static string GetClientIPType()
        {
            string ip, trueIP;
            string ipType = "";
            string[] ipRange;
            int le;
            try
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (!string.IsNullOrEmpty(ip))
                {
                    ipRange = ip.Split(',');
                    le = ipRange.Length - 1;
                    trueIP = ipRange[le];
                }
                {
                    trueIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                if (trueIP.Length > 15)
                {
                    trueIP = trueIP.Substring(0, 14);
                }
                double ipAdd = Dot2LongIPNumber(trueIP);
                if (ipAdd > Dot2LongIPNumber("10.0.0.0") && ipAdd < Dot2LongIPNumber("10.255.255.255"))
                {
                    ipType = "Internal";
                }
                if (ipAdd > Dot2LongIPNumber("172.16.0.0") && ipAdd < Dot2LongIPNumber("172.31.255.255"))
                {
                    ipType = "Internal";
                }
                else if (ipAdd > Dot2LongIPNumber("192.168.0.0") && ipAdd < Dot2LongIPNumber("192.168.255.255"))
                {
                    ipType = "Internal";
                }
                else
                {
                    ipType = "External";
                }
            }
            catch (Exception)
            {
                ipType = "Internal";
            }
            return ipType;
        }

        public static double Dot2LongIPNumber(string DottedIP)
        {
            int i;
            string[] arrDec;
            double num = 0;
            if (DottedIP == "")
            {
                return 0;
            }
            else
            {
                arrDec = DottedIP.Split('.');
                for (i = arrDec.Length - 1; i >= 0; i--)
                {
                    num += ((int.Parse(arrDec[i]) % 256) * Math.Pow(256, (3 - i)));
                }
                return num;
            }
        }

        private delegate void FeAppAPIDelegate(string jsonFeApp);
        private void FeAppAPIAsync(string jsonFeApp)
        {
            try
            {
                exception_log("FeAppAPIAsync","FeAppAPI -Log", jsonFeApp);

                string response = FeAppAPIPost(jsonFeApp);
                if (response != "")
                {
                    dynamic json = JsonConvert.DeserializeObject(response);
                    if (json.code == 0)
                    {
                        Database.DA da = new Database.DA();

                        string strmessage = json.error;
                        strmessage = strmessage.Replace("'", "''");
                        string strSQL = string.Format("INSERT INTO public.base_queueexception(que_body, created_on, message) VALUES ( '{0}','{1}','{2}')", jsonFeApp, DateTime.Now, strmessage);
                        // class_name, traceback, status
                        int resultQueue = da.ExecuteQuery(strSQL);
                    }
                }
                else
                {
                    Database.DA da = new Database.DA();
                    string strSQL = string.Format("INSERT INTO public.base_queueexception(que_body, created_on, message)VALUES ( '{0}','{1}','{2}')", jsonFeApp, DateTime.Now, "service down");
                    int resultQueue = da.ExecuteQuery(strSQL);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void FeAppAPI(string jsonFeApp)
        {
            try
            {
                FeAppAPIDelegate delegateFunc = new FeAppAPIDelegate(FeAppAPIAsync);
                delegateFunc.BeginInvoke(jsonFeApp, null, null);
            }
            catch
            {
            }
        }

        public string FeAppAPIPost(string strBody)
        {
            try
            {
                JObject jsonfeapp = JObject.Parse(strBody);
                string Key = Constant.FeAppApiKey;// "kh!r1K24@L78W;ERT%^=";
                string target = "fe";

                string json = "";
                string requestURL = Constant.sparrowFeAppAPI;
                var client = new RestClient(requestURL);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
                request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"key\"\r\n\r\n" + Key + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"target\"\r\n\r\n" + target + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"body\"\r\n\r\n" + jsonfeapp + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                json = response.Content;
                return json;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string getFilePath(string entityNumber)
        {
            string sql = "";
            if (entityNumber.StartsWith("B"))
            {
                sql = string.Format(" select file_path,file_type from sales_file where entity_number = '{0}' and file_type = '{1}'", entityNumber, "INQ");
            }
            else
            {
                sql = string.Format("select file_path,file_type from sales_file where entity_number = '{0}' and file_type = '{1}'", entityNumber, "ORD");
            }
            Database.DA gd = new Database.DA();
            DataSet ds = gd.GetDataSet(sql);
            string path = "";
            string RootPath = Constant.FileServerPath;
            if (ds.Tables[0].Rows.Count > 0 && (RootPath + Convert.ToString(ds.Tables[0].Rows[0]["file_path"])) != "")
            {
                path = RootPath + ds.Tables[0].Rows[0]["file_path"].ToString();
            }
            return path;

        }
        public static string GetOrderXMLFromFileServer(string filePath, string entityNumber)
        {
            try
            {

                string xmlData = "";
                if (ReportBuilderUtilities.IsImpersonation())
                {
                    string RootPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FileServerPath"]);
                    if (Directory.Exists(RootPath + filePath))
                    {
                        string fullfilepath = RootPath + filePath + "\\" + entityNumber.Trim() + ".xml";
                        if (File.Exists(fullfilepath))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.Load(fullfilepath);
                            xmlData = doc.OuterXml;
                        }
                    }
                }
                return xmlData;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static bool InsertException(string createdBy, string orderNumber, string PredefineSolutionCode, string solutionDescription, string exceptiontype)
        {
            Database.DA da = new Database.DA();
            DataTable dt = new DataTable();
            long orderId = 0;
            long execStatusId = 0;
            long prevOrdStatId = 0;
            long predefineProblemId = 0;
            long predefineSolutionId = 0;
            byte? isConfirm = 0;
            byte? isModify = 0;
            byte? isCancelOfOrder = 0;
            string puttocustDate = null;
            string problemDescription = "See attached document.";
            long orderStatusId = 0;
            string sql = string.Format("select id,order_status_id from sales_orderdetail where order_num = '{0}'", orderNumber);
            dt = da.GetDataTable(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                long.TryParse(dt.Rows[0]["id"].ToString(), out orderId);
                long.TryParse(dt.Rows[0]["order_status_id"].ToString(), out prevOrdStatId);
            }

            sql = string.Format("select id from sales_predefine_solution where code = '{0}'", PredefineSolutionCode);
            dt = da.GetDataTable(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                long.TryParse(dt.Rows[0]["id"].ToString(), out predefineSolutionId);
            }
            sql = string.Format("SELECT id FROM public.sales_exeception_status WHERE code = '{0}'", "PutToCustomer");
            dt = da.GetDataTable(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                long.TryParse(dt.Rows[0]["id"].ToString(), out execStatusId);
            }
            sql = string.Format("SELECT id FROM sales_predefine_problem WHERE code = '{0}'", "See attached document.");
            dt = da.GetDataTable(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                long.TryParse(dt.Rows[0]["id"].ToString(), out predefineProblemId);
            }

            if (exceptiontype.ToUpper() != "RBPPAEXCEPTION")
            {
                isModify = 1;
                isCancelOfOrder = 1;
                isConfirm = 0;
                puttocustDate = null;
                string sql1 = string.Format("select id from sales_orderstatus  where code = '{0}'", "INCOMINGEXCEPTION");
                dt = da.GetDataTable(sql1);
                if (dt != null && dt.Rows.Count > 0)
                {
                    long.TryParse(dt.Rows[0]["id"].ToString(), out orderStatusId);
                }
                sql = string.Format("" +
                    "INSERT INTO public.sales_order_exceptions" +
                    "(order_id, exce_status_id, prev_ord_stat_id, exception_date, predefine_problem_id, predefine_solution_id, problem_description, " +
                    "solution_description, created_by, created_date,  modified_date, is_confirm, is_modify, is_cancel_of_order," +
                    " is_document, ppa_exp_reply) VALUES" +
                    "('{0}','{1}','{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}','{14}', '{15}') " +
                    "", orderId, execStatusId, prevOrdStatId, DateTime.UtcNow, predefineProblemId, predefineSolutionId, problemDescription, solutionDescription,
                    createdBy, DateTime.UtcNow, DateTime.UtcNow, isConfirm, isModify, isCancelOfOrder,
                    1, "ppaExpReply pending");
            }
            if (exceptiontype.ToUpper() == "RBPPAEXCEPTION")
            {
                isModify = null;
                isCancelOfOrder = null;
                isConfirm = null;
                string sql1 = string.Format("select id from sales_orderstatus  where code = '{0}'", "SIORDERPROCESS");
                dt = da.GetDataTable(sql1);
                if (dt != null && dt.Rows.Count > 0)
                {
                    long.TryParse(dt.Rows[0]["id"].ToString(), out orderStatusId);
                }

                sql = string.Format("" +
                "INSERT INTO public.sales_order_exceptions" +
                "(order_id, exce_status_id, prev_ord_stat_id, exception_date, predefine_problem_id, predefine_solution_id, problem_description, " +
                "solution_description, created_by, created_date,  modified_date," +
                " puttocust_date, is_document, ppa_exp_reply) VALUES" +
                "('{0}','{1}','{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}') " +
                "", orderId, execStatusId, prevOrdStatId, DateTime.UtcNow, predefineProblemId, predefineSolutionId, problemDescription, solutionDescription,
                createdBy, DateTime.UtcNow, DateTime.UtcNow,
                DateTime.UtcNow, 1, "ppaExpReply pending");
            }

            int resultException = da.ExecuteQuery(sql);
            if (resultException == 1)
            {
                long objectID = orderId;
                string actionOn = DateTime.UtcNow.ToString();
                string ipAddr = "Internal";
                string descr = "Exception Generated";
                long contentTypeId = 33;
                string actionCode = "update";
                string actionById = createdBy;
                string document_num = "";
                sql = string.Format("select max(id) as id FROM public.sales_order_exceptions");
                dt = da.GetDataTable(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    document_num = dt.Rows[0]["id"].ToString();
                }
                bool resultHistory = InsertHistory(objectID, actionOn, ipAddr, descr, contentTypeId, actionCode, actionById, document_num,actionById);//da.ExecuteQuery(sql); 
                if (resultHistory)
                {
                    sql = string.Format("update sales_orderdetail set order_status_id ='{0}' where order_num = '{1}'", orderStatusId, orderNumber);
                    int resultUpdate = da.ExecuteQuery(sql);
                    if (resultUpdate > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }

            return false;

            //long getId(string sqlQuery, string field = "id")
            //{
            //    long id = 0;
            //    if (sqlQuery.Contains(field) && sqlQuery.Contains("select"))
            //    {
            //        dt = da.GetDataTable(sqlQuery);
            //        if (dt != null && dt.Rows.Count > 0)
            //        {
            //            long.TryParse(dt.Rows[0][field].ToString(), out id);
            //        }
            //    }
            //    return id;
            //}
        }
        public static bool InsertHistory(long object_id, string action_on, string ip_addr, string descr, long content_type_id, string action_code, string action_by_id, string document_num,string operator_id)
        {
            string sql = string.Format("INSERT INTO public.history_history(object_id, action_on, ip_addr, descr, content_type_id, action_code, action_by_id,document_num)" +
                    " VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}','{7}','{8}')", object_id, action_on, ip_addr, descr, content_type_id, action_code, action_by_id, document_num, operator_id);
            Database.DA da = new Database.DA();
            int resultHistory = da.ExecuteQuery(sql);
            if (resultHistory == 1)
            {
                return true;
            }
            return false;
        }
        public static long GetCrmRelIdFromUserid(long userid)
        {
            try
            {
                Database.DA da = new Database.DA();
                DataSet dsuserid = da.GetDataSet(string.Format("SELECT id, crm_rel_id FROM public.accounts_userprofile where user_id = {0}", userid));
                if (dsuserid != null && dsuserid.Tables.Count > 0 && dsuserid.Tables[0].Rows.Count > 0)
                {
                    long lngCrmId = 0;
                    long.TryParse(dsuserid.Tables[0].Rows[0]["crm_rel_id"].ToString(),out lngCrmId);
                    return lngCrmId;
                }
                return 0;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        [Serializable]
        #region "GetloadCompData class"
        public class LoadCompanyData
        {
            private string _Name;
            private string _Initials;
            private string _Description;
            private string _ShortName;
            private string _VATNo;
            private long _TaxNumberTypeId;
            private long? _StatusId;

            private long? _AccountManagerId;
            private long _AddressId;
            private long _AddressRelationId;
            private long _TypeId;
            private long? _CompanyId;
            private long _CountryId;
            private long? _StateId;
            private bool _IsContractAttached;
            private bool _IsPrepaid;
            private bool _IsEuropean;
            private bool _IsRootCompany;
            private bool? _IsAllowSendMail;
            private bool _IsAlwaysVAT;

            private string _AddressName;
            private string _ContactName;

            public string AddressName
            {
                get { return _AddressName; }
                set { _AddressName = value; }
            }

            public string ContactName
            {
                get { return _ContactName; }
                set { _ContactName = value; }
            }

            public string Name
            {
                get { return _Name; }
                set { _Name = value; }
            }

            public string Initials
            {
                get { return _Initials; }
                set { _Initials = value; }
            }
            public string Description
            {
                get { return _Description; }
                set { _Description = value; }
            }
            public string ShortName
            {
                get { return _ShortName; }
                set { _ShortName = value; }
            }
            public string VATNo
            {
                get { return _VATNo; }
                set { _VATNo = value; }
            }

            public long TaxNumberTypeId
            {
                get { return _TaxNumberTypeId; }
                set { _TaxNumberTypeId = value; }
            }
            public long? AccountManagerId
            {
                get { return _AccountManagerId; }
                set { _AccountManagerId = value; }
            }
            public long AddressId
            {
                get { return _AddressId; }
                set { _AddressId = value; }
            }
            public long AddressRelationId
            {
                get { return _AddressRelationId; }
                set { _AddressRelationId = value; }
            }
            public long? StatusId
            {
                get { return _StatusId; }
                set { _StatusId = value; }
            }
            public long TypeId
            {
                get { return _TypeId; }
                set { _TypeId = value; }
            }
            public long? StateId
            {
                get { return _StateId; }
                set { _StateId = value; }
            }
            public long CountryId
            {
                get { return _CountryId; }
                set { _CountryId = value; }
            }
            public long? CompanyId
            {
                get { return _CompanyId; }
                set { _CompanyId = value; }
            }

            public bool? IsAllowSendMail
            {
                get { return _IsAllowSendMail; }
                set { _IsAllowSendMail = value; }
            }
            public bool IsAlwaysVAT
            {
                get { return _IsAlwaysVAT; }
                set { _IsAlwaysVAT = value; }
            }
            public bool IsContractAttached
            {
                get { return _IsContractAttached; }
                set { _IsContractAttached = value; }
            }
            public bool IsEuropean
            {
                get { return _IsEuropean; }
                set { _IsEuropean = value; }
            }
            public bool IsPrepaid
            {
                get { return _IsPrepaid; }
                set { _IsPrepaid = value; }
            }
            public bool IsRootCompany
            {
                get { return _IsRootCompany; }
                set { _IsRootCompany = value; }
            }
        }
        #endregion

        [Serializable]
        #region "Get Role class"
        public class RoleData
        {
            private string _Name;
            private long _RoleId;
            private string _Code;

            public string Name
            {
                get { return _Name; }
                set { _Name = value; }
            }
            public long RoleId
            {
                get { return _RoleId; }
                set { _RoleId = value; }
            }
            public string Code
            {
                get { return _Code; }
                set { _Code = value; }
            }
        }
        #endregion

        [Serializable]
        #region "Get Dimension class"
        public class LoadDimensionData
        {
            private string _Code;
            private long _DimensionId;

            public long DimensionId
            {
                get { return _DimensionId; }
                set { _DimensionId = value; }
            }
            public string Code
            {
                get { return _Code; }
                set { _Code = value; }
            }
        }

        #endregion

        public static void UpdateOrderDetailFromEC(string entityNumber)
        {
            try
            {
                long object_id = 0;
                if (entityNumber.StartsWith("B"))
                {
                    string strSQL = string.Format("exec FeApp_BasketDetail '{0}'", entityNumber);  ///Remove XML_Data Columns 
                    DataSet ds = objgd.GetData(strSQL, Keydata);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    { 
                        DateTime delivery_date = new DateTime();
                        DateTime prepdue_date = new DateTime();

                        DateTime.TryParse(ds.Tables[0].Rows[0]["DeliveryDate"].ToString(), out delivery_date);
                        DateTime.TryParse(ds.Tables[0].Rows[0]["PrepDueDate"].ToString(), out prepdue_date);

                        string sql = "update sales_basketinquirydetail set delivery_date='" + delivery_date.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                                + "prepdue_date='" + prepdue_date.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                                + "delivery_term ='" + ds.Tables[0].Rows[0]["DeliveryTerm"].ToString() + "',"
                                                //+ "repeat_order='" + ds.Tables[0].Rows[0][""].ToString() + "',"
                                                //+ "order_status_id='" + ds.Tables[0].Rows[0][""].ToString() + "',"
                                                + "service='" + ds.Tables[0].Rows[0]["Service"].ToString() + "',"
                                                + "layers ='" + ds.Tables[0].Rows[0]["Layers"].ToString() + "',"
                                                + "pcb_qty ='" + ds.Tables[0].Rows[0]["PCBQty"].ToString() + "',"
                                                + "panel_qty='" + ds.Tables[0].Rows[0]["PanelQty"].ToString() + "',"
                                                + "visulizer_status='" + ds.Tables[0].Rows[0]["VisStatus"].ToString() + "',"
                                                + "remark='" + ds.Tables[0].Rows[0]["isRemarks"].ToString() + "',"
                                                + "report_builder='" + ds.Tables[0].Rows[0]["report_builder"].ToString() + "',"
                                                + "previous_status='" + ds.Tables[0].Rows[0]["PreviousStageName"].ToString() + "',"
                                                + "is_panel ='" + ds.Tables[0].Rows[0]["is_panel"].ToString() + "',"
                                                + "v_cut='" + ds.Tables[0].Rows[0]["Vcut"].ToString() + "',"
                                                + "bbt='" + ds.Tables[0].Rows[0]["BBT"].ToString() + "',"
                                                + "drill='" + ds.Tables[0].Rows[0]["Drill"].ToString() + "',"
                                                + "edge_connector='" + ds.Tables[0].Rows[0]["EdgeConnector"].ToString() + "',"
                                                + "material='" + ds.Tables[0].Rows[0]["Material"].ToString() + "',"
                                                + "material_thickness='" + ds.Tables[0].Rows[0]["MaterialThickness"].ToString() + "',"
                                                + "tg='" + ds.Tables[0].Rows[0]["Tg"].ToString() + "',"
                                                + "is_assembly='" + ds.Tables[0].Rows[0]["is_assembly_data"].ToString() + "',"
                                                + "double_printed_solder_mask='" + ds.Tables[0].Rows[0]["DoublePrintedSolderMask"].ToString() + "',"
                                                + "inner_start_cu='" + ds.Tables[0].Rows[0]["InnStartCu"].ToString() + "',"
                                                + "out_start_cu='" + ds.Tables[0].Rows[0]["OutStartCu"].ToString() + "',"
                                                + "solder_mask_bottom='" + ds.Tables[0].Rows[0]["SolderMaskBottom"].ToString() + "',"
                                                + "build_up_code='" + ds.Tables[0].Rows[0]["Buildupcode"].ToString() + "',"
                                                + "full_checked='" + ds.Tables[0].Rows[0]["IsFC"].ToString() + "',"
                                                + "is_plated_slots='" + ds.Tables[0].Rows[0]["IsPlatedSlots"].ToString() + "',"
                                                + "is_via_fill='" + ds.Tables[0].Rows[0]["IsViaFill"].ToString() + "',"
                                                + "legend_bottom='" + ds.Tables[0].Rows[0]["LegendBottom"].ToString() + "',"
                                                + "pack_quantity='" + ds.Tables[0].Rows[0]["PackQuantity"].ToString() + "',"
                                                + "pack_with_paper='" + ds.Tables[0].Rows[0]["PackWithPaper"].ToString() + "',"
                                                + "solder_mask_top='" + ds.Tables[0].Rows[0]["SolderMaskTop"].ToString() + "',"
                                                + "pcb_name='" + ds.Tables[0].Rows[0]["pcb_name"].ToString() + "',"
                                                + "pattern_classification='" + ds.Tables[0].Rows[0]["pattern_classification"].ToString() + "',"
                                                + "is_custsupply_parts='" + ds.Tables[0].Rows[0]["IscustsupplyParts"].ToString() + "',"
                                                + "is_free_parts='" + ds.Tables[0].Rows[0]["isFreeParts"].ToString() + "' "
                                                + " where order_num = '" + entityNumber + "'";
                        Database.DA da = new Database.DA();
                        da.ExecuteQuery(sql);
                    }
                }
                else
                {

                    string strSQL = string.Format("exec FeApp_GetOrderDetail '{0}'", entityNumber);  ///Remove XML_Data Columns 
                    DataSet ds = objgd.GetData(strSQL, Keydata);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DateTime delivery_date = new DateTime();
                        DateTime prepdue_date = new DateTime();

                        DateTime.TryParse(ds.Tables[0].Rows[0]["DeliveryDate"].ToString(), out delivery_date);
                        DateTime.TryParse(ds.Tables[0].Rows[0]["PrepDueDate"].ToString(), out prepdue_date);


                        string sql = "update sales_orderdetail set delivery_date='" + delivery_date.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                                + "prepdue_date='" + prepdue_date.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                                + "delivery_term ='" + ds.Tables[0].Rows[0]["DeliveryTerm"].ToString() + "',"
                                                //+ "repeat_order='" + ds.Tables[0].Rows[0][""].ToString() + "',"
                                                //+ "order_status_id='" + ds.Tables[0].Rows[0][""].ToString() + "',"
                                                + "service='" + ds.Tables[0].Rows[0]["Service"].ToString() + "',"
                                                + "layers ='" + ds.Tables[0].Rows[0]["Layers"].ToString() + "',"
                                                + "pcb_qty ='" + ds.Tables[0].Rows[0]["PCBQty"].ToString() + "',"
                                                + "panel_qty='" + ds.Tables[0].Rows[0]["PanelQty"].ToString() + "',"
                                                + "visulizer_status='" + ds.Tables[0].Rows[0]["VisStatus"].ToString() + "',"
                                                + "remark='" + ds.Tables[0].Rows[0]["isRemarks"].ToString() + "',"
                                                + "report_builder='" + ds.Tables[0].Rows[0]["report_builder"].ToString() + "',"
                                                + "previous_status='" + ds.Tables[0].Rows[0]["PreviousStageName"].ToString() + "',"
                                                + "is_panel ='" + ds.Tables[0].Rows[0]["is_panel"].ToString() + "',"
                                                + "v_cut='" + ds.Tables[0].Rows[0]["Vcut"].ToString() + "',"
                                                + "bbt='" + ds.Tables[0].Rows[0]["BBT"].ToString() + "',"
                                                + "drill='" + ds.Tables[0].Rows[0]["Drill"].ToString() + "',"
                                                + "edge_connector='" + ds.Tables[0].Rows[0]["EdgeConnector"].ToString() + "',"
                                                + "material='" + ds.Tables[0].Rows[0]["Material"].ToString() + "',"
                                                + "material_thickness='" + ds.Tables[0].Rows[0]["MaterialThickness"].ToString() + "',"
                                                + "tg='" + ds.Tables[0].Rows[0]["Tg"].ToString() + "',"
                                                + "is_assembly='" + ds.Tables[0].Rows[0]["is_assembly_data"].ToString() + "',"
                                                + "double_printed_solder_mask='" + ds.Tables[0].Rows[0]["DoublePrintedSolderMask"].ToString() + "',"
                                                + "inner_start_cu='" + ds.Tables[0].Rows[0]["InnStartCu"].ToString() + "',"
                                                + "out_start_cu='" + ds.Tables[0].Rows[0]["OutStartCu"].ToString() + "',"
                                                + "solder_mask_bottom='" + ds.Tables[0].Rows[0]["SolderMaskBottom"].ToString() + "',"
                                                + "build_up_code='" + ds.Tables[0].Rows[0]["Buildupcode"].ToString() + "',"
                                                + "full_checked='" + ds.Tables[0].Rows[0]["IsFC"].ToString() + "',"
                                                + "is_plated_slots='" + ds.Tables[0].Rows[0]["IsPlatedSlots"].ToString() + "',"
                                                + "is_via_fill='" + ds.Tables[0].Rows[0]["IsViaFill"].ToString() + "',"
                                                + "legend_bottom='" + ds.Tables[0].Rows[0]["LegendBottom"].ToString() + "',"
                                                + "pack_quantity='" + ds.Tables[0].Rows[0]["PackQuantity"].ToString() + "',"
                                                + "pack_with_paper='" + ds.Tables[0].Rows[0]["PackWithPaper"].ToString() + "',"
                                                + "solder_mask_top='" + ds.Tables[0].Rows[0]["SolderMaskTop"].ToString() + "',"
                                                + "pcb_name='" + ds.Tables[0].Rows[0]["pcb_name"].ToString() + "',"
                                                + "pattern_classification='" + ds.Tables[0].Rows[0]["pattern_classification"].ToString() + "',"
                                                + "is_custsupply_parts='" + ds.Tables[0].Rows[0]["IscustsupplyParts"].ToString() + "',"
                                                + "is_free_parts='" + ds.Tables[0].Rows[0]["isFreeParts"].ToString() + "' "
                                                + " where order_num = '" + entityNumber + "'";
                        Database.DA da = new Database.DA();
                        da.ExecuteQuery(sql);
                    }
                }

                object_id = GetEntityId(entityNumber);
                if(object_id != 0)
                {
                    InsertHistory(object_id, DateTime.UtcNow.ToString(), "Internal", "Modified", 33, "update", AppSessions.FEUserId, "","");
                }  
            }
            catch (Exception ex)
            {
                exception_log("UpdateOrderDetailFromEC", ex.Message, ex.Source + "-" + ex.StackTrace);
            }
        }

        public static long GetEntityId(string entityNumber)
        {
            long entityid = 0;
            try
            {
                Database.DA da = new Database.DA();
                DataTable dt = new DataTable();
                string sql = "";
                if (entityNumber.StartsWith("B"))
                { 
                    sql = string.Format("select id  from sales_basketinquirydetail where order_num = '{0}'", entityNumber);
                }
                else
                {
                    sql = string.Format("select id  from sales_orderdetail where order_num = '{0}'", entityNumber);
                }
                dt = da.GetDataTable(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    long.TryParse(dt.Rows[0]["id"].ToString(), out entityid);
                }
                return entityid;
            }
            catch (Exception ex)
            {
                exception_log("GetEntityId", ex.Message, ex.Source + "-" + ex.StackTrace);
                return entityid;
            }
        }

        public static void exception_log(string class_name, string message, string traceback)
        {
            try
            {
                string sql = "";
                sql = string.Format(" INSERT INTO public.exception_log_errorbase(class_name, level, message, traceback, created_on) VALUES ( '{0}', {1}, '{2}', '{3}', '{4}');", class_name, 0, message, traceback, DateTime.UtcNow);
                Database.DA gd = new Database.DA();
                gd.ExecuteQuery(sql);
            }
            catch
            {

            }
        }

        public static string ContentTypedata(string FileExtension)
        {
            switch (FileExtension)
            {
                case ".bmp":
                    return "image/bmp";
                case ".gif":
                    return "image/gif";
                case ".jpeg":
                    return "image/jpeg";
                case ".jpg":
                    return "image/jpeg";
                case ".png":
                    return "image/png";
                case ".tif":
                    return "image/tiff";
                case ".tiff":
                    return "image/tiff";
                case ".doc":
                    return "application/msword";
                case ".docx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".pdf":
                    return "application/pdf";
                case ".ppt":
                    return "application/vnd.ms-powerpoint";
                case ".pptx":
                    return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case ".xlsx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case ".xls":
                    return "application/vnd.ms-excel";
                case ".csv":
                    return "text/csv";
                case ".xml":
                    return "text/xml";
                case ".txt":
                    return "text/plain";
                case ".zip":
                    return "application/x-zip-compressed";
                case ".ogg":
                    return "application/ogg";
                case ".mp3":
                    return "audio/mpeg";
                case ".wma":
                    return "audio/x-ms-wma";
                case ".wav":
                    return "audio/x-wav";
                case ".wmv":
                    return "audio/x-ms-wmv";
                case ".swf":
                    return "application/x-shockwave-flash";
                case ".avi":
                    return "video/avi";
                case ".mp4":
                    return "video/mp4";
                case ".mpeg":
                    return "video/mpeg";
                case ".mpg":
                    return "video/mpeg";
                case ".qt":
                    return "video/quicktime";
                default:
                    return "application/download";
            }
        }

        public static long GetOperatorId(string entityNumber)
        {
            long entityid = 0;
            try
            {
                Database.DA da = new Database.DA();
                DataTable dt = new DataTable();
                string sql = "";
                if (entityNumber.StartsWith("B"))
                {
                    sql = string.Format("select operator_id  from sales_basketinquirydetail where order_num = '{0}'", entityNumber);
                }
                else
                {
                    sql = string.Format("select operator_id  from sales_orderdetail where order_num = '{0}'", entityNumber);
                }
                dt = da.GetDataTable(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    long.TryParse(dt.Rows[0]["operator_id"].ToString(), out entityid);
                }
                return entityid;
            }
            catch (Exception ex)
            {
                exception_log("GetOperatorId", ex.Message, ex.Source + "-" + ex.StackTrace);
                return entityid;
            }
        }

    }
}
